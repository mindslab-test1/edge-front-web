package ai.maum.automl.frontapi.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import ai.maum.automl.frontapi.commons.enums.Api;
import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.commons.utils.RestUtil;
import ai.maum.automl.frontapi.model.dto.RoadDto;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.Device;
import ai.maum.automl.frontapi.model.entity.DeviceHistory;
import ai.maum.automl.frontapi.model.entity.PolicyHistory;
import ai.maum.automl.frontapi.model.entity.Road;
import ai.maum.automl.frontapi.model.entity.Status;
import ai.maum.automl.frontapi.model.entity.User;
import ai.maum.automl.frontapi.model.dto.DeviceSearchDto;
import ai.maum.automl.frontapi.model.dto.PageRequest;
import ai.maum.automl.frontapi.repository.DeviceHistoryRepository;
import ai.maum.automl.frontapi.repository.DeviceRepository;
import ai.maum.automl.frontapi.repository.PolicyHistoryRepository;
import ai.maum.automl.frontapi.repository.RoadRepository;
import ai.maum.automl.frontapi.repository.StatusRepository;

/**
 * DeviceService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일        / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주   / 최초 생성
 * </pre>
 * @since 2020-01-22
 */
@Service
public class HistoryService {

	Logger logger = LoggerFactory.getLogger(HistoryService.class);
    final DeviceHistoryRepository historyRepository;
    final StatusRepository statusRepository;
    final PolicyHistoryRepository policyRepository;

    public HistoryService(DeviceHistoryRepository historyRepository, StatusRepository statusRepository, PolicyHistoryRepository policyRepository) {
        this.historyRepository = historyRepository;
        this.statusRepository = statusRepository;
        this.policyRepository = policyRepository;
    }
    
    public boolean addDeviceHistory(Long deviceId, Api api, Map<String, Object> param, Map<String, Object> response) {
    	
    	DeviceHistory history = new DeviceHistory();
    	
    	history.setAction(api.name());
    	history.setRequest(param.toString());
    	history.setResponse(response.toString());
    	
    	if (deviceId == null) {
    		history.setDevice(null);
    	} else {
    		Device device = new Device();
    		device.setDeviceId(deviceId);
    		
    		history.setDevice(device);    		
    	}
    	
    	
    	if(Optional.ofNullable(historyRepository.save(history)).isPresent()) {
    		return true;
    	} else {
    		return false;    		
    	}
    	
    }
    
    public boolean addStatus(Long deviceId, Map<String, Object> response) {
    	Status status = new Status();
    	
    	if (deviceId == null) {
    		status.setDevice(null);
    	} else {
    		Device device = new Device();
    		device.setDeviceId(deviceId);
    		status.setDevice(device);
    	}
    	
    	
    	status.setResponse(response.toString());
    	
    	if (Optional.ofNullable(statusRepository.save(status)).isPresent()) {
    		return true;
    	} else {
    		return false;
    	}
    	
    }
    
    public boolean addPolicyHistory(Long edgeNodeSeq, float version, LocalDateTime updated, Api api, Map<String, Object> request, Map<String, Object> response) {
    	PolicyHistory policy = new PolicyHistory();
    	
    	if (edgeNodeSeq == 0) {
    		policy.setRoad(null);
    	} else {
    		Road road = new Road();
    		road.setEdgeNodeSeq(edgeNodeSeq);
    		
    		policy.setRoad(road);
    	}
    	
    	policy.setAction(api.name());
    	policy.setRequest(request.toString());
    	policy.setResponse(response.toString());
    	policy.setVersion(version);
    	policy.setUpdated(updated);
    	
    	if (Optional.ofNullable(policyRepository.save(policy)).isPresent()) {
    		return true;
    	} else {
    		return false;
    	}
    }
        
}
