package ai.maum.automl.frontapi.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.Role;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class UserEdgeDto implements Serializable {
	
	private Long id; 
	private String email;
	private String name;
	private String tel;
	private String clientCompanyId;
	private String clientCompanyName;
	//private ClientCompany clientCompany;
	//private Role role;
	private Long authId;
	private String useYn;
	private String delYn;
	private LocalDateTime regDt;
	private LocalDateTime modDt;
	
	@Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode(callSuper = true)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class UserSearchEx extends UserDto {
		private String name;
    	private String email;
    }
}
