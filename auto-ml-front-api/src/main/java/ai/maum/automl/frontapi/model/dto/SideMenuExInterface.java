package ai.maum.automl.frontapi.model.dto;

public interface SideMenuExInterface {
	
	Long getId();
	
	String getMenuCode();
	
	String getMenuName();
	
	String getUrl();
	
	String getIcoName();
	
	Integer getDepth();
	
	Integer getTopMenuId();
}
