package ai.maum.automl.frontapi.commons.enums;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;

@Getter
public enum Code {
	
	//edge 종류
	EDGE_ROAD("A0101","Edge Road","EDGE_ROAD"),
	EDGE_STREET("A0102","Edge Street","EDGE_STREET"),
	EDGE_FACTORY("A0103","Edge Factory","EDGE_FACTORY"),
	EDGE_FARM("A0104","Edge Farm","EDGE_FARM"),
	EDGE_HOME("A0105","Edge Home","EDGE_HOME"),
	EDGE_DRONE("A0106","Edge Drone","EDGE_DRONE"),
	
	//Edge 상태
	WORKING("Working", "정상 작동", "정상 작동"),
	ABNORMAL("Abnormal", "비정상 작동", "비정상 작동"),
	NOTWORKING("Notworking", "작동 안 함", "작동 안 함"),
	NOTCONNECT("Notconnect", "연결 안 됨", "연결 안 됨"),
	
	//사용여부
	USE_Y("Y", "사용", "사용"),
	USE_N("N", "미사용", "미사용"),
	
	//차량 종류
	CAR_UNKNOWN("unknown","알수없음","알수없음"),
	CAR_NORMAL("normal","일반","일반"),
	CAR_TAXI("taxi","택시","택시"),
	CAR_BUS("bus","버스","버스"),
	CAR_OLD("old","구번호판","구번호판"),
	CAR_NEW("new","신번호판","신번호판"),
	
	//인식방향
	FRONT("front","전면","전면"),
	BACK("back","후면","후면"),
	BOTH("both","양방향","양방향"),
	
	ENTRY("entry","진입","카메라 기준으로 진입 방향"),
	EXIT("exit","진출","카메라 기준으로 진출 방향"),
	
	//사용자 권한
	EDGE_USER("1","일반사용자","일반사용자"),
	EDGE_ENGINEER("2","설치기사","설치기사"),
	EDGE_COMP_ADMIN("3","회사관리자","회사관리자"),
	EDGE_ADMIN("4","관리자","관리자");
	
	private String code;
	private String codeName;
	private String description;
	
	public static final Map<String, Code> map = new HashMap<>(); 
	static { 
		for (Code os : Code.values()) { 
			map.put(os.getCode(), os); 
		} 
	}

	
	Code(String code, String codeName, String description) {
		this.code = code;
		this.codeName = codeName;
		this.description = description;
	}
	
	public static Code getCodeData(String code) { 
		return map.get(code); 
	}

	
}
