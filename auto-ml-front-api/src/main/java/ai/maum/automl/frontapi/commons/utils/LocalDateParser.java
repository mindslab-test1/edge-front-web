package ai.maum.automl.frontapi.commons.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import lombok.Getter;

@Getter
public class LocalDateParser {
	
	private LocalDate date;
	
	public LocalDateParser (String currentDate) {
		this.date = LocalDate.parse(currentDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	public LocalDateParser(String currentDate, String parttern) {
		this.date = LocalDate.parse(currentDate, DateTimeFormatter.ofPattern(parttern));
	}
	
	public LocalDateTime getStartDate() {
		return this.date.atStartOfDay();
	}
	
	public LocalDateTime getEndDate() {
		return LocalDateTime.of(this.date, LocalTime.of(23, 59, 59));
	}
	
}
