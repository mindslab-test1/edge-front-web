package ai.maum.automl.frontapi.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.commons.utils.LocalDateParser;
import ai.maum.automl.frontapi.model.dto.ClientCompanyDto;
import ai.maum.automl.frontapi.model.dto.ClientCompanySearchDto;
import ai.maum.automl.frontapi.model.dto.MenuDto;
import ai.maum.automl.frontapi.model.dto.MenuExInterface;
import ai.maum.automl.frontapi.model.dto.SideMenuExInterface;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.Menu;
import ai.maum.automl.frontapi.model.entity.QClientCompany;
import ai.maum.automl.frontapi.model.entity.QMenu;
import ai.maum.automl.frontapi.model.entity.RoleName;
import ai.maum.automl.frontapi.model.entity.TopMenu;

public interface MenuRepository extends JpaRepository<Menu, Long> {
	
	QMenu menu = QMenu.menu;
	
	@Query(value = "select a.id "
			+ "	   		   ,a.menu_code as menuCode "
			+ "	 		   ,a.menu_name as menuName "
			+ "	   		   ,b.url "
			+ "       from top_menu a "
			+ "  left join ( select * "
			+ "  			   from ( select IFNULL(a.url, b.url) url "
			+ "  							 ,a.top_menu_id "
			+ "  							 ,ROW_NUMBER() OVER ( PARTITION BY a.top_menu_id ORDER BY a.ord, b.ord ) as rownum "
			+ "  						from menu a "
			+ "				       left join menu b "
			+ "						      on a.id = b.parent_id "
			+ "				      inner join role_menu_management c "
			+ "						      on IFNULL(b.id, a.id) = c.menu_id "
			+ "						     and c.role_auth_id = (select auth_id from user where id = :userId) "
			+ "						   where a.parent_id = 0 "
			+ "					    ) c "
			+ "			      where rownum = 1 "
			+ "			   ) b "
			+ "         on a.id = b.top_menu_id "
			+ " 	 where a.menu_code in ( select edge_type "
			+ " 						      from device "
			+ " 						     where user_id = :userId "
			+ " 						     group by edge_type "
			+ "					           ) "
			+ "      order by a.ord", nativeQuery = true)
	List<MenuExInterface> getHeaderMenuList(@Param("userId") Long userId);
	
	default List<MenuDto> getHeaderList(EntityManager entityManager, UserPrincipal user, String compId) {
		
		boolean isAdmin = (user.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_ADMIN.name())));
		boolean isEngineer  = (user.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_ENGINEER.name())));
		boolean isCompAdmin = (user.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_COMP_ADMIN.name())));
		
		String query = "select a.id "
				+ "	   		   ,a.menu_code as menuCode "
				+ "	 		   ,a.menu_name as menuName "
				+ "	   		   ,b.url "
				+ "       from top_menu a "
				+ "  left join ( select * "
				+ "  			   from ( select IFNULL(a.url, b.url) url "
				+ "  							 ,a.top_menu_id "
				+ "  							 ,ROW_NUMBER() OVER ( PARTITION BY a.top_menu_id ORDER BY a.ord, b.ord ) as rownum "
				+ "  						from menu a "
				+ "				       left join menu b "
				+ "						      on a.id = b.parent_id "
				+ "				      inner join role_menu_management c "
				+ "						      on IFNULL(b.id, a.id) = c.menu_id "
				+ "						     and c.role_auth_id = (case :userId when 0 then (select auth_id from role where auth_name = 'ROLE_USER') else (select auth_id from user where id = :userId ) end)  "
				+ "						   where a.parent_id = 0 "
				+ "					    ) c "
				+ "			      where rownum = 1 "
				+ "			   ) b "
				+ "         on a.id = b.top_menu_id "
				+ "		 where a.use_yn = 'Y' ";
		
		if(isAdmin || isEngineer) {
			query += "";
		}else {
			query += "	and (a.show_yn = 'Y' or a.menu_code in ( select edge_type";
			if(isCompAdmin) {
				query += "	from device dev join user us on dev.user_id = us.id where us.client_company_id = '"+compId+"'";
			}else {
				query += "	from device where user_id = :userId";
			}
			query += "	group by edge_type ) )";
		}
		
		query += "	order by a.ord";
		
		List<MenuDto> list = entityManager.createNativeQuery(query, "topMenuMapping")
											.setParameter("userId", user.getUserNo())
											.getResultList();
		
		
		return list;
	}
	
	
	@Query(value = "WITH RECURSIVE sort_menu AS "
				+ "( SELECT t.id "
				+ "         ,t.parent_id "
				+ "		    ,t.menu_code "
				+ "		    ,t.menu_name "
				+ "		    ,t.ico_name "
				+ "		 	,t.ord "
				+ "		 	,t.top_menu_id "
				+ "		 	,t.url "
				+ "			,t.depth "	
				+ "         ,t.side_yn "
				+ "		    ,t.use_yn "
				+ "		 	,1 AS level	"
				+ "		 	,CAST(t.ord AS VARCHAR(255)) as sort "
				+ "    FROM menu t "
				+ "   WHERE t.parent_id = 0 "
				+ "   UNION all "
				+ "  SELECT t.id "
				+ "         ,t.parent_id "
				+ "		 	,t.menu_code "
				+ "		 	,t.menu_name "
				+ "		 	,t.ico_name "
				+ "		 	,t.ord "
				+ "		 	,t.top_menu_id "
				+ "		 	,t.url "
				+ "			,t.depth "
				+ "         ,t.side_yn "
				+ "		    ,t.use_yn "
				+ "		 	,1+level as level "
				+ "		 	,CONCAT(sort, ' > ', t.ord) AS sort "
				+ "    FROM menu t "
				+ "    join sort_menu r "
				+ "      ON r.id = t.parent_id "
				+ ")"
				+ "  select a.id "
				+ "  		 ,a.menu_code as menuCode "
				+ "  		 ,a.menu_name as menuName "
				+ "  		 ,a.ico_name as icoName "
				+ "  		 ,a.ord "
				+ "		 	 ,a.url "
				+ "  		 ,a.depth "
				+ "          ,a.top_menu_id as topMenuId "
				+ "    From sort_menu a  "
				+ "    join role_menu_management b "
				+ "      on a.id = b.menu_id "
				+ "    join top_menu c  "
				+ "      on a.top_menu_id = c.id "
				+ "   where b.role_auth_id = (select auth_id from user where id = :userId) "
				+ "     and a.side_yn = 'Y' "
				+ "	    and a.use_yn = 'Y' "
				+ "		and c.use_yn = 'Y' "
				+ "   order by c.ord, sort, a.id", nativeQuery = true)
	List<SideMenuExInterface> getSideMenuList(@Param("userId") Long userId);
	
	@Query(value = "  select a.id "
			+ "  		 ,a.menu_code as menuCode "
			+ "  		 ,a.menu_name as menuName "
			+ "  		 ,a.ico_name as icoName "
			+ "  		 ,a.ord "
			+ "		 	 ,a.url "
			+ "  		 ,a.depth "
			+ "          ,a.top_menu_id as topMenuId "
			+ "       from menu a "
			+ "       join role_menu_management b "
			+ "         on a.id = b.menu_id "
			+ "		  join user c "
			+ "         on b.role_auth_id = c.auth_id "
			+ "      where url is not null "
			+ "        and c.id = :userId", nativeQuery = true)
	List<SideMenuExInterface> findByRoleAuthId(@Param("userId") Long userId);
	
}
