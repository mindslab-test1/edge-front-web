package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.dto.DeviceDto;
import ai.maum.automl.frontapi.model.dto.DeviceSetDto;
import ai.maum.automl.frontapi.model.dto.DeviceSetSearchDto;
import ai.maum.automl.frontapi.model.dto.RoadRecogDto;
import ai.maum.automl.frontapi.model.dto.RoadRecogSearchDto;
import ai.maum.automl.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.automl.frontapi.model.entity.Device;
import ai.maum.automl.frontapi.model.entity.DeviceHistory;
import ai.maum.automl.frontapi.model.entity.DeviceSet;
import ai.maum.automl.frontapi.model.entity.QClientCompany;
import ai.maum.automl.frontapi.model.entity.QDevice;
import ai.maum.automl.frontapi.model.entity.QDeviceSet;
import ai.maum.automl.frontapi.model.entity.QRoad;
import ai.maum.automl.frontapi.model.entity.QUser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * ServiceRequestRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-26  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-01-26
 */
@Repository
public interface DeviceSetRepository extends JpaRepository<DeviceSet, Long> {
	
	QDevice device = QDevice.device;
	QDeviceSet deviceSet = QDeviceSet.deviceSet;
	QClientCompany clientCompany = QClientCompany.clientCompany;
	QUser user = QUser.user;
	QRoad road = QRoad.road;
	
	//조회
    default Page<DeviceSetDto> getDeviceSetList(EntityManager entityManager, DeviceSetSearchDto searchDto, Pageable pageable) {
    	JPAQuery<DeviceSetDto> deviceSetQuery = new JPAQuery<DeviceSetDto>(entityManager)
				.select(Projections.fields(
											DeviceSetDto.class
											, deviceSet.edgeSetSeq
											, deviceSet.deviceId
											, device.edgeId
											, device.addr
											, device.addrDetail
											, road.cameraDir
											, clientCompany.clientCompanyName
											, clientCompany.clientCompanyId
											, user.name.as("userName")
											, user.tel.as("userTel")
											, deviceSet.setUserId
											, ExpressionUtils.as(JPAExpressions.select(user.name).from(user).where(user.id.eq(deviceSet.setUserId)), "setUserName")
											, deviceSet.setDt
											, deviceSet.setYn
											))
				.from(deviceSet)
				.join(device).on(device.deviceId.eq(deviceSet.deviceId))
				.join(road).on(device.deviceId.eq(road.deviceId))
				.leftJoin(user).on(device.userId.eq(user.id), user.useYn.eq("Y"))
				.leftJoin(clientCompany).on(user.clientCompanyId.eq(clientCompany.clientCompanyId), clientCompany.useYn.eq("Y"))
				.where(
						device.useYn.eq("Y")
						,likeSearchType(searchDto.getSearchType(), searchDto.getSearchTxt())
						,isState(searchDto.getSetState())
						,isDate(searchDto.getStartDate(), searchDto.getEndDate())
					  )
				.orderBy(deviceSet.setDt.desc());
    	
    	//System.out.println(deviceSetQuery);
    	//return new PageImpl<>(deviceSetQuery.getResults(), pageable, deviceSetQuery.getTotal());
    	
    	boolean isPage = false;
		if (searchDto.getEdgeAiStatus() == null || "".equals(searchDto.getEdgeAiStatus())) {
			isPage = true;
			deviceSetQuery = deviceSetQuery.offset(pageable.getOffset())
						 	.limit(pageable.getPageSize());			
		}
		
		QueryResults<DeviceSetDto> list = deviceSetQuery.fetchResults();
		
		return (isPage) ? new PageImpl<>(list.getResults(), pageable, list.getTotal()) : new PageImpl<DeviceSetDto>(list.getResults());
		    	
	}
	
    //검색어
    private BooleanExpression likeSearchType(String searchType, String searchTxt) {
		if ("".equals(searchTxt) || searchTxt  == null) return null;
		
		BooleanExpression exp = null;
		
		switch (searchType) {
			case "all":
				exp = clientCompany.clientCompanyName.contains(searchTxt).or(user.name.contains(searchTxt)).or(device.edgeId.contains(searchTxt)).or(device.addr.contains(searchTxt).or(device.addrDetail.contains(searchTxt)));
				break;
			case "clientCompanyName":
				exp = clientCompany.clientCompanyName.contains(searchTxt);
				break;
			case "userName":
				exp = user.name.contains(searchTxt);
				break;
			case "edgeId":
				exp = device.edgeId.contains(searchTxt);
				break;
			case "addr":
				exp = device.addr.contains(searchTxt).or(device.addrDetail.contains(searchTxt));
				break;
			
		}
		
		return exp;
	}
    
    //상태
  	private BooleanExpression isState(String setState) {
  		if ("".equals(setState) || setState == null) return null;
  		return deviceSet.setYn.eq(setState);
  	}
    
    //시간
    private BooleanExpression isDate(String sDate, String eDate) {
		
		boolean isSdate = true;
		boolean isEdate = true;
		BooleanExpression exp = null;
		
		if (("".equals(sDate) || sDate == null)) isSdate = false;
		if (("".equals(eDate) || eDate == null)) isEdate = false;
		
		if (!isSdate && !isEdate) return null;
		
		String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_FORMAT);
		
		//LocalDateTime sDateTime = LocalDateTime.parse(sDate, formatter);
		//LocalDateTime eDateTime = LocalDateTime.parse(eDate, formatter);
		
		LocalDateTime sDateTime;
		LocalDateTime eDateTime;
		if(("".equals(sDate) || sDate == null)){
			sDateTime = null;
		}else {
			sDateTime = LocalDateTime.parse(sDate, formatter);
		}
		if(("".equals(eDate) || eDate == null)){
			eDateTime = null;
		}else {
			eDateTime = LocalDateTime.parse(eDate, formatter);
		}
		
		//sDateTime = LocalDateTime.parse(sDate, formatter);
		//eDateTime = LocalDateTime.parse(eDate, formatter);
		
		if (isSdate && isEdate) {
			exp = deviceSet.setDt.between(sDateTime, eDateTime);
		} else if (isSdate && !isEdate) {
			exp = deviceSet.setDt.gt(sDateTime);
		} else if (!isSdate && isEdate) {
			exp = deviceSet.setDt.lt(eDateTime);
		}
		
		return exp;
		
	}
}
