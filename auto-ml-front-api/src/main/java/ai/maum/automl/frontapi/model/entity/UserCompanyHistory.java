package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

/**
 * 회원 - 고객사 매핑 이력
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"user"})
@Table(
        indexes = {
                @Index(name = "IX_user_company_history", columnList = "action"),
                @Index(name = "IX_user_company_history2", columnList = "clientCompanyId"),
                @Index(name = "IX_user_company_history3", columnList = "id"),
        }
)
public class UserCompanyHistory {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long histSeq; // hist_seq(PK)	이력 순번(PK)	순번	BIGINT	NOT NULL
    
    @Column(nullable = false, insertable = false, updatable = false)
    private Long id; // mapping_seq(FK)	매핑 순번(FK)	순번	BIGINT	NOT NULL
    //private int mappingSeq; // mapping_seq(FK)	매핑 순번(FK)	순번	BIGINT	NOT NULL

    @Column(nullable = false, length = 12)
    private String action; // action	매핑 액션	동작	VARCHAR(12)	NOT NULL

    @Column(nullable = false, length = 24)
    private String userId; // user_id	회원 아이디	아이디	VARCHAR(24)	NOT NULL

    @Column(nullable = false, length = 24)
    private String clientCompanyId; // client_company_id	고객사 아이디	아이디	VARCHAR(24)	NOT NULL

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date regDt; // reg_dt	등록일시	일시	DATETIME	NOT NULL

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id")
    private User user; // 회원 - 고객사 매핑
    
    /*
	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int histSeq; // hist_seq(PK)	이력 순번(PK)	순번	BIGINT	NOT NULL

    @Column(nullable = false)
    //private int mappingSeq; // mapping_seq(FK)	매핑 순번(FK)	순번	BIGINT	NOT NULL
    private int id; // mapping_seq(FK)	매핑 순번(FK)	순번	BIGINT	NOT NULL

    @Column(nullable = false, length = 12)
    private String action; // action	매핑 액션	동작	VARCHAR(12)	NOT NULL

    @Column(nullable = false, length = 24)
    private String userEmail; // user_id	회원 아이디	아이디	VARCHAR(24)	NOT NULL

    @Column(nullable = false, length = 24)
    private String clientCompanyId; // client_company_id	고객사 아이디	아이디	VARCHAR(24)	NOT NULL

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date regDt; // reg_dt	등록일시	일시	DATETIME	NOT NULL

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(
    				updatable=false, insertable=false, name = "id", referencedColumnName="id"
    			)
    
    private User user; // 회원 - 고객사 매핑
     */

}
