package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

/**
 * 고객문의
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(
        indexes = {
                @Index(name = "IX_customer_service_request_01", columnList = "regDt"),
        }
)
public class CustomerServiceRequest {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long requestSeq; // request_seq(PK)	요청 순번(PK)	순번	BIGINT	NOT NULL

    @Column(nullable = false, length = 24)
    private String name; // name	이름	이름	VARCHAR(24)	NOT NULL

    @Column(nullable = false, length = 128)
    private String email; // email	이메일	이메일	VARCHAR(128)	NOT NULL

    @Column(nullable = false, length = 13)
    private String tel; // tel	전화번호	전화번호	VARCHAR(13)	NOT NULL

    @Lob
    @Column(nullable = false)
    private String content; // content	내용	내용	TEXT	NOT NULL

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date regDt; // reg_dt	등록일시	일시	DATETIME	NOT NULL

    @PrePersist
    public void prePersist() {
        this.regDt = Optional.ofNullable(this.regDt).orElse(new Date());
    }

}
