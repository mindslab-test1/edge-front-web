package ai.maum.automl.frontapi;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.querydsl.jpa.impl.JPAQuery;

import ai.maum.automl.frontapi.repository.ClientCompanyRepository;
import ai.maum.automl.frontapi.repository.CmmnCodeRepository;
import ai.maum.automl.frontapi.repository.DeviceHistoryRepository;
import ai.maum.automl.frontapi.repository.DeviceRepository;
import ai.maum.automl.frontapi.repository.DeviceSetRepository;
import ai.maum.automl.frontapi.model.dto.HomeInfoDto;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.CmmnCode;
import ai.maum.automl.frontapi.model.entity.Device;
import ai.maum.automl.frontapi.model.entity.DeviceHistory;
import ai.maum.automl.frontapi.model.entity.DeviceSet;
import ai.maum.automl.frontapi.model.entity.HomeInfo;
import ai.maum.automl.frontapi.model.entity.Road;
import ai.maum.automl.frontapi.model.entity.Role;
import ai.maum.automl.frontapi.model.entity.RoleName;
import ai.maum.automl.frontapi.model.entity.SequenceManagement;
import ai.maum.automl.frontapi.model.entity.User;
import ai.maum.automl.frontapi.repository.HomeInfoRepository;
import ai.maum.automl.frontapi.repository.RoadRepository;
import ai.maum.automl.frontapi.repository.RoleRepository;
import ai.maum.automl.frontapi.repository.UserRepository;

@Component
public class InitTestData implements ApplicationRunner {
	
	@Override
	@Transactional
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
