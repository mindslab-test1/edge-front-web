package ai.maum.automl.frontapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ai.maum.automl.frontapi.model.entity.CmmnCode;

/**
 * CmmnCodeRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-06  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-01-06
 */
@Repository
public interface CmmnCodeRepository extends JpaRepository<CmmnCode, Integer> {

	Optional<List<CmmnCode>> findByPrtCodeOrderByOrdAsc(String prtCode);
}
