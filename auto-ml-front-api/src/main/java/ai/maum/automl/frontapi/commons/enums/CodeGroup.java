package ai.maum.automl.frontapi.commons.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;

@Getter
public enum CodeGroup {
	
	EDGE_GROUP("엣지 정보",Arrays.asList(Code.EDGE_ROAD, Code.EDGE_STREET, Code.EDGE_FACTORY, Code.EDGE_FARM, Code.EDGE_HOME, Code.EDGE_DRONE)),
	EDGE_AI_STATUS("엣지 상태", Arrays.asList(Code.WORKING, Code.ABNORMAL, Code.NOTWORKING, Code.NOTCONNECT)),
	USE_YN("사용여부", Arrays.asList(Code.USE_Y, Code.USE_N)),
	EMPTY("없음", Collections.EMPTY_LIST),
	CAR_TYPE("차량 종류", Arrays.asList(Code.CAR_UNKNOWN, Code.CAR_NORMAL, Code.CAR_TAXI, Code.CAR_BUS, Code.CAR_OLD, Code.CAR_NEW)),
	DIRECTION("인식 방향", Arrays.asList(Code.FRONT, Code.BACK, Code.BOTH)),
	CAM_DIR("도로 방향", Arrays.asList(Code.ENTRY, Code.EXIT)),
	EDGE_USER_AUTH("사용자 권한", Arrays.asList(Code.EDGE_USER, Code.EDGE_ENGINEER, Code.EDGE_COMP_ADMIN, Code.EDGE_ADMIN));
	
	private String title;
	
	private List<Code> codeList;
	
	public static final Map<String, CodeGroup> map = new HashMap<>(); 
	static { 
		for (CodeGroup os : CodeGroup.values()) { 
			map.put(os.name(), os); 
		} 
	}
	
	CodeGroup(String title, List<Code> codeList) {
		this.title = title;
		this.codeList = codeList;
	}
	
	public static CodeGroup findByCodeGroup(Code code) {
		return Arrays.stream(CodeGroup.values())
				.filter(data -> data.hasCode(code))
				.findAny()
				.orElse(EMPTY);
	}
	
	public boolean hasCode(Code code) {
		return codeList.stream().anyMatch(cd -> cd == code);
	}
	
	public static CodeGroup getCodeGroup(String name) {
		return map.get(name);
	}
	
	
}
