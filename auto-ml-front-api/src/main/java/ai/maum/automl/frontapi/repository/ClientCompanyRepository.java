package ai.maum.automl.frontapi.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.hibernate.sql.Select;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.commons.utils.LocalDateParser;
import ai.maum.automl.frontapi.model.dto.ClientCompanyDto;
import ai.maum.automl.frontapi.model.dto.ClientCompanySearchDto;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.QClientCompany;
import ai.maum.automl.frontapi.model.entity.QUser;
import ai.maum.automl.frontapi.model.entity.RoleName;

public interface ClientCompanyRepository extends JpaRepository<ClientCompany, String> {
	
	QClientCompany clientCompany = QClientCompany.clientCompany;
	QUser user = QUser.user;
	
	default Page<ClientCompanyDto.list> getClientCompany(EntityManager entityManager, UserPrincipal authUser, ClientCompanySearchDto dto, Pageable pageable) {
		
		JPAQuery<ClientCompanyDto.list> query = new JPAQuery<ClientCompanyDto.list>(entityManager)
				.select(Projections.fields(ClientCompanyDto.list.class, clientCompany.clientCompanyId, clientCompany.clientCompanyName, clientCompany.useYn, clientCompany.regDt))
				.from(clientCompany)
				.where(
						isAdminUser(authUser)
						,likeSearchType(dto.getSearchType(), dto.getSearchTxt())
						,isUseYn(dto.getUseYn())
						,isRegDate(dto.getStartDate(), dto.getEndDate())
					  )
				.orderBy(clientCompany.regDt.desc(), clientCompany.clientCompanyId.desc());
		
		if (pageable != null) {
			query = query.offset(pageable.getOffset())
					 	 .limit(pageable.getPageSize());
		} 
		
		QueryResults<ClientCompanyDto.list> list = query.fetchResults();
		
		return (pageable != null) ? new PageImpl<>(list.getResults(), pageable, list.getTotal()) : new PageImpl<ClientCompanyDto.list>(list.getResults());
		
		
	}
	
	ClientCompany findByClientCompanyId(String clientCompanyId);
	Optional<ClientCompany> findByClientCompanyNameIgnoreCase(String clientCompanyName);
	Optional<ClientCompany> findByClientCompanyNameIgnoreCaseAndClientCompanyIdNot(String clientCompanyName, String clientCompanyId);
	List<ClientCompanyDto> findByUseYn(String useYn);
	
	private BooleanExpression isAdminUser(UserPrincipal authUser) {
		BooleanExpression exp = null;
		
		if (!authUser.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_ADMIN.name()))) {
			exp = clientCompany.clientCompanyId.eq(JPAExpressions.select(user.clientCompanyId).from(user).where(user.id.eq(authUser.getUserNo())).limit(1));
		} 
		
		return exp;
	}
	
	
	private BooleanExpression likeSearchType(String searchType, String searchTxt) {
		
		
		if ("".equals(searchTxt) || searchTxt  == null) return null;
		//if ("".equals(searchType) || searchType == null) return null;
		
		BooleanExpression exp = null;
		
		switch (searchType) {
		case "companyNm":
			exp = clientCompany.clientCompanyName.contains(searchTxt);
			break;
		case "clientCompanyId":
			exp = clientCompany.clientCompanyId.contains(searchTxt);
			break;
		default : 
			exp = clientCompany.clientCompanyId.contains(searchTxt).or(clientCompany.clientCompanyName.contains(searchTxt));
			break;
		}
		
		return exp;
	}
	
	private BooleanExpression isUseYn(String useYn) {
		
		if ("".equals(useYn) || useYn == null) return null;
		
		return clientCompany.useYn.eq(useYn);
	}
	
	private BooleanExpression isRegDate(String sDate, String eDate) {
		
		boolean isSdate = true;
		boolean isEdate = true;
		BooleanExpression exp = null;
		
		if (("".equals(sDate) || sDate == null)) isSdate = false;
		if (("".equals(eDate) || eDate == null)) isEdate = false;
		
		if (!isSdate && !isEdate) return null;
		
		if (isSdate && isEdate) {
			exp = clientCompany.regDt.between(new LocalDateParser(sDate).getStartDate(), new LocalDateParser(eDate).getEndDate());
		} else if (isSdate && !isEdate) {
			exp = clientCompany.regDt.gt(new LocalDateParser(sDate).getStartDate());
		} else if (!isSdate && isEdate) {
			exp = clientCompany.regDt.lt(new LocalDateParser(eDate).getEndDate());
		}
		
		return exp;
		
	}
	
}
