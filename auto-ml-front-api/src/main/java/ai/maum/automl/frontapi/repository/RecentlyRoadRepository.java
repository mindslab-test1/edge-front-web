package ai.maum.automl.frontapi.repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import ai.maum.automl.frontapi.model.dto.DeviceViewDto;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class RecentlyRoadRepository {
	
	private static final String RECENTLY_ROAD = "RECENTLY_ROAD";
	
	private final RedisTemplate<String, Object> redisTemplate;
	
	private HashOperations<String, String, List<Map<String, Object>>> opsHashRecently;
	
	private final Comparator<Map<String, Object>> comp = (p1, p2) -> Long.compare( Integer.parseInt(String.valueOf(p1.get("lane"))), Integer.parseInt(String.valueOf(p2.get("lane"))));
	
	@PostConstruct
	private void init() {
		opsHashRecently = redisTemplate.opsForHash();
	}
	
	public Map<String, List<Map<String, Object>>> get() {
		return opsHashRecently.entries(RECENTLY_ROAD);
	}
	
	public List<Map<String, Object>> findByEdgeId(String edgeId) {
		return opsHashRecently.get(RECENTLY_ROAD, edgeId);
	}
	
	public void insertRecentlyRoad(String edgeId, Map<String, Object> obj) {
		List<Map<String, Object>> list = this.findByEdgeId(edgeId);
		
		if (list != null && !list.isEmpty()) {
			List<Map<String, Object>> filterList = list.stream().filter(data -> Integer.parseInt(String.valueOf(data.get("lane"))) != Integer.parseInt(String.valueOf(obj.get("lane")))).collect(Collectors.toList());
			filterList.add(obj);
			opsHashRecently.put(RECENTLY_ROAD, edgeId, filterList.stream().sorted(comp).collect(Collectors.toList()));
		} else {
			list = new ArrayList<Map<String, Object>>();
			list.add(obj);
			opsHashRecently.put(RECENTLY_ROAD, edgeId, list);
		}
		
	}
	
	public void deleteRecentlyRoad(String edgeId) {
		opsHashRecently.delete(RECENTLY_ROAD, edgeId);
	}
	
}
