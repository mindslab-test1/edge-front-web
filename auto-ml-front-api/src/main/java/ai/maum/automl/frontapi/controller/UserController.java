package ai.maum.automl.frontapi.controller;

import java.util.Map;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.automl.frontapi.commons.payload.ApiResponse;
import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.model.dto.UserEdgeDto;
import ai.maum.automl.frontapi.model.dto.UserSearchDto;
import ai.maum.automl.frontapi.service.UserEdgeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * UserController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-01-11  / 차승호	 / 최초 생성
 * </pre>
 * @since 2021-01-11
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/user/admin")
@Secured("ROLE_ADMIN")
public class UserController {
	
    final UserEdgeService userEdgeService;

    public UserController(UserEdgeService userEdgeService) {
        this.userEdgeService = userEdgeService;
    }

    /**
     * 사용자 조회
     * @param userServiceRequest
     * @return
     **/
    @ApiOperation(value = "사용자 조회")
    @PostMapping("/list")
    public ResponseEntity<?> getUser(@CurrentUser UserPrincipal user, @RequestBody UserSearchDto userSearchDto) {
    	//System.out.println("test@@@@@@@@@@@@@@@@@@@@@");
    	//String logTitle = "list/email[" + user.getEmail() + "]/";
    	log.info("/getUser["+user.getUserNo()+"]");
    	try {
    		Page<UserEdgeDto> list = userEdgeService.getUserEdgeList(userSearchDto); 
    		
            return new ResponseEntity<>(list, HttpStatus.OK);
    	}catch (EmptyResultDataAccessException e){
    		log.error("failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}catch(RuntimeException e) {
    		log.error("failed:" + e.getMessage());
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}
    }
    
    @ApiOperation(value = "사용자 검색")
    @PostMapping("/search")
    public ResponseEntity<?> getTest(@RequestBody Map<String, String> params) {
    	//System.out.println("test@@@@@@@@@@@@@@@@@@@@@");
    	//String logTitle = "list/email[" + user.getEmail() + "]/";
    	//log.info("/getUser["+user.getUserNo()+"]");
    	Map<String, Object> map = userEdgeService.getUserSearch(params, (long)1);

    	if ("success".equals(map.get("result"))) {
    		return ResponseEntity.ok(new ApiResponse(true, "조회성공", map));
    	} else {
    		return new ResponseEntity<>(new ApiResponse(false, "조회실패", map), HttpStatus.BAD_REQUEST);    		
    	}
    }
    
    /**
     * 사용자 상세
     * @param userServiceRequest
     * @return
     **/
    @ApiOperation(value = "사용자 상세")
    @PostMapping("/view/{id}")
    public ResponseEntity<?> getUserEdgenfo(@CurrentUser UserPrincipal user, @PathVariable Long id) {
    	log.info("/view["+user.getUserNo()+"]["+id+"]");
    	return ResponseEntity.ok(new ApiResponse(true, "조회성공", userEdgeService.getUserEdgeInfo(id)));
    }
    
    /**
     * 사용자 상세
     * @param userServiceRequest
     * @return
     **/
    @ApiOperation(value = "테스트")
    @PostMapping("/test")
    public ResponseEntity<?> getTest(@CurrentUser UserPrincipal user, @PathVariable Long id) {
    	log.info("/test["+user.getUserNo()+"]["+id+"]");
    	return ResponseEntity.ok(new ApiResponse(true, "조회성공", userEdgeService.getTest(id)));
    }
    
    /**
     * 사용자 중복 체크
     * @param userServiceRequest
     * @return
     */
    @ApiOperation(value = "사용자 중복 체크")
    @PostMapping("/check")
    public ResponseEntity<?> checkUserEmail(@RequestBody UserEdgeDto userEdgeDto) {
    	try {
    		boolean emailCheck = userEdgeService.checkUserEmail(userEdgeDto);    		
    		return new ResponseEntity<>(emailCheck, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}	
    }
	
    /**
     * 사용자 등록
     * @param userServiceRequest
     * @return
     */
    @ApiOperation(value = "사용자 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dto", value = "사용자 정보", dataType = "UserEdgeDto")
    })
    @PostMapping("/create")
    public ResponseEntity<?> addUser(@CurrentUser UserPrincipal user, @RequestBody UserEdgeDto userEdgeDto) {
    	System.out.println("test@@@@@@@@@@@@@@@@@@@@@");
    	String logTitle = "addUser/[]/";
    	log.info("/addUser["+user.getUserNo()+"]");
    	boolean addBool = userEdgeService.addUserEdge(user.getUserNo(), userEdgeDto);
    	
    	if(addBool) {
            //log.info(logTitle + "user id:" + userEx.getId());
    		return ResponseEntity.ok(new ApiResponse(true, "등록", true));
        }else {
            //log.error(logTitle + "failed");
        	return new ResponseEntity<>(new ApiResponse(false, "등록실패", false), HttpStatus.BAD_REQUEST);
        }
    }
    
    /**
     * 사용자 수정
     * @param userServiceRequest
     * @return
     */
    @ApiOperation(value = "사용자 수정")
    @PostMapping("/edit")
    public ResponseEntity<?> editUserEdge(@CurrentUser UserPrincipal user, @RequestBody UserEdgeDto userEdgeDto) {
    	//if (userEdgeService.editUserEdge((long)1, userEdgeDto)) {
    	log.info("/edit["+user.getUserNo()+"]");
    	if (userEdgeService.editUserEdge(user.getUserNo(), userEdgeDto)) {
    		userEdgeService.removeCache(userEdgeDto.getId());
    		return ResponseEntity.ok(new ApiResponse(true, "수정성공", true));    		
    	} else {
    		return new ResponseEntity<>(new ApiResponse(false, "수정실패", false), HttpStatus.BAD_REQUEST);     		
    	}
    }
    
    /**
     * 사용자 삭제
     * @param userServiceRequest
     * @return
     */
    @ApiOperation(value = "사용자 삭제")
    @PostMapping("/delete")
    public ResponseEntity<?> deleteUserEdge(@CurrentUser UserPrincipal user, @RequestBody long[] arr) {
    	log.info("/delete["+user.getUserNo()+"]");
    	try {
    		boolean isDel = userEdgeService.deleteUserEdge(user.getUserNo(), arr);
    		return ResponseEntity.ok(new ApiResponse(true, "삭제성공", isDel));    		
    	} catch (RuntimeException e) {
    		return new ResponseEntity<>(new ApiResponse(false, "삭제실패", e.getMessage()), HttpStatus.BAD_REQUEST); 
		}
    }
    
    
}
