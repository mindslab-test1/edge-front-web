package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * 공통코드
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(
        indexes = {
                @Index(name = "IX_cmmn_code", columnList = "code,prtCode,useYn,delYn"),
                @Index(name = "IX_cmmn_code2", columnList = "code,prtCode,ord,useYn,delYn"),
        }
)
public class CmmnCode {

    /*@Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codeSeq; // code_seq(PK)	코드 순번(PK)	순번	BIGINT	NOT NULL*/
    
    @Id
    @Column(nullable = false, length = 12)
    private String code; // code	코드	코드	VARCHAR(12)	NOT NULL

    @Column(nullable = false, length = 24)
    private String codeName; // code_name	코드 이름	이름	VARCHAR(24)	NOT NULL

    @Column(nullable = false, length = 12)
    private String prtCode; // prt_code	부모 코드 	코드	VARCHAR(12)	NOT NULL

    @Column(nullable = false)
    private int ord; // ord	코드 순서	숫자	INTEGER	NOT NULL

    @Column
    private String remark; // remark	코드 설명	설명	VARCHAR(255)	NULL

    @Column(nullable = false, length = 1)
    private String useYn; // use_yn	사용여부	여부	CHAR(1)	NOT NULL

    @Column(nullable = false, length = 1)
    private String delYn; // del_yn	삭제여부	여부	CHAR(1)	NOT NULL

    @Column(nullable = false)
    private Long regId; // reg_id	등록자	아이디	VARCHAR(24)	NOT NULL

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date regDt; // reg_dt	등록일시	일시	DATETIME	NOT NULL

    @Column
    private Long modId; // mod_id	수정자	아이디	VARCHAR(24)	NULL

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date modDt; // mod_dt	수정일시	일시	DATETIME	NULL

}
