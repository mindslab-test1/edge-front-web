package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.entity.CustomerQuoteRequest;
import ai.maum.automl.frontapi.model.entity.CustomerServiceRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * QuoteRequestRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-30  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-30
 */
@Repository
public interface QuoteRequestRepository extends JpaRepository<CustomerQuoteRequest, Integer> {



}
