package ai.maum.automl.frontapi.commons.enums;

import lombok.Getter;
import org.springframework.http.MediaType;

/**
 * ai.maum.automl.frontapi.commons.enums
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-30  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-30
 */
@Getter
public enum Api {

    SEND_MAIL("https://maum.ai/support/sendMail", MediaType.APPLICATION_FORM_URLENCODED, "이메일 전송"),
	GET_EDGE_STATUS("http://15.165.170.235:50053/edge_bridge/avr/get_edge_status/", MediaType.APPLICATION_JSON, "엣지 정보 조회"),
	SEARCH_USER("https://sso.maum.ai/maum/userList", MediaType.APPLICATION_JSON, "사용자 조회"),
	EDGE_RESTART("http://15.165.170.235:50053/edge_bridge/avr/req_restart/", MediaType.APPLICATION_JSON,"엣지 재시작"),
	EDGE_REGISTER("http://15.165.170.235:50053/edge_bridge/avr/req_register/", MediaType.APPLICATION_JSON,"엣지 등록"),
	EDGE_UNREGISTER("http://15.165.170.235:50053/edge_bridge/avr/req_unregister/", MediaType.APPLICATION_JSON, "엣지 삭제"),
	EDGE_UPDATE_POLICY("http://15.165.170.235:50053/edge_bridge/avr/update_policy/", MediaType.APPLICATION_JSON, "엣지 정책 업데이트"),
	EDGE_STREAM_URL("http://15.165.170.235:50053/edge_bridge/avr/get_stream_url/", MediaType.APPLICATION_JSON, "엣지 URL 정보");
	
    private final String path;
    private final MediaType header;
    private final String desc;

	Api(String path, MediaType header, String desc) {
		// TODO Auto-generated constructor stub
		this.path = path;
        this.header = header;
        this.desc = desc;
	}

}
