package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * 디바이스
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"deviceHistories,statuses,clientCompany,deviceSet"})
@Table(
        indexes = {
                @Index(name = "IX_device_01", columnList = "edgeType"),
                @Index(name = "IX_device_02", columnList = "edgeId"),
                @Index(name = "IX_device_04", columnList = "clientCompanyId,addr,useYn"),
        }
)
public class Device implements Serializable {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long deviceId;
	
    @Column
    private String edgeId; // edge_id(PK)	엣지 아이디(PK)	엣지아이디	VARCHAR(255)	NOT NULL

    @Column(nullable = false, length = 12)
    private String edgeType; // edge_type	엣지 유형	코드	VARCHAR(12)	NOT NULL

    @Column(length = 24, insertable = false, updatable = false)
    private String clientCompanyId; // client_company_id(FK)	회사 아이디(FK)	아이디	VARCHAR(24)	NULL

    @Column(length = 12)
    private String releaseNo; // release_no	엣지 버전정보	버전T	VARCHAR(12)	NULL

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date releaseDate; // release_date	엣지 버전 업데이트일	일시	DATETIME	NULL
    
    @Column
    private String addr; // addr_code	설치장소 코드	코드	VARCHAR(255)	NULL

    @Column
    private String addrDetail; // addr_detail	설치장소 상세	상세	VARCHAR(255)	NULL

    @Column(nullable = false, columnDefinition = "VARCHAR(1) default 'Y'")
    private String useYn; // use_yn	사용여부	여부	CHAR(1)	NOT NULL

    @Column(nullable = false)
    private Long regId; // reg_id	등록자	아이디	VARCHAR(24)	NOT NULL

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt; // reg_dt	등록일시	일시	DATETIME	NOT NULL

    @Column
    private Long lastModId; // last_mod_id	최종 수정자	아이디	VARCHAR(24)	NULL

    @Column
    private LocalDateTime lastModDt; // last_mod_dt	최종 수정일시	일시	DATETIME	NULL
    
    @Column(length = 24, insertable = false, updatable = false)
    private Long userId;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="userId", referencedColumnName = "id")
    private User user;
    

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "clientCompanyId")
    private ClientCompany clientCompany; // 엣지 장비 정보

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "device")
    private Set<DeviceHistory> deviceHistories = new LinkedHashSet<>(); // 엣지 장비 이력
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "device")
    private Set<Status> statuses = new LinkedHashSet<>(); // 엣지 모니터링 상태 이력
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "device", orphanRemoval=true)
    private Set<DeviceSet> deviceSet = new LinkedHashSet<>(); // 엣지 장비 설치 정보
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "device", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Road> road = new LinkedHashSet<>();
    
    @PrePersist
    public void prePersist() {
    	this.regDt = Optional.ofNullable(this.regDt).orElse(LocalDateTime.now());
    	this.useYn = Optional.ofNullable(this.useYn).orElse("Y");
    }
    
    @PreRemove
    private void preRemove() {
        for (DeviceHistory h : deviceHistories) {
            h.setDevice(null);
        }
        
        for (Status s : statuses) {
        	s.setDevice(null);
        }
        
        for (DeviceSet s : deviceSet) {
        	s.setDevice(null);
        }
        
        for (Road r : road) {
        	r.setDevice(null);
        }
    }
    
}
