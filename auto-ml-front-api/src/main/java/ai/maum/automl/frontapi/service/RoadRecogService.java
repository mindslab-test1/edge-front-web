package ai.maum.automl.frontapi.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.querydsl.jpa.impl.JPADeleteClause;

import ai.maum.automl.frontapi.commons.enums.Api;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.commons.utils.LocalDateParser;
import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.commons.utils.RestUtil;
import ai.maum.automl.frontapi.model.dto.ClientCompanyDto;
import ai.maum.automl.frontapi.model.dto.DeviceDto;
import ai.maum.automl.frontapi.model.dto.PageRequest;
import ai.maum.automl.frontapi.model.dto.RoadDto;
import ai.maum.automl.frontapi.model.dto.RoadRecogDto;
import ai.maum.automl.frontapi.model.dto.RoadRecogSearchDto;
import ai.maum.automl.frontapi.model.dto.UserEdgeDto;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.Device;
import ai.maum.automl.frontapi.model.entity.Road;
import ai.maum.automl.frontapi.model.entity.RoadRecog;
import ai.maum.automl.frontapi.model.entity.Role;
import ai.maum.automl.frontapi.model.entity.User;
import ai.maum.automl.frontapi.repository.ClientCompanyRepository;
import ai.maum.automl.frontapi.repository.DeviceRepository;
import ai.maum.automl.frontapi.repository.RecentlyRoadRepository;
import ai.maum.automl.frontapi.repository.RoadRecogRepository;
import ai.maum.automl.frontapi.repository.RoadRepository;
import ai.maum.automl.frontapi.repository.RoleRepository;
import ai.maum.automl.frontapi.repository.UserEdgeRepository;
import ai.maum.automl.frontapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class RoadRecogService {
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	ClientCompanyRepository clientCompanyRepository;
	
    Logger logger = LoggerFactory.getLogger(RoadRecogService.class);
    
    final UserRepository userRepository;
    final RoadRepository roadRepository;
    final DeviceRepository deviceRepository;
    final RoadRecogRepository roadRecogRepository;
    final UserEdgeRepository userEdgeRepository;
    final EntityManager entityManager;
    final ModelMapperService modelMapperService;
    final RecentlyRoadRepository recentlyRoadRepository;
    final RestUtil restUtil;
    final SocketService socketService;
    
    //public boolean addRoadRecog(String recogJson){
    //@Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    @Transactional
    public Map<String, Object> addRoadRecog(String recogJson){
    	//User user = modelMapperService.map(userEdgeDto, User.class);
    	
    	
    	Map<String, Object> params = new HashMap<String, Object>();
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	
    	String[] strArr = {"pass_time", "edit_time", "car_number", "car_type", "car_img_path", "plate_img_path", "direction", };
    	String[] intArr = {"lane"};
    	String[] floatArr = {""};
    	
    	//params = (Map<String, Object>) modelMapperService.map(recogJson, RoadRecog.class);
    	
    	JSONParser jsonParser = new JSONParser();
    	Object obj;
    	try {
            List<String> list1 = new ArrayList();
            
            //String test = Arrays.toString("pass_time");
            
            obj = jsonParser.parse(recogJson);
            //System.out.println(obj);
            JSONObject jsonObj = (JSONObject) obj;
            //System.out.println(jsonObj);
             
            Set<String> key = jsonObj.keySet();
            Iterator<String> i = key.iterator();
            
            ObjectMapper mapper = new ObjectMapper();
             
            while(i.hasNext()) {
            	String keyName = i.next();
                Object objValue = jsonObj.get(keyName);
                Object keyType = objValue.getClass();
                
                mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
                
                //mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
                //System.out.println(" type : "+objValue.getClass() +" / key : "+keyName+" / value : "+objValue);
                
                if(!"edit_time".equals(keyName)) {
                	if("".equals(objValue) || objValue == null) {
                		resultMap.put("result", "fail");
                    	resultMap.put("msg", keyName+"의 값이 NULL 입니다.");
                    	
                    	return resultMap;
                    }
                }
                
                /*if(objValue instanceof String) {
                   System.out.println("key : "+keyName+"는 String 타입입니다.");
                }else if(objValue instanceof Integer) {
                   System.out.println("key : "+keyName+"는 Integer 타입입니다.");
                }else if(objValue instanceof Double) {
                   System.out.println("key : "+keyName+"는 Double 타입입니다.");
                }else if(objValue instanceof Float) {
                    System.out.println("key : "+keyName+"는 Float 타입입니다.");
                }else if(keyType instanceof Long) {
                   System.out.println("key : "+keyName+"는 Long 타입입니다.");
                }else {
                   System.out.println("key : "+keyName+"는 그 외 타입입니다.");
                }*/
                params.put(keyName, objValue);   
                //instanceof
            }
            
            //Device device = deviceRepository.findByDeviceId((long) 1);
             
            RoadRecog roadRecog = modelMapperService.map(params, RoadRecog.class);
             
            //int test1 = Integer.valueOf(jsonObj.get("lane").toString());
            //int test2 = Integer.parseInt(jsonObj.get("lane").toString());
             
            String passTime = (String) params.get("pass_time");
            String editTime = (String) params.get("edit_time");
            LocalDateTime passDate;
            LocalDateTime editDate;
            if(!"".equals(passTime)) {
            	 passDate = LocalDateTime.parse(passTime);
            	 //Date date = Date.from(passDate.atZone(ZoneId.systemDefault()).toInstant());
            	 //System.out.println("passDate ==> "+ passDate);
            	 //System.out.println("date ==> "+ date);
            	 roadRecog.setPassTime(passDate);
            }
            
            if(!"".equals(editTime)) {
            	 editDate = LocalDateTime.parse(editTime);
            	 roadRecog.setEditTime(editDate);
            }
            
            //roadRecog.setEdgeId((String) params.get("edge_id"));
            roadRecog.setCarNumber((String) params.get("car_number"));
            roadRecog.setCarType((String) params.get("car_type"));
            roadRecog.setCarImgPath((String) params.get("car_img_path"));
            roadRecog.setPlateImgPath((String) params.get("plate_img_path"));
            roadRecog.setDirection((String) params.get("direction"));
            roadRecog.setConfLevel(Float.valueOf(params.get("conf_level").toString()));
            roadRecog.setLane(Integer.valueOf(params.get("lane").toString()));
            roadRecog.setSpeed(Float.valueOf(params.get("speed").toString()));
            
            roadRecog.setRequestJson(recogJson);
            
            Optional<Device> device = deviceRepository.findByEdgeId((String)params.get("edge_id"));
            if(device.isEmpty()) {
            	resultMap.put("result", "fail");
            	resultMap.put("edge_id", params.get("edge_id"));
            	resultMap.put("msg", "엣지 정보를 찾을 수 없습니다.");
            	return resultMap;
            }else {
            	DeviceDto deviceDto = modelMapperService.mapAmbiguityIgnored(device.get(), DeviceDto.class);
            	roadRecog.setDeviceId(deviceDto.getDeviceId());
            	roadRecog.setEdgeId(deviceDto.getEdgeId());
            	roadRecog.setUserId(device.get().getUserId());
            	roadRecog.setAddr(deviceDto.getAddr());
            	roadRecog.setAddrDetail(deviceDto.getAddrDetail());
            	
            	ClientCompany company = clientCompanyRepository.findByClientCompanyId(device.get().getUser().getClientCompanyId());
            	if(company == null) {
            		resultMap.put("result", "fail");
                	resultMap.put("msg", "디바이스에 등록된 회사 정보를 찾을 수 없습니다.");
                	return resultMap;
            	}else {
            		roadRecog.setClientCompanyId(company.getClientCompanyId());
            		roadRecog.setClientCompanyName(company.getClientCompanyName());
            	}
            }
            
            Device deviceGetId = modelMapperService.map(device.get(), Device.class);
            
            Road road = roadRepository.findByDeviceId(deviceGetId.getDeviceId());
            if(road == null) {
            	resultMap.put("result", "fail");
            	resultMap.put("msg", "Road 정보가 없습니다.");
            	return resultMap;
            }else {
            	roadRecog.setCameraDir(road.getCameraDir());
                roadRecog.setCameraNo(road.getCameraNo());
                roadRecog.setRoad(road);
            }
            
            //roadRecog.setEdgeId(road.getEdgeId());
            //roadRecog.setEdgeNodeSeq(road.getEdgeNodeSeq());
            
            if(Optional.ofNullable(roadRecogRepository.save(roadRecog)).isPresent()) {
            	String vehSide = road.getVehSide();
            	if("both".equals(vehSide)) {
            		recentlyRoadRepository.insertRecentlyRoad((String)params.get("edge_id"), params);
            	}else {
            		String direction = (String) params.get("direction");
            		if(vehSide.equals(direction)) {
            			recentlyRoadRepository.insertRecentlyRoad((String)params.get("edge_id"), params);
            		}
            	}
            	
            	//socketService.sendRecentlyRoad((String)params.get("edge_id"));
            	
            	resultMap.put("result", "success");
            	resultMap.put("edge_id", params.get("edge_id"));
            	
            }else {
            	resultMap.put("result", "fail");
            	resultMap.put("edge_id", params.get("edge_id"));
            	resultMap.put("msg", resultMap.get("error"));
            }
         } catch (Exception e) {
        	resultMap.put("result", "fail");
        	resultMap.put("edge_id", params.get("edge_id"));
        	resultMap.put("msg", e.getMessage());
         }
         
         return resultMap; 
    }
    
    
    /*@Transactional
    public Page<UserDto.UserDtoEx> list(Long id, Pageable pageable) {

//        Page<Project> list = projectRepository.findAllByUserIdOrderByIdDesc(userNo, pageable);
//        return list.map(project -> modelMapperService.map(project, ProjectDto.class));
    	
        Page<UserExInterface> list2 = userRepository.findAllById(id, pageable);
        
        //List<User> list2 = userRepository.getUserList(entityManager, params);
        Page<UserDto.UserDtoEx> p2 = list2.map(user -> modelMapperService.map(user, UserDto.UserDtoEx.class));
        return p2;
    }*/
    
    @Transactional
    public Page<RoadRecogDto> getRoadRecogList(UserPrincipal user, RoadRecogSearchDto roadRecogSearchDto) {
    	PageRequest page = new PageRequest();
    	
    	page.setPage(roadRecogSearchDto.getPage());
    	page.setSize(roadRecogSearchDto.getSize());
    	page.setDirection(Sort.Direction.DESC);
    	
    	String compId = userRepository.findAllById(user.getUserNo()).get().getClientCompanyId();
    	
		return roadRecogRepository.getRoadRecogList(user, entityManager, roadRecogSearchDto, page.of("regDt"), compId);
        
    }

    //테스트
    public RoadDto getTestInfo(Long id) {
    	//Optional<User> user = userEdgeRepository.findById((long)1);
    	Optional<Device> device = deviceRepository.findByDeviceId(id);
    	
    	if (device.isPresent()) {
    		//DeviceDto dto = modelMapperService.map(device.get(), DeviceDto.class);
    		RoadDto dto = modelMapperService.map(device.get(), RoadDto.class);
    		return dto;
    	} else {
    		return null;
    	}
    	
    }
    
    //상세
    public UserEdgeDto getUserEdgeInfo(Long id) {
    	Optional<User> user = userEdgeRepository.findById(id);
    	if (user.isPresent()) {
    		UserEdgeDto dto = modelMapperService.map(user.get(), UserEdgeDto.class);    		
    		return dto;
    	} else {
    		return null;
    	}
    }
    
    //수정
    public boolean editRoadRecog(Long userId, RoadRecogDto roadRecogDto) {
    	Optional<RoadRecog> c = roadRecogRepository.findById((long) roadRecogDto.getRecogSeq());
    	if (c.isPresent()) {
    		RoadRecog editRoadRecog = modelMapperService.map(c.get(), RoadRecog.class);
    		editRoadRecog.setCarNumber(roadRecogDto.getCarNumber());
    		editRoadRecog.setDirection(roadRecogDto.getDirection());
    		editRoadRecog.setModId(userId);
    		editRoadRecog.setModName(userRepository.findAllById(userId).get().getName());
    		editRoadRecog.setEditTime(LocalDateTime.now());
    		
    		recentlyRoadRepository.deleteRecentlyRoad(roadRecogDto.getEdgeId());
    		
    		return Optional.ofNullable(roadRecogRepository.save(editRoadRecog)).isPresent();
    		
    	} else {
    		return false;
    	}
    	
    }
    
    public int callRoadRecogBackupProc() {
    	return roadRecogRepository.callRoadRecogBackupProc();
    }
    
    public List<String> roadRecogBackupDateList(String date) {
    	return roadRecogRepository.roadRecogBackupDateList(date);
    }
    
    public Long getBackupRoadRecogCount(EntityManager entityManager, LocalDateParser date) {
    	return roadRecogRepository.getBackupRoadRecogCount(entityManager, date);
    }
    
    public List<RoadRecog> getBackupRoadRecogList(EntityManager entityManager, LocalDateParser date, Pageable pageable) {
    	return roadRecogRepository.getBackupRoadRecogList(entityManager, date, pageable);
    }
    
    public Long deleteRoadRecog(EntityManager entityManager, LocalDateParser date) {
		
		return roadRecogRepository.deleteRoadRecog(entityManager, date);
	}
    
}