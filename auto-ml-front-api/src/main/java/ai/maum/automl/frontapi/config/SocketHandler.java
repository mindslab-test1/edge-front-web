package ai.maum.automl.frontapi.config;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import ai.maum.automl.frontapi.commons.security.JwtTokenProvider;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.repository.DeviceViewRepository;
import ai.maum.automl.frontapi.service.UserSecurityDetailService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 200)
public class SocketHandler implements ChannelInterceptor {
	
	@Autowired
	private DeviceViewRepository deviceViewRepository;
	
	@Autowired
	private JwtTokenProvider tokenProvider;
	
	@Autowired
	private UserSecurityDetailService userDetailSvc;
	
	@Autowired
	private AuthenticationManager authManager;
	
	@Override
	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		
		StompHeaderAccessor acc = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
		
		if (StompCommand.CONNECT.equals(acc.getCommand())) {
			String jwt = acc.getFirstNativeHeader("Authorization");
			if (StringUtils.hasText(jwt) && jwt.startsWith("Bearer ")) {
                jwt = jwt.substring(7);
                Long userId = tokenProvider.getUserNoFromJWT(jwt);
                
                UserPrincipal userDetails = (UserPrincipal) userDetailSvc.loadUserById(userId);
                
                if (userDetails != null) {
                	Authentication auth =  new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                	SecurityContextHolder.getContext().setAuthentication(auth);
                	acc.setUser(auth);
                }
            }
			
		}
		
		return message;
	}

	@Override
	public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
		
		StompHeaderAccessor acc = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
		
		if (StompCommand.DISCONNECT.equals(acc.getCommand())) {
			UserPrincipal user = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			deviceViewRepository.removeDeviceView(user.getUserNo());
		}
		
	}
	
	
	
	
}
