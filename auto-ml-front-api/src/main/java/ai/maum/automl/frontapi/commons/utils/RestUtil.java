package ai.maum.automl.frontapi.commons.utils;

import ai.maum.automl.frontapi.commons.enums.Api;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.Header;
import org.aspectj.weaver.patterns.HasThisTypePatternTriedToSneakInSomeGenericOrParameterizedTypePatternMatchingStuffAnywhereVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ai.maum.automl.frontapi.commons.utils
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-30  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-30
 */
@Component("restUtil")
public class RestUtil {

    Logger logger = LoggerFactory.getLogger(RestUtil.class);
    
    RestTemplate restTemplate;

    public RestUtil(){
        this.restTemplate = new RestTemplate();
    }

    /**
     * GET (Map)
     * @param api
     * @param param
     * @return
     */
    public Map<String, Object> get(Api api, Map<String, Object> param){
        ResponseEntity responseEntity = null;
        Map<String, Object> rtnMap = new HashMap<>();
        logger.info("API CALL : {}", api.name());
        if(param != null){
            logger.info("--- API CALL PARAM START ---", "");
            for(String key : param.keySet()){
                logger.info("API CALL PARAM KEY : {} / VALUE : {}", key, param.get(key));
            }
            logger.info("--- API CALL PARAM END ---", "");
        }
        try{
            SSLUtil.turnOffSslChecking();
            responseEntity = param != null ? restTemplate.getForEntity(api.getPath()+"?"+urlEncodeUTF8(param), Map.class) :  restTemplate.getForEntity(api.getPath(), Map.class);
            logger.info("statusCode : {}", responseEntity.getStatusCode());
            logger.info("body : {}", responseEntity.getBody());
            return (Map<String, Object>) responseEntity.getBody();
        }catch (HttpServerErrorException e){
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            } catch (IOException ioException) {
                ioException.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Server ERROR, Api = {}", api.name());
        }catch (HttpClientErrorException e){
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            }  catch (Exception exception){
                exception.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Client ERROR, Api = {}", api.name());
        }catch (Exception exception){
            exception.printStackTrace();
            rtnMap.put("error", exception.getMessage());
        }
        return rtnMap;
    }

    /**
     * GET (List)
     * @param api
     * @param param
     * @return
     */
    public List get(Api api, Map<String, Object> param, Object ignore){
        ResponseEntity responseEntity = null;
        Map<String, Object> rtnMap = new HashMap<>();
        logger.info("API CALL : {}", api.name());
        if(param != null){
            logger.info("--- API CALL PARAM START ---", "");
            for(String key : param.keySet()){
                logger.info("API CALL PARAM KEY : {} / VALUE : {}", key, param.get(key));
            }
            logger.info("--- API CALL PARAM END ---", "");
        }
        try{
            SSLUtil.turnOffSslChecking();
            responseEntity = param != null ? restTemplate.getForEntity(api.getPath()+"?"+urlEncodeUTF8(param), List.class) :  restTemplate.getForEntity(api.getPath(), List.class);
            logger.info("statusCode : {}", responseEntity.getStatusCode());
            logger.info("body : {}", responseEntity.getBody());
            return (List) responseEntity.getBody();
        }catch (HttpServerErrorException e){
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            } catch (IOException ioException) {
                ioException.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Server ERROR, Api = {}", api.name());
        }catch (HttpClientErrorException e){
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            }  catch (Exception exception){
                exception.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Client ERROR, Api = {}", api.name());
        }catch (Exception exception){
            exception.printStackTrace();
            rtnMap.put("error", exception.getMessage());
        }
        List list = new ArrayList();
        list.add(rtnMap);
        return list;
    }

    /**
     * GET (String)
     * @param url
     * @param param
     * @return
     */
    public Map<String, Object> get(String url, Map<String, Object> param){
        ResponseEntity responseEntity = null;
        Map<String, Object> rtnMap = new HashMap<>();
        logger.info("API CALL SERVICE_URL : {}", url);
        try{
            MultiValueMap<String, Object> multiValueMap = new LinkedMultiValueMap();
            multiValueMap.setAll(param);
            SSLUtil.turnOffSslChecking();
            responseEntity = param != null ? restTemplate.getForEntity(url, Map.class, multiValueMap) :  restTemplate.getForEntity(url, Map.class);
            logger.info("statusCode : {}", responseEntity.getStatusCode());
            logger.info("body : {}", responseEntity.getBody());
            return (Map<String, Object>) responseEntity.getBody();
        }catch (HttpServerErrorException e){
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            } catch (IOException ioException) {
                ioException.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Server ERROR, Api_URL = {}", url);
        }catch (HttpClientErrorException e){
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            }  catch (Exception exception){
                exception.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Client ERROR, Api_URL = {}", url);
        }catch (Exception exception){
            exception.printStackTrace();
            rtnMap.put("error", exception.getMessage());
        }
        return rtnMap;
    }

    /**
     * POST
     * @param api
     * @param param
     * @return
     */
    public Map<String, Object> post(Api api, Map<String, Object> param){
        ResponseEntity responseEntity = null;
        Map<String, Object> rtnMap = new HashMap<>();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(api.getHeader());
        
        HttpEntity<?> request = null;
        
        logger.info("API CALL : {}", api.name());
        logger.info("--- API CALL PARAM START ---", "");
        for(String key : param.keySet()){
            logger.info("API CALL PARAM KEY : {} / VALUE : {}", key, param.get(key));
        }
        logger.info("--- API CALL PARAM END ---", "");
        try{
            SSLUtil.turnOffSslChecking();
            
            if (api.getHeader().equals(MediaType.APPLICATION_FORM_URLENCODED)) {
            	MultiValueMap<String, Object> multiValueMap = new LinkedMultiValueMap();
            	multiValueMap.setAll(param);
            	request = new HttpEntity(multiValueMap, headers);
            } else {
            	request = new HttpEntity(param, headers);
            }
            
            responseEntity = restTemplate.postForEntity(api.getPath(), request, Map.class);
            logger.info("statusCode : {}", responseEntity.getStatusCode());
            logger.info("body : {}", responseEntity.getBody());
            
            return (Map<String, Object>) responseEntity.getBody();
        }catch (HttpServerErrorException e){
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            } catch (IOException ioException) {
                ioException.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Server ERROR, Api = {}", api.name());
        }catch (HttpClientErrorException e){
            e.printStackTrace();
            try {
                rtnMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
            }  catch (Exception exception){
                exception.printStackTrace();
                rtnMap.put("error", e.getResponseBodyAsString());
            }
            logger.error("restTemplate Client ERROR, Api = {}", api.name());
        }catch (Exception exception){
            exception.printStackTrace();
            rtnMap.put("error", exception.getMessage());
        }
        return rtnMap;
    }

    /**
     * query string encode UTF-8
     * @param s
     * @return
     */
    static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    /**
     * query string encode UTF-8
     * @param map
     * @return
     */
    static String urlEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        if(map != null){
            for (Map.Entry<?,?> entry : map.entrySet()) {
                if(entry.getKey() != null && entry.getValue() != null){
                    if (sb.length() > 0) {
                        sb.append("&");
                    }
                    sb.append(String.format("%s=%s",
                            entry.getKey().toString(),
                            entry.getValue().toString()
                    ));
                }
            }
        }
        return sb.toString();
    }

}
