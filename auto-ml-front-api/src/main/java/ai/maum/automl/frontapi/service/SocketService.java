package ai.maum.automl.frontapi.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

import ai.maum.automl.frontapi.repository.RecentlyRoadRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class SocketService {
	
	private final ChannelTopic channelTopic;
	
	private final RedisTemplate redisTemplate;
	
	private final RecentlyRoadRepository recentlyRoadRepository;
	
	
	public void sendRecentlyRoad(String edgeId) {
		
		List<Map<String, Object>> recentlyRoadList = recentlyRoadRepository.findByEdgeId(edgeId);
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("edgeId", edgeId);
		result.put("data", recentlyRoadList);
		
		redisTemplate.convertAndSend(channelTopic.getTopic(), result);
	}
	
}
