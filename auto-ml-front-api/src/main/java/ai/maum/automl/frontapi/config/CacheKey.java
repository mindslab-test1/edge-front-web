package ai.maum.automl.frontapi.config;

public class CacheKey {
    public static final int DEFAULT_EXPIRE_SEC = 3600; // 1 hours
    public static final int LOGIN_EXPIRE_SEC = 300; // 5 minutes

    public static final String loadUserById = "loadUserById";
    public static final String getEngineList = "getEngineList";
    public static final String getLang = "getLang";
    public static final String getSampleRateList = "getSampleRateList";
    public static final String getLearningDataList = "getLeaningDataList";
    public static final String getNoiseDataList = "getNoiseDataList";
    public static final String getCommonModelList = "getCommonModelList";
    public static final String getMyModelList = "getMyModelList";
    public static final String getModelFileList = "getModelFileList";
    public static final String getMenuByUserId = "getMenuByUserId";
    public static final String getDuplicationUser = "getDuplicationUser";
}