package ai.maum.automl.frontapi.model.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SideMenuDto implements Serializable {
	
	private Long id;
	
	private String menuCode;
	
	private String menuName;
	
	private String url;
	
	private String icoName;
	
	private int depth;
	
	private int topMenuId;
	
	public SideMenuDto (SideMenuExInterface menu) {
		this.id = menu.getId();
		this.menuCode = menu.getMenuCode();
		this.menuName = menu.getMenuName();
		this.url = menu.getUrl();
		this.icoName = menu.getIcoName();
		this.depth = menu.getDepth();
		this.topMenuId = menu.getTopMenuId();
	}
	
}
