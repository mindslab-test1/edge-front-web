package ai.maum.automl.frontapi.controller;

import ai.maum.automl.frontapi.commons.exception.StackTrace;
import ai.maum.automl.frontapi.commons.payload.ApiResponse;
import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.model.dto.JwtTokenDto;
import ai.maum.automl.frontapi.model.entity.CustomerQuoteRequest;
import ai.maum.automl.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.automl.frontapi.service.CustomerService;
import io.swagger.annotations.ApiOperation;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ApiTest
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@RestController
public class ApiDummyController {

    
    /**
     * 견적 문의 코드 조회
     * @param customerServiceRequest
     * @return
     */
    @ApiOperation(value = "견적문의 코드 조회")
    @PostMapping("/edge_bridge/avr/get_edge_status")
    public Map<String, Object> getEdgeList(HttpServletRequest request, HttpServletResponse response) {
    	
    	
    	JSONObject map = new JSONObject();
    	
    	JSONObject deviceStatusList = new JSONObject();
    	JSONObject deviceMap = new JSONObject();
    	
    	JSONObject edgeStatusMap = new JSONObject();
    	edgeStatusMap.put("edge_ai_status", "Working");
    	
    	JSONObject devUsage = new JSONObject();
    	devUsage.put("cpu", String.format("%.2f", (double) (Math.random()*100)));
    	devUsage.put("mem", String.format("%.2f", (double) (Math.random()*100)));
    	devUsage.put("gpu", String.format("%.2f", (double) (Math.random()*100)));
    	devUsage.put("gpu_mem", String.format("%.2f", (double) (Math.random()*100)));
    	
    	edgeStatusMap.put("edge_dev_usage", devUsage);
    	
    	deviceMap.put("A10100027", edgeStatusMap);
    	
    	map.put("edges_device_status", deviceMap);
    	
    	JSONObject deviceMap2 = new JSONObject();
    	
    	JSONObject edgeStatusMap2 = new JSONObject();
    	edgeStatusMap2.put("edge_ai_status", "Working");
    	
    	JSONObject devUsage2 = new JSONObject();
    	devUsage2.put("cpu", String.format("%.2f", (double) (Math.random()*100)));
    	devUsage2.put("mem", String.format("%.2f", (double) (Math.random()*100)));
    	devUsage2.put("gpu", String.format("%.2f", (double) (Math.random()*100)));
    	devUsage2.put("gpu_mem", String.format("%.2f", (double) (Math.random()*100)));
    	
    	edgeStatusMap2.put("edge_dev_usage", devUsage2);
    	
    	deviceMap.put("A10100028", edgeStatusMap2);
    	
    	
    	JSONArray edgeModuleList = new JSONArray();
    	
    	String[] arr = {"FPS", "IE last", "ImgExtract", "PlateDetect", "IA last", "ImgAVR", "PlateRecog"};
    	
    	JSONObject moduleInfoMap = new JSONObject();
    	String[] dataArr = {"26.3", "2020-12-04 11:19:01", "0.037", "0.013", "2020-12-04 11:18:58", "0.058", "0.054"};
    	
    	moduleInfoMap.put("A10100027", dataArr);
    	moduleInfoMap.put("A10100028", dataArr);
    	
    	edgeModuleList.add(arr);
    	edgeModuleList.add(moduleInfoMap);
    	
    	map.put("edges_module_status", edgeModuleList);
    	
    	
    	map.put("status", "Ok");
    	return map;
    	
    }

}
