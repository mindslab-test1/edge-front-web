package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

/**
 * 엣지 모니터링 상태 이력
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"device"})
@Table(
        indexes = {
                @Index(name = "IX_status", columnList = "regDt"),
        }
)
public class Status {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int statusSeq; // status_seq(PK)	엣지 상태 순번(PK)	순번	BIGINT	NOT NULL

    @Column(insertable = false, updatable = false)
    private Long deviceId; // edge_id(FK)	엣지 아이디(FK)	엣지아이디	VARCHAR(255)	NOT NULL

    @Lob
    @Column(nullable = false)
    private String response; // response	상태 응답	JSON	LONGTEXT	NOT NULL

    @Column
    private LocalDateTime regDt; // last_mod_dt	최종 수정일시	일시	DATETIME	NULL

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="deviceId")
    private Device device; // 엣지 장비 정보
    
    @PrePersist
    public void prePersist() {
    	this.regDt = Optional.ofNullable(this.regDt).orElse(LocalDateTime.now());
    }

}
