package ai.maum.automl.frontapi.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.automl.frontapi.commons.enums.CodeGroup;
import ai.maum.automl.frontapi.commons.payload.ApiResponse;
import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.model.dto.CodeDto;
import ai.maum.automl.frontapi.service.CommonsService;
import io.swagger.annotations.ApiOperation;

/**
 * CustomerController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-18  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@RestController
public class CommonsController {

    final CommonsService commonsService;

    public CommonsController(CommonsService commonsService) {
        this.commonsService = commonsService;
    }
    
    /**
     * 메뉴 전체 조회
     * @param customerServiceRequest
     * @return
     */
    @ApiOperation(value = "메뉴 전체 조회")
    @GetMapping("/api/menu/list")
    public ResponseEntity<?> getEdgeList(HttpServletRequest request, HttpServletResponse response) {
    	return ResponseEntity.ok(new ApiResponse(true, "조회성공", commonsService.getAllMenuList()));
    	
    }
    
    /**
     * 헤더 조회
     * @param customerServiceRequest
     * @return
     */
    @ApiOperation(value = "메뉴 전체 조회")
    @GetMapping("/api/menu/header")
    public ResponseEntity<?> getHeaderList(@CurrentUser UserPrincipal user) {
    	return new ResponseEntity<>(commonsService.getHeaderMenuList(user), HttpStatus.OK);
    	
    }
    
    /**
     * 사이드 메뉴 조회
     * @param customerServiceRequest
     * @return1
     */
    @ApiOperation(value = "사이드 메뉴 조회")
    @GetMapping("/api/menu/side")
    public ResponseEntity<?> getSideMenuList(@CurrentUser UserPrincipal user) {
    	return new ResponseEntity<>(commonsService.getSideMenuList(user.getUserNo()), HttpStatus.OK);
    	
    }
    
    /**
     * 코드 조회
     * @param customerServiceRequest
     * @return1
     */
    @ApiOperation(value = "코드 조회")
    @PostMapping("/api/code/list")
    public ResponseEntity<?> getCodeGroup(@RequestBody List<String> codes) {
    	return new ResponseEntity<>(commonsService.getCodeLists(codes), HttpStatus.OK);
    }
    
}
