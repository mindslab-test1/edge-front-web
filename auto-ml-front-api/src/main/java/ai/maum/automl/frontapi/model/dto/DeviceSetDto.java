package ai.maum.automl.frontapi.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

import ai.maum.automl.frontapi.commons.enums.Code;
import ai.maum.automl.frontapi.commons.enums.CodeGroup;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DeviceSetDto implements Serializable {
	
	private String clientCompanyName;
	private Long userId;
	//private String name;
	private String userName;
	private String userTel;
	private String edgeId;
	private String addr;
	private String addrDetail;
	private String cameraDir;
	private int cameraNo;
	
	private Long edgeSetSeq;
	private Long deviceId;
	private String setYn;
	private Long setUserId;
	private String setUserName;
	private LocalDateTime setDt;
	//private Long redId;
	//private String regDt;
	//private Long modId;
	//private String modDt;
	
	private String edgeAiStatus;
	
	public void setApiData(Object obj) {
		
		if (obj != null) {
			Map<String, Object> deviceStatus = (Map<String, Object>) obj;
			String status = String.valueOf(deviceStatus.get("edge_ai_status"));
			this.edgeAiStatus = CodeGroup.EDGE_AI_STATUS.getCodeList().stream()
					.filter(c -> c.getCode().toLowerCase().equals(status.toLowerCase())).findAny().orElse(Code.NOTCONNECT).getCode();
			
		} else {
			this.edgeAiStatus = Code.NOTCONNECT.getCode();
		}
	}
} 
