package ai.maum.automl.frontapi.model.entity;

import ai.maum.automl.frontapi.model.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@Table(name = "ROLE")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long authId;
    
    @Column(nullable = false, length = 24)
    private String authName;

//    @OneToMany(mappedBy = "role")
//    @JsonIgnore
//    private List<User> users;
}
