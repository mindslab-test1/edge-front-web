package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.commons.utils.LocalDateParser;
import ai.maum.automl.frontapi.model.dto.RoadRecogDto;
import ai.maum.automl.frontapi.model.dto.RoadRecogSearchDto;
import ai.maum.automl.frontapi.model.entity.QClientCompany;
import ai.maum.automl.frontapi.model.entity.QDevice;
import ai.maum.automl.frontapi.model.entity.QRoad;
import ai.maum.automl.frontapi.model.entity.QRoadRecog;
import ai.maum.automl.frontapi.model.entity.QUser;
import ai.maum.automl.frontapi.model.entity.RoadRecog;
import ai.maum.automl.frontapi.model.entity.RoleName;

import org.hibernate.jpa.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Repository;

import com.querydsl.core.QueryFactory;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
//public interface UserRepository extends JpaRepository<User, Integer> {
public interface RoadRecogRepository extends JpaRepository<RoadRecog, Long> {
    
	QUser user = QUser.user;
	QUser user1 = new QUser("user1");
	QClientCompany clientCompany = QClientCompany.clientCompany;
	QDevice device = QDevice.device;
	QRoad road = QRoad.road;
	QRoadRecog roadRecog = QRoadRecog.roadRecog;
	
	//Optional<User> findById(Long id);
	
	List<RoadRecog> findByEdgeIdAndLaneAndDirection(String edgeId, int lane, String vehSide);
	
	List<RoadRecog> findByEdgeIdAndLane(String edgeId, int lane);
	
	RoadRecog findByEdgeIdAndLaneAndDirectionOrderByPassTime(String edgeId, int lane, String vehSide);
	
	//조회
    default Page<RoadRecogDto> getRoadRecogList(UserPrincipal userPrincipal, EntityManager entityManager, RoadRecogSearchDto searchDto, Pageable pageable, String compId) {
    	
    	
    	QueryResults<Long> seqList = new JPAQuery<Long>(entityManager)
    			.select(roadRecog.recogSeq).from(roadRecog)
    			.where(
						likeSearchType(searchDto.getSearchType(), searchDto.getSearchTxt())
						,isAuth(userPrincipal, compId)
						,isLane(searchDto.getLane())
						,isDirection(searchDto.getDirection())
						,isCarType(searchDto.getCarType())
						,isConfLevel(searchDto.getConfLevelFrom(), searchDto.getConfLevelTo())
						,isDate(searchDto.getSearchDateType(), searchDto.getStartDate(), searchDto.getEndDate())
					  )
				.orderBy(roadRecog.recogSeq.desc(), roadRecog.passTime.desc())
		    	.offset(pageable.getOffset())
				.limit(pageable.getPageSize())
				.fetchResults();
    	
    	QueryResults<RoadRecogDto> roadRecogQuery = new JPAQuery<RoadRecogDto>(entityManager)
				.select(Projections.fields(
												RoadRecogDto.class,
												roadRecog.edgeId,
												roadRecog.addr,
												roadRecog.addrDetail,
												roadRecog.cameraDir,
												roadRecog.cameraNo,
												roadRecog.recogSeq,
												roadRecog.edgeNodeSeq,
												roadRecog.passTime,
												roadRecog.editTime,
												roadRecog.carNumber,
												roadRecog.carType,
												roadRecog.carImgPath,
												roadRecog.plateImgPath,
												roadRecog.direction,
												roadRecog.confLevel,
												roadRecog.lane,
												roadRecog.speed,
												roadRecog.clientCompanyName,
												roadRecog.modName
												//ExpressionUtils.as(JPAExpressions.select(user.name).from(user).where(user.id.eq(roadRecog.modId)), "modName")
												//user.name.as("modName")
											))
				.from(roadRecog)
				.where(roadRecog.recogSeq.in(seqList.getResults()))
				.orderBy(roadRecog.recogSeq.desc())
				.fetchResults();
    	
    	return new PageImpl<>(roadRecogQuery.getResults(), pageable, seqList.getTotal());
		    	
	}
    
    
    default Page<RoadRecogDto> getRoadRecogList1(UserPrincipal userPrincipal, EntityManager entityManager, RoadRecogSearchDto searchDto, Pageable pageable, String compId) {
    	
    	
    	List<Long> roadRecogQuery1 = (List<Long>) new JPAQueryFactory(entityManager)
    			.select(roadRecog.recogSeq)
    			.from(roadRecog)
    			.where(
						likeSearchType(searchDto.getSearchType(), searchDto.getSearchTxt())
						,isAuth(userPrincipal, compId)
						,isLane(searchDto.getLane())
						,isDirection(searchDto.getDirection())
						,isCarType(searchDto.getCarType())
						,isConfLevel(searchDto.getConfLevelFrom(), searchDto.getConfLevelTo())
						,isDate(searchDto.getSearchDateType(), searchDto.getStartDate(), searchDto.getEndDate())
					  );
    										
    	
    	
    	QueryResults<RoadRecogDto> roadRecogQuery = new JPAQuery<RoadRecogDto>(entityManager)
				.select(Projections.fields(
												RoadRecogDto.class,
												roadRecog.edgeId,
												roadRecog.addr,
												roadRecog.addrDetail,
												roadRecog.cameraDir,
												roadRecog.cameraNo,
												roadRecog.recogSeq,
												roadRecog.edgeNodeSeq,
												roadRecog.passTime,
												roadRecog.editTime,
												roadRecog.carNumber,
												roadRecog.carType,
												roadRecog.carImgPath,
												roadRecog.plateImgPath,
												roadRecog.direction,
												roadRecog.confLevel,
												roadRecog.lane,
												roadRecog.speed,
												roadRecog.clientCompanyName,
												roadRecog.modName
												//ExpressionUtils.as(JPAExpressions.select(user.name).from(user).where(user.id.eq(roadRecog.modId)), "modName")
												//user.name.as("modName")
											))
				.from(roadRecog)
				//.leftJoin(device).on(roadRecog.deviceId.eq(device.deviceId),device.useYn.eq("Y"))
				//.leftJoin(road).on(roadRecog.edgeNodeSeq.eq(road.edgeNodeSeq))
				//.leftJoin(device).on(road.deviceId.eq(device.deviceId),device.useYn.eq("Y"))
				//.leftJoin(user).on(roadRecog.modId.eq(user.id), user.useYn.eq("Y"))
				//.leftJoin(user1).on(device.userId.eq(user1.id))
				.where(
						likeSearchType(searchDto.getSearchType(), searchDto.getSearchTxt())
						,isAuth(userPrincipal, compId)
						,isLane(searchDto.getLane())
						,isDirection(searchDto.getDirection())
						,isCarType(searchDto.getCarType())
						,isConfLevel(searchDto.getConfLevelFrom(), searchDto.getConfLevelTo())
						,isDate(searchDto.getSearchDateType(), searchDto.getStartDate(), searchDto.getEndDate())
					  )
				//.orderBy(roadRecog.recogSeq.desc(), roadRecog.edgeNodeSeq.desc(), roadRecog.modId.desc())
		    	.offset(pageable.getOffset())
				.limit(pageable.getPageSize())
				.fetchResults();
    	
    	System.out.println(roadRecogQuery);
    	
    	return new PageImpl<>(roadRecogQuery.getResults(), pageable, roadRecogQuery.getTotal());
		    	
	}
    
    
    //검색어
    private BooleanExpression likeSearchType(String searchType, String searchTxt) {
		if ("".equals(searchTxt) || searchTxt  == null) return null;

		BooleanExpression exp = null;
		
		switch (searchType) {
			case "all":
				exp = roadRecog.clientCompanyName.contains(searchTxt)
						.or(roadRecog.modName.contains(searchTxt))
						.or(roadRecog.edgeId.contains(searchTxt))
						.or(roadRecog.addr.contains(searchTxt))
						.or(roadRecog.cameraDir.contains(searchTxt))
						.or(roadRecog.addrDetail.contains(searchTxt))
						.or(roadRecog.carNumber.contains(searchTxt));
				break;
			case "companyName":
				exp = roadRecog.clientCompanyName.contains(searchTxt);
				break;
			case "name":
				exp = roadRecog.modName.contains(searchTxt);
				break;
			case "edgeId":
				exp = roadRecog.edgeId.contains(searchTxt);
				break;
			case "addr":
				exp = roadRecog.addr.contains(searchTxt).or(roadRecog.addrDetail.contains(searchTxt)).or(roadRecog.cameraDir.contains(searchTxt));
				break;
			case "carNumber":
				exp = roadRecog.carNumber.contains(searchTxt);
				break;
			
		}
		
		return exp;
	}
	
    private BooleanExpression isAuth(UserPrincipal userPrincipal, String compId) {
    	/*ArrayList getAuth = (ArrayList) userPrincipal.getAuthorities();
    	String userRole = getAuth.get(0).toString();
    	if("ROLE_USER".equals(userRole)) {
    		return device.userId.eq(userPrincipal.getUserNo());
    	}else {
    		return null;
    	}*/
    	BooleanExpression exp = null;
    	
    	if (userPrincipal.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_COMP_ADMIN.name()))) {
			if(compId == null) {
				return exp;
			}
			//exp = user1.clientCompanyId.eq(compId);
			exp = roadRecog.clientCompanyId.eq(compId);
		}
    	
    	if(exp == null && userPrincipal.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(RoleName.ROLE_USER.name()))) {
    		//exp = device.userId.eq(userPrincipal.getUserNo());
    		exp = roadRecog.userId.eq(userPrincipal.getUserNo());
    	}
    	
    	return exp;
    }
    
    //차량 방향
	private BooleanExpression isDirection(String direction) {
		if ("".equals(direction) || direction == null) return null;
		return roadRecog.direction.eq(direction);
	}
	
	//인식 차로
	private BooleanExpression isLane(int lane) {
		if (lane == 0) return null;
		return roadRecog.lane.eq(lane);
	}
	
	//차량 종류
	private BooleanExpression isCarType(String carType) {
		if ("".equals(carType) || carType == null) return null;
		return roadRecog.carType.eq(carType);
	}
	
	//인식 신뢰도
	private BooleanExpression isConfLevel(float sLevelFrom, float sLevelto) {
		
		boolean isSlevel = true;
		boolean isElevel = true;
		BooleanExpression exp = null;
		
		if(sLevelFrom == 0) isSlevel = false;
		if(sLevelto == 0) isElevel = false;
		
		if (!isSlevel && !isElevel) return null;
		
		if (isSlevel && isElevel) {
			//exp = roadRecog.confLevel.between(sLevelFrom, sLevelto);
			exp = roadRecog.confLevel.between(sLevelFrom, sLevelto);
		} else if (isSlevel && !isElevel) {
			exp = roadRecog.confLevel.gt(sLevelFrom);
		} else if (!isSlevel && isElevel) {
			exp = roadRecog.confLevel.lt(sLevelto);
		}
		
		return exp;
		
	}
	
	//시간
	private BooleanExpression isDate(String dateType, String sDate, String eDate) {
		
		boolean isSdate = true;
		boolean isEdate = true;
		BooleanExpression exp = null;
		
		if (("".equals(sDate) || sDate == null)) isSdate = false;
		if (("".equals(eDate) || eDate == null)) isEdate = false;
		
		if (!isSdate && !isEdate) return null;
		
		String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_FORMAT);
		
		LocalDateTime sDateTime;
		LocalDateTime eDateTime;
		if(("".equals(sDate) || sDate == null)){
			sDateTime = null;
		}else {
			sDateTime = LocalDateTime.parse(sDate, formatter);
		}
		if(("".equals(eDate) || eDate == null)){
			eDateTime = null;
		}else {
			eDateTime = LocalDateTime.parse(eDate, formatter);
		}
		
		//sDateTime = LocalDateTime.parse(sDate, formatter);
		//eDateTime = LocalDateTime.parse(eDate, formatter);
		
		switch (dateType) {
			case "":
				if (isSdate && isEdate) {
					exp = roadRecog.passTime.between(sDateTime, eDateTime).or(roadRecog.editTime.between(sDateTime, eDateTime));
				} else if (isSdate && !isEdate) {
					exp = roadRecog.passTime.gt(sDateTime).or(roadRecog.editTime.gt(sDateTime));
				} else if (!isSdate && isEdate) {
					exp = roadRecog.passTime.lt(eDateTime).or(roadRecog.editTime.lt(eDateTime));
				}
				break;
			case "passTime":
				if (isSdate && isEdate) {
					exp = roadRecog.passTime.between(sDateTime, eDateTime);
				} else if (isSdate && !isEdate) {
					exp = roadRecog.passTime.gt(sDateTime);
				} else if (!isSdate && isEdate) {
					exp = roadRecog.passTime.lt(eDateTime);
				}
				break;
			case "editTime":
				if (isSdate && isEdate) {
					exp = roadRecog.editTime.between(sDateTime, eDateTime);
				} else if (isSdate && !isEdate) {
					exp = roadRecog.editTime.gt(sDateTime);
				} else if (!isSdate && isEdate) {
					exp = roadRecog.editTime.lt(eDateTime);
				}
				break;
		}
		
		return exp;
		
	}
	
	@Procedure(value = "proc_road_recog_backup")
	int callRoadRecogBackupProc();
	
	@Query(value = "SELECT DISTINCT DATE_FORMAT(pass_time, '%Y%m%d') "
			 	 +"	  FROM road_recog "
			 	 + " WHERE date(pass_time) < STR_TO_DATE(:date, '%Y-%m-%d')", nativeQuery = true)
	List<String> roadRecogBackupDateList(@Param("date") String date);
	
	default Long getBackupRoadRecogCount(EntityManager entityManager, LocalDateParser date) {
		return new JPAQuery<Long>(entityManager).select(roadRecog)
										.from(roadRecog)
										.where(
												roadRecog.passTime.between(date.getStartDate(), date.getEndDate())
												)
										.fetchCount();
	}
	
	default Long deleteRoadRecog(EntityManager entityManager, LocalDateParser date) {
		
		return new JPADeleteClause(entityManager, roadRecog)
					.where(roadRecog.passTime.between(date.getStartDate(), date.getEndDate()))
					.execute();
	}
	
	default List<RoadRecog> getBackupRoadRecogList(EntityManager entityManager, LocalDateParser date, Pageable pageable) {
    	
    	
		List<Long> seqList = new JPAQuery<Long>(entityManager)
    			.select(roadRecog.recogSeq).from(roadRecog)
    			.where(
    					roadRecog.passTime.between(date.getStartDate(), date.getEndDate())
					  )
				.orderBy(roadRecog.recogSeq.desc(), roadRecog.passTime.desc())
		    	.offset(pageable.getOffset())
				.limit(pageable.getPageSize())
				.fetch();
    	
    	List<RoadRecog> list = new JPAQuery<RoadRecog>(entityManager)
				.select(roadRecog)
				.from(roadRecog)
				.where(roadRecog.recogSeq.in(seqList))
				.orderBy(roadRecog.recogSeq.desc())
				.fetch();
    	
    	return list;
		    	
	}
}
