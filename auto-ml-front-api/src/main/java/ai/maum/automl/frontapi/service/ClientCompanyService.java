package ai.maum.automl.frontapi.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ai.maum.automl.frontapi.repository.ClientCompanyRepository;
import ai.maum.automl.frontapi.commons.enums.Api;
import ai.maum.automl.frontapi.commons.enums.Email;
import ai.maum.automl.frontapi.commons.enums.HomeInfo;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.commons.utils.RestUtil;
import ai.maum.automl.frontapi.repository.DeviceRepository;
import ai.maum.automl.frontapi.model.dto.ClientCompanyDto;
import ai.maum.automl.frontapi.model.dto.ClientCompanySearchDto;
import ai.maum.automl.frontapi.model.dto.HomeInfoDto;
import ai.maum.automl.frontapi.model.dto.PageRequest;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.CustomerQuoteRequest;
import ai.maum.automl.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.automl.frontapi.model.entity.Device;
import ai.maum.automl.frontapi.repository.QuoteRequestRepository;
import ai.maum.automl.frontapi.repository.ServiceRequestRepository;

/**
 * CustomerService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@Service
public class ClientCompanyService {

	Logger logger = LoggerFactory.getLogger(ClientCompanyService.class);
    final ClientCompanyRepository clientCompanyRepository;
    final EntityManager entityManager;
    final ModelMapperService modelMapperService;

    public ClientCompanyService(ClientCompanyRepository clientCompanyRepository, EntityManager entityManager, ModelMapperService modelMapperService) {
        this.clientCompanyRepository = clientCompanyRepository;
        this.entityManager = entityManager;
        this.modelMapperService = modelMapperService;
    }
    
    public Page<ClientCompanyDto.list> getClientCompanyList(UserPrincipal user , ClientCompanySearchDto clientCompanySearchDto) {
    	
    	
    	PageRequest page = new PageRequest();
    	
    	page.setPage(clientCompanySearchDto.getPage());
    	page.setSize(clientCompanySearchDto.getSize());
    	page.setDirection(Sort.Direction.DESC);
    	
		return clientCompanyRepository.getClientCompany(entityManager, user, clientCompanySearchDto, page.of("regDt"));
    }

    
    public int addClientCompany(Long userId, ClientCompanyDto company) {
    	
    	// 이름 중복 체크
    	if (clientCompanyRepository.findByClientCompanyNameIgnoreCase(company.getClientCompanyName()).isPresent()) {
    		return -1;
    	}
    	
    	ClientCompany c = modelMapperService.map(company, ClientCompany.class);
    	c.setRegId(userId);
    	
    	if (Optional.ofNullable(clientCompanyRepository.save(c)).isPresent()) {
    		return 1;
    	} else {
    		return -2;
    	}
    	
    }
    
    public ClientCompanyDto getClientCompanyInfo(String id) {
    	
    	Optional<ClientCompany> company = clientCompanyRepository.findById(id);
    	
    	if (company.isPresent()) {
    		ClientCompanyDto dto = modelMapperService.map(company.get(), ClientCompanyDto.class);    		
    		return dto;
    	} else {
    		return null;
    	}
    	
    }
    
    public int editClientCompany(Long userId, ClientCompanyDto company) {
    	
    	if (!this.checkClientCompanyName(company)) {
    		return -1;
    	}
    	
    	Optional<ClientCompany> c = clientCompanyRepository.findById(company.getClientCompanyId());
    	
    	if (c.isPresent()) {
    		ClientCompany editCompany = c.get();
    		editCompany.setClientCompanyName(company.getClientCompanyName());
    		editCompany.setUseYn(company.getUseYn());
    		editCompany.setModDt(LocalDateTime.now());
    		editCompany.setModId(userId);
    		
    		if (Optional.ofNullable(clientCompanyRepository.save(editCompany)).isPresent()) {
    			return 1;
    		} else {
    			return -2;
    		}
    		
    	} else {
    		return -2;
    	}
    	
    }
    
    public boolean checkClientCompanyName(ClientCompanyDto company) {
    	
    	if (company.getClientCompanyId() == null) {
    		company.setClientCompanyId("");
    	}
    	
    	if (clientCompanyRepository.findByClientCompanyNameIgnoreCaseAndClientCompanyIdNot(company.getClientCompanyName(), company.getClientCompanyId()).isPresent()) {
    		return false;
    	} else {
    		return true;
    	}
    	
    	
    }
    
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public boolean delClientCompany(Long userId, String[] arr) {
    	
    	for (String clientCompanyId : arr) {
    		ClientCompany company  = clientCompanyRepository.findByClientCompanyId(clientCompanyId);
    		
    		if (Optional.ofNullable(company).isPresent()) {
    			
    			clientCompanyRepository.delete(company);
    			
    		} else {
    			throw new RuntimeException("존재하지 않는 회사 삭제 시도");
    		}
    	}
    	
    	return true;
    	
    }
    
}
