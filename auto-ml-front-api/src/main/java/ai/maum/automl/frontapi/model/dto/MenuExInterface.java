package ai.maum.automl.frontapi.model.dto;

public interface MenuExInterface {
	
	Long getId();
	
	String getMenuCode();
	
	String getMenuName();
	
	String getUrl();
}
