package ai.maum.automl.frontapi.model.dto;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserSearchDto {
	
	private String useYn;
	
	private String searchType;
	
	private String searchTxt;
	
	private String startDate;
	
	private String endDate;
	
	private Long auth;
	
	private int page;
	
	private int size;
	
	private String clientCompanyId;
	
}
