package ai.maum.automl.frontapi.commons.enums;

import java.util.ArrayList;
import java.util.List;

import ai.maum.automl.frontapi.model.dto.HomeInfoDto;
import lombok.Getter;

@Getter
public enum HomeInfo {
	
	EDGE_ROAD("A0101","엣지 컴퓨팅(Edge Computing)을 이용한 차량 통행 도로 AI surveillance입니다. <br /> 차량 종류 및 번호를 인식하고 차선 위반, 속도위반 등의 정보를 검출할 수 있습니다.","ico_edgeCloud01.svg","img_product_edgeRoad.png"),
	EDGE_STREET("A0102","엣지 컴퓨팅(Edge Computing)을 이용한 거리 통행 행인 AI surveillance입니다. <br>이상행동(폭행, 납치, 실신 등) 탐지 및 통보가 가능하며 특정인 수색 및 추적할 수 있습니다.","ico_edgeCloud02.svg","img_product_edgeStreet.png"),
	EDGE_FACTORY("A0103","엣지 컴퓨팅(Edge Computing)을 이용한 고가의 GPU를 대체하는 실용적인 솔루션 제공합니다. <br>AI 기반의 실시간 분석 및 제어를 할 수 있습니다.","ico_edgeCloud03.svg","img_product_edgeFactory.png"),
	EDGE_FARM("A0104","엣지 컴퓨팅(Edge Computing)을 이용한 스마트 농장 AI surveillance입니다. <br>스마트 농장 실시간 제어, 농장 침입자 및 도둑 탐지가 가능하며 작황 및 생육 상황 분석을 제공합니다.","ico_edgeCloud04.svg","img_product_edgeFarm.png"),
	EDGE_HOME("A0105","개인정보 비식별화가 필요한 사적/공적 공간 AI surveillance입니다.<br>이상행동 검출 및 통보, 행동 패턴 분석을 제공합니다.","ico_edgeCloud05.svg","img_product_edgeHome.png"),
	EDGE_DRONE("A0106","강력한 AI 계산 기능을 갖춘 소형의 저전력 드론 솔루션 제공 합니다.<br>물체 및 인물 인식 및 추적 기능, 드론 자율 비행을 위한 메타 데이터 추출을 할 수 있습니다.","ico_edgeCloud06.svg","img_product_edgeDrone.png");

	
	private String code;
	private String description;
	private String icoName;
	private String imgName;
	private String title;
	
	public static final List<HomeInfoDto> list = new ArrayList<HomeInfoDto>();
	static { 
		for (HomeInfo os : HomeInfo.values()) { 
			HomeInfoDto homeDto = new HomeInfoDto(os.code, os.title, os.description, os.icoName, os.imgName);
			list.add(homeDto);
		} 
	}

	
	HomeInfo(String code, String description, String icoName, String imgName) {
		this.code = code;
		this.description = description;
		this.icoName = icoName;
		this.imgName = imgName;
		this.title = Code.getCodeData(this.code).getCodeName();
	}
	
	public static List<HomeInfoDto> getDtoList() {
		return list;
	}
	
}
