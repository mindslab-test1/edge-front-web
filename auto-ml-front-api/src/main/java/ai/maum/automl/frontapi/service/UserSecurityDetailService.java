package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.config.CacheKey;
import ai.maum.automl.frontapi.model.dto.SideMenuDto;
import ai.maum.automl.frontapi.model.dto.SideMenuExInterface;
import ai.maum.automl.frontapi.model.entity.User;
import ai.maum.automl.frontapi.repository.MenuRepository;
import ai.maum.automl.frontapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserSecurityDetailService implements UserDetailsService {

    private final UserRepository userRepo;
    
    private final MenuRepository menuRepo;
    
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {
        User user = userRepo.findByEmailAndUseYn(email, "Y");
        
        if (user == null) {
        	return UserPrincipal.create((long)0, email, "");
        } else {
        	return UserPrincipal.create(user);        	
        }
        
    }

    @Cacheable(value = CacheKey.loadUserById, key = "#id", unless = "#result == null")
    public UserDetails loadUserById(Long id)
    {
        Optional<User> optUser = userRepo.findByIdAndUseYn(id, "Y");

        if(optUser.isEmpty()) {
            return null;
        }

        User user = optUser.get();
        return UserPrincipal.create(user);
    }
    
    @Cacheable(value = CacheKey.getMenuByUserId, key = "#id", unless = "#result == null")
    public List<SideMenuDto> getMenuByUserId(Long id) {
    	
    	List<SideMenuExInterface> list = menuRepo.findByRoleAuthId(id);
    	
    	if (list != null && list.size() > 0) {
    		return list.stream().map(menu -> new SideMenuDto(menu)).collect(Collectors.toList());    		
    	} else {
    		return null;
    	}
    }
}
