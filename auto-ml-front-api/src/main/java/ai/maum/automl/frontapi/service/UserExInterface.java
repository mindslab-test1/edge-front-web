package ai.maum.automl.frontapi.service;

import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-11-24
 */
public interface UserExInterface {
    Long getId();
    String getClientCompanyId();
    String getEmail();
    String getName();
    String getPassword();
    String getTel();
    Long getRegId();
    LocalDateTime getRegDt();
    Long getModId();
    LocalDateTime getModDt();
}
