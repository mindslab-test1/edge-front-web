package ai.maum.automl.frontapi.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import ai.maum.automl.frontapi.commons.enums.Api;
import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.commons.utils.RestUtil;
import ai.maum.automl.frontapi.model.dto.RoadDto;
import ai.maum.automl.frontapi.model.dto.RoadRecogDto;
import ai.maum.automl.frontapi.model.dto.RoadRecogSearchDto;
import ai.maum.automl.frontapi.model.dto.UserEdgeDto;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.Device;
import ai.maum.automl.frontapi.model.entity.DeviceHistory;
import ai.maum.automl.frontapi.model.entity.DeviceSet;
import ai.maum.automl.frontapi.model.entity.Road;
import ai.maum.automl.frontapi.model.entity.Role;
import ai.maum.automl.frontapi.model.entity.User;
import ai.maum.automl.frontapi.model.dto.DeviceDto;
import ai.maum.automl.frontapi.model.dto.DeviceSearchDto;
import ai.maum.automl.frontapi.model.dto.DeviceSetDto;
import ai.maum.automl.frontapi.model.dto.DeviceSetSearchDto;
import ai.maum.automl.frontapi.model.dto.PageRequest;
import ai.maum.automl.frontapi.repository.DeviceHistoryRepository;
import ai.maum.automl.frontapi.repository.DeviceRepository;
import ai.maum.automl.frontapi.repository.DeviceSetRepository;
import ai.maum.automl.frontapi.repository.RoadRepository;

/**
 * DeviceService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일        / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주   / 최초 생성
 * </pre>
 * @since 2020-01-22
 */
@Service
public class DeviceSetService {
	
	Logger logger = LoggerFactory.getLogger(DeviceSetService.class);
	final RoadRepository roadRepository;
    final DeviceSetRepository deviceSetRepository;
    final EntityManager entityManager;
    final RestUtil restUtil;
    final ModelMapperService modelMapperService;
    final HistoryService historyService;
    
    public DeviceSetService(RoadRepository roadRepository,HistoryService historyService, ModelMapperService modelMapperService, RestUtil restUtil, EntityManager entityManager, DeviceSetRepository deviceSetRepository) {
    	this.roadRepository = roadRepository;
    	this.historyService = historyService;
    	this.modelMapperService = modelMapperService;
    	this.restUtil = restUtil;
    	this.entityManager = entityManager;
        this.deviceSetRepository = deviceSetRepository;
    }
    
    public boolean addDeviceHistory(Device device, Long userId) {
    	
    	DeviceSet deviceSet = new DeviceSet();
    	
    	deviceSet.setDevice(device);
    	deviceSet.setSetYn("N");
    	deviceSet.setRegId(userId);
    	
    	if (Optional.ofNullable(deviceSetRepository.save(deviceSet)).isPresent()) {
    		return true;
    	} else {
    		return false;
    	}
    	
    }
    
    @Transactional
    public Page<DeviceSetDto> getDeviceSetList(DeviceSetSearchDto deviceSetSearchDto) {
    	PageRequest page = new PageRequest();
    	
    	page.setPage(deviceSetSearchDto.getPage());
    	page.setSize(deviceSetSearchDto.getSize());
    	page.setDirection(Sort.Direction.DESC);
    	
    	Page<DeviceSetDto> data = deviceSetRepository.getDeviceSetList(entityManager, deviceSetSearchDto, page.of("regDt"));
		
		if (data.hasContent()) {
    		List<String> list = data.stream().map(dto -> dto.getEdgeId()).collect(Collectors.toList());
    		
    		Map<String, Object> params = new HashMap<String, Object>();
    		params.put("edge_id_list", list);
    		
    		Map<String, Object> result = restUtil.post(Api.GET_EDGE_STATUS, params);
    		
    		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
    		
    		if (result != null && "ok".equals(apiResult.toLowerCase())) {
    			Map<String, Object> statusMap = (Map<String, Object>) result.get("edges_device_status");
    			
    			data.getContent().stream().forEach(device -> {
    				device.setApiData(statusMap.get(device.getEdgeId()));
    			});
    			
    			if (deviceSetSearchDto.getEdgeAiStatus() != null && !"".equals(deviceSetSearchDto.getEdgeAiStatus())) {
    				List<DeviceSetDto> filterList = data.getContent().stream().filter(d -> deviceSetSearchDto.getEdgeAiStatus().equals(d.getEdgeAiStatus())).collect(Collectors.toList());
    				
    		        int fromIndex = deviceSetSearchDto.getSize() * (deviceSetSearchDto.getPage() - 1);
    		        int toIndex = deviceSetSearchDto.getSize() * deviceSetSearchDto.getPage();
    		        if (toIndex > filterList.size()) {
    		            toIndex = filterList.size();
    		        }
    		        if (fromIndex > toIndex) {
    		            fromIndex = toIndex;
    		        }
    		        
    		        return new PageImpl<DeviceSetDto>(filterList.subList(fromIndex, toIndex), page.of("regDt"), filterList.size()); 
    			}
    		}
    		return data;
    	} else {
    		return data;
    	}
    }
    
    //수정
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public Map<String, Object> editDeviceSet(Long userId, DeviceSetDto deviceSetDto) {
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	
    	Optional<DeviceSet> c = deviceSetRepository.findById(deviceSetDto.getEdgeSetSeq());
    	if(c.isPresent()) {
    		DeviceSet editDeviceSet = modelMapperService.map(c.get(), DeviceSet.class);
    		editDeviceSet.setSetYn(deviceSetDto.getSetYn());
    		
    		if("Y".equals(deviceSetDto.getSetYn())) {
    			editDeviceSet.setSetUserId(userId);
        		editDeviceSet.setSetDt(LocalDateTime.now());
    		}else {
    			editDeviceSet.setSetUserId(null);
        		editDeviceSet.setSetDt(null);
    		}
    		
    		editDeviceSet.setModId(userId);
    		editDeviceSet.setModDt(LocalDateTime.now());
    		
    		if(Optional.ofNullable(deviceSetRepository.save(editDeviceSet)).isPresent()) {
    			if("Y".equals(deviceSetDto.getSetYn())) {
    				Map<String, Object> param = new HashMap<String, Object>();
            		param.put("edge_id", deviceSetDto.getEdgeId());
            		
            		Map<String, Object> result = restUtil.post(Api.EDGE_REGISTER, param);
            		
            		if (result != null) {
            			historyService.addDeviceHistory(deviceSetDto.getDeviceId(), Api.EDGE_REGISTER, param, result);
                		
                		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
                		
                		if ("ok".equals(apiResult.toLowerCase())) {
                			resultMap.put("result", "success");
                			this.firstInitialPolicy(userId, deviceSetDto.getDeviceId());
                    	} else {
                    		errDeviceSet(userId, editDeviceSet);
                    		resultMap.put("result", "fail");
                    		resultMap.put("msg", "엣지 등록 API 등록 실패");
                    		//throw new RuntimeException("api 등록 실패");
                    	}
            		} else {
            			errDeviceSet(userId, editDeviceSet);
            			resultMap.put("result", "fail");
            			resultMap.put("msg", "엣지 등록 API 결과값이 없습니다.");
            			//throw new RuntimeException("api 결과값 없음");
            		}
    			}else {
    				resultMap.put("result", "success");
    			}
    		}else {
    			resultMap.put("result", "fail");
    			resultMap.put("msg", "설치이력 정보 수정 실패");
    		}
    	}else {
    		resultMap.put("result", "fail");
			resultMap.put("msg", "설치이력 정보 수정 조회 실패");
    		return resultMap;
    	}
    	return resultMap;
    }
    
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public boolean errDeviceSet(Long userId, DeviceSet deviceSet) {
    	
    	deviceSet.setSetYn("N");
    	deviceSet.setSetUserId(null);
    	deviceSet.setSetDt(null);
    	deviceSet.setModId(userId);
    	deviceSet.setModDt(LocalDateTime.now());
    	
    	return Optional.ofNullable(deviceSetRepository.save(deviceSet)).isPresent();
    }
    
    public Map<String, Object> firstInitialPolicy(Long userId, Long deviceId) {
    	//api 전송
    	Map<String, Object> params = new HashMap<String, Object>();
    	Map<String, Object> dataMap = new HashMap<String, Object>();
    	List<Map<String, Object>> recogCriteriaList = new ArrayList<Map<String,Object>>();
    	
    	Road road = roadRepository.findByDeviceId(deviceId);
    	Device device = road.getDevice();
    	
    	String desc = device.getAddr() + " " + device.getAddrDetail() + " " + road.getCameraDir() + " 방향";
    	String setRoiData = "0.193,0.195,0.815,0.195,0.082,0.865,0.926,0.865";    	
    	List<Float> roiList = Arrays.asList((setRoiData.split(","))).stream().map(roi -> Float.parseFloat(roi)).collect(Collectors.toList());
    	
    	params.put("edge_id", road.getDevice().getEdgeId());
    	
    	dataMap.put("desc", desc);
    	dataMap.put("connect", "rtsp");
    	dataMap.put("url", "empty");
    	dataMap.put("camera_domain", "normal");
    	dataMap.put("roi", roiList);
    	dataMap.put("cam_dir", "entry");
    	dataMap.put("one_way", "False");
    	dataMap.put("veh_side", "front");
    	dataMap.put("start_lane", 1);
    	dataMap.put("lanes", 2);
    	dataMap.put("updated", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
    	
    	recogCriteriaList.add(dataMap);
    	
    	params.put("recog_criteria", dataMap);
    	
    	Map<String, Object> result = restUtil.post(Api.EDGE_UPDATE_POLICY, params);
    	
		historyService.addPolicyHistory(road.getEdgeNodeSeq(), (float) 1.0, LocalDateTime.now(), Api.EDGE_UPDATE_POLICY, params, result);
    	
		Optional<Road> opRoad = roadRepository.findByEdgeNodeSeq(road.getEdgeNodeSeq());
    	
    	if (opRoad.isPresent()) {
    		Road updateRoad = opRoad.get();
    		updateRoad.setRoi(setRoiData);
    		updateRoad.setStartLane((int) dataMap.get("start_lane"));
    		updateRoad.setRangeLane((int)dataMap.get("lanes"));
    		updateRoad.setCamDir((String)dataMap.get("cam_dir"));
            updateRoad.setVehSide((String)dataMap.get("veh_side"));
            updateRoad.setConnect((String)dataMap.get("connect"));
            updateRoad.setUrl((String)dataMap.get("url"));
            updateRoad.setOneWay((String)dataMap.get("one_way"));
            updateRoad.setCameraDomain((String)dataMap.get("camera_domain"));
            updateRoad.setUpdated(LocalDateTime.now());
            updateRoad.setLastModId(userId);
            updateRoad.setLastModDt(LocalDateTime.now());
            
            if(!Optional.ofNullable(roadRepository.save(updateRoad)).isPresent()) {
    			throw new RuntimeException("road 수정 중 오류 발생");
    		}
            
    	}		
		
		return result;
    }
    
    public boolean checkDeviceAiStatus(DeviceSetDto dto) {
    	
    	if (dto.getDeviceId() == null || dto.getDeviceId() == 0) return false;
    	if (dto.getEdgeId() == null || "".equals(dto.getEdgeId())) return false;
    	if("Y".equals(dto.getSetYn())) {
    		Map<String, Object> params = new HashMap<String, Object>();
    		String[] edgeList = {dto.getEdgeId()};
    		params.put("edge_id_list", edgeList);
    		Map<String, Object> statusResult = restUtil.post(Api.GET_EDGE_STATUS, params);
        	
    		String apiStatusResult = statusResult.get("status") == null ? "" : (String) statusResult.get("status");
    		if ("ok".equals(apiStatusResult.toLowerCase())) {
    			Map<String, Object> statusMap = (Map<String, Object>) statusResult.get("edges_device_status");
    			List<Object> moduleStatus = (List<Object>) statusResult.get("edges_module_status");
    			if(statusMap.size() == 0 || moduleStatus.size() == 0) {
    				return false;
    			}
    		}
    	}
		
		return true;
    }
}
