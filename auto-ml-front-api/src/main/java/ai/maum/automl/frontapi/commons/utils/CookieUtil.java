package ai.maum.automl.frontapi.commons.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CookieUtil {
	
	public static final String REFRESH_TOKEN = "refreshToken";
	
	@Value("${spring.profiles.active}")
    private String active;
 	
	
	public Cookie setCookie(String key, String value, int maxAge) {
		
		Cookie cookie = new Cookie(key, value);
		
		
		if (REFRESH_TOKEN.equals(key)) {
			cookie.setHttpOnly(true);
			
			if (!"default".equals(active) && !"local".equals(active)) {
				cookie.setSecure(true);
			}
		}
		
		cookie.setPath("/");
		cookie.setMaxAge(maxAge);
		
		return cookie;
	}
	
	public Cookie getCookie(HttpServletRequest req, String key) {
		Cookie[] cookies = req.getCookies();
		
		if (cookies == null) return null;
		
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals(key)) {
				return cookie;
			}
		}
		
		return null;
	}
	
}
