package ai.maum.automl.frontapi.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import ai.maum.automl.frontapi.commons.enums.Code;
import ai.maum.automl.frontapi.commons.enums.CodeGroup;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DeviceDto implements Serializable {
	
	private Long deviceId;
	
	private String edgeId;
	
	private String clientCompanyId;
	
	private String addr;
	
	private String addrDetail;
	
	private String clientCompanyName;
	
	private String name;
	
	private Long userId;
	
	private Long id;
	
	@Setter
	@Getter
	public static class list extends DeviceDto {
		
		
		private String cameraDir;
		
		private String edgeAiStatus;
		
		private double cpu;
		
		private double mem;
		
		private double gpu;
		
		private double gpuMem;
		
		private List<String> explanList;
		
		private List<String> edgeInfoList;
		
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
		private LocalDateTime setDt;
		
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
	    private LocalDateTime regDt;
		
		
		public void setApiData(Object obj) {
					
			if (obj != null) {
				Map<String, Object> deviceStatus = (Map<String, Object>) obj;
				//this.edgeAiStatus = EdgeAiStatus.getCode(String.valueOf(deviceStatus.get("edge_ai_status")));
				String status = String.valueOf(deviceStatus.get("edge_ai_status"));
				this.edgeAiStatus = CodeGroup.EDGE_AI_STATUS.getCodeList().stream()
									.filter(c -> c.getCode().toLowerCase().equals(status.toLowerCase())).findAny().orElse(Code.NOTCONNECT).getCode();
				
				if (deviceStatus.get("edge_dev_usage") != null) {
					
					Map<String, String> devMap = (Map<String, String>) deviceStatus.get("edge_dev_usage");
					this.cpu = (devMap.get("cpu") == null || "".equals(devMap.get("cpu")))?  0 : Double.parseDouble(String.valueOf(devMap.get("cpu"))); 
					this.mem = (devMap.get("mem") == null || "".equals(devMap.get("mem")))?  0 : Double.parseDouble(String.valueOf(devMap.get("mem"))); 
					this.gpu = (devMap.get("gpu") == null || "".equals(devMap.get("gpu")))?  0 : Double.parseDouble(String.valueOf(devMap.get("gpu")));
					this.gpuMem = (devMap.get("gpu_mem") == null || "".equals(devMap.get("gpu_mem"))) ?  0 :  Double.parseDouble(String.format("%.2f", Double.parseDouble(String.valueOf(devMap.get("gpu_mem"))))); 
				}
				
			} else {
				this.edgeAiStatus = Code.NOTCONNECT.getCode();
			}
			
			
		}
	}
	
	@Setter
	@Getter
	public static class info extends DeviceDto {
		
		private List<String> explanList;
		
		private List<String> edgeInfoList;
		
	}
	
	@Setter
	@Getter
	public static class RoadList extends DeviceDto {
		
		private List<RoadDto> roadList;
		
	}
	
	@Setter
	@Getter
	public static class RoadDetail extends DeviceDto {
		
		private RoadDto.Policy policy;
	}
	
} 
