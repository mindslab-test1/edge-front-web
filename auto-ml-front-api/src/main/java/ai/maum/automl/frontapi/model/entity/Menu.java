package ai.maum.automl.frontapi.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Menu {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;				// id(PK) 메뉴 코드(PK) 아이디 VARCHAR(24) NOT NULL
	
	@Column(length = 24, nullable = false)
	private String menuCode;	 // menu(PK) 메뉴 코드(PK) 아이디 VARCHAR(24) NOT NULL
	
	@Column(length = 100, nullable = false)
	private String menuName;	// menuName 메뉴 이름 NOT NULL
	
	@Column(length = 300)
	private String url;			// 주소 url
	
	@Column(length = 200)
	private String icoName;		// 사이드에 노출될 icoName 아이콘 이름
	
	@Column(nullable = false)
	private int depth;			// depth 댑스 NOT NULL
	
	@Column(nullable = false)
	private int ord;			// ord 정렬 순서 NOT NULL
	
	@Column(length = 1, nullable = false)
	private String sideYn;		// 메뉴 노출 여부
	
	@Column(length = 1, nullable = false)
	private String useYn;		// 사용 여부
	
	@ManyToOne
	private TopMenu topMenu;
	
	@Column
	private int parentId;	// 상위 메뉴 아이디 
	
	@Column 
    private Long regId; // reg_id 등록자 아이디 VARCHAR(24) NOT NULL
	
    @Column(name="reg_dt")
	@CreationTimestamp 
	private LocalDateTime regDt; // reg_dt 등록일시 일시 DATETIME NOT NULL
    
	@Column(length = 24) 
    private Long modId; // mod_id 수정자 아이디 VARCHAR(24) NULL
    
	@Column(name="mod_dt")
    @CreationTimestamp
	private LocalDateTime modDt; // mod_dt 수정일시 일시 DATETIME NULL
}
