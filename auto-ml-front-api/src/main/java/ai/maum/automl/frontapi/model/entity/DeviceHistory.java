package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * 엣지 상태 이력
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"device"})
@Table(
        indexes = {
                @Index(name = "IX_device_history", columnList = "action"),
                @Index(name = "IX_device_history2", columnList = "deviceId,action"),
        }
)
public class DeviceHistory {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long histSeq; // hist_seq(PK)	장비 이력 순번(PK)	순번	BIGINT	NOT NULL

    @Column(insertable = false, updatable = false)
    private Long deviceId; // edge_id(FK)	엣지 아이디(FK)	엣지아이디	VARCHAR(255)	NOT NULL


    @Column(nullable = false, length = 100)
    private String action; // action	장비 액션	동작	VARCHAR(100)	NOT NULL

    @Lob
    @Column(nullable = false)
    private String request; // request	요청	JSON	LONGTEXT	NOT NULL

    @Lob
    @Column(nullable = false)
    private String response; // response	응답	JSON	LONGTEXT	NOT NULL

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="deviceId")
    private Device device; // 엣지 장비 정보

}
