package ai.maum.automl.frontapi.model.dto;

import ai.maum.automl.frontapi.commons.enums.Code;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CodeDto {
	
	public CodeDto(Code code) {
		this.code = code.getCode();
		this.codeNm = code.getCodeName();
	}
	
	private String code;
	
	private String codeNm;
	
	
	
}
