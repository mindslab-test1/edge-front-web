package ai.maum.automl.frontapi.model.dto;


import lombok.Getter;

@Getter
public class HomeInfoDto {
	
    private String code; // code	코드	코드	VARCHAR(12)	NOT NULL
    
    private String title;

    private String description; // description	설명
    
    private String icoName;		// icoName 아이콘 이름
    
    private String imgName;		// 이미지
    
    public HomeInfoDto () {
    	
    }
    
    public HomeInfoDto (String code, String title, String description, String icoName, String imgName) {
    	this.code = code;
    	this.title = title;
    	this.description = description;
    	this.icoName = icoName;
    	this.imgName = imgName;
    }
}
