package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.commons.utils.LocalDateParser;
import ai.maum.automl.frontapi.model.dto.ClientCompanyDto;
import ai.maum.automl.frontapi.model.dto.DeviceDto;
import ai.maum.automl.frontapi.model.dto.DeviceDto.RoadDetail;
import ai.maum.automl.frontapi.model.dto.DeviceDto.list;
import ai.maum.automl.frontapi.model.dto.DeviceSearchDto;
import ai.maum.automl.frontapi.model.dto.RealTimeSearchDto;
import ai.maum.automl.frontapi.model.dto.RoadDto;
import ai.maum.automl.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.automl.frontapi.model.entity.Device;
import ai.maum.automl.frontapi.model.entity.DeviceHistory;
import ai.maum.automl.frontapi.model.entity.QClientCompany;
import ai.maum.automl.frontapi.model.entity.QDevice;
import ai.maum.automl.frontapi.model.entity.QDeviceSet;
import ai.maum.automl.frontapi.model.entity.QRoad;
import ai.maum.automl.frontapi.model.entity.QUser;
import ai.maum.automl.frontapi.model.entity.Road;
import ai.maum.automl.frontapi.model.entity.RoadId;
import ai.maum.automl.frontapi.model.entity.RoleName;
import ai.maum.automl.frontapi.model.entity.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.hibernate.criterion.Projection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

/**
 * ServiceRequestRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@Repository
public interface RoadRepository extends JpaRepository<Road, Integer> {
	
	QDevice device = QDevice.device;
	QClientCompany company = QClientCompany.clientCompany;
	QRoad road = QRoad.road;
	QUser user = QUser.user;
	QDeviceSet deviceSet = QDeviceSet.deviceSet;
	
	Optional<Road> findByEdgeNodeSeq(Long id);
	
	Road findByDeviceId(long deviceId);
	
	default Page<DeviceDto.list> getEdgeRoadList(EntityManager entityManager, DeviceSearchDto dto, UserPrincipal authUser, Pageable pageable, String compId) {
		
		JPAQuery<DeviceDto.list> query = new JPAQuery<DeviceDto.list>(entityManager)
												.select(Projections.fields(DeviceDto.list.class
														,device.deviceId
														,device.edgeId
														,user.name
														,company.clientCompanyName
														,company.clientCompanyId
														,device.addr
														,device.addrDetail
														,road.cameraDir
														,deviceSet.setDt
														,device.regDt))
												.from(device)
												.leftJoin(deviceSet).on(device.deviceId.eq(deviceSet.deviceId))
												.join(road).on(device.deviceId.eq(road.deviceId))
												.leftJoin(user).on(device.userId.eq(user.id), user.useYn.eq("Y"))
												.leftJoin(company).on(user.clientCompanyId.eq(company.clientCompanyId), company.useYn.eq("Y"))
												.where(device.useYn.eq("Y")
												   ,isCompAdminUser(authUser, compId)
												   ,likeSearchType(dto.getSearchType(), dto.getSearchTxt())
												   ,isSetDate(dto.getStartDate(), dto.getEndDate()))
												.orderBy(device.regDt.desc(), device.deviceId.desc());
		
		boolean isPage = false;
		if (dto.getEdgeAiStatus() == null || "".equals(dto.getEdgeAiStatus())) {
			isPage = true;
			query = query.offset(pageable.getOffset())
						 .limit(pageable.getPageSize());			
		}
		
		QueryResults<DeviceDto.list> list = query.fetchResults();
		
		return (isPage) ? new PageImpl<>(list.getResults(), pageable, list.getTotal()) : new PageImpl<DeviceDto.list>(list.getResults());
	}
	
	private BooleanExpression likeSearchType(String searchType, String searchTxt) {
		
		if ("".equals(searchTxt) || searchTxt == null) return null;
		
		BooleanExpression exp = null;
		
		switch (searchType) {
			case "companyId":
				exp = company.clientCompanyId.contains(searchTxt);
				break;
			case "companyNm":
				exp = company.clientCompanyName.contains(searchTxt);
				break;
			case "userNm":
				exp = user.name.contains(searchTxt);
				break;
			case "edgeId":
				exp = device.edgeId.contains(searchTxt);
				break;
			case "addrNm":
				exp = device.addr.contains(searchTxt).or(device.addrDetail.contains(searchTxt));
				break;
			default :
				exp = company.clientCompanyName.contains(searchTxt)
					  .or(user.name.contains(searchTxt))
					  .or(device.edgeId.contains(searchTxt))
					  .or(device.addr.contains(searchTxt))
					  .or(device.addrDetail.contains(searchTxt));
				break;
		}
		
		return exp;
	}
	
	private BooleanExpression isSetDate(String sDate, String eDate) {
		
		boolean isSdate = true;
		boolean isEdate = true;
		BooleanExpression exp = null;
		
		if (("".equals(sDate) || sDate == null)) isSdate = false;
		if (("".equals(eDate) || eDate == null)) isEdate = false;
		
		if (!isSdate && !isEdate) return null;
		
		if (isSdate && isEdate) {
			exp = deviceSet.setDt.between(new LocalDateParser(sDate).getStartDate(), new LocalDateParser(eDate).getEndDate());
		} else if (isSdate && !isEdate) {
			exp = deviceSet.setDt.gt(new LocalDateParser(sDate).getStartDate());
		} else if (!isSdate && isEdate) {
			exp = deviceSet.setDt.lt(new LocalDateParser(eDate).getEndDate());
		}
		
		return exp;
		
	}
	
	private BooleanExpression isCompAdminUser(UserPrincipal authUser, String compId) {
		BooleanExpression exp = null;
		
		if (authUser.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_COMP_ADMIN.name()))) {
			if(compId == null) {
				return exp;
			}
			exp = user.clientCompanyId.eq(compId);
		}
		
		if (exp == null && !authUser.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_ADMIN.name()))) {
			exp = device.userId.eq(authUser.getUserNo());
		} 
		
		return exp;
	}
	
	private BooleanExpression isAdminUser(UserPrincipal authUser) {
		BooleanExpression exp = null;
		
		if (exp == null && !authUser.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_ADMIN.name()))) {
			exp = device.userId.eq(authUser.getUserNo());
		} 
		
		return exp;
	}
	
	default List<Device> getEdgeRoadInfo(EntityManager entityManager, UserPrincipal authUser, RealTimeSearchDto dto, String compId) {
		
		QueryResults<Device> result = new JPAQuery<Device>(entityManager)
												.select(device)
												.from(device)
												.join(road).on(device.deviceId.eq(road.deviceId))
												.join(deviceSet).on(road.deviceId.eq(deviceSet.deviceId), deviceSet.setYn.eq("Y"))
												.leftJoin(user).on(device.userId.eq(user.id), user.useYn.eq("Y"))
												.leftJoin(company).on(user.clientCompanyId.eq(company.clientCompanyId), company.useYn.eq("Y"))
												.where(device.useYn.eq("Y")
													   ,isCompAdminUser(authUser, compId)
													   ,(dto.getDeviceId() != null && dto.getDeviceId() != 0 ? device.deviceId.eq(dto.getDeviceId()) : null)
													   ,(dto.getClientCompanyId() != null && !"".equals(dto.getClientCompanyId())) ? company.clientCompanyId.eq(dto.getClientCompanyId()) : null
													   ,(dto.getAddr() != null && !"".equals(dto.getAddr())) ? device.addr.eq(dto.getAddr()) : null
													   ,(dto.getAddrDetail() != null && !"".equals(dto.getAddrDetail())) ? device.addrDetail.eq(dto.getAddrDetail()) : null
													   ,(dto.getCameraDir() != null && !"".equals(dto.getCameraDir())) ? road.cameraDir.eq(dto.getCameraDir()) : null
													  )
												.orderBy(device.regDt.desc(), device.deviceId.desc(), road.cameraNo.asc())
												.fetchResults();
		
		
		return result.getResults();
	}
	
	default List<String> getEdgeAddrList(EntityManager entityManager, UserPrincipal authUser, RealTimeSearchDto dto, String groupPath, String compId) {
		
		StringPath path = null;
		
		switch (groupPath) {
			case "addr" :
				path = device.addr;
				break;
			case "addrDetail" :
				path = device.addrDetail;
				break;
			case "cameraDir" :
				path = road.cameraDir;
				break;
		}
		
		QueryResults<String> query = new JPAQuery<String>(entityManager)
				.select(path)
				.from(device)
				.join(road).on(device.deviceId.eq(road.deviceId))
				.join(deviceSet).on(road.deviceId.eq(deviceSet.deviceId), deviceSet.setYn.eq("Y"))
				.leftJoin(user).on(device.userId.eq(user.id), user.useYn.eq("Y"))
				.leftJoin(company).on(user.clientCompanyId.eq(company.clientCompanyId), company.useYn.eq("Y"))
				.where(device.useYn.eq("Y")
					   ,isCompAdminUser(authUser, compId)
					   ,(dto.getClientCompanyId() != null && !"".equals(dto.getClientCompanyId())) ? company.clientCompanyId.eq(dto.getClientCompanyId()) : company.clientCompanyId.isNull().or(company.clientCompanyId.eq(""))
					   ,("addrDetail".equals(groupPath) || "cameraDir".equals(groupPath)) ? device.addr.eq(dto.getAddr()) : null
					   ,("cameraDir".equals(groupPath)) ? device.addrDetail.eq(dto.getAddrDetail()) : null
					  )
				.groupBy(path)
				.orderBy(path.asc())
				.fetchResults();
		
		return query.getResults();
		
	}
	
	private StringPath getAddrPath(RealTimeSearchDto dto) {
		StringPath path = null;
		
		if ("".equals(dto.getAddr()) || dto.getAddr() == null) {
			path = device.addr;
		} else if ("".equals(dto.getAddrDetail()) || dto.getAddrDetail() == null) {
			path = device.addrDetail;
		} else if ("".equals(dto.getCameraDir()) || dto.getCameraDir() == null) {
			path = road.cameraDir;
		}
		
		return path;
	}
	
}
