package ai.maum.automl.frontapi.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

public class UserDto {
	
    public static class Simple
    {
        private String uuid;

        private String name;
    }

    public static class Base
    {
        private String uuid;

        private String name;

        @JsonIgnore
        private String email;
    }
    
    @Getter
    public static class ReqUpdate
    {
        private Long id;

        private String name;
    }
    
    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode(callSuper = true)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class UserDtoEx extends UserDto {
    	private int id;
    	private Long rold;
    	private String email;
    	private String name;
    	private String password;
    	private String tel;
    	private String roleId;
    	private Long regId;
    	private String regDt;
    }
}
