package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 회원 - 고객사 매핑 이력
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"road"})
@Table(
        indexes = {
                @Index(name = "IX_policy_history", columnList = "updated"),
                @Index(name = "IX_policy_history2", columnList = "action"),
        }
)
public class PolicyHistory {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long histSeq; // hist_seq(PK)	정책 이력 순번(PK)	순번	BIGINT	NOT NULL

    @Column(insertable = false, updatable = false)
    private Long edgeNodeSeq; // edge_node_seq(FK)	노드 순번(FK)	순번	BIGINT	NOT NULL

    @Column(nullable = false)
    private float version; // version	정책 버전	버전	FLOAT	NOT NULL

    @Column(nullable = false)
    private String action; // action	정책 동작	동작	VARCHAR(12)	NOT NULL

    @Lob
    @Column(nullable = false)
    private String request; // request	정책 요청	JSON	LONGTEXT	NOT NULL

    @Lob
    @Column(nullable = false)
    private String response; // response	정책 응답	JSON	LONGTEXT	NOT NULL

    @Column(nullable = false)
    private LocalDateTime updated; // updated	정책 적용 일자	일시	DATETIME	NOT NULL

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="edgeNodeSeq")
    private Road road; // 엣지 로드

}
