package ai.maum.automl.frontapi.model.dto;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RoadRecogSearchDto {
	
	private String searchType;
	
	private String searchTxt;
	
	private String startDate;
	
	private String endDate;
	
	private String searchDateType;
	
	private String direction;
	
	private int lane;
	
	private String carType;
	
	private float confLevelFrom;
	
	private float confLevelTo;
	
	private int page;
	
	private int size;
	
}
