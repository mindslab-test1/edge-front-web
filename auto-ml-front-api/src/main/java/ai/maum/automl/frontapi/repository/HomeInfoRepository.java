package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.dto.HomeInfoDto;
import ai.maum.automl.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.automl.frontapi.model.entity.HomeInfo;
import ai.maum.automl.frontapi.model.entity.QCmmnCode;
import ai.maum.automl.frontapi.model.entity.QHomeInfo;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * HomeInfoRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-06  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-01-06
 */
@Repository
public interface HomeInfoRepository extends JpaRepository<HomeInfo, Integer> {
	
	QHomeInfo homeInfo = QHomeInfo.homeInfo;
	QCmmnCode cmmnCode = QCmmnCode.cmmnCode;
	
	default Optional<List<HomeInfoDto>> getEdgeInfoList(EntityManager entityManager) {
		return Optional.ofNullable(new JPAQuery<HomeInfo>(entityManager)
					.select(Projections.fields(HomeInfoDto.class, homeInfo.code, homeInfo.description, homeInfo.icoName, homeInfo.imgName, cmmnCode.codeName.as("title")))
					.from(homeInfo)
					.join(cmmnCode).on(homeInfo.code.eq(cmmnCode.code))
					.where(cmmnCode.prtCode.eq("A01"))
					.orderBy(cmmnCode.ord.asc())
					.fetch());
	}
	
}
