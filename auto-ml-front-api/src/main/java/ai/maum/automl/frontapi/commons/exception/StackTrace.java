package ai.maum.automl.frontapi.commons.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StackTrace {

    public static String getStackTrace(Throwable e)
    {
        StringWriter writer = new StringWriter(1024);
        e.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

}
