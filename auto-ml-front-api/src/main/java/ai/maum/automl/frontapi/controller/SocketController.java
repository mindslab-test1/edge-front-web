package ai.maum.automl.frontapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.model.dto.DeviceViewDto;
import ai.maum.automl.frontapi.repository.DeviceViewRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class SocketController {
	
	private final DeviceViewRepository deviceViewRepository;
	
	@MessageMapping("/{edgeId}") 
	public void SendToMessage(@DestinationVariable String edgeId, @CurrentUser UserPrincipal user) throws Exception { 
		DeviceViewDto dto = DeviceViewDto.builder().edgeId(edgeId).userId(user.getUserNo()).build();
		
		//deviceViewRepository.insertDeviceView(dto);
		
	} 
	

}
