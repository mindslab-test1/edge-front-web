package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.enums.Api;
import ai.maum.automl.frontapi.commons.enums.Email;
import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.commons.utils.RestUtil;
import ai.maum.automl.frontapi.model.entity.User;
import ai.maum.automl.frontapi.model.entity.UserCompanyHistory;
import ai.maum.automl.frontapi.repository.RoleRepository;
import ai.maum.automl.frontapi.repository.UserCompanyHistoryRepository;
import ai.maum.automl.frontapi.repository.UserRepository;
import ai.maum.automl.frontapi.model.dto.ClientCompanySearchDto;
import ai.maum.automl.frontapi.model.dto.PageRequest;
import ai.maum.automl.frontapi.model.dto.UserDto;
import ai.maum.automl.frontapi.model.dto.UserSearchDto;
import ai.maum.automl.frontapi.model.entity.ClientCompany;
import ai.maum.automl.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.automl.frontapi.model.entity.Role;
import ai.maum.automl.frontapi.model.entity.RoleName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository repo;
    private final AuthService authSvc;
    private final RoleRepository roleRepo;
    private final ModelMapperService modelMapperService;
    private final UserRepository userRepository;
    private final UserCompanyHistoryRepository userCompanyHistoryRepository;
    private final EntityManager entityManager;
    Logger logger = LoggerFactory.getLogger(UserService.class);
    
    public User signup(User user, Role userRoles)
    {
        user.setRole(userRoles);
        user.setPassword(authSvc.encode(user.getPassword()));

        add(user);

        return user;
    }

    public UserDto.Base loadProfile(Long userId)
    {
        User entity = loadByUserNo(userId);
        if(entity == null) return null;

        return modelMapperService.map(entity, UserDto.Base.class);
    }

    public UserDto.Base updateProfile(UserDto.ReqUpdate req)
    {
        User entity = loadByUserNo(req.getId());

        entity.setName(req.getName());

        return modelMapperService.map(entity, UserDto.Base.class);
    }

    @Transactional
    public User add(User user) {
        return repo.save(user);
    }

    public User loadByUserNo(Long userNo)
    {
        return repo.findById(userNo).orElse(null);
    }

//    public User loadByUuid(String userUuid)
//    {
//        return repo.findByUuid(userUuid).orElse(null);
//    }

    public boolean doesUserExist(String email)
    {
        return loadByEmail(email) != null;
    }

    public User loadByEmail(String email)
    {
        return repo.findByEmail(email);
    }

    public Role getRole(RoleName roleName) {
        return roleRepo.findByAuthName(roleName.name());
    }
    
}