package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.maum.automl.frontapi.repository.SeqGenerator;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * 고객사
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 *    수정일     / 수정자   / 수정내용
 *    ------------------------------------------
 *    2020-12-19  / 최재민    / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"devices", "user"})
@Table(
        indexes = {
        }
)
public class ClientCompany {

    @Id
    @GenericGenerator(name = "idGenerator", 
    strategy = "ai.maum.automl.frontapi.repository.SeqGenerator", 
    parameters = @org.hibernate.annotations.Parameter( 
            name = SeqGenerator.SEQ_KEY, 
            value = "client_company" 
	    )
	)
    @GeneratedValue(generator = "idGenerator")
    @Column(length = 24)
    private String clientCompanyId; // client_company_id(PK)   회사 아이디(PK)   아이디   VARCHAR(24)   NOT NULL
    
    @Column(nullable = false, length = 100)
    private String clientCompanyName; // client_company_name   회사명   이름      VARCHAR(24)   NOT NULL
    
    @Column(nullable = false, columnDefinition = "VARCHAR(1) default 'Y'")
    private String useYn; // use_yn   사용여부   여부   CHAR(1)   NOT NULL
    
    @Column(nullable = false)
    private Long regId; // reg_id   등록자   아이디   VARCHAR(24)   NOT NULL
    
    @Column(name="reg_dt", nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt; // reg_dt   등록일시   일시   DATETIME   NOT NULL
    
    @Column
    private Long modId; // mod_id   수정자   아이디   VARCHAR(24)   NULL
    
    @Column(name="mod_dt")
    @CreationTimestamp
    private LocalDateTime modDt; // mod_dt   수정일시   일시   DATETIME   NULL
    
    @OneToMany(fetch = FetchType.LAZY,  mappedBy = "clientCompany")
    private Set<Device> devices = new LinkedHashSet<>(); // 디바이스

    @OneToMany(fetch = FetchType.LAZY,  mappedBy = "clientCompany")
    private Set<User> user = new LinkedHashSet<>(); // 유저 고객사 매핑
    
    @PrePersist
    public void prePresist() {
    	this.regDt = Optional.ofNullable(this.regDt).orElse(LocalDateTime.now());
    	this.useYn = Optional.ofNullable(this.useYn).orElse("Y");
    }
    
    @PreRemove
    private void preRemove() {
        for (Device device : devices) {
            device.setClientCompany(null);
        }
        
        for (User u : user) {
        	u.setClientCompany(null);
        }
    }

}