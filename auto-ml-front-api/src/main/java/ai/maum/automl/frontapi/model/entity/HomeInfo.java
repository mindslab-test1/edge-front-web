package ai.maum.automl.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import ai.maum.automl.frontapi.commons.enums.Code;

import java.util.Date;
import java.util.Optional;

/**
 * 공통코드
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
public class HomeInfo {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
    @Column(nullable = false, length = 12)
    private String code; // code	코드	코드	VARCHAR(12)	NOT NULL
    
    @Transient
    private String codeName;

    @Column
    private String description; // description	설명
    
    @Column
    private String icoName;		// icoName 아이콘 이름
    
    @Column
    private String imgName;		// 이미지
    
    
    public String getCodeName() {
    	return Code.getCodeData(this.code).getCodeName();
    }
}
