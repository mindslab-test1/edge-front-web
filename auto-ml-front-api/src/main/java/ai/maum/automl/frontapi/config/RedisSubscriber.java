package ai.maum.automl.frontapi.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ai.maum.automl.frontapi.model.dto.ClientCompanyDto.list;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class RedisSubscriber {
	
	private final ObjectMapper objectMapper;
	
	private final SimpMessageSendingOperations messageTemplate;
	
	public void sendMessage(String message) {
		try {
			Map<String, Object> result = objectMapper.readValue(message, Map.class);
			messageTemplate.convertAndSend("/edge/" + (String) result.get("edgeId"), result.get("data"));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
