package ai.maum.automl.frontapi.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RoadDto implements Serializable {
	
	private Long edgeNodeSeq;
	
	private int cameraNo;
	
	private String cameraDir;
	
	@Setter
	@Getter
	public static class Policy extends RoadDto {
		
		private String connect;
		
		private String url;
		
		private String cameraDomain;
		
		private String roi;
		
		private String camDir;
		
		private String oneWay;
		
		private String vehSide;
		
		private int startLane;
		
		private int rangeLane;
		
	}
	
} 
