package ai.maum.automl.frontapi.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class RoleMenuManagement {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;				// id(PK) 메뉴 코드(PK) 아이디 VARCHAR(24) NOT NULL
	
	@ManyToOne
	private Role role;
	
	@ManyToOne
	private Menu menu;
	
}
