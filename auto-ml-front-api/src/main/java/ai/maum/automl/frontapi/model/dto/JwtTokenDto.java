package ai.maum.automl.frontapi.model.dto;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
public class JwtTokenDto {

    private String accessToken;
    private String tokenType = "Bearer";
    private Collection<? extends GrantedAuthority> roles;

    public JwtTokenDto(String accessToken, Collection<? extends GrantedAuthority> roles)
    {
        this.accessToken = accessToken;
        this.roles = roles;
    }
}
