const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require( 'webpack' );
const HtmlWebPackPlugin = require( 'html-webpack-plugin' );
const dotenv = require( 'dotenv' );
const path = require( 'path' );
const DashboardPlugin = require( "webpack-dashboard/plugin" );
// const {
//   getThemeVariables
// } = require( 'antd/dist/theme' );

module.exports = () => {
  // call dotenv and it will return an Object with a parsed key 
  const env = dotenv.config( {
    path: path.resolve( './.env' )
  } ).parsed

  // reduce it to a nice object, the same as before
  const envKeys = Object.keys( env ).reduce( ( prev, next ) => {
    prev[ `process.env.${ next }` ] = JSON.stringify( env[ next ] );
    return prev;
  }, {} );
 
  return {
    mode: 'development',
    entry: {
      bundle: [
        '@babel/polyfill',
        './src/index.tsx',
      ],
    },
    output: {
      path: path.resolve( 'dist/local/' ),
      publicPath: '/',
      filename: '[name].[hash].js',
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
      runtimeChunk: true,
    },
    devServer: {
      host: '127.0.0.1',
      port: 10000,
      historyApiFallback: true,
      overlay: true,
      hot: true,
      open: true,
      before: (app, server, compiler) => {
        app.get('/mock-api/lang', (req, res) => {
          res.json([
            {code: "LN_KO", codeName: "ko", codeDisp: "한국어"},
            {code: "LN_EN", codeName: "en", codeDisp: "English"},
          ])
        }),
        app.get('/mock-api/samplerate', (req, res) => {
          res.json([
            {code: "SR001", codeName: "8", codeDisp: "8k"},
            {code: "SR002", codeName: "16", codeDisp: "16k"},
          ])
        }),
        app.get('/mock-api/model', (req, res) => {
          res.json([
            {atchFileId: "file01", atchFileDisp: "file01", atchFilePath: "/file01path", atchFileName: "file01name", orgFileName: "file01orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file02", atchFileDisp: "file02", atchFilePath: "/file02path", atchFileName: "file02name", orgFileName: "file02orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file03", atchFileDisp: "file03", atchFilePath: "/file03path", atchFileName: "file03name", orgFileName: "file03orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file04", atchFileDisp: "file04", atchFilePath: "/file04path", atchFileName: "file04name", orgFileName: "file04orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file05", atchFileDisp: "file05", atchFilePath: "/file05path", atchFileName: "file05name", orgFileName: "file05orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file06", atchFileDisp: "file06", atchFilePath: "/file06path", atchFileName: "file06name", orgFileName: "file06orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file07", atchFileDisp: "file07", atchFilePath: "/file07path", atchFileName: "file07name", orgFileName: "file07orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file08", atchFileDisp: "file08", atchFilePath: "/file08path", atchFileName: "file08name", orgFileName: "file08orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file09", atchFileDisp: "file09", atchFilePath: "/file09path", atchFileName: "file09name", orgFileName: "file09orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file10", atchFileDisp: "file10", atchFilePath: "/file10path", atchFileName: "file10name", orgFileName: "file10orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file11", atchFileDisp: "file11", atchFilePath: "/file11path", atchFileName: "file11name", orgFileName: "file11orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file12", atchFileDisp: "file12", atchFilePath: "/file12path", atchFileName: "file12name", orgFileName: "file12orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file13", atchFileDisp: "file13", atchFilePath: "/file13path", atchFileName: "file13name", orgFileName: "file13orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file14", atchFileDisp: "file14", atchFilePath: "/file14path", atchFileName: "file14name", orgFileName: "file14orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file15", atchFileDisp: "file15", atchFilePath: "/file15path", atchFileName: "file15name", orgFileName: "file15orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
          ])
        }),
        app.get('/mock-api/noiseData', (req, res) => {
          res.json([
            {atchFileId: "1", atchFileDisp: "file01", atchFilePath: "/file01path", atchFileName: "file01name", orgFileName: "file01orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "2", atchFileDisp: "file02", atchFilePath: "/file02path", atchFileName: "file02name", orgFileName: "file02orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "3", atchFileDisp: "file03", atchFilePath: "/file03path", atchFileName: "file03name", orgFileName: "file03orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file04", atchFileDisp: "file04", atchFilePath: "/file04path", atchFileName: "file04name", orgFileName: "file04orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file05", atchFileDisp: "file05", atchFilePath: "/file05path", atchFileName: "file05name", orgFileName: "file05orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file06", atchFileDisp: "file06", atchFilePath: "/file06path", atchFileName: "file06name", orgFileName: "file06orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file07", atchFileDisp: "file07", atchFilePath: "/file07path", atchFileName: "file07name", orgFileName: "file07orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file08", atchFileDisp: "file08", atchFilePath: "/file08path", atchFileName: "file08name", orgFileName: "file08orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file09", atchFileDisp: "file09", atchFilePath: "/file09path", atchFileName: "file09name", orgFileName: "file09orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file10", atchFileDisp: "file10", atchFilePath: "/file10path", atchFileName: "file10name", orgFileName: "file10orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file11", atchFileDisp: "file11", atchFilePath: "/file11path", atchFileName: "file11name", orgFileName: "file11orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file12", atchFileDisp: "file12", atchFilePath: "/file12path", atchFileName: "file12name", orgFileName: "file12orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file13", atchFileDisp: "file13", atchFilePath: "/file13path", atchFileName: "file13name", orgFileName: "file13orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file14", atchFileDisp: "file14", atchFilePath: "/file14path", atchFileName: "file14name", orgFileName: "file14orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
            {atchFileId: "file15", atchFileDisp: "file15", atchFilePath: "/file15path", atchFileName: "file15name", orgFileName: "file15orgname", atchFileType: "S", atchFileSize: 11111, sampleRate: 8000, playSec: 22222, playSecAvg: 33333, totFileCount: 44444, language: "ko", useEosYn: "Y"},
          ])
        }),
        app.get('/mock-api/testModelData', (req, res) => {
          res.json([
            {
              "mdlId": "8_STT_20201111192111645_1",
              "mdlGrp": "8_STT",
              "mdlFileName": "001_model_last.bin",
              "mdlFilePath": "/AUTOML/training_result/8_STT_20201111192111645/model",
              "mdlFileSize": "2506881157",
              "regDate": "2020-11-26 21:12:21",
              "langCd": 'LN_KO',
              "smplRate": 'SR001',
              "useEosYn": "N"
            },
            {
              "mdlId": "8_STT_20201111192111645_1",
              "mdlGrp": "8_STT",
              "mdlFileName": "002_model_last.bin",
              "mdlFilePath": "/AUTOML/training_result/8_STT_20201111192111645/model",
              "mdlFileSize": "2506881157",
              "regDate": "2020-11-26 21:12:21",
              "langCd": 'LN_KO',
              "smplRate": 'SR001',
              "useEosYn": "N"
            },
            {
              "mdlId": "8_STT_20201111192111645_1",
              "mdlGrp": "8_STT",
              "mdlFileName": "003_model_last.bin",
              "mdlFilePath": "/AUTOML/training_result/8_STT_20201111192111645/model",
              "mdlFileSize": "2506881157",
              "regDate": "2020-11-26 21:12:21",
              "langCd": 'LN_KO',
              "smplRate": 'SR001',
              "useEosYn": "N"
            },
            {
              "mdlId": "8_STT_20201111192111645_2",
              "mdlGrp": "8_STT",
              "mdlFileName": "004_model_last.bin",
              "mdlFilePath": "/AUTOML/training_result/8_STT_20201111192111645/model",
              "mdlFileSize": "2506881157",
              "regDate": "2020-11-26 21:12:21",
              "langCd": 'LN_KO',
              "smplRate": 'SR001',
              "useEosYn": "N"
            },
            {
              "mdlId": "8_STT_20201111192111645_2",
              "mdlGrp": "8_STT",
              "mdlFileName": "005_model_last.bin",
              "mdlFilePath": "/AUTOML/training_result/8_STT_20201111192111645/model",
              "mdlFileSize": "2506881157",
              "regDate": "2020-11-26 21:12:21",
              "langCd": 'LN_KO',
              "smplRate": 'SR001',
              "useEosYn": "N"
            },
            {
              "mdlId": "8_STT_20201111192111645_3",
              "mdlGrp": "8_STT",
              "mdlFileName": "006_model_last.bin",
              "mdlFilePath": "/AUTOML/training_result/8_STT_20201111192111645/model",
              "mdlFileSize": "2506881157",
              "regDate": "2020-11-26 21:12:21",
              "langCd": 'LN_KO',
              "smplRate": 'SR001',
              "useEosYn": "N"
            },
          ])
        })
      }
    },
    module: {
      rules: [ {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.html$/,
          use: [ {
            loader: "html-loader",
            options: {
              minimize: true
            }
          } ]
        },
        {
          test: /\.css$/,
          loader: "style-loader!css-loader",
        },
        {
          test: /\.(woff|woff2|eot|ttf|png|jpg|jpeg|gif|ico|svg)$/,
          loader: 'url-loader',
          options: {
            name: 'assets/[name].[ext]?[hash]',
            limit: 10000, //10kb
            publicPath: '/',
          }
        },
        {
          test: /\.(sa|sc|)ss$/,
          use: [ "style-loader", "css-loader", "sass-loader" ]
        },
        {
          test: /\.tsx?$/,
          loader: "ts-loader"
        },
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.DefinePlugin( envKeys ),
      new DashboardPlugin(),
      new HtmlWebPackPlugin( {
        template: "./public/index.html",
        filename: "./index.html",
      } ),
    ],
    resolve: {
      alias: {
        'react-dom': '@hot-loader/react-dom'
      },
      modules: [
        path.join( 'src' ),
        'node_modules',
      ],
      extensions: [ '*', '.js', '.jsx', '.css', '.less', ".ts", ".tsx" ],
    },
  }
};
