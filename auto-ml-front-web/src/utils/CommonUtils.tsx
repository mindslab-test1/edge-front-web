import {API, callApi} from 'utils/ApiClient';
import loadingImg from 'assets/images/ani_api_loading.gif';
import { TrophyOutlined } from '@ant-design/icons';

export const formatPrice = (value: number) => {
  const amount = formatNumber(value);
  return amount + '원';
};

export const formatNumber = (value: number) => {
  if (value === 0) return 0;

  let amount = value.toString();
  amount = amount.replace(/\D/g, '');
  amount = amount.replace(/\./g, '');
  amount = amount.replace(/-/g, '');

  let numAmount = Number(amount);
  amount = numAmount.toFixed(0).replace(/./g, function (c, i, a) {
    return i > 0 && c !== ',' && (a.length - i) % 3 === 0 ? ',' + c : c;
  });

  return amount;
};

export const scrollTop = () => {
  window.scrollTo(0, 0);
};

export const deepCopyObject = (obj: object) => {
  return JSON.parse(JSON.stringify(obj));
};

export function isObjectEmpty(obj: object) {
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

export const defaultDisplay = (code: string) => {
  return code;
}

export const smplRateDisplay = (code: string) => {
  if (code === 'SR001') {
    return '8K';
  }
  else if (code === 'SR002') {
    return '16K';
  }
  else {
    return 'unknown(' + code + ')';
  }
}

export const langCdDisplay = (code: string) => {
  if(code === 'LN_KO') {
    return '한국어';
  }
  else if(code === 'LN_EN') {
    return 'English';
  }
  else {
    return 'unknown(' + code + ')';
  }
}

export const showLoading = () => {

  let el : any = document.getElementById('pageldg');
  if (typeof(el) == 'undefined' || el == null) {
    el = document.createElement('div');
    el.setAttribute("id", "pageldg");

    const element = `<span class="out_bg"><em><strong>&nbsp;</strong><strong>&nbsp;</strong><strong>&nbsp;</strong><b>&nbsp;</b></em></span>`;

    el.innerHTML = element;

    document.body.appendChild(el);
    document.body.insertBefore(el, document.body.firstChild);
  }
}

export const hideLoading = () => {
  const el : any = document.getElementById('pageldg');
  if (typeof(el) != 'undefined' && el != null) {
    el.classList.add('pageldg_hide');
    setTimeout(() => {
      el.remove();
    }, 500);
  }

}


export const showLoadingBar = (image? : string, msg? : string) => {
  let el : any = document.getElementById('loadingbar');
  if (typeof(el) == 'undefined' || el == null) {
    el = document.createElement('div');
    el.setAttribute("id", "loadingbar");
    el.classList.add('lyrWrap');
    el.classList.add('moment');

    const element = `<div class="lyr_bg"></div>
                        <div class="lyrBox" >
                        <div class="lyr_mid">
                            <div class="ani_icoBox">
                                <img src="${image ? image : '/assets/ani_loading.gif'}" alt="엣지 연결">
                            </div>
                            <p class="massage">${msg ? msg : '로딩 중 입니다.<br>잠시만 기다려 주세요.'}</p>
                        </div>
                    </div>`;

    el.innerHTML = element;

    document.body.appendChild(el);
    document.body.insertBefore(el, document.body.firstChild);
  }
}

export const hideLoadingBar = () => {
  const el : any = document.getElementById('loadingbar');
  if (typeof(el) != 'undefined' && el != null) {
    el.classList.add('lyr_hide');
    setTimeout(() => {
      el.remove();
    }, 300);
  }
}

export const apiShowLoading = () => {
  let el : any = document.getElementById('apiPageldg');
  if (typeof(el) == 'undefined' || el == null) {
    el = document.createElement('div');
    el.setAttribute("id", "apiPageldg");
    el.classList.add('lyrWrap');
    el.classList.add('moment');

    const element = `<div class="lyr_bg"></div>
                        <div class="lyrBox" >
                        <div class="lyr_mid">
                            <div class="ani_icoBox">
                                <img src="${loadingImg}" alt="엣지 연결">
                            </div>
                            <p class="massage">Edge 연결을 시작합니다.<br>잠시만 기다려 주세요.</p>
                        </div>
                    </div>`;

    el.innerHTML = element;

    document.body.appendChild(el);
    document.body.insertBefore(el, document.body.firstChild);
  }
}

export const apiHideLoading = () => {
  let el : any = document.getElementById('apiPageldg');
  el.classList.add('lyr_hide');
  setTimeout(() => {
    el.remove();
  }, 300);
}

export const phoneFormat = (hpNo : string) => {
  return hpNo ? hpNo.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3") : hpNo;
}

export const numberExp = (number : string) => {

  if (!number) return false;

  const exp = /^[0-9]/g;
  return exp.test(number);
}

export const emailExp = (email : string) => {

  if (!email) return false;

  const exp = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;
  return exp.test(email);
}

export const changeDate = (date : any) => {
  const moment = require('moment');
  const change_date = moment(date).format('YYYY년 MM월 DD일');
  if(date == "" || date == null){
    return date;
  }else{
    return change_date;
  }
}

export const changeDateTime = (date : any) => {
  const moment = require('moment');
  const change_date = moment(date).format('YYYY-MM-DD HH:mm:ss');
  if(date == "" || date == null){
    return date;
  }else{
    return change_date;
  }
}

export const validationCheck = (formId : string) => {
  const validArr : any = document.querySelectorAll(`#${formId} input, #${formId} select`);
  let isValid : boolean = true;

  if (validArr.length > 0) {
    for (let idx in validArr) {
      let element : any = validArr[idx];

      if (typeof(element) == 'object' && element.getAttribute('data-require') == 'false') {
        continue;
      }

      if (element.value == '') {
        element.focus();
        const {store} = require('react-notifications-component');
        store.addNotification({
          // title: 'ERROR',
          message: `[${element.title}] 값을 입력바랍니다.`,
          type: 'danger',
          insert: 'top',
          container: 'bottom-right',
          animationIn: ['animated', 'fadeIn'],
          animationOut: ['animated', 'fadeOut'],
          dismiss: {
            duration: 3000,
            showIcon: true,
            // onScreen: true,
          }
        });
        isValid = false;
        break;
      }
    }

  }
  return isValid;
}

export const getCodeList = (codes : Array<string>) => {
  return new Promise(async (resolve, reject) => {

    const res = await callApi(API.CODE_LIST, codes);

    if (res.success) {
        resolve(res.data);
    } else {
        resolve([]);
    }

  });
}

export const dateDiff = (date1, date2) => {
  if(date1 == "" || date2 == ""){
    return true;
  }
  let lDate1 = date1 instanceof Date ? date1 : new Date(date1);
  let lDate2 = date2 instanceof Date ? date2 : new Date(date2);

  let diff = lDate2.getTime() - lDate1.getTime();
  //diff = Math.ceil(diff / (1000*3600*24));

  if(diff >= 0){
    return true;
  }else{
    return false;
  }
}

export const numberCheck = (value) => {
  let num = value || 0;
  if(!isFinite(num)) return false;
  num = num.toString();

  if(num.length !== '0' && !num.includes('.')){
    num = num.replace(/^0+/,'');
    return true;
  }else{
    return false;
  }
}

export const initPopupFunc = () => {
  const el : HTMLElement | null = document.getElementById('aside');
  if (el != null) {
    el!.classList.remove('active');
  }
}
