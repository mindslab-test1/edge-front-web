export const openDaumAddrApi = (completeCallback: any) => {
  new window.daum.Postcode({
      oncomplete: (data) => {
        if (typeof(completeCallback) == 'function') {
            completeCallback(data);
        }
      }
  }).open();
};
