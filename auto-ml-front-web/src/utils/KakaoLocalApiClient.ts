import axios from 'axios';
import { appendGetParams } from 'utils/ApiClient';

const _axios = axios.create({
  baseURL: 'https://dapi.kakao.com/v2/local/search',
  headers: {
    Authorization: 'KakaoAK 02f28cc089f938b1243f87ac61ff4246',
  },
});

// curl -X GET "https://dapi.kakao.com/v2/local/search/address.json?page=1&size=10&query=%EC%84%A0%EB%A6%89%EB%A1%9C161%EA%B8%B8+21" -H "Authorization: KakaoAK {REST_API_KEY}"

interface IParams {
  page: number;
  size: number;
  query: string;
}

export const callKakaoApi = async (params: IParams) => {
  const apiUri = '/address.json';
  const res = await _axios.get(apiUri + encodeURI(appendGetParams(params)));

  return res.data;
};
