import { createBrowserHistory } from 'history';
import { ERouteUrl } from 'router/RouteLinks';

export const history = createBrowserHistory();
let _history: any;

export const setHistory = (history: any) => {
  _history = history;
};

export const getHistory = () => _history;

export const push = (to: ERouteUrl | string, state?: object | null | undefined) => {
  history.push(to, state);
};

export const replacePage = (to: ERouteUrl | string, state?: object | null | undefined) => {
  history.replace(to.toString());
  // history.replace({
  //   pathname: to,
  //   ...state,
  // });
};

export const goBack = () => {
  history.back();
};

export const activeRemove = () => {

}

// export const getIdFromPath = (pathName: string, parentUri: string) => {
//   return pathName.replace(`/${parentUri}/`, '');
// };

export const getIdFromPath = (pathName: string) => {
  const pathSplit = pathName.split('/');
  return pathSplit[pathSplit.length - 1];
};
