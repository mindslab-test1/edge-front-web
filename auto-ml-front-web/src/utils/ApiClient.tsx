import { Cookies } from 'react-cookie';
import axios, { AxiosResponse } from 'axios';
import { replacePage } from './Navigation';
import { ERouteUrl } from 'router/RouteLinks';
import { pullUserToken, logoutProcess, removeUserToken } from 'service/auth/AuthService';
import {showLoadingBar, hideLoadingBar} from 'utils/CommonUtils';

enum HTTP_METHOD {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE',
}

export enum HTTP_RES_CODE {
  OK = 200,
  CREATED = 201,
  ACCEPTED = 202,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  SERVER_ERROR = 500,
}

export interface IApiDefinition {
  url: string;
  method: HTTP_METHOD;
  successCode: HTTP_RES_CODE;
}

export interface IApiResponse {
  status: HTTP_RES_CODE;
  message?: string;
  data?: any;
  success: boolean;
  unauthorized: boolean;
}

const BASE_URL = process.env.REACT_APP_API_URL;

export const API = {
  SIGNUP: { url: '/auth/admin/signup', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.CREATED },
  LOGIN: { url: '/auth/user/login', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  LOGOUT: { url: '/auth/user/logout', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  LOGIN_CHECK: { url: '/auth/user/callback', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  AUTH_PING: { url: '/auth/user/ping', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  HEADER_MENU: { url: '/menu/header', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  SIDE_MENU: { url: '/menu/side', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  //AUTH_PING: { url: '/', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  USER_LIST: { url: '/user/admin/list', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  USER_CREATE: { url: '/user/admin/create', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  USER_INFO: { url: '/user/admin/view/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  USER_CHECK: { url: '/user/admin/check', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  USER_SEARCH: { url: '/user/admin/search', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  USER_EDIT: { url: '/user/admin/edit', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  USER_DELETE: { url: '/user/admin/delete', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  //USERS_LIST: { url: '/user/lists', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  ROAD_RECOG_RECENTLY: { url: '/road/recentlyEdgeLine/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  ROAD_RECOG_LIST: { url: '/road/recog/list', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  ROAD_RECOG_EDIT: { url: '/road/recog/edit', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  DEVICE_SET_LIST: { url: '/device/set/engineer/list', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  DEVICE_SET_EDIT: { url: '/device/set/engineer/edit', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  DEVICE_SET_CHECK : {url: '/device/set/engineer/duplicate', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},

  PROJECT_LIST: { url: '/project/', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  /*PROJECT_INFO: { url: '/project/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_DELETE: { url: '/project/_ID_', method: HTTP_METHOD.DELETE, successCode: HTTP_RES_CODE.OK },
  PROJECT_SNAPSHOT_LIST: { url: '/project/_ID_/snapshot', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_SNAPSHOT_REG: { url: '/project/_ID_/snapshot', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_SNAPSHOT_APPLY: { url: '/project/_ID_/snapshot/_SNAPSHOT_ID_', method: HTTP_METHOD.PATCH, successCode: HTTP_RES_CODE.OK },*/

  PROJECT_STT_REG: { url: '/project/stt', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_DETAIL: { url: '/project/stt/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_REG_LEARNING_DATA: { url: '/project/stt/_ID_/learningData', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_REG_TRAINING_DATA: { url: '/project/stt/_ID_/trainingData', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  // mock api
  /*MOCK_LANG: { url: '/lang', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_SAMPLERATE: { url: '/samplerate', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_MODELDATA: { url: '/model', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_NOISEDATA: { url: '/noiseData', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_TEST_MODEL_DATA: { url: '/testModelData', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },*/

  // data api
  DMNG_ENGINE_LIST: { url: '/dmng/getEngineList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_LANG_LIST: { url: '/dmng/getLang', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_LEARNING_DATA_LIST: { url: '/dmng/getLearningDataList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_NOISE_DATA_LISE: { url: '/dmng/getNoiseDataList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_SAMPLE_RATE_LIST: { url: '/dmng/getSampleRateList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_PRE_TRAINED_MODEL_COMMON_LIST: { url: '/dmng/getCommonModelList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_PRE_TRAINED_MODEL_PRIVATE_LIST: { url: '/dmng/getMyModelList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_MODELFILE_LIST: { url: '/dmng/getModelFileList/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  // Training api
  /*TRAINING_BEGIN: { url: '/training/begin', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},
  TRAINING_CANCEL: { url: '/training/cancel', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},
  TRAINING_FINISH: { url: '/training/finish', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},
  TRAINING_MONITORING: { url: '/training/monitoring/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK},*/

  // code
  CODE_LIST : { url: '/code/list', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  // home
  EDGE_LIST : { url: '/customer/edge', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  EDGE_STATUS : { url: '/customer/edge/_ID_/status', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  REQUEST_ESTIMATE : { url: '/customer/quote', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  INQUIRY_REG: { url: '/customer/service', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  //Edge Road 관리
  ROAD_LIST : { url: '/road', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DEVICE_INFO : { url: '/device/_ID_/info', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DEVICE_RESTART : { url: '/device/_ID_/restart', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DEVICE_DUPLICATE : { url: '/device/duplicate', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  ROAD_ADD : { url: '/road/admin', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  ROAD_DETAIL : { url: '/road/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  ROAD_EDIT : { url: '/road/admin/_ID_', method: HTTP_METHOD.PUT, successCode: HTTP_RES_CODE.OK },
  ROAD_DEL : { url: '/road/admin', method: HTTP_METHOD.DELETE, successCode: HTTP_RES_CODE.OK },
  ROAD_REFRESH : { url: '/road/refresh', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  //실시간 차량 관리
  ROAD_POLICY : { url: '/road/policy/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  ROAD_POLICY_EDIT : { url: '/road/policy/_ID_', method: HTTP_METHOD.PUT, successCode: HTTP_RES_CODE.OK },
  ROAD_REAL : { url: '/road/real', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  ROAD_ADDRLIST : { url: '/road/addrList', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  // clientCompany
  COMPANY_LIST : { url: '/clientCompany', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  COMPANY_DUPLICATE : { url: '/clientCompany/duplicate', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  COMPANY_ADD : { url: '/clientCompany', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  COMPANY_DETAIL : { url: '/clientCompany/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  COMPANY_EDIT : { url: '/clientCompany/_ID_', method: HTTP_METHOD.PUT, successCode: HTTP_RES_CODE.OK },
  COMPANY_DEL : { url: '/clientCompany', method: HTTP_METHOD.DELETE, successCode: HTTP_RES_CODE.OK },
};

const republishToken = async () => {

  return new Promise(async (resolve, reject) => {

    let headers = {};

    // TODO: Set Token
    const token = pullUserToken();

    if (token) {
      headers['Authorization'] = 'Bearer ' + token;
    }

    const _axios = axios.create({
      baseURL: BASE_URL,
      withCredentials : true,
      headers,
    });

    try {
      let res: AxiosResponse<any> = await _axios.get(encodeURI('/auth/user/republishToken'));
      let isRePublish : boolean = (HTTP_RES_CODE.OK === res?.status && res.data.accessToken);
      if (isRePublish) {
        const maxAge = 60 * 60 * 24 * 7;
        const cookies = new Cookies();
        cookies.set('userToken', res.data.accessToken, { path: '/', maxAge });

      }

      resolve(isRePublish);
    } catch (err) {
      let isDup : boolean = false;
      if (HTTP_RES_CODE.UNAUTHORIZED === err.response.status) {
        if (err.response.data && err.response.data.message == 'duplicateLogin') {
          isDup = true;
        }
      }
      reject(isDup);
    }


  });

};

const duplicateLoginCallback = async (isDup : boolean) => {
  if (isDup) alert("다른 기기에서 로그인 하여\n이 기기에서는 자동으로 로그아웃 되었습니다.");
  await logoutProcess().then(res => {
    removeUserToken();
  });
}

export const callApi = async (apiUrl: IApiDefinition, params?: object) => {
  let headers = {};

  // TODO: Set Token
  const token = pullUserToken();

  if (token) {
    headers['Authorization'] = 'Bearer ' + token;
  }
  
  const _axios = axios.create({
    baseURL: BASE_URL,
    withCredentials : true,
    headers,
  });

  try {
    let res: AxiosResponse<any>;
    if (HTTP_METHOD.POST === apiUrl.method) {
      res = await _axios.post(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PUT === apiUrl.method) {
      res = await _axios.put(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PATCH === apiUrl.method) {
      res = await _axios.patch(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.DELETE === apiUrl.method) {
      res = await _axios.delete(encodeURI(apiUrl.url), {data: params});
    } else {
      // Get
      const _apiUrl = apiUrl.url + appendGetParams(params);
      res = await _axios.get(encodeURI(_apiUrl));
    }

    return returnResponse({ success: true, unauthorized: false, status: res.status, data: res.data });

  } catch (err) {
    console.log('API ERROR !!!!!!!!!!!!: ' + err.response);

    let unauthorized = false;

    if (err.response) {
      // api call success, response != 2XX
      unauthorized = HTTP_RES_CODE.UNAUTHORIZED === err.response.status;

      if (unauthorized && err.response.data) {
        let response : any;
        let logout : boolean = false;
        if (err.response.data.message == 'expired') {
          await republishToken().then(async (res) => {
            if (res) {
              response = await callApi(apiUrl, params);
            } else {
              //연장 실패 시 로그아웃
              logout = true;
              await duplicateLoginCallback(false);
            }
          })
          .catch(async (err) => {
            logout = true;
            await duplicateLoginCallback(err);
          });

        } else if (err.response.data.message == 'duplicateLogin') {
          logout = true;
          await duplicateLoginCallback(true);
        }

        if (response) {
          return response;
        } else {
          return returnResponse({ success: false, unauthorized, status: err.response.status, data: {logout} });
        }

      } else if (process.env.NODE_ENV !== 'development') {
        window.location.href = '/error';
      }

    } else {

      if (err.request) {
        // api call failed, network error.
        if (process.env.NODE_ENV !== 'development') {
          window.location.href = '/network_error';
        }
      }
      else {
        // client side error.
        if (process.env.NODE_ENV !== 'development') {
          window.location.href = '/error';
        }
      }

    }

    return returnResponse({ success: false, unauthorized, status: err.response.status, data: err.response.data });

  }
};

export const callMockApi = async (apiUrl: IApiDefinition, params?: object) => {
  let headers = {};

  // TODO: Set Token
  const token = pullUserToken();
  if (token) {
    headers['Authorization'] = 'Bearer ' + token;
  }

  const _axios = axios.create({
    baseURL: '/mock-api',
    headers,
  });

  try {
    let res: AxiosResponse<any>;
    if (HTTP_METHOD.POST === apiUrl.method) {
      res = await _axios.post(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PUT === apiUrl.method) {
      res = await _axios.put(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PATCH === apiUrl.method) {
      res = await _axios.patch(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.DELETE === apiUrl.method) {
      res = await _axios.delete(encodeURI(apiUrl.url), params);
    } else {
      // Get
      const _apiUrl = apiUrl.url + appendGetParams(params);
      res = await _axios.get(encodeURI(_apiUrl));
    }

    return returnResponse({ success: true, unauthorized: false, status: res.status, data: res.data });
  } catch (err) {
    // console.log(err.response);
    let unauthorized = false;

    if (err.response) {
      unauthorized = HTTP_RES_CODE.UNAUTHORIZED === err.response.status;
    }
    // handleUnauthorized(err.response.status);
    return returnResponse({ success: false, unauthorized, status: err.response.status, data: err.response.data });
  }
};

const returnResponse = (params: IApiResponse) => {
  return params;
};

export const appendGetParams = (params) => {
  if (!params) return '';

  let paramSuffix = '';

  let loop = 0;
  for (const paramKey in params) {
    // eslint-disable-next-line no-prototype-builtins
    if (!params.hasOwnProperty(paramKey)) {
      continue;
    }

    const symbol = loop === 0 ? '?' : '&';
    paramSuffix += symbol + paramKey + '=' + params[paramKey];
    loop++;
  }

  return paramSuffix;
};

// const handleUnauthorized = (status: HTTP_RES_CODE) => {
//   if (HTTP_RES_CODE.UNAUTHORIZED !== status) return false;
//   replacePage(ERouteUrl.LOGIN);
//   // if ( navigation !== undefined ) {
//   //   logout( navigation );
//   // }
//   return true;
// };
