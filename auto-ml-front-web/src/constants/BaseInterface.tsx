import { RouteComponentProps } from 'react-router-dom';

export interface IContainerBaseProps extends RouteComponentProps {}
