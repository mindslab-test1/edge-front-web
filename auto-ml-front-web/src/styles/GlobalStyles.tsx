export enum EColor {
  // DEFAULT = 'rgba(255, 255, 255, 0.65)',
  DEFAULT = 'rgba(0, 0, 0, 0.65)',
  WHITE = '#FFF',
  GREY = '#c8c8c8',
  MAIN_COLOR = '#9789F5',
  RED = 'red',
  LINK = 'skyblue',
}


