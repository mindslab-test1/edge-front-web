import { observable, action } from 'mobx';

export default class LayoutStore {
  private root: any;
  constructor(root) {
    this.root = root;
  }

  @observable showHeader: boolean = false;
  @observable isMainPage: boolean = false;
  @observable showSidebar: boolean = false;
  @observable showContents: boolean = false;
  @observable topMenu: number = 0;

  @action toggleMainHeader = (toggle: boolean, isMainPage: boolean) => {
    this.showHeader = toggle;
    this.isMainPage = isMainPage;
  };

  @action toggleHeader = (toggle: boolean) => {
    this.showHeader = toggle;
    this.isMainPage = false;
  };

  @action toggleSidebar = (toggle: boolean) => {
    this.showSidebar = toggle;
  };
  @action toggleContents = (toggle: boolean) => {
    this.showContents = toggle;
  };

  @action showTopMenu = (selMenu: number) => {
    this.topMenu = selMenu;
  }
}
