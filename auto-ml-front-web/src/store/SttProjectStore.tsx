import {observable, action} from 'mobx';
import {deepCopyObject} from '../utils/CommonUtils';
import {API, callApi} from '../utils/ApiClient';

export default class SttProjectStore {
  private root: any;
  constructor(root) {
    this.root = root;
  }
  // check step data is changed
  @observable step1Changed : boolean = false;
  getStep1Changed = () => { return this.step1Changed; }
  setStep1Changed = (step1Changed) => { this.step1Changed = step1Changed; }

  @observable step2Changed : boolean = false;
  getStep2Changed = () => { return this.step2Changed; }
  setStep2Changed = (step2Changed) => { this.step2Changed = step2Changed; }

  // projectDto
  @observable id: number = -1;    // 프로젝트 ID
  @observable projectName: string = 'T';  // 프로젝트 이름
  @observable projectDescription: string = 'D'; // 프로젝트 설명
  @observable projectType: string = 'T';  // 프로젝트 타입: STT, TTS, ...
  @observable regDt: string = ''; // 등록일자
  @observable modDt: string = ''; // 수정일자
  @observable modelName: string = ''; // 모델명

  @action setProjectInfo(prj: any){
    this.id = prj.id;
    this.projectDescription = prj.projectDescription;
    this.projectName = prj.projectName;
    this.projectType = prj.projectType;
  }

  // projectDto.stt
  @observable completedStep: number = 1;  // 완료 스텝
  @observable language: string = '';  // 언어
  @action getLanguage = () => {
    return this.language;
  }
  @action setLanguage = (language) => {
    console.log('SttProjectStore setLanguage:' + language);
    this.language = language;
    this.step1Changed = true;
  };

  @observable sampleRate: string = '';  // 샘플링레이트
  @action getSampleRate = () => {
    return this.sampleRate;
  }
  @action setSampleRate = (sampleRate) => {
    console.log('SttProjectStore setSampleRate:' + sampleRate);
    this.sampleRate = sampleRate;
    this.step1Changed = true;
  };

  @observable eos: string = ''; // EOS 사용여부 Y/N
  @action getEos = () => {
    return this.eos;
  }

  @action setEos = (eos) => {
    console.log('SttProjectStore setEos:' + eos);
    this.eos = eos;
    this.step1Changed = true;
  };

  @observable learningDataList: any = [];  // 학습에 사용할 데이터

  @observable epochSize: number = 0;  // epoch 사이즈
  @action getEpochSize = () => {
    return this.epochSize;
  }
  @action setEpochSize = (epochSize) => {
    this.epochSize = epochSize;
    this.step2Changed = true;
  }

  @observable batchSize: number = 0;  // batch 사이즈
  @action getBatchSize = () => {
    return this.batchSize;
  }
  @action setBatchSize = (batchSize) => {
    this.batchSize = batchSize;
    this.step2Changed = true;
  }

  @observable rate: number = 0; // rate
  @action getRate(){ return this.rate; }
  @action setRate(rate){
    this.rate = rate;
    this.step2Changed = true;
  }

  @observable sampleLength: number = 0; // sample length
  @action getSampleLength(){ return this.sampleLength; }
  @action setSampleLength(sampleLength){
    this.sampleLength = sampleLength;
    this.step2Changed = true;
  }

  @observable minNoiseSample: number = 0; //
  @action getMinNoiseSample(){ return this.minNoiseSample; }
  @action setMinNoiseSample(minNoiseSample){
    this.minNoiseSample = minNoiseSample;
    this.step2Changed = true;
  }

  @observable maxNoiseSample: number = 0;
  @action getMaxNoiseSample(){ return this.maxNoiseSample; }
  @action setMaxNoiseSample(maxNoiseSample){
    this.maxNoiseSample = maxNoiseSample;
    this.step2Changed = true;
  }

  @observable minSnr: number = 0;
  @action getMinSnr(){ return this.minSnr; }
  @action setMinSnr(minSnr){
    this.minSnr = minSnr;
    this.step2Changed = true;
  }

  @observable maxSnr: number = 0;
  @action getMaxSnr(){ return this.maxSnr; }
  @action setMaxSnr(maxSnr){
    this.maxSnr = maxSnr;
    this.step2Changed = true;
  }

  @observable noiseDataList: any = [];

  @observable preTrainedModel;
  @action setPreTrainedModel = (modelName, mdlId, isCommon) => {
    let list;
    if (isCommon) {
      list = this.root.legacyDataStore.preTrainedModelCommonList;
    }
    else {
      list = this.root.legacyDataStore.preTrainedModelPrivateList;
    }

    let model = list.find(obj => (obj.mdlFileName === modelName && obj.mdlId === mdlId));
    this.preTrainedModel = model;
  }

  @observable currentStep: number = 0;

  setData = (data) => {
    this.id = data.id;
    this.projectName = data.projectName;
    this.projectDescription = data.projectDescription;
    this.projectType = data.projectType;
    this.modelName = data.modelName;
    this.regDt = data.regDt;
    this.modDt = data.modDt;

    this.completedStep = data.completedStep;
    this.currentStep = this.completedStep;

    this.language = data.language ? data.language : this.root.legacyDataStore.getLang()[0].code;
    this.sampleRate = data.sampleRate ? data.sampleRate : this.root.legacyDataStore.getSampleRate()[0].code;
    this.eos = data.eos;
    this.learningDataList = data.learningDataList;

    this.epochSize = data.epochSize;
    this.batchSize = data.batchSize;
    this.rate = data.rate;
    this.sampleLength = data.sampleLength;
    this.minNoiseSample = data.minNoiseSample;
    this.maxNoiseSample = data.maxNoiseSample;
    this.minSnr = data.minSnr;
    this.maxSnr = data.maxSnr;
    this.noiseDataList = data.noiseDataList;
    this.preTrainedModel = data.preTrainedModel;

    this.setStep(this.currentStep);
  }

  @action setStep = (no) => {
    // console.log('SttProjectStore setStep:' + no);
    this.currentStep = no;
    // TODO: learning data save
    if(this.completedStep < no) {
      this.completedStep = no;
    }
    switch (no) {
      case 0: this.onStep1Active(); break;
      case 1: this.onStep2Active(); break;
      case 2: this.onStep3Active(); break;
      case 3: this.onStep4Active(); break;
    }
  }

  @action getStep = () => {
    return this.currentStep;
  }

  @action setCompletedStep = (no) => {
    this.completedStep = no;
  }

  @action getCompletedStep = () => {
    return this.completedStep;
  }

  fetchData = async (id) => {
    const _api = deepCopyObject(API.PROJECT_STT_DETAIL);
    _api.url = _api.url.replace('_ID_', id);
    const res = await callApi(_api);
    console.log('get stt project data success:');
    console.log(res.data);
    this.setData(res.data);
  }

// ----------------------------- 데이터 선택 (step 01) ----------------------------- //
  @action
  saveLearningData = async (stepId) => {
    const _api = deepCopyObject(API.PROJECT_STT_REG_LEARNING_DATA);
    _api.url = _api.url.replace('_ID_', this.id);
    let param = {
      eos : this.eos,
      language : this.language,
      sampleRate : this.sampleRate,
      learningDataList : Object(this.learningDataList)
    }

    await callApi(_api, param).then((res)=>{
      if(res.status === 200){
        this.root.notiStore.infoGreen('학습 데이터 저장 성공');
        console.log('%c HTTP status : '+ res.status + ' : saveLearningData success');
        this.setStep(stepId+1);
        this.step1Changed = false;
      }else{
        this.root.notiStore.error('학습 데이터 저장 실패');
        console.log('%c HTTP status : ' + res.status + ' : saveLearningData fail\n', res.data);
      }
    }).catch((res)=>{
      this.root.notiStore.error('학습 데이터 저장 실패');
      console.log('%c HTTP status : ' + res.status + ' : saveLearningData fail\n', res.data);
    })


  }

  @action
  putLearningData(item: any) {
    if(typeof this.learningDataList === 'undefined') {
      this.learningDataList = [];
    }
    this.learningDataList.push(item);
    this.step1Changed = true;
  }

  @action
  takeLearningData(item: any) {
    if(typeof this.learningDataList === 'undefined') {
      this.learningDataList = [];
      return;
    }
    this.learningDataList.remove(item);
    this.step1Changed = true;
  }


  // ----------------------------- 학습 설정 (step 02) ----------------------------- //

  @action
  saveTrainSetting = async (setModelRegPopPopShow, modelRegPopShow) =>{
    const _api = deepCopyObject(API.PROJECT_STT_REG_TRAINING_DATA);
    _api.url = _api.url.replace('_ID_', this.id);
    let param = {
      batchSize : this.batchSize,
      epochSize : this.epochSize,
      rate : this.rate,
      sampleLength : this.sampleLength,
      maxNoiseSample : this.maxNoiseSample,
      minNoiseSample : this.minNoiseSample,
      maxSnr : this.maxSnr,
      minSnr : this.minSnr,
      noiseDataList : Object(this.noiseDataList),
      preTrainedModel : Object(this.preTrainedModel)
    }

    await callApi(_api, param).then((res)=>{
      if(res.status === 200){
        this.root.notiStore.infoGreen('학습 설정 저장 성공');
        console.log('%c HTTP status : '+ res.status + ' : saveTrainSetting success');
        setModelRegPopPopShow(!modelRegPopShow);
        this.step2Changed = false;
      }else{
        this.root.notiStore.error('학습 설정 저장 실패');
        console.log('%c HTTP status : ' + res.status + ' : saveTrainSetting fail\n', res.data);
      }
    }).catch((res)=>{
      this.root.notiStore.error('학습 설정 저장 실패');
      console.log('%c HTTP status : ' + res.status + ' : saveTrainSetting fail\n', res.data);
    })
  }


  @action
  putNoiseData(item: any){
    if(typeof this.noiseDataList === 'undefined'){
      this.noiseDataList = [];
    }
    this.noiseDataList.push(item);
    this.step2Changed = true;
  }

  @action
  takeNoiseData(item: any){
    if(typeof this.noiseDataList === 'undefined'){
      this.noiseDataList = [];
      return;
    }
    this.noiseDataList.remove(item);
    this.step2Changed = true;
  }

  onStep1Active()  {
    console.log('on Step1 Active');
    this.root.legacyDataStore.fetchLearningData(false);
    this.root.trainingStore.clearData();
  }
  onStep2Active()  {
    console.log('on Step2 Active');
    this.root.legacyDataStore.fetchPreTrainedModel();
    void this.root.legacyDataStore.fetchNoiseData();
    this.root.trainingStore.clearData();
  }
  onStep3Active()  {
    console.log('on Step3 Active');
    void this.root.trainingStore.fetchTrainingData(this.id);
  }
  onStep4Active()  {
    console.log('on Step4 Active');
    void this.root.trainingStore.fetchTrainingData(this.id);
    void this.root.sttModelTestStore.fetchModelList(this.root.sttProjectStore.id);
  }

}
