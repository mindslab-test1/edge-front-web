import StoreTabStore from './StoreTabStore';
import LayoutStore from './LayoutStore';
import UserStore from './UserStore';
import SttProjectStore from './SttProjectStore';
import LegacyDataStore from './LegacyDataStore';
import NotificationStore from './NotificationStore';
import AlertStore from './AlertStore';

import {createContext} from 'react';


class RootStore {
  storeTabStore: StoreTabStore;
  layoutStore: LayoutStore;
  userStore: UserStore;
  sttProjectStore: SttProjectStore;
  legacyDataStore: LegacyDataStore;
  notiStore: NotificationStore;
  alertStore : AlertStore;

  constructor() {
    this.storeTabStore = new StoreTabStore(this);
    this.layoutStore = new LayoutStore(this);
    this.userStore = new UserStore(this);
    this.sttProjectStore = new SttProjectStore(this);
    this.legacyDataStore = new LegacyDataStore(this);
    this.notiStore = new NotificationStore(this);
    this.alertStore = new AlertStore(this);

  }
}

export default createContext(new RootStore());
