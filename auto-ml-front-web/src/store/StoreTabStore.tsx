import { observable, action } from 'mobx';

export default class StoreTabStore {
  private root: any;
  constructor(root) {
    this.root = root;
  }

  @observable tabKey: string = '1';

  @action setTabKey = (key: string) => {
    this.tabKey = key;
  };

  @action getTabKey = () => {
    return this.tabKey;
  };
}
