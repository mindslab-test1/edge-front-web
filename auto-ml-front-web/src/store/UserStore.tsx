import {observable, action, toJS} from 'mobx';
import {pullUserToken} from '../service/auth/AuthService';
import jwt_decode from 'jwt-decode';

export default class UserStore {
  private root: any;

  constructor(root) {
    this.root = root;
    const token = pullUserToken();
    if (token) {
      let decoded = jwt_decode(token);
      this.setUserNo(decoded.sub);
      this.setName(decoded.name);
      this.setEmail(decoded.email);
    }
  }

  @observable userNo: string = '';
  @observable email: string = '';
  @observable name: string = '';

  @action setUserNo = (newUserNo) => {
    this.userNo = newUserNo;
  }
  @action setName = (newName) => {
    this.name = newName;
  }
  @action setEmail = (newEmail) => {
    this.email = newEmail;
  }

  @action getUserName = () => {
    return this.name;
  }
  @action getUserEmail = () => {
    return this.email;
  }
  @action getUserNo = () => {
    return this.userNo;
  }

  @action handleSetProfile = (jwt : any | null) => {

    let userMail : string = '';

    if (jwt != null && jwt.email != '') {
      userMail = jwt.email;
    }

    this.setName(jwt != null ? jwt.name : '');
    this.setEmail(userMail);

    if (userMail.length > 0) {
      this.root.notiStore.info('로그인 되었습니다.');
    }
    else {
      this.root.notiStore.info('로그아웃 되었습니다.');
    }
  }
}
