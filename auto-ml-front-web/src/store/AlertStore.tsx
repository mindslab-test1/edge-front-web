import React from 'react';
import ReactDOM from 'react-dom';
import { store } from 'react-notifications-component';
import ComConfirm from 'components/commons/ComConfirm';

export default class AlertStore {
  private root: any;
  constructor(root) {
    this.root = root;
  }


  confirm = (title, content, callback, closeCallback?) => {
    this.showConfirm(title, content, callback, closeCallback ? closeCallback : null);
  };

  close = () => {
    let el : any = document.getElementById('altWrap');
    if (typeof(el) != 'undefined' && el != null) {
      document.body.removeChild(el);
    }
  }

  private showConfirm = (title : string, content : string, callback : any, closeCallback? : any) => {
    let el : any = document.getElementById('altWrap');
    if (typeof(el) == 'undefined' || el == null) {
      el = document.createElement('div');
      el.classList.add('altWrap');
      el.setAttribute("id", "altWrap");
      document.body.appendChild(el);
    }

    return ReactDOM.render(<ComConfirm title={title} content={content} confirmCallback={callback} close={this.close} closeCallback={closeCallback} />, el);
  };

}

// export const info = (message) => {
//   store.addNotification({
//     // title: 'ERROR',
//     message: message,
//     type: 'info',
//     insert: 'top',
//     container: 'bottom-right',
//     animationIn: ['animated', 'fadeIn'],
//     animationOut: ['animated', 'fadeOut'],
//     dismiss: {
//       duration: 3000,
//       showIcon: true,
//       // onScreen: true,
//     }
//   });
// }
