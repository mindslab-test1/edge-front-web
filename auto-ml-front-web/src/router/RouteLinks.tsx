import {PageLogin, PageLoginComplete, PageSignup, PageSsoLogin} from 'pages/auth';

import {PageManagementList, PageRoadRealTime, PageRoadResult, PageInquiryList, PageUserManagementList, PageSettingList, PageCompanyManagementList} from 'pages/edge';

import {PageHome} from 'pages/home';

import {PageMain} from 'pages/main';

import {PageError, PageNetworkError} from 'pages/error';

export enum ERouteUrl {
  MAIN = '/',
  HOME = '/home',
  LOGIN = '/login',
  SSO_LOGIN = '/sso_login',
  LOGIN_COMPLETE = '/login/complete',
  SIGNUP = '/signup',
  PROJECT_DETAIL = '/prj/:id',
  ERROR_GENERAL = '/error',
  ERROR_NETWORK = '/network_error',
  //Edge Road 조회
  EDGE_INQUIRY = '/edge/inquiry',
  //실시간 차량 인식
  EDGE_REALTIME = '/edge/roadRealTime',
  //차량 인식 결과
  EDGE_ROAD_RESULT = '/edge/roadResult',
  //설치 이력 조회
  SETTING_HISTORY = '/edge/settingHistory',
  //Edge Road 관리
  EDGE_MANAGEMENT = '/edge/management',
  //사용자 관리
  EDGE_USER_MANAGEMENT = '/edge/userManagement',
  //회사 관리
  EDGE_COMPANY_MANAGEMENT = '/edge/companyManagement',
}

export const ROUTES = [
  { path: ERouteUrl.MAIN, exact: true, component: PageMain },
  { path: ERouteUrl.HOME, exact: true, component: PageHome },
  { path: ERouteUrl.LOGIN, exact: true, component: PageLogin },
  { path: ERouteUrl.SSO_LOGIN, exact: true, component: PageSsoLogin },
  { path: ERouteUrl.LOGIN_COMPLETE, exact: true, component: PageLoginComplete },
  { path: ERouteUrl.SIGNUP, exact: true, component: PageSignup },
  { path: ERouteUrl.ERROR_GENERAL, exact: true, component: PageError },
  { path: ERouteUrl.ERROR_NETWORK, exact: true, component: PageNetworkError },
  { path: ERouteUrl.EDGE_INQUIRY, exact: true, component: PageInquiryList },
  { path: ERouteUrl.EDGE_REALTIME, exact: true, component: PageRoadRealTime },
  { path: ERouteUrl.EDGE_ROAD_RESULT, exact: true, component: PageRoadResult },
  { path: ERouteUrl.SETTING_HISTORY, exact: true, component: PageSettingList },
  { path: ERouteUrl.EDGE_MANAGEMENT, exact: true, component: PageManagementList },
  { path: ERouteUrl.EDGE_USER_MANAGEMENT, exact: true, component: PageUserManagementList },
  { path: ERouteUrl.EDGE_COMPANY_MANAGEMENT, exact: true, component: PageCompanyManagementList },
];
