import React from 'react';
import ReactDOM from 'react-dom';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import 'animate.css/animate.compat.css';

/* vxgplayer */
import 'assets/js/vxgplayer.js';
import 'assets/css/vxgplayer.css';

/* dateFormat */
import 'assets/js/dateformat.js';

import '../public/favicon.ico';
import '../public/logo192.png';

import 'assets/css/reset.css';
import 'assets/css/all.css';
import 'assets/css/font.css';
import 'assets/css/maum_common.css';
import 'assets/css/maumEdgeAI.css';


import App from 'App';
import * as serviceWorker from 'serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <ReactNotification />
    <App />
  </React.StrictMode>,
  document.getElementById('wrap')
);

serviceWorker.unregister();
