import React, {useContext, useEffect, useState} from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Form, Input, Button, message, Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { FORM_VALIDATE_MESSAGES } from 'constants/FormValidateMessages';
import {login, loginCallback} from 'service/auth/AuthService';
import {getIdFromPath, push} from 'utils/Navigation';
import { ERouteUrl } from 'router/RouteLinks';
import { IContainerBaseProps } from 'constants/BaseInterface';
import RootStore from '../../store/RootStore';
import {showLoading, hideLoading} from 'utils/CommonUtils';

interface Props extends IContainerBaseProps {}

const ConLoginComplete: React.FC<Props> = (navigation) => {
  const {layoutStore, userStore} = useContext(RootStore);

  const [processing, setProcessing] = useState<boolean>(false);
  const { handleSetProfile } = userStore;
  const param = location.search;

  const handleLoginError = () => {
    setProcessing(false);
    message.error('로그인에 실패하였습니다.');
    hideLoading();
  };

  const submit = () => {
    setProcessing(true);
    void loginCallback(navigation, param,() => handleLoginError(), (token) => handleSetProfile(token));
  };

  useEffect(()=>{
    submit();

  },[])
  return (
    <div>

    </div>
  );
};

export default ConLoginComplete;
