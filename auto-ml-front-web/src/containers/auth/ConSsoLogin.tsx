import React, {useEffect} from 'react';
import {IContainerBaseProps} from '../../constants/BaseInterface';
import {ERouteUrl} from '../../router/RouteLinks';

interface Props extends IContainerBaseProps {}

const ConSsoLogin: React.FC<Props> = ({ history }) => {

  useEffect( () => {
    window.location.href = process.env.REACT_APP_SSO_LOGIN_URL + '?response_type=code&client_id='
      + process.env.REACT_APP_SSO_CLIENT_ID + '&redirect_uri=' + process.env.REACT_APP_CALLBACK_URL;
  })

  return (
    <div>
    </div>
  );
};

export default ConSsoLogin;
