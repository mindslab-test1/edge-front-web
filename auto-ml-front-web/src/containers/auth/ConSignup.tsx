import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import { Form, Input, Button, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { FORM_VALIDATE_MESSAGES } from 'constants/FormValidateMessages';
import { signup } from 'service/auth/AuthService';
import { IContainerBaseProps } from 'constants/BaseInterface';
import RootStore from '../../store/RootStore';

interface Props extends IContainerBaseProps {}

const ConSignup: React.FC<Props> = (navigation) => {
  const {layoutStore, userStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;
  const [processing, setProcessing] = useState<boolean>(false);
  const { handleSetProfile } = userStore;

  toggleHeader(false);
  toggleSidebar(false);

  const handleSignupError = () => {
    setProcessing(false);
    message.error('회원 가입에 실패하였습니다.');
  };

  const submit = (form: object) => {
    setProcessing(true);
    void signup(form, navigation, () => handleSignupError(), (token) => handleSetProfile(token));
  };

  const onFinishFailed = () => {
    message.error('모든 항목을 입력해 주세요.');
  };

  return (
    <Container>
      <_Form name='signupForm' layout='vertical' validateMessages={FORM_VALIDATE_MESSAGES} onFinish={submit} onFinishFailed={onFinishFailed}>
        <Form.Item label='이메일' name='email' rules={[{ required: true, type: 'email' }]}>
          <Input prefix={<UserOutlined className='site-form-item-icon' />} placeholder='이메일 입력' />
        </Form.Item>
        <Form.Item label='암호' name='token' rules={[{ required: true, type: 'string', min: 6 }]}>
          <Input prefix={<LockOutlined className='site-form-item-icon' />} type='password' placeholder='암호 6자리 이상 입력' />
        </Form.Item>
        <Form.Item label='이름' name='name' rules={[{ required: true, type: 'string' }]}>
          <Input />
        </Form.Item>
        <Form.Item label='가입 코드' name='signupCode' rules={[{ required: true, type: 'string' }]}>
          <Input />
        </Form.Item>

        <div style={{ height: 30 }} />
        <Form.Item>
          <_RegButton type='primary' loading={processing} htmlType='submit'>
            가입
          </_RegButton>
        </Form.Item>
      </_Form>
    </Container>
  );
};

const Container = styled.div`
  flex: 1;
`;

const _Form = styled(Form)`
  max-width: 300px;
  margin: auto;
  padding: 70px 0;
`;

const _RegButton = styled(Button)`
  width: 100%;
`;

export default ConSignup;
