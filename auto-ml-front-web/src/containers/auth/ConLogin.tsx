import React, { useContext, useState } from 'react';
import styled from 'styled-components';
import { Link, useHistory } from 'react-router-dom';
import { Form, Input, Button, message, Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { FORM_VALIDATE_MESSAGES } from 'constants/FormValidateMessages';
import { login } from 'service/auth/AuthService';
import { push } from 'utils/Navigation';
import { ERouteUrl } from 'router/RouteLinks';
import { IContainerBaseProps } from 'constants/BaseInterface';
import RootStore from '../../store/RootStore';

interface Props extends IContainerBaseProps {}

const ConLogin: React.FC<Props> = (navigation) => {
  const {layoutStore, userStore} = useContext(RootStore);

  const { toggleHeader, toggleSidebar } = layoutStore;
  const [processing, setProcessing] = useState<boolean>(false);
  const { handleSetProfile } = userStore;
  let history = useHistory();

  toggleHeader(false);
  toggleSidebar(false);

  const handleLoginError = () => {
    setProcessing(false);
    message.error('로그인에 실패하였습니다.');
  };

  const submit = (form: object) => {
    console.log(form);
    setProcessing(true);
    void login(form, navigation, () => handleLoginError(), loginCallback);
  };

  const onSubmitFailed = () => {
    message.error('모든 항목을 입력해 주세요.');
  };

  const loginCallback = (token) => {
    handleSetProfile(token);
    console.log("loginCallback");
    history.push('/');
  }

  return (
    <div>
      <_Form name='loginForm' layout='vertical' validateMessages={FORM_VALIDATE_MESSAGES} onFinish={submit} onFinishFailed={onSubmitFailed}>
        <Form.Item label='이메일' name='id' rules={[{ required: true, type: 'email' }]}>
          <Input prefix={<UserOutlined className='site-form-item-icon' />} placeholder='이메일 입력' />
        </Form.Item>
        <Form.Item label='암호' name='password' rules={[{ required: true, type: 'string', min: 6 }]}>
          <Input prefix={<LockOutlined className='site-form-item-icon' />} type='password' placeholder='암호 6자리 이상 입력' />
        </Form.Item>

        <div style={{ height: 30 }} />
        <Form.Item>
          <_RegButton type='primary' loading={processing} htmlType='submit'>
            로그인
          </_RegButton>
        </Form.Item>
        <Row justify='end' style={{ marginTop: -20 }}>
          <Col>
            <Link to={ERouteUrl.SIGNUP}>
              <Button type='link' htmlType='button'>
                가입하기
              </Button>
            </Link>
          </Col>
        </Row>
      </_Form>
    </div>
  );
};

const _Form = styled(Form)`
  max-width: 300px;
  margin: auto;
  padding: 70px 0;
`;

const _RegButton = styled(Button)`
  width: 100%;
`;

export default ConLogin;
