import { Cookies } from 'react-cookie';
import axios from 'axios';
import { callApi, API, HTTP_RES_CODE } from 'utils/ApiClient';
import { ERouteUrl } from 'router/RouteLinks';
import { IContainerBaseProps } from 'constants/BaseInterface';
import jwt_decode from 'jwt-decode';
import {deepCopyObject} from 'utils/CommonUtils';
import {showLoading, hideLoading} from 'utils/CommonUtils';

export const signup = async (form: any, navigation: IContainerBaseProps, handleError: Function, handleSetProfile: Function) => {
  const res = await callApi(API.SIGNUP, form);

  if (HTTP_RES_CODE.CREATED === res?.status) {
    const loginForm = {
      id: form.email,
      password: form.token,
    };
    void login(loginForm, navigation, handleError, handleSetProfile);
  }
};

export const login = async (form: object, navigation: IContainerBaseProps, handleError: Function, handleSetProfile: Function) => {
  const res = await callApi(API.LOGIN, form);

  if (HTTP_RES_CODE.OK === res?.status) {
    storeUserToken(res.data.accessToken, handleSetProfile);
    //navigation.history.push(ERouteUrl.HOME);
  } else {
    handleError();
  }

};export const loginCallback = async (navigation: IContainerBaseProps,param:string, handleError: Function, handleSetProfile: Function) => {
  const callbackApi = deepCopyObject(API.LOGIN_CHECK);
  callbackApi.url = callbackApi.url + param;
  const res = await callApi(callbackApi);

  if (HTTP_RES_CODE.OK === res?.status) {
    storeUserToken(res.data.accessToken, handleSetProfile);
    hideLoading();
    navigation.history.push(ERouteUrl.MAIN);
  } else {
    handleError();
  }
};

export const storeUserToken = (token: string, handleSetProfile: Function) => {
  // const d = new Date();
  // d.setTime(d.getTime() + 60 * 60 * 24 * 7);
  const maxAge = 60 * 60 * 24 * 7;
  const cookies = new Cookies();
  const decoded = jwt_decode(token);
  cookies.set('userToken', token, { path: '/', maxAge });
  handleSetProfile(decoded);
};

export const logoutProcess = async () => {
  return new Promise(async (resolve) => {
    const res = await callApi(API.LOGOUT);
    resolve(HTTP_RES_CODE.OK === res?.status);
  });
};

export const removeUserToken = () => {
  const cookies = new Cookies();
  const token = cookies.get('userToken');
  let accessToken : string = '1';
  if (token) {
    const decoded = jwt_decode(token);
    accessToken = decoded.accessToken;
  }

  cookies.remove('userToken', { path: '/' });
  window.location.replace(`${process.env.REACT_APP_SSO_LOGOUT_URL}?access_token=${accessToken}&client_id=${process.env.REACT_APP_SSO_CLIENT_ID}&returnUrl=${process.env.REACT_APP_SSO_LOGOUT_RETURN_URL}`);

};

export const pullUserToken = () => {
  const cookies = new Cookies();
  return cookies.get('userToken');
};

export const isLoggedIn = async () => {
  return new Promise(async (resolve, reject) => {
    const res = await callApi(API.AUTH_PING);
    if (res.data && res.data.logout) {
      reject();
    } else {
      resolve(HTTP_RES_CODE.OK === res?.status);
    }

  });
};
