import React, {useContext, useState, useEffect, useRef} from 'react';
import RootStore from '../../store/RootStore';
import {CompanyDetail} from 'components/edge/CompanyManagementList';
import { openDaumAddrApi } from 'utils/DaumAddrApiClient';
import {getCodeList} from 'utils/CommonUtils';


interface Props{
  close:  () => void;
  item : CompanyDetail;
  show : string;
  companyModifyCallback : (item : CompanyDetail) => void;
  duplicateFunc : (name : string, id : string) => any;
  dupCheck : boolean;
}

const CompanyManagementModify: React.FC<Props> = ({close, item, show, companyModifyCallback, duplicateFunc, dupCheck}) => {

  if (show != 'aside_modify') {
    return <></>
  }

  const {userStore, notiStore} = useContext(RootStore);
  const [company, setCompany] = useState(item);
  const [dupMsg, setDupMsg] = useState<React.ReactNode>(null);
  const [codes, setCodes] = useState<any>({});

  const handleOnChange = ({target : {name, value}}) => {
    if (name == 'companyUseYn') name = 'useYn';

    setCompany({
      ...company,
      [name] : value
    });
  };

  const duplicateCompanyName = () => {
    if (company.clientCompanyName == '') {
      notiStore.error('회사명을 입력해주세요.');
      return false;
    }

    duplicateFunc(company.clientCompanyName, company.clientCompanyId)
    .then(isDup => {
      setCompany({
        ...company,
        dupCheck : isDup
      });

      if (isDup) {
        setDupMsg(<p className='msg_check'><span className='usable'>사용 가능한 회사명입니다.</span></p>);
      } else {
        setDupMsg(<p className='msg_check'><span className='unusable'>등록 된 회사명입니다.</span></p>);
      }
    })
    .catch(err => {
      notiStore.error("회사 중복 체크에 실패했습니다.");
    });
  };

  useEffect(() => {
    setCompany(item);
    getCodeList(['USE_YN']).then((result : any) => {
      setCodes(result);
    });
  },[item]);

  useEffect(() => {
    setCompany({
      ...company,
      dupCheck : false
    });
    setDupMsg(null);
  },[dupCheck]);

    return (
      <>
        <div id='aside_modify' className='aside_edit' style={ show == 'aside_modify' ? {display : 'block'} : {display : 'none'}}>
            <div className='titArea'>
                <h3>회사 수정</h3>
            </div>
            <div className='contArea'>
                <div className='tit'>
                    <h4>회사 정보</h4>
                </div>
                <div className='cont'>
                    <table className='tbl_edit'>
                        <caption className='hide'>회사 정보</caption>
                        <colgroup>
                            <col width='30%' />
                            <col />
                        </colgroup>
                        <tbody>
                          <tr>
                              <th scope='row'>회사명</th>
                              <td id='modify_company'>
                                  <input type='text' className='ipt_txt_check' maxLength={15} name='clientCompanyName' value={company.clientCompanyName || ''} onChange={handleOnChange} readOnly={company.dupCheck} disabled={company.dupCheck} />
                                  <button type='button' className='btn_ipt_srch' onClick={() => {duplicateCompanyName()}} disabled={company.dupCheck}>중복확인</button>
                                  {dupMsg}
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>회사 ID</th>
                              <td id='modify_companyID'>
                                  <span className='ipt_field'>{company.clientCompanyId ? company.clientCompanyId : ''}</span>
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>사용여부</th>
                              <td>
                                  <div className='radioBox'>
                                      {
                                        codes.USE_YN && codes.USE_YN.length > 0 ? (
                                          codes.USE_YN.map(code => ((
                                            <React.Fragment key={code.code}>
                                              <input type='radio' name='companyUseYn' id={'company_modify_use_' + code.code} value={code.code} className='ipt_radio' checked={company.useYn == code.code ? true : false} onChange={handleOnChange} />
                                              <label htmlFor={'company_modify_use_' + code.code}>{code.codeNm}</label>
                                            </React.Fragment>
                                          )))
                                        ) : <></>
                                      }
                                  </div>
                              </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='foo_btnBox'>
                <button type='button' className='btn_aside_close' onClick={close}>취소</button>
                <button type='button' className='ft_point' onClick={() => {companyModifyCallback(company)}}>저장</button>
            </div>
        </div>
        <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
      </>
    )
}

export default CompanyManagementModify;
