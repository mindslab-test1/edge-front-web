import React, {useContext, useState, useEffect} from 'react';
import RootStore from '../../store/RootStore';
import { openDaumAddrApi } from 'utils/DaumAddrApiClient';
import {getCodeList} from 'utils/CommonUtils';


interface Props{
  close:  () => void;
  show : string;
  companyInsertCallback : (item : any) => void;
  duplicateFunc : (name : string, id : string) => any;
  dupCheck : boolean;
}

const CompanyManagementInsert: React.FC<Props> = ({close, show, companyInsertCallback, duplicateFunc, dupCheck}) => {

    if (show != 'aside_add') {
      return <></>
    }

    const initData = {
      clientCompanyName : '',
      useYn : 'Y',
      dupCheck : false
    }

    console.log('asdasd');

    const {userStore, notiStore} = useContext(RootStore);
    const [dupMsg, setDupMsg] = useState<React.ReactNode>(null);
    const [insertData, setInsertData] = useState(initData);
    const [codes, setCodes] = useState<any>({});

    const duplicateCompanyName = () => {
      if (insertData.clientCompanyName == '') {
        notiStore.error('회사명을 입력해주세요.');
        return false;
      }

      duplicateFunc(insertData.clientCompanyName, '')
      .then(isDup => {
        setInsertData({
          ...insertData,
          dupCheck : isDup
        });

        if (isDup) {
          setDupMsg(<p className='msg_check'><span className='usable'>사용 가능한 회사명입니다.</span></p>);
        } else {
          setDupMsg(<p className='msg_check'><span className='unusable'>등록 된 회사명입니다.</span></p>);
        }
      })
      .catch(err => {
        notiStore.error("회사 중복 체크에 실패했습니다.");
      });
    }

    const handleOnChange = ({target : {name , value }}) => {

      if (name == 'insertUseYn') name = 'useYn';

      setInsertData({
        ...insertData,
        [name] : value
      });
    }

    useEffect(() => {
      getCodeList(['USE_YN']).then((result : any) => {
        setCodes(result);
      });
    },[]);

    useEffect(() => {
      setInsertData({
        ...insertData,
        dupCheck : false
      });
      setDupMsg(null);
    },[dupCheck]);

    return (
      <>
        <div id='aside_add' className='aside_edit' style={ show == 'aside_add' ? {display : 'block'} : {display : 'none'}}>
            <div className='titArea'>
                <h3>회사 등록</h3>
            </div>
            <div className='contArea'>
                <div className='tit'>
                    <h4>회사 정보</h4>
                </div>
                <div className='cont'>
                    <table className='tbl_edit'>
                        <caption className='hide'>회사 정보</caption>
                        <colgroup>
                            <col width='30%' />
                            <col />
                        </colgroup>
                        <tbody>
                          <tr>
                              <th scope='row'>회사명</th>
                              <td id='modify_company'>
                                  <input type='text' className='ipt_txt_check' maxLength={15} name='clientCompanyName' value={insertData.clientCompanyName} onChange={handleOnChange} readOnly={insertData.dupCheck} />
                                  <button type='button' className='btn_ipt_srch' onClick={(e) => {duplicateCompanyName()}} disabled={insertData.dupCheck}>중복확인</button>
                                  {dupMsg}
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>사용여부</th>
                              <td>
                                  <div className='radioBox'>
                                      {
                                        codes.USE_YN && codes.USE_YN.length > 0 ? (
                                          codes.USE_YN.map(code => ((
                                            <React.Fragment key={code.code}>
                                              <input type='radio' name='insertUseYn' id={'company_add_use_' + code.code} value={code.code} className='ipt_radio' checked={insertData.useYn == code.code ? true : false} onChange={handleOnChange} />
                                              <label htmlFor={'company_add_use_' + code.code}>{code.codeNm}</label>
                                            </React.Fragment>
                                          )))
                                        ) : <></>
                                      }
                                  </div>
                              </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='foo_btnBox'>
                <button type='button' className='btn_aside_close' onClick={close}>취소</button>
                <button type='button' className='ft_point' onClick={() => {companyInsertCallback(insertData)}}>등록</button>
            </div>
        </div>
        <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
      </>
    )
}

export default CompanyManagementInsert;
