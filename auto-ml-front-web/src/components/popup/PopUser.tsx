import React, {useCallback, useRef, useEffect, useContext, useState} from 'react';
import ContactItem from './ContactItem';
import RootStore from 'store/RootStore';
import {API, callApi} from '../../utils/ApiClient';
import Paging from 'components/commons/Paging';
import {ApiUserListResponse} from 'components/edge/UserManagementList';
import UserManagementTableRow from 'components/edge/road/userManagement/UserManagementTableRow';
import _ from 'lodash';

interface Props {
  close : () => void;
  popTitle: string;
  sendUser : (e, item : any) => any;
  onChange: (item : any) => void;
  isEdgeUser? : boolean;
  clientCompanyId? : string;
}

const PopUser: React.FC<Props> = ({onChange, sendUser, close, popTitle, isEdgeUser, clientCompanyId}) => {

  const initSearch = {
    searchType : 'all',
    searchTxt : '',
    page : 1,
    size : 10
  };

  const searchTypeOptions = [
    {
      code : 'email',
      codeNm : '아이디'
    },
    {
      code : 'name',
      codeNm : '연락처'
    }
  ];

  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [userSearchList, setUserSearchList] = useState<ApiUserListResponse[]>([]);
  const [selectItem, setSelectItem] = useState({});
  const [checkItems, setCheckItems] = useState<any>([]);
  //const [search, setSearch] = useState(initSearch);
  const [userSearch, setUserSearch] = useState(initSearch);
  const [popShow, setPopShow] = useState(false);
  const [selectUserId, setSelectUserId] = useState('');
  const [isActive, setActive] = useState(false);
  const ref = useRef(null);
  const [bOpen, setOpen] = useState(false);
  const {userStore, notiStore} = useContext(RootStore);

  const iptCheckOneClick = (checked, item, email, name) => {
    /*if(checkItems.length = 1){
      console.log(checkItems.length);
    }*/

    if (checked) {
      if(checkItems.length >= 1){
        notiStore.error('1건만 선택해주시기 바랍니다.');
        setCheckItems([]);
        return false;
        //setCheckItems([]);
      }
      setCheckItems([...checkItems, item]);
    } else {
      setCheckItems(checkItems.filter((el) => el != item));
      //setCheckItems(checkItems.filter((el) => el != name));
    }

  };


  const toggleClass = (e) => {
    //debugger
    e.preventDefault();

    if(e.target.parentNode.className === 'btn_ipt_srch'){
      setActive(true);
    }
    if(e.target.parentNode === ref.current) {
    //if(e.target.className != 'lyr_bg') {
      //setActive(!isActive);
      //setActive(true);
      //setOpen(true);
    }
    else {
      //setOpen(false);
      //setActive(false);
    }
  }

  const has = (element, target) => {
    let isHas = false;

    if (element == target) {
      return true;
    } else {

      if (element.children.length > 0) {
        Array.from(element.children).forEach((item : any) => {
             if(has(item, target)) {
               isHas = true;
               return true;
             }
        });
      }

    }
    return isHas;
  };

  const onUserSearchBtnCallback = (e) => {
    callApiUserSearchList(1);
  };

  const handleOnUserChange = ({target : {name, value}}) => {
    onChange({name : [name], value : value});
    setUserSearch({
      ...userSearch,
      [name] : value
    });
  }

  const handleKeyPress = (e) => {
    //e.preventDefault();
    if(e.key == "Enter"){
      onUserSearchBtnCallback(e);
    }
  };

  const callApiUserSearchList = useCallback(async (pageNum) => {
    try {
        if(!isEdgeUser && userSearch.searchTxt == ""){
          notiStore.error('검색어를 입력해주시기 바랍니다.');
          return false;
        }
        setCheckItems([]);

        const searchs = _.cloneDeep(userSearch);
        searchs.page = pageNum-1;
        searchs.size = 10;

        if (clientCompanyId) searchs.clientCompanyId = clientCompanyId;

        const res = await callApi(isEdgeUser ? API.USER_LIST : API.USER_SEARCH, searchs);
        const data : any = isEdgeUser? res : res.data;

        if (data.success) {
          setUserSearchList(data.data.content);
          setTotalPages(data.data.totalPages);
        }else{

        }
        //const res = await callApi(API.USER_SEARCH, {page: pageNum - 1, size: 10});
        // console.log(res);

        setCurrentPage(pageNum);

    } catch {
        setUserSearchList([]);
        notiStore.error('조회에 실패했습니다.');
        //setUserList(dummyData);
        console.log('error');
    }
},[totalPages, userSearch, clientCompanyId, isEdgeUser]);

useEffect(() => {
  if (isEdgeUser) {
    callApiUserSearchList(1);
  }
},[]);

  return (
        <div className='lyrWrap'>
          <div className='lyr_bg' onClick={close}/>
          <div id="lyr_user_srch" ref={ref} className='lyrBox'>
            <div className="lyr_top">
                <h3>사용자 조회</h3>
                <button type="button" className="btn_lyr_close" onClick={close}>닫기</button>
            </div>
            <div id="lyr_contact_us" className='lyr_mid'>
                <div className="srchBox">
                    <select className="select" onChange={handleOnUserChange} name="searchType" >
                        <option value="all">전체</option>
                        <option value={isEdgeUser ? 'userEmail' : 'email'}>ID(E-mail)</option>
                        <option value={isEdgeUser ? 'userNm' :'name'}>이름</option>
                    </select>
                    <input type="text" className="ipt_txt" name="searchTxt" onKeyDown={handleKeyPress} onChange={handleOnUserChange}/*autocomplete="off"*//>
                    <button type="button" className="btn_ipt_srch" onClick={onUserSearchBtnCallback}><span className="fas fa-search" title="검색"></span></button>
                </div>
                <div className='srchTblBox'>
                  <table className="tbl_lst">
                      <caption className="hide">사용자 조회</caption>
                      <colgroup>
                          <col width="40"></col><col></col><col></col><col></col>
                      </colgroup>
                      <thead>
                          <tr>
                              <th scope="col">선택</th>
                              <th scope="col">No.</th>
                              <th scope="col">ID(E-mail)</th>
                              <th scope="col">이름</th>
                          </tr>
                      </thead>
                      <tbody>
                        {
                          userSearchList.length > 0 ? (
                            userSearchList.map((item, index)=>((
                              <tr key={index} className={selectUserId == item.email ? 'active':''}>
                                <td scope='row'>
                                    <div className='checkOnlyBox'>
                                        <input type='checkbox' name='ipt_tbl_check' id={'ipt_check_' + index} className='ipt_check' checked={checkItems.includes(item) ? true : false} onChange={(e) => {iptCheckOneClick(e.target.checked, item, item.email, item.name)}} />
                                        <label htmlFor={'ipt_check_' + index}>체크박스</label>
                                    </div>
                                </td>
                                <td>{((currentPage-1)*userSearch.size)+(index+1)}</td>
                                <td>{item.email}</td>
                                <td>{item.name}</td>
                              </tr>
                            )))
                          ) : (
                            <tr>
                              <td scope='row' colSpan={12} className='dataNone'>데이터가 없습니다.</td>
                            </tr>
                          )
                        }
                      </tbody>
                  </table>
                </div>
              </div>
              <Paging totalPages={totalPages} paginate={callApiUserSearchList} currentPage={currentPage}/>
              <div className='tbl_btm'>
                <div className="lyr_btm">
                  <div className="btnBox sz_small">
                      <button className="btn_lyr_close" onClick={close}>닫기</button>
                      <button id="btn_userFind_select" onClick={(e) => {sendUser(e, checkItems)}} className='btn_alt_open' disabled={checkItems.length == 0} >선택완료</button>
                  </div>
                </div>
              </div>
          </div>
        </div>

  );
};

export default PopUser;
