import React, {useContext, useState, useRef} from 'react';
import ContactItem from './ContactItem';
import RootStore from 'store/RootStore';
import {API, callApi} from '../../utils/ApiClient';
import { phoneFormat, emailExp, numberExp } from 'utils/CommonUtils';

interface Props {
  close:  () => void;
  popTitle: string;
}

const PopContact: React.FC<Props> = ({close, popTitle}) => {

  const {userStore, notiStore} = useContext(RootStore);
  const {sttProjectStore} = useContext(RootStore);
  const [showLabel, setShowLabel] = useState(true);
  const [userText, setUserText] = useState('');
  const [userPhone, setUserPhone] = useState('');
  const form = useRef(document.createElement("input"));

  const [item, setItem] = useState({
    name : userStore.name,
    email : userStore.email,
    tel : '',
    content : ''
  });

  const handleTyping = ({target : {name, value}}) => {
    const key : string = name.replace('user_','');

    setItem({
      ...item,
      [key] : value
    })
  };

  const sendMail = async() => {
    if (!validationCheck()) return false;

    try {
      let params = item;
      params.tel = phoneFormat(params.tel);

      const res = await callApi(API.INQUIRY_REG, params);
      if(res.success){
        notiStore.infoBlue('문의가 접수되었습니다. 담당자 확인 후 연락드리겠습니다.');
      }else{
        notiStore.error('[실패] 잠시 후 다시 시도해 주세요.');
        return false;  
      }
      
      close();
    } catch (err){
      console.error(err);
      notiStore.error('[실패] 잠시 후 다시 시도해 주세요.');
      return false;
    }
  }

  const validationCheck = () => {
    let isBlank : boolean = false;
    let keyArr = Object.keys(item);
    let key = '';

    for (let idx in keyArr) {
      key = keyArr[idx];

      if (item[key] == '') {
        isBlank = true;
        break;
      }
    }

    if (isBlank) {
      const blankElement : any = document.getElementById('user_' + key);
      notiStore.error(`${blankElement.title}을 입력해주세요.`);
      blankElement.focus();
      return false;
    }

    if (!emailExp(item.email)) {
      const blankElement : any = document.getElementById('user_email');
      notiStore.error(`이메일 형식이 올바르지 않습니다.`);
      blankElement.focus();
      return false;
    }

    if (!numberExp(item.tel)) {
      const blankElement : any = document.getElementById('user_tel');
      notiStore.error(`전화번호는 숫자만 입력 가능합니다.`);
      blankElement.focus();
      return false;
    }

    return true;
  }

  return (
    <div className='lyrWrap'>
      <div className='lyr_bg' onClick={close}/>
      <div id="lyr_contact_us" className='lyrBox contactBox'>
        <div className='contact_tit'>
          <h3>{popTitle}</h3>
          <p className='info_txt'>서비스에 관련된 문의를 남겨주시면 담당자가 확인 후, 답변 메일을 보내드리겠습니다.</p>
        </div>
        <div className='contact_cnt'>
          <ul className='contact_lst'>
              <li><a href="tel:1661-3222">1661-3222</a></li>
              <li><span>hello@mindslab.ai</span></li>
          </ul>
          <div className='contact_form'>
            <ContactItem inputId={'user_name'} iconClass={'fas fa-user'} span={'이름'} value={item.name} changeEvent={handleTyping} />
            <ContactItem inputId={'user_email'} iconClass={'fas fa-envelope'} span={'이메일'} value={item.email} changeEvent={handleTyping} />
            <ContactItem inputId={'user_tel'} iconClass={'fas fa-mobile-alt'} span={'연락처'} value={item.tel} changeEvent={handleTyping} />
            <div className='contact_item_block'>
              <textarea id="user_content" value={item.content} className='textArea' name='user_content' onChange={handleTyping} title='문의내용' />
              <label htmlFor='textArea' style={{display: item.content.length <= 0 ? '': 'none'}}><span className='fas fa-align-left' />문의내용</label>
              <p className='msg'>※ 문의내용에 욕설, 성희롱 등의 내용이 포함된 경우 문의가 제한될 수 있습니다.</p>
            </div>
          </div>
        </div>
        <div className='contact_btn'>
          <button className='btn_inquiry' onClick={sendMail}>문의하기</button>
          <button className='btn_lyr_close' onClick={close} >닫기</button>
        </div>
      </div>
    </div>
  );
};

export default React.memo(PopContact);
