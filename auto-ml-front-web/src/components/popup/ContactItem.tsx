import React, {useContext} from 'react';
import RootStore from '../../store/RootStore';


interface Props{
    inputId : string,
    iconClass : string,
    span: string,
    value: string,
    changeEvent : (e) => void;
}

const ContactItem: React.FC<Props> = ({inputId,iconClass,span, value, changeEvent}) => {
    const {userStore} = useContext(RootStore);
    return (
        <div className='contact_item'>
          <input id={inputId} name={inputId} className='ipt_txt' autoComplete="off" value={value} onChange={changeEvent} required exp-type="email" title={span} />
          <label htmlFor='user_name' style={{display: value.length <= 0 ? '': 'none'}} >
              <span className={iconClass}></span>{span}
          </label>
        </div>
        // <div className='contact_item'>
        //     <input type='text' id='user_name' value={userStore.name}  className='ipt_txt' autoComplete='off' />
        //     <label htmlFor='user_name'><span className='fas fa-user' />{name}</label>
        // </div>
    )
}

export default React.memo(ContactItem);
