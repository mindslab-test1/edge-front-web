import React, {useContext, useState, useEffect, useRef} from 'react';
import RootStore from '../../store/RootStore';
import {API, callApi} from 'utils/ApiClient';
import { openDaumAddrApi } from 'utils/DaumAddrApiClient';


interface Props{
  close:  () => void;
  item : any;
  show : string;
  isAdmin? : boolean;
  saveBtnCallback : (item : any) => void;
  duplicateFunc? : (item : any) => any;
  userSearchPopOnclick? : (e, clientCompanyId : string) => any;
  selectUser? : any;
}

const EdgeManagementModify: React.FC<Props> = ({close, item, show, isAdmin, saveBtnCallback, duplicateFunc, userSearchPopOnclick, selectUser}) => {

  if (show != 'aside_modify') {
    return <></>
  }

  const {userStore, notiStore} = useContext(RootStore);
  const [edgeInfo, setEdgeInfo] = useState(item);
  const [compList, setCompList] = useState<any>([]);
  const [dupMsg, setDupMsg] = useState<React.ReactNode>(null);

  const addrDetailInput = useRef(document.createElement("input"));

  const handleAddress = (data) => {
    setEdgeInfo({
      ...edgeInfo,
      addr : data.roadAddress
    });

    addrDetailInput.current.focus();
  };

  const handleOnChange = ({target : {name, value}}) => {

    if (name == 'clientCompanyId') {
      setEdgeInfo({
        ...edgeInfo,
        [name] : value,
        userId : '',
        name : ''
      });
    } else {
      setEdgeInfo({
        ...edgeInfo,
        [name] : value
      });
    }

  };

  const handleListOnChange = ({target : {name , value }}, index) => {
    let newArr = [...edgeInfo.roadList];
    newArr[index] = {
      ...newArr[index],
      [name] : value
    };

    setEdgeInfo({
      ...edgeInfo,
      roadList : newArr
    });
  }

  const duplicateEdgeId = () => {

    if (!duplicateFunc) return false;

    if (edgeInfo.edgeId == '') {
      notiStore.error('아이디를 입력해주세요.');
      return false;
    }

    duplicateFunc({edgeId : edgeInfo.edgeId, deviceId : edgeInfo.deviceId})
    .then(isDup => {
      setEdgeInfo({
        ...edgeInfo,
        dupCheck : isDup
      });

      if (isDup) {
        setDupMsg(<p className='msg_check'><span className='usable'>사용 가능한 ID입니다.</span></p>);
      } else {
        setDupMsg(<p className='msg_check'><span className='unusable'>등록 된 ID입니다.</span></p>);
      }

    })
    .catch(err => {
      notiStore.error("회사 중복 체크에 실패했습니다.");
    })
  }

  const companyLists = async () => {

    let isError : boolean = false;

    try {
      const res = await callApi(API.COMPANY_LIST, {});
      const data : any = res.data;

      if (data.success) {
        setCompList(data.data.content);

        //setInsertData({
        //  ...insertData,
        //  clientCompanyId : res.data.data.content[0].clientCompanyId
        //});
      } else {
        isError = true;
      }

    } catch {
      isError = true;
      console.log("error");
    } finally {
      if (isError) notiStore.error("회사 정보 조회에 실패했습니다.");
    }
  };

  const userPopupOpen = (e) => {
    if (!userSearchPopOnclick) return false;

    if (edgeInfo.clientCompanyId == '' || !edgeInfo.clientCompanyId) {
      notiStore.error("회사를 선택해주세요.");
      return false;
    }

    userSearchPopOnclick(e, edgeInfo.clientCompanyId);
  }

  useEffect(() => {
    setEdgeInfo(item);
  },[item]);

  /*useEffect(() => {
    if (!edgeInfo.clientCompanyId && compList.length > 0) {
      setEdgeInfo({
        ...edgeInfo,
        clientCompanyId : compList[0].clientCompanyId
      });
    }
  },[edgeInfo, compList]);*/

  useEffect(() => {
    if(selectUser) {
      setEdgeInfo({
        ...edgeInfo,
        userId : selectUser.userId,
        name : selectUser.userNm
      })
    }
  },[selectUser]);

  useEffect(() => {
    if (isAdmin) {
        companyLists();
    }
  }, []);

    return (
      <>
        <div id='aside_modify' className='aside_edit' style={ show == 'aside_modify' ? {display : 'block'} : {display : 'none'}}>
            <div className='titArea'>
                <h3>
                {
                  !isAdmin ? '설치장소 수정' : 'Edge Road 수정'
                }
                </h3>
            </div>
            <div className='contArea'>
                <div className='tit'>
                    <h4>
                    {
                      !isAdmin ? 'Edge Road 정보' : 'Edge Road 기본정보'
                    }
                    </h4>
                </div>
                <div className='cont'>
                    <table className='tbl_edit'>
                        <caption className='hide'>
                          {
                            !isAdmin ? 'Edge Road 정보' : 'Edge Road 기본정보'
                          }
                        </caption>
                        <colgroup>
                            <col width={!isAdmin ? '25%' : '30%'} />
                            <col />
                        </colgroup>
                        <tbody>
                            {
                              !isAdmin ? (
                                <>
                                  <tr>
                                      <th scope='row'>회사명</th>
                                      <td id='modify_company'>
                                          <span className='ipt_field'>{edgeInfo.clientCompanyName}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>사용자명</th>
                                      <td id='modify_userName'>
                                          <span className='ipt_field'>{edgeInfo.name}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>Edge ID</th>
                                      <td id='modify_edgeID'>
                                          <span className='ipt_field'>{edgeInfo.edgeId}</span>
                                      </td>
                                  </tr>
                                </>
                              ) : (
                                <>
                                  <tr>
                                      <th scope='row'>Edge 아이디</th>
                                      {/*
                                        <td id='modify_edgeID'>
                                            <input type='text' className='ipt_txt_check' placeholder='시리얼 번호를 입력하세요.' title='Edge 아이디'
                                                   name='edgeId' value={edgeInfo.edgeId} readOnly={edgeInfo.dupCheck} onChange={handleOnChange} />
                                            <button type='button' className='btn_ipt_srch' onClick={() => {duplicateEdgeId()}} disabled={edgeInfo.dupCheck}>중복확인</button>
                                            {dupMsg}
                                        </td>
                                      */}
                                      <td id='modify_edgeID'>
                                          <span className='ipt_field'>{edgeInfo.edgeId}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>회사명</th>
                                      <td id='modify_company'>
                                        <select className='select' value={edgeInfo.clientCompanyId ? edgeInfo.clientCompanyId : ''} name='clientCompanyId' onChange={handleOnChange} title='회사명'>
                                          <option value=''>선택</option>
                                          {compList.map(
                                            (option, index) => <option key={index} value={option.clientCompanyId}>{option.clientCompanyName}</option>
                                          )}
                                        </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>회사 ID</th>
                                      <td id='modify_companyID'>
                                          <span className='ipt_field'>{edgeInfo.clientCompanyId}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>사용자명</th>
                                      <td id='modify_userName'>
                                          <input type='text' className='ipt_txt_srch' name='name' value={edgeInfo.name ? edgeInfo.name : ''} readOnly title='사용자명' />
                                          <button type='button' className='btn_ipt_srch' onClick={(e) => { userPopupOpen(e) }}><span className='fas fa-search' title='검색'></span></button>

                                      </td>
                                  </tr>
                                </>
                              )
                            }
                            <tr>
                                <th scope='row'>Edge 설치장소</th>
                                <td id='modify_edgePlace'>
                                    <input type="text" className='ipt_txt_srch' name='addr' maxLength={15} value={edgeInfo.addr == null ? '' : edgeInfo.addr} readOnly title='설치장소' />
                                    <button type='button' className='btn_ipt_srch' onClick={() => {openDaumAddrApi(handleAddress)}}><span className='fas fa-search' title='검색'></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>설치장소 상세</th>
                                <td id='modify_edgeDetailPlace'>
                                    <input type='text' className='ipt_txt' name='addrDetail' maxLength={15}  value={edgeInfo.addrDetail} onChange={handleOnChange} ref={addrDetailInput} title='설치장소 상세' />
                                </td>
                            </tr>

                            {
                              edgeInfo.roadList != null ? (
                                edgeInfo.roadList.map((item, index)=>((
                                  <tr key={index}>
                                      <th scope='row'>카메라 방향</th>
                                      <td id='modify_edgeDirection'>
                                          <input type='text' className='ipt_txt_unite' style={{width:'80%'}} maxLength={15} name='cameraDir' value={item.cameraDir} onChange={(e) => {handleListOnChange(e, index)}} title='카메라 방향' />
                                          <span>방향</span>
                                      </td>
                                  </tr>
                                )))
                              ) : <></>
                            }

                            {/*<tr>
                                <th scope='row'>카메라 번호</th>
                                <td id='modify_edgeNum'>
                                    <select className='select_unite' name='cameraNo' value={edgeInfo.cameraNo} onChange={handleOnChange}>
                                        <option value='1'>1</option>
                                        <option value='2'>2</option>
                                        <option value='3'>3</option>
                                        <option value='4'>4</option>
                                        <option value='5'>5</option>
                                    </select>
                                    <span>번 카메라</span>
                                </td>
                            </tr>*/}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='foo_btnBox'>
                <button type='button' className='btn_aside_close' onClick={close}>취소</button>
                <button type='button' className='ft_point' onClick={(e) => {saveBtnCallback(edgeInfo)}}>저장</button>
            </div>
        </div>
        <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
      </>
    )
}

export default React.memo(EdgeManagementModify);
