import React, {useContext, useState, useRef, useEffect} from 'react';
import RootStore from '../../store/RootStore';
//import sample1 from 'src/components/popup/resources/images/sample/img_sample0101.jpg'
//import sample2 from 'src/components/popup/resources/images/sample/img_sample0102.jpg'
import sample1 from 'assets/images/sample/img_sample0101.jpg'
import sample2 from 'assets/images/sample/img_sample0102.jpg'
import noImg from 'assets/images/sample/ico_noImg.jpg'

interface Props{
  close:  () => void;
  item : any;
  resultModifyCallback : (item : any) => void;
  show: string;
  directionOptions : any;
}

const EdgeRoadResultInfo: React.FC<Props> = ({close, item, resultModifyCallback, show, directionOptions}) => {
    const {userStore} = useContext(RootStore);
    const [selectItem, setSelectItem] = useState(0);
    const carNumberInput = useRef(document.createElement("input"));
    const [result, setResult] = useState(item);
    const [noImgStyle, setNoImgStyle]  = useState(true);
    const [noImgStyle1, setNoImgStyle1]  = useState(true);

    const handleOnChange = ({target : {name, value}}) => {
        setResult({
          ...result,
          [name] : value
        });
    }

    useEffect(() => {
        setSelectItem(item.no);
        setResult(item);
    },[item]);

    useEffect(() => {
      carNumberInput.current.focus();
    },[selectItem]);

    const handleImgError = (e, sampleImg) => {
      setNoImgStyle(false);
      e.target.src = sampleImg;
    }

    const handleImgError1 = (e, sampleImg) => {
        setNoImgStyle1(false);
        e.target.src = sampleImg;
    }

    return (
      <>
      <div id='aside_modify' className='aside_edit' style={ show == 'aside_modify' ? {display : 'block'} : {display : 'none'}}>
          <div className='titArea'>
              <h3>인식결과 수정</h3>
          </div>
          <div className='contArea'>
              <div className='tit'>
                  <h4>인식 정보</h4>
              </div>
              <div className='cont'>
                  {/*</div><div className='img_loadView'>*/}
                  <div className={noImgStyle ? 'img_loadView' : 'img_loadView1'}>
                      <img src={process.env.REACT_IMAGE_URL+result.carImgPath} onError={(e) => { handleImgError(e, noImg) }} alt='차량 로드뷰 이미지' />
                  </div>
                  {/*<div className='img_numPlate'>*/}
                  <div className={noImgStyle1 ? 'img_numPlate' : 'img_numPlate1'}>
                      <img src={process.env.REACT_IMAGE_URL+result.plateImgPath} onError={(e) => { handleImgError1(e, noImg) }}   alt='번호판 이미지' />
                  </div>
                  <table className='tbl_edit'>
                      <caption className='hide'>Edge Road 정보</caption>
                      <colgroup>
                          <col width='30%' />
                          <col />
                      </colgroup>
                      <tbody>
                          <tr>
                              <th scope='row'>회사명</th>
                              <td id='modify_company'>
                                  <span className='ipt_field'>{result.clientCompanyName}</span>
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>설치장소</th>
                              <td id='modify_edgePlace'>
                                  <span className='ipt_field'>
                                    {/*{result.addr}&nbsp;{result.addrDetail}&nbsp;{result.cameraDir}&nbsp;방향&nbsp;{result.cameraNo}번&nbsp;카메라*/}
                                    {result.addr}&nbsp;{result.addrDetail}&nbsp;{result.cameraDir}&nbsp;방향
                                  </span>
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>Edge 아이디</th>
                              <td id='modify_edgeID'>
                                  <span className='ipt_field'>{result.edgeId}</span>
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>차량번호</th>
                              <td id='modify_CarNumber'>
                                  <input type='text' className='ipt_txt' name='carNumber' autoComplete='off' value={result.carNumber} onChange={handleOnChange} ref={carNumberInput} />
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>차량방향</th>
                              <td>
                                  <select className='select' name='direction' value={result.direction} onChange={handleOnChange}>
                                    {directionOptions.map(
                                      (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
                                    )}
                                  </select>
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>차량 사진 경로</th>
                              <td>
                                  <em className='ipt_route'>{process.env.REACT_IMAGE_URL + result.carImgPath}</em>
                              </td>
                          </tr>
                          <tr>
                              <th scope='row'>번호판 사진 경로</th>
                              <td>
                                  <em className='ipt_route'>{process.env.REACT_IMAGE_URL + result.plateImgPath}</em>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div className='foo_btnBox'>
              <button type='button' className='btn_aside_close' onClick={close} >취소</button>
              <button type='button' className='ft_point' onClick={() => {resultModifyCallback(result)}}>수정</button>
          </div>
      </div>
      <button type='button' className='btn_aside_close ico' onClick={close} >닫기</button>
      </>
    )
}

export default React.memo(EdgeRoadResultInfo);
