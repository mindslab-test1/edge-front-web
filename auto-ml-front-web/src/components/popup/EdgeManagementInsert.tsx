import React, {useContext, useState, useEffect, useCallback} from 'react';
import RootStore from '../../store/RootStore';
import {API, callApi} from 'utils/ApiClient';
import { openDaumAddrApi } from 'utils/DaumAddrApiClient';


interface Props{
  close:  () => void;
  show : string;
  insertCallback : (item : any) => void;
  duplicateFunc : (item : any) => any;
  userSearchPopOnclick : (e, clientCompanyId : string) => any;
  selectUser : any;
  dupCheck : boolean;
}

const EdgeManagementInsert: React.FC<Props> = ({close, show, insertCallback, duplicateFunc, userSearchPopOnclick, selectUser, dupCheck}) => {

    if (show != 'aside_add') {
      return <></>
    }

    const initData = {
      edgeId : '',
      clientCompanyId : '',
      userNm : '',
      userId : '',
      addr : '',
      addrDetail : '',
      cameraNo : '',
      dupCheck : false,
      roadList : [{
        cameraDir : ''
      }]
    }

    const {userStore, notiStore} = useContext(RootStore);
    const [dupMsg, setDupMsg] = useState<React.ReactNode>(null);
    const [insertData, setInsertData] = useState(initData);
    const [compList, setCompList] = useState<any>([]);

    const handleAddress = (data) => {
      setInsertData({
        ...insertData,
        addr : data.roadAddress
      });
    };

    const duplicateEdgeId = () => {

      if (insertData.edgeId == '') {
        notiStore.error('아이디를 입력해주세요.');
        return false;
      }

      duplicateFunc({edgeId : insertData.edgeId})
      .then(isDup => {
        setInsertData({
          ...insertData,
          dupCheck : isDup
        });

        if (isDup) {
          setDupMsg(<p className='msg_check'><span className='usable'>사용 가능한 ID입니다.</span></p>);
        } else {
          setDupMsg(<p className='msg_check'><span className='unusable'>등록 된 ID입니다.</span></p>);
        }

      })
      .catch(err => {
        notiStore.error("아이디 중복 체크에 실패했습니다.");
      })
    }

    const companyLists = async () => {
      let isError : boolean = false;

      try {
        const res = await callApi(API.COMPANY_LIST, {});
        const data : any = res.data;

        if (data.success) {
          setCompList(res.data.data.content);
          setInsertData({
            ...insertData,
            clientCompanyId : res.data.data.content[0].clientCompanyId
          });
        } else {
          isError = true;
        }

      } catch {
        console.log("error");
        isError = true;
      } finally {
        if (isError) notiStore.error("회사 정보 조회에 실패했습니다.");
      }
    };

    useEffect(() => {
      companyLists();
    }, []);

    useEffect(() => {
      if(selectUser) {
        setInsertData({
          ...insertData,
          userId : selectUser.userId,
          userNm : selectUser.userNm
        })
      }
    },[selectUser]);

    useEffect(() => {
      setInsertData({
        ...insertData,
        dupCheck : false
      });
      setDupMsg(null);
    },[dupCheck]);

    const handleOnChange = ({target : {name , value }}) => {

      if (name == 'clientCompanyId') {
        setInsertData({
          ...insertData,
          [name] : value,
          userId : '',
          userNm : ''
        });
      } else {
        setInsertData({
          ...insertData,
          [name] : value
        });
      }
    }

    const handleListOnChange = ({target : {name , value }}, index) => {
      let newArr = [...insertData.roadList];
      newArr[index] = {
        ...newArr[index],
        [name] : value
      };

      setInsertData({
        ...insertData,
        roadList : newArr
      });
    }

    return (
      <>
        <div id='aside_add' className='aside_edit' style={ show == 'aside_add' ? {display : 'block'} : {display : 'none'}}>
            <div className='titArea'>
                <h3>Edge Road 등록</h3>
            </div>
            <div className='contArea'>
                <div className='tit'>
                    <h4>Edge Road 정보</h4>
                </div>
                <div className='cont'>
                    <table className='tbl_edit'>
                        <caption className='hide'>Edge 정보</caption>
                        <colgroup>
                            <col width='30%' />
                            <col />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope='row'>Edge 아이디</th>
                                <td>
                                    <input type='text' className='ipt_txt_check' placeholder='시리얼 번호를 입력하세요.' value={insertData.edgeId}
                                           name='edgeId' onChange={handleOnChange} readOnly={insertData.dupCheck} title='Edge 아이디' />
                                    <button type='button' className='btn_ipt_srch' onClick={() => {duplicateEdgeId()}} disabled={insertData.dupCheck}>중복확인</button>
                                    {dupMsg}
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>회사명</th>
                                <td>
                                    <select className='select' name='clientCompanyId' value={insertData.clientCompanyId} onChange={handleOnChange} title='회사명'>
                                      {compList.map(
                                        (option, index) => <option key={index} value={option.clientCompanyId}>{option.clientCompanyName}</option>
                                      )}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>회사 ID</th>
                                <td>
                                    <span className='ipt_field'>{insertData.clientCompanyId}</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>사용자명</th>
                                <td>
                                    <input type='text' className='ipt_txt_srch' name='userNm' value={insertData.userNm} readOnly title='사용자명' />
                                    <button type='button' className='btn_ipt_srch' onClick={(e) => {userSearchPopOnclick(e, insertData.clientCompanyId)}}><span className='fas fa-search' title='검색'></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>설치장소</th>
                                <td>
                                    <input type='text' className='ipt_txt_srch'name='addr' value={insertData.addr} readOnly title='설치장소' />
                                    <button type='button' className='btn_ipt_srch' onClick={() => {openDaumAddrApi(handleAddress) }}><span className='fas fa-search' title='검색'></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>설치장소 상세</th>
                                <td>
                                    <input type='text' className='ipt_txt' maxLength={15} autoComplete='off' name='addrDetail' value={insertData.addrDetail} onChange={handleOnChange} title='설치장소 상세' />
                                </td>
                            </tr>
                            {
                              insertData.roadList.length > 0 ? (
                                insertData.roadList.map((item, index)=>((
                                  <tr key={index}>
                                      <th scope='row'>카메라 방향</th>
                                      <td>
                                          <input type='text' className='ipt_txt_unite' style={{width:'80%'}} maxLength={15} name='cameraDir' value={item.cameraDir} onChange={(e) => {handleListOnChange(e, index)}} title='카메라 방향' />
                                          <span>방향</span>
                                      </td>
                                  </tr>
                                )))
                              ) : <></>
                            }
                            {/*
                            <tr>
                                <th scope='row'>카메라 번호</th>
                                <td>
                                    <select className='select_unite' name='cameraNo' value={insertData.cameraNo} onChange={handleOnChange} >
                                        <option value='1'>1</option>
                                        <option value='2'>2</option>
                                        <option value='3'>3</option>
                                        <option value='4'>4</option>
                                        <option value='5'>5</option>
                                    </select>
                                    <span>번 카메라</span>
                                </td>
                            </tr>
                            */}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='foo_btnBox'>
                <button type='button' className='btn_aside_close' onClick={close}>취소</button>
                <button type='button' className='ft_point' onClick={(e) => {insertCallback(insertData)}}>등록</button>
            </div>
        </div>
        <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
      </>
    )
}

export default React.memo(EdgeManagementInsert);
