import React, {useContext, useEffect, useState} from 'react';
import RootStore from '../../store/RootStore';
import {ApiListResponse} from 'components/edge/Home';


interface Props{
  close : (e) => void;
  edge : ApiListResponse | null;
  estimateCallback : (item : any) => void;
}

const InquiryEstimate: React.FC<Props> = ({close, edge, estimateCallback}) => {
    const {userStore} = useContext(RootStore);

    const [item, setItem] = useState({
      device : 'device1',
      edgeType : edge? edge.code : '',
      name : userStore.getUserName(),
      companyNm : '',
      rank : '',
      tel : '',
      email : userStore.getUserEmail()
    });

    const showDetailEstimate = (e) => {
      e.stopPropagation();

      if (!e.target.classList.contains('dvc_detail')) return false;

      let offsetX = e.offsetX;
      let offsetY = e.offsetY;
      //console.log(offsetX);
      //console.log(offsetY);
      let ulElement : any = e.target.childNodes[0];

      ulElement.style.top = (offsetY + 10) + 'px';
      ulElement.style.left = (offsetX + 10) + 'px';
      ulElement.style.display = 'inline-block';

    };

    const hideDetailEstimate = (e) => {
      if (!e.target.classList.contains('dvc_detail')) return false;
      let ulElement : any = e.target.childNodes[0];
      ulElement.style.display = 'none';
    };

    const handleOnChange = ({target : {name, value}}) => {
      const key : string = name.replace('inquiry_','');
      setItem({
        ...item,
        [key] : value
      });
    };

    useEffect(() => {
      //console.log('컴포넌트가 화면에 나타남');

      //const classList = Array.from(document.getElementsByClassName('dvc_detail'));

      //classList.forEach(element => {
        //element.addEventListener('mousemove', showDetailEstimate);
        //element.addEventListener('mouseleave', hideDetailEstimate);
      //});

      //return () => {
        //console.log('컴포넌트가 화면에서 사라짐');
        //classList.forEach(element => {
          //element.removeEventListener('mousemove', showDetailEstimate);
          //element.removeEventListener('mouseleave', hideDetailEstimate);
        //});

      //};
    }, []);

    return (
      <div className='lyrWrap'>
        <div className='lyr_bg' onClick={close}/>
        <div id='lyr_inquiry_estimate' className='lyrBox contactBox home'>
          <div className='estimate_tit'>
              <h3>견적요청하기</h3>
              <div className='estimate_dvc'>
                  <ul className='dvc_card_list'>
                      <li>
                          <span className='tit'>Device 1</span>

                          <div className='dvc_detail'>
                              <ul className='specify_list'>
                                  <li>GSP &colon; <span>16 core (16 TOPS)</span></li>
                                  <li>CPU &colon; <span>ARM A53 (1GHz)</span></li>
                                  <li>RAM &colon; <span>DDR4 2/4/8 GB</span></li>
                                  <li>Power &colon; <span>7W</span></li>
                                  <li>Camera &colon; <span>MIPI CSI2</span></li>
                                  <li>Display &colon; <span>MIPI DSI</span></li>
                                  <li>Ethernet, USB</li>
                              </ul>
                          </div>

                          <div className='radioBox'>
                              <input type='radio' name='inquiry_device' id='device1' value='device1' checked={item.device == 'device1' ? true : false} onChange={handleOnChange} title='디바이스' />
                              <label htmlFor='device1'></label>
                          </div>
                      </li>
                      <li>
                          <span className='tit'>Device 2</span>

                          <div className='dvc_detail'>
                              <ul className='specify_list'>
                                  <li>GSP &colon; <span>17 core (16 TOPS)</span></li>
                                  <li>CPU &colon; <span>ARM A53 (1GHz)</span></li>
                                  <li>RAM &colon; <span>DDR4 2/4/8 GB</span></li>
                                  <li>Power &colon; <span>7W</span></li>
                                  <li>Camera &colon; <span>MIPI CSI2</span></li>
                                  <li>Display &colon; <span>MIPI DSI</span></li>
                                  <li>Ethernet, USB</li>
                              </ul>
                          </div>

                          <div className='radioBox'>
                              <input type='radio' name='inquiry_device' id='device2' value='device2' checked={item.device == 'device2' ? true : false} onChange={handleOnChange} title='디바이스' />
                              <label htmlFor='device2'></label>
                          </div>
                      </li>
                      <li>
                          <span className='tit'>Device 3</span>

                          <div className='dvc_detail'>
                              <ul className='specify_list'>
                                  <li>GSP &colon; <span>18 core (16 TOPS)</span></li>
                                  <li>CPU &colon; <span>ARM A53 (1GHz)</span></li>
                                  <li>RAM &colon; <span>DDR4 2/4/8 GB</span></li>
                                  <li>Power &colon; <span>7W</span></li>
                                  <li>Camera &colon; <span>MIPI CSI2</span></li>
                                  <li>Display &colon; <span>MIPI DSI</span></li>
                                  <li>Ethernet, USB</li>
                              </ul>
                          </div>

                          <div className='radioBox'>
                              <input type='radio' name='inquiry_device' id='device3' value='device3' checked={item.device == 'device3' ? true : false} onChange={handleOnChange} title='디바이스' />
                              <label htmlFor='device3'></label>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>

          <div className='estimate_form'>
              <div className='contact_item'>
                  <span>Edge</span>
                  <input type='text' className='ipt_txt' autoComplete='off' disabled readOnly value={edge? edge.title : ''} title='EDGE' />
              </div>
              <div className='contact_item'>
                  <span>이름</span>
                  <input type='text' id='inquiry_name' className='ipt_txt' autoComplete='off' name='inquiry_name' value={item.name} onChange={handleOnChange} title='이름' />
              </div>
              <div className='contact_item'>
                  <span>회사</span>
                  <input type='text' id='inquiry_companyNm' className='ipt_txt' autoComplete='off' name='inquiry_companyNm' value={item.companyNm} onChange={handleOnChange} title='회사'  />
              </div>
              <div className='contact_item'>
                  <span>직급</span>
                  <input type='text' id='inquiry_rank' className='ipt_txt' autoComplete='off' name='inquiry_rank' value={item.rank} onChange={handleOnChange} title='직급' />
              </div>
              <div className='contact_item'>
                  <span>연락처</span>
                  <input type='number' id='inquiry_tel'  className='ipt_txt' autoComplete='off' name='inquiry_tel' value={item.tel} onChange={handleOnChange} title='연락처' />
              </div>
              <div className='contact_item'>
                  <span>이메일</span>
                  <input type='email' id='inquiry_email'  className='ipt_txt' autoComplete='off' name='inquiry_email' value={item.email} onChange={handleOnChange} title='이메일' />
              </div>
          </div>

          <div className='estimate_btn'>
              <button className='btn_estimate' onClick={() => {estimateCallback(item)}}>견적요청하기</button>
              <button className='btn_lyr_close' onClick={close}>닫기</button>
          </div>
        </div>
      </div>
    )
}

export default React.memo(InquiryEstimate);
