import React, {useContext, useState, useEffect, useRef} from 'react';
import RootStore from '../../store/RootStore';
import { openDaumAddrApi } from 'utils/DaumAddrApiClient';
import ComPopBox from '../commons/popup/ComPopBox';
import {API, callApi} from 'utils/ApiClient';
import {ApiListResponse, ApiCompanyListResponse, ApiUserListResponse} from 'components/edge/UserManagementList';
import {numberCheck, getCodeList} from 'utils/CommonUtils';

interface Props{
  close:  () => void;
  item : any;
  userModifyCallback : (item : any) => void;
  show : string;
  userSearchPopOnclick : (e)  => void;
}

const UserManagementModify: React.FC<Props> = ({close, item, userModifyCallback, show, userSearchPopOnclick}) => {

  if (show != 'aside_modify') {
    return <></>
  }

  const [compList, setCompList] = useState<ApiCompanyListResponse[]>([]);
  const {userStore, notiStore} = useContext(RootStore);
  const ref = useRef(null);
  /*const [compList, setCompList] = useState([{
    comp : '',
    compNm : ''
  }]);*/
  const [authList, setAuthList] = useState([
    {
      code : 1,
      codeNm : '일반사용자'
    },
    {
      code : 2,
      codeNm : '설치기사'
    },
    {
      code : 3,
      codeNm : '회사관리자'
    },
    {
      code : 4,
      codeNm : '관리자'
    }
  ]);
  const [user, setUser] = useState(item);

  const handleOnChange = ({target : {name, value}}) => {
    if(name == "tel"){
      if(!numberCheck(value)){
        notiStore.error("숫자만 입력해주세요.");
        return false;
      }
    }
    setUser({
      ...user,
      [name] : value
    });
  }

  const handleSet = (data) => {
    setUser({
      ...user,
      name : data.name,
      email : data.email
    });
  };

  const companyLists = async (item) => {
    try {
      const res = await callApi(API.COMPANY_LIST, {useYn : 'Y'});
      //const data : any = res.data;

      setCompList(res.data.data.content);

    } catch {
      console.log("error");
      notiStore.error("회사 정보 조회에 실패했습니다.");
    }
  };

  useEffect(() => {
    if (!user.clientCompanyId && compList.length > 0) {
      setUser({
        ...user,
        clientCompanyId : compList[0].clientCompanyId
      });
    }
  },[user, compList]);

  useEffect(() => {

    if(item.id){
      setUser(item);
    }else{
      handleSet(item);
    }

    //setUser(item);
  },[item]);

  useEffect(() => {
    companyLists({});
  }, []);

    return (
      <>
        <div id='aside_modify' className='aside_edit' style={ show == 'aside_modify' ? {display : 'block'} : {display : 'none'}}>
            <div className='titArea'>
                <h3>사용자 수정</h3>
            </div>
            <div className='contArea'>
                <div className='tit'>
                    <h4>사용자 정보</h4>
                </div>
                <div className='cont'>
                    <table className='tbl_edit'>
                        <caption className='hide'>사용자 정보</caption>
                        <colgroup>
                            <col width='30%' />
                            <col />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope='row'>사용자명</th>
                                <td id='modify_userName'>
                                    {/*<input type='text' className='ipt_txt_srch' name="name" value={user.name} onChange={handleOnChange} />*/}
                                    <span className='ipt_field'>{user.name}</span>
                                    {/*<a href='#lyr_user_srch' className='btn_lyr_open btn_ipt_srch' onClick={userSearchPopOnclick}><span className='fas fa-search' title='검색'></span></a>*/}
                                    {/*<button type='button' className='btn_ipt_srch' onClick={setPopIsOpen} ><span className='fas fa-search' title='검색'></span>
                                      <ComPopBox show={popShow}>
                                        <PopUser close={closePopup} popTitle={'사용자 검색'} modelId={false} modelName={''}/>
                                      </ComPopBox>
                                    </button>
                                    {/*<dd className='btn'><a className='btn_lyr_open' href='#lyr_inquiry_estimate' onClick={openPopup}>견적요청하기</a></dd>
                                    <button type='button' className='btn_ipt_srch' onClick={() => {openDaumAddrApi(handleAddress)}}><span className='fas fa-search' title='검색'></span></button>*/}
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>아이디 이메일</th>
                                <td id='modify_userId'>
                                    <span className='ipt_field'>{user.email}</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>사용자 연락처</th>
                                <td id='modify_phoneNum'>
                                    <input type='tel' className='ipt_txt phone_numHyphen' maxLength={13} placeholder="'-'를 제외하고 숫자만 입력하세요."  name='tel' value={user.tel} onChange={handleOnChange} />
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>회사명</th>
                                <td id='modify_company'>
                                    <select className='select' name='clientCompanyId' value={user.clientCompanyId ? user.clientCompanyId : ''} onChange={handleOnChange}>
                                      {compList.map(
                                        (option, index) => <option key={index} value={option.clientCompanyId}>{option.clientCompanyName}</option>
                                      )}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>회사 ID</th>
                                <td id='modify_companyID'>
                                    <span className='ipt_field'>{user.clientCompanyId}</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>권한유형</th>
                                <td>
                                    <select className='select' name='authId' value={user.authId} onChange={handleOnChange}>
                                      {authList.map(
                                        (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
                                      )}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>사용여부</th>
                                <td>
                                    <div className='radioBox'>
                                        <input type='radio' name='useYn' id='company_modify_use' className='ipt_radio' value='Y' checked={user.useYn == 'Y' ? true : false} onChange={handleOnChange} />
                                        <label htmlFor='company_modify_use'>사용</label>
                                        <input type='radio' name='useYn' id='company_modify_unused' className='ipt_radio' value='N' checked={user.useYn == 'N' ? true : false} onChange={handleOnChange} />
                                        <label htmlFor='company_modify_unused'>미사용</label>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='foo_btnBox'>
                <button type='button' className='btn_aside_close' onClick={close}>취소</button>
                <button type='button' className='ft_point' onClick={() => {userModifyCallback(user)}}>저장</button>
            </div>
        </div>
        <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
      </>
    )
}

export default UserManagementModify;
