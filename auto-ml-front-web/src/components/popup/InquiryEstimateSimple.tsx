import React, {useContext, useState} from 'react';
import RootStore from '../../store/RootStore';


interface Props{
    deviceInfo : any;
    close : (e) => void;
    estimateCallback : (item : any) => void;
}

const InquiryEstimateSimple: React.FC<Props> = ({deviceInfo, close, estimateCallback}) => {

    const {userStore} = useContext(RootStore);

    const [item, setItem] = useState({
        device : deviceInfo.tit,
        edgeType : 'Edge Platform',
        name : userStore.getUserName(),
        companyNm : '',
        rank : '',
        tel : '',
        email : userStore.getUserEmail()
    });

    const handleOnChange = ({target : {name, value}}) => {
        const key : string = name.replace('inquiry_','');
        setItem({
            ...item,
            [key] : value
        });
    };


    return (

        <div className='lyrWrap'>
            <div className='lyr_bg' onClick={close}/>
            <div id='lyr_inquiry_estimate' className='lyrBox contactBox'>
                <div className='estimate_tit'>
                    <h3>견적요청하기</h3>

                    <div className='estimate_dvc'>
                        <span>{deviceInfo.tit}</span>
                        <img src={deviceInfo.srcImg} alt='Edge Device 이미지'/>
                    </div>
                </div>

                <div className='estimate_form'>
                    <div className='contact_item'>
                        <span>이름</span>
                        <input type='text' className='ipt_txt' autoComplete='off' id='inquiry_name' name='inquiry_name' value={item.name}  onChange={handleOnChange} title='이름'/>
                    </div>
                    <div className='contact_item'>
                        <span>회사</span>
                        <input type='text' className='ipt_txt' autoComplete='off' id='inquiry_companyNm' name='inquiry_companyNm' value={item.companyNm}  onChange={handleOnChange} title='회사'/>
                    </div>
                    <div className='contact_item'>
                        <span>직급</span>
                        <input type='text' className='ipt_txt' autoComplete='off' id='inquiry_rank' name='inquiry_rank' value={item.rank}  onChange={handleOnChange} title='직급' />
                    </div>
                    <div className='contact_item'>
                        <span>연락처</span>
                        <input type='number' className='ipt_txt' autoComplete='off' id='inquiry_tel' name='inquiry_tel' value={item.tel}  onChange={handleOnChange} title='연락처' />
                    </div>
                    <div className='contact_item'>
                        <span>이메일</span>
                        <input type='email' className='ipt_txt' autoComplete='off' id='inquiry_email' name='inquiry_email' value={item.email}  onChange={handleOnChange} title='이메일' />
                    </div>
                </div>

                <div className='estimate_btn'>
                    <button className='btn_estimate' onClick={() => {estimateCallback(item)}}>견적요청하기</button>
                    <button className='btn_lyr_close' onClick={close}>닫기</button>
                </div>
            </div>

        </div>
    )
}

export default React.memo(InquiryEstimateSimple);
