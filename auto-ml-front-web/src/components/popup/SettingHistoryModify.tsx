import React, {useContext, useState, useEffect, useRef} from 'react';
import RootStore from '../../store/RootStore';
import {ApiListResponse} from '../edge/SettingHistoryList';
import { openDaumAddrApi } from 'utils/DaumAddrApiClient';
import {phoneFormat, changeDate} from 'utils/CommonUtils';

interface Props{
  close:  () => void;
  testChangeValue: (e) => void;
  item : any;
  show : string;
  isAdmin? : boolean;
  setModifyCallback : (item : any) => void;
}

const SettingHistoryModify: React.FC<Props> = ({close, item, setModifyCallback, testChangeValue, show, isAdmin}) => {

  const [compList, setCompList] = useState([{
    comp : '',
    compNm : ''
  }]);

  const addrDetailInput = useRef(document.createElement("input"));
  const [set, setSet] = useState(item);
  const [deviceSetYn, setDeviceSetYn] = useState(false);

  const handleAddress = (data) => {
    let event = {
      target : {
        name : 'addrNm',
        value : data.roadAddress
      }
    }

    testChangeValue(event);

    addrDetailInput.current.focus();
  };

  const setOption = [
    {
      code : 'Y',
      codeNm : '설치완료'
    },
    {
      code : 'N',
      codeNm : '설치중'
    }
  ]

  useEffect(() => {
      setSet(item);
      
      if(item.setYn == 'Y'){
        setDeviceSetYn(true);
      }else{
        setDeviceSetYn(false);
      }

      if (isAdmin) {
          setCompList(
            [
              {
                comp : 'comp01',
                compNm : '서울특별시'
              },
              {
                comp : 'comp02',
                compNm : '서초구청'
              },
              {
                comp : 'comp03',
                compNm : '용인시'
              },
              {
                comp : 'comp04',
                compNm : '성남시청'
              }
          ]
        )
      }
  }, [item]);

    return (
      <>
        <div id='aside_modify' className='aside_edit' style={ show == 'aside_modify' ? {display : 'block'} : {display : 'none'}}>
            <div className='titArea'>
                <h3>
                {
                  //!isAdmin ? '설치완료처리' : 'Edge Road 설치완료처리'
                  set.viewYn == 'Y' ? '설치완료처리 수정' : '설치완료처리'
                }
                </h3>
            </div>
            <div className='contArea'>
                <div className='tit'>
                    <h4>
                    {
                      !isAdmin ? 'Edge Road 설치정보' : 'Edge Road 기본설치정보'
                    }
                    </h4>
                </div>
                <div className='cont'>
                    <table className='tbl_edit'>
                        <caption className='hide'>
                          {
                            !isAdmin ? 'Edge Road 정보' : 'Edge Road 기본정보'
                          }
                        </caption>
                        <colgroup>
                            <col width={!isAdmin ? '25%' : '30%'} />
                            <col />
                        </colgroup>
                        <tbody>
                            {
                              !isAdmin ? (
                                <>
                                  <tr>
                                      <th scope='row'>회사명</th>
                                      <td id='modify_company'>
                                          <span className='ipt_field'>{set.clientCompanyName}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>고객명</th>
                                      <td id='modify_userName'>
                                          <span className='ipt_field'>{set.userName}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>고객 연락처</th>
                                      <td id='modify_phoneNum'>
                                          <span className='ipt_field'>{phoneFormat(set.userTel)}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>Edge 아이디</th>
                                      <td id='modify_edgeID'>
                                          <span className='ipt_field'>{set.edgeId}</span>
                                      </td>
                                  </tr>
                                </>
                              ) : (
                                <>
                                  <tr>
                                      <th scope='row'>Edge 아이디</th>
                                      <td id='modify_edgeID'>
                                          <span className='ipt_field'>{set.edgeId}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>회사명</th>
                                      <td id='modify_company'>
                                        <select className='select' value={set.clientCompanyId} name='clientCompanyId' onChange={testChangeValue}>
                                        {compList.map(
                                          (option, index) => <option key={index} value={option.comp}>{option.compNm}</option>
                                        )}
                                        </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>회사 ID</th>
                                      <td id='modify_companyID'>
                                          <span className='ipt_field'>{set.clientCompanyId}</span>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>고객명</th>
                                      <td id='modify_userName'>
                                          <span className='ipt_txt_srch'>{set.userNm}</span>
                                          <button type='button' className='btn_ipt_srch'><span className='fas fa-search' title='검색'></span></button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <th scope='row'>고객 연락처</th>
                                      <td id='modify_phoneNum'>
                                          <span className='ipt_field'>{phoneFormat(set.userTel)}</span>
                                      </td>
                                  </tr>
                                </>
                              )
                            }
                            <tr>
                                <th scope='row'>설치장소</th>
                                <td id='modify_edgePlace'>
                                  <span className='ipt_field'>
                                    {set.addr}
                                    &nbsp;{set.addrDetail}
                                    &nbsp;{set.cameraDir}&nbsp;방향
                                    {/*{set.cameraNo}번&nbsp;카메라*/}
                                    {/*<p>{item.addrNm}</p>
                                    <p>
                                      {item.addrDetail}
                                      {item.cameraDirection}&nbsp;방향
                                    </p>
                                    <p>{item.cameraNo}번&nbsp;카메라</p>*/}
                                  </span>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>설치완료처리</th>
                                <td id='modify_edgeNum'>
                                    <select className='select_unite' name='setYn' value={set.setYn} onChange={testChangeValue}>
                                        {setOption.map(
                                          (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
                                        )}
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='foo_btnBox'>
                <button type='button' className='btn_aside_close' onClick={close}>취소</button>
                <button type='button' className='ft_point' onClick={() => {setModifyCallback(set)}}>저장</button>
            </div>
        </div>
        <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
      </>
    )
}

export default SettingHistoryModify;
