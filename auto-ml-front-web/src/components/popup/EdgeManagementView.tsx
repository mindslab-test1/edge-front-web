import React, {useContext, useEffect} from 'react';
import RootStore from '../../store/RootStore';
import {API, callApi} from 'utils/ApiClient';

interface Props{
  close:  () => void;
  show : string;
  data : any;
  roadEdgeRealTime : (deviceId : number) => void;
}

const EdgeManagementView: React.FC<Props> = ({close, show, data, roadEdgeRealTime}) => {

    if (show != 'aside_view') {
      return <></>
    }

    if (!data) data = {};

    const {userStore} = useContext(RootStore);

    return (
      <>
        <div className='aside_view' id="aside_view" style={ show == 'aside_view' ? {display : 'block'} : {display : 'none'}}>
          <div className='titArea'>
              <h3>Edge 정보</h3>
          </div>
          <div className='contArea'>
              <div className='tit'>
                  <h4>모듈 상세정보</h4>
              </div>
              <div className='cont'>
                  <table className='tbl_view'>
                      <caption className='hide'>모듈 상세정보</caption>
                      <colgroup>
                          <col width='30%' />
                          <col />
                      </colgroup>
                      <tbody>
                          {
                            data.explanList && data.edgeInfoList && data.explanList.length > 0 && data.edgeInfoList.length > 0 ? (
                              data.explanList.map((item, index)=>{
                                return ( <tr key={index}>
                                            <th scope='row'>{item}</th>
                                            <td>{data.edgeInfoList[index] ? data.edgeInfoList[index] : ''}</td>
                                         </tr>
                                       )
                              })
                            ) : (
                              <tr>
                                <td scope='row' colSpan={2} className='dataNone' style={{textAlign : 'center'}}>데이터가 없습니다.</td>
                              </tr>
                            )
                          }
                          <tr>
                            <th scope='row'>실시간 차량 인식</th>
                            <td>
                                <button type='button' className='btn_line' onClick={() => {roadEdgeRealTime(data.deviceId)}}>바로가기</button>
                            </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div className='foo_btnBox'>
              <button type='button' className='btn_aside_close' onClick={close}>닫기</button>
          </div>
      </div>
      <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
    </>
    )
}

export default React.memo(EdgeManagementView);
