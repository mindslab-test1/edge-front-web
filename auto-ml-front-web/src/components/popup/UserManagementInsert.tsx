import React, {useContext, useState, useEffect, useCallback} from 'react';
import RootStore from '../../store/RootStore';
import { openDaumAddrApi } from 'utils/DaumAddrApiClient';
import {API, callApi} from 'utils/ApiClient';
import {ApiListResponse, ApiCompanyListResponse, ApiUserListResponse} from 'components/edge/UserManagementList';
import {numberCheck, getCodeList} from 'utils/CommonUtils';

interface Props{
  close:  () => void;
  show : string;
  userInsertCallback : (item : any) => void;
  userSearchPopOnclick : (e)  => void;
  item : any;
}

const UserManagementInsert: React.FC<Props> = ({item, close, show, userInsertCallback, userSearchPopOnclick}) => {

    if (show != 'aside_add') {
      return <></>
    }

    /*const initData = {
      email : '',
      name : '',
      tel : '',
      companyNm : '',
      clientCompanyId : '',
      roleId : '',
      useYn : 1
    }*/
    const initData = {
      email : '',
      name : '',
      tel : '',
      companyNm : '',
      clientCompanyId : '',
      authId : 1,
      useYn : 'Y',
      delYn : 'N'
    }

    const [compList, setCompList] = useState<ApiCompanyListResponse[]>([]);
    const {userStore, notiStore} = useContext(RootStore);
    //const {userStore} = useContext(RootStore);
    const [insertData, setInsertData] = useState(initData);
    const [companyId, setCompanyId] = useState("");
    const [tel, setTel] = useState("");
    const [authList, setAuthList] = useState([
      {
        code : 1,
        codeNm : '일반사용자'
      },
      {
        code : 2,
        codeNm : '설치기사'
      },
      {
        code : 3,
        codeNm : '회사관리자'
      },
      {
        code : 4,
        codeNm : '관리자'
      }
    ]);

    const handleOnChange = ({target : {name , value }}) => {
      if (name == 'insertUseYn'){
        name = 'useYn';
      } else if(name == 'clientCompanyId'){
        setCompanyId(value);
      } else if(name == 'tel'){
        if(!numberCheck(value)){
          notiStore.error("숫자만 입력해주세요.");
          return false;
        }
      }
      setInsertData({
        ...insertData,
        [name] : value
      });
    }

    const companyLists = async (item) => {
      try {
        const res = await callApi(API.COMPANY_LIST, {useYn : 'Y'});
        //const data : any = res.data;

        setCompList(res.data.data.content);
        setCompanyId(res.data.data.content[0].clientCompanyId);

      } catch {
        console.log("error");
        notiStore.error("회사 정보 조회에 실패했습니다.");
      }
    };

    const handleAddress = (data) => {
      setInsertData({
        ...insertData,
        email : data.email,
        name : data.name
      });
    };

    useEffect(() => {
      setInsertData({
        ...insertData,
        authId : 1,
        useYn : 'Y',
        clientCompanyId : companyId,
        tel : tel
      });

      if(item.length != 0){
        item.useYn = 'Y';
        item.authId = 1;
        item.clientCompanyId = companyId;
        item.tel = tel;
        setInsertData(item);
      }
    },[item]);

    useEffect(() => {
      companyLists({});
    }, []);

    return (
      <>
        <div id='aside_add' className='aside_edit' style={ show == 'aside_add' ? {display : 'block'} : {display : 'none'}}>
            <div className='titArea'>
                <h3>사용자 등록</h3>
            </div>
            <div className='contArea'>
                <div className='tit'>
                    <h4>사용자 정보</h4>
                </div>
                <div className='cont'>
                    <table className='tbl_edit'>
                        <caption className='hide'>사용자 정보</caption>
                        <colgroup>
                            <col width='30%' />
                            <col />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th scope='row'>사용자명</th>
                                <td>
                                    {/*<input type='text' className='ipt_txt' value={insertData.name} name="name" onChange={handleOnChange}/>*/}
                                    {/*<input type='text' className='ipt_txt_srch' value={insertData.name} readOnly />
                                    <button type='button' className='btn_ipt_srch'><span className='fas fa-search' title='검색'></span></button>*/}
                                    <input type='text' className='ipt_txt_srch' name="name" value={insertData.name} style={{width:'80%',display:'inline-block'}} onChange={handleOnChange}/>
                                    <a href='#lyr_user_srch' className='btn_lyr_open btn_ipt_srch' onClick={userSearchPopOnclick}><span className='fas fa-search' title='검색'></span></a>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>아이디 이메일</th>
                                <td>
                                    <input type='text' className='ipt_txt_srch' value={insertData.email} style={{width:'100%'}} name="email" onChange={handleOnChange}/>
                                    {/*<span className='ipt_field'>{insertData.userEmail}</span>*/}
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>사용자 연락처</th>
                                <td>
                                    <input type='tel' className='ipt_txt phone_numHyphen' maxLength={13} placeholder="'-'를 제외하고 숫자만 입력하세요."  name='tel' value={insertData.tel} onChange={handleOnChange} />
                                    {/*<input type='number' className='ipt_txt phone_numHyphen' placeholder="'-'를 제외하고 입력하세요."  name='hpNo' value={insertData.hpNo} onChange={handleOnChange} />*/}
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>회사명</th>
                                <td>
                                    <select className='select' name='clientCompanyId' value={insertData.clientCompanyId} onChange={handleOnChange}>
                                      {compList.map(
                                        (option, index) => <option key={index} value={option.clientCompanyId}>{option.clientCompanyName}</option>
                                      )}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>회사 ID</th>
                                <td>
                                    <span className='ipt_field'>{insertData.clientCompanyId == '' ? companyId : insertData.clientCompanyId}</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>권한유형</th>
                                <td>
                                    <select className='select' name='authId' value={insertData.authId} onChange={handleOnChange}>
                                      {authList.map(
                                        (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
                                      )}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th scope='row'>사용여부</th>
                                <td>
                                    <div className='radioBox'>
                                         <input type='radio' name='insertUseYn' id='user_insert_use' value='Y' className='ipt_radio' checked={(insertData.useYn == 'Y' || insertData.useYn == '') ? true : false} onChange={handleOnChange} />
                                        <label htmlFor='user_insert_use'>사용</label>
                                        <input type='radio' name='insertUseYn' id='user_insert_unused' value='N' className='ipt_radio' checked={insertData.useYn == 'N' ? true : false} onChange={handleOnChange} />
                                        <label htmlFor='user_insert_unused'>미사용</label>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='foo_btnBox'>
                <button type='button' className='btn_aside_close' onClick={close}>취소</button>
                <button type='button' className='ft_point' onClick={() => {userInsertCallback(insertData)}}>등록</button>
            </div>
        </div>
        <button type='button' className='btn_aside_close ico' onClick={close}>닫기</button>
      </>
    )
}

export default UserManagementInsert;
