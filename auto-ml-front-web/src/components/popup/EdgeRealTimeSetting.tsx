import React, {useContext, useState, useEffect} from 'react';
import RootStore from '../../store/RootStore';
import {ApiRoadPolicy} from 'components/edge/RoadRealTime';
import {getCodeList} from 'utils/CommonUtils';


interface Props{
  close:  () => void;
  show : string;
  policy? : ApiRoadPolicy;
  initRoiFunc? : () => void;
  modifyPolicyCallback : (e, item : any) => void;
}

const EdgeRealTimeSetting: React.FC<Props> = ({show, close, policy, initRoiFunc, modifyPolicyCallback}) => {

    if (show != 'aside_setup') {
      return <></>
    }

    const startLaneLength : number = 10;
    const rangeLaneLength : number = 2;


    const initData = {
      camDir : 'entry',
      oneWay : 'False',
      vehSide : 'front',
      startLane : 1,
      rangeLane : 1,
      url : 'empty',
      connect : 'rtsp',
      cameraDomain : 'normal'
    };

    const {userStore} = useContext(RootStore);
    const [data, setData] = useState<ApiRoadPolicy | undefined>(policy);
    const [codes, setCodes] = useState<any>({});

    const handleOnChange = ({target : {name, value}}) => {
      if (data) {
        setData({
          ...data,
          [name] : value
        });
      }

    };

    const resetData = () => {

      if (data) {
        setData({
          ...data,
          ...initData
        });
      }

      if (initRoiFunc) initRoiFunc();
    }

    useEffect(() => {
      if (policy && policy.startLane == 0) {
        setData({
          ...policy,
          ...initData
        });
      }

    },[policy]);

    useEffect(() => {
      getCodeList(['DIRECTION','CAM_DIR']).then((result : any) => {
        setCodes(result);
      });
    },[]);

    return (
      <>
      <div id='aside_setup' className='aside_edit' style={ show == 'aside_setup' ? {display : 'block'} : {display : 'none'}}>
        <div className='titArea'>
            <h3>차량 인식 설정</h3>
        </div>
        <div className='contArea'>
            <div className='tit'>
                <h4>차량 인식 정보</h4>
            </div>
            <div className='cont'>
                <table className='tbl_edit'>
                    <caption className='hide'>Edge 설정</caption>
                    <colgroup>
                        <col width='35%' />
                        <col />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th scope='row'>진출입 설정</th>
                            <td>
                                <div className='radioBox'>
                                    {
                                      codes.CAM_DIR && codes.CAM_DIR.length > 0 ? (
                                        codes.CAM_DIR.map(code => ((
                                          <React.Fragment key={code.code}>
                                            <input type='radio' name='camDir' id={'edge_postin_in' + code.code} value={code.code}  className='ipt_radio' checked={data && data.camDir == code.code ? true : false} onChange={handleOnChange} />
                                            <label htmlFor={'edge_postin_in' + code.code}>{code.codeNm}</label>
                                          </React.Fragment>
                                        )))
                                      ) : <></>
                                    }
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope='row'>인식 방향 설정</th>
                            <td>
                                <div className='radioBox'>
                                    {
                                      codes.DIRECTION && codes.DIRECTION.length > 0 ? (
                                        codes.DIRECTION.map(code => ((
                                          <React.Fragment key={code.code}>
                                            <input type='radio' name='vehSide' id={'radio_dertin_' + code.code} value={code.code} className='ipt_radio' checked={data && data.vehSide == code.code ? true : false} onChange={handleOnChange} />
                                            <label htmlFor={'radio_dertin_' + code.code}>{code.codeNm}</label>
                                          </React.Fragment>
                                        )))
                                      ) : <></>
                                    }
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope='row'>차로 인식 설정</th>
                            <td>
                                <dl>
                                    <dt>시작차로</dt>
                                    <dd>
                                        <select className='select' name='startLane' value={data && data.startLane ? data.startLane : ''} onChange={handleOnChange}>
                                          {[...Array(startLaneLength)].map((n, index) => (
                                              <option key={index} value={index + 1}>{index + 1}</option>
                                          ))}
                                        </select>
                                        차로
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>범위</dt>
                                    <dd>
                                        <select className='select' name='rangeLane' value={data && data.rangeLane ? data.rangeLane : ''} onChange={handleOnChange}>
                                          {[...Array(rangeLaneLength)].map((n, index) => (
                                              <option key={index} value={index + 1}>{index + 1}</option>
                                          ))}
                                        </select>
                                        개 차로
                                    </dd>
                                </dl>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div className='foo_btnBox'>
            <button type='button' className='btn_aside_close' onClick={() => {close()}}>취소</button>
            <button type='button' className='btn_line' onClick={resetData}>초기화</button>
            <button type='button' className='ft_point' onClick={(e) => {modifyPolicyCallback(e, data)}}>저장</button>
        </div>
    </div>
    <button type='button' className='btn_aside_close ico' onClick={() => {close()}}>닫기</button>
      </>
    )
}

export default EdgeRealTimeSetting;
