import React, {useState} from 'react';

interface Props {
  onChange: (item : any) => void;
  options: any;
  title: string;
  selectName : string;
  selectValue : any;
  inputName : string;
  inputValue : string;
  callback : (e) => void;
}

const ComSearchSelectInput: React.FC<Props> = ({callback, onChange, options, title, selectName, selectValue, inputName, inputValue}) => {

  const handleOnChange = ({target : {name, value}}) => {
    onChange({name : [name], value : value});
  }
  const [searchValue, setSearchValue] = useState({});

  const handleKeyPress = (e) => {
    //e.preventDefault();
    if(e.key == "Enter"){
      callback(searchValue);
    }
  };

  return (
    <dl>
        <dt>{title}</dt>
        <dd>
            <select className='select' name={selectName} onChange={handleOnChange} value={selectValue ? selectValue : '' }>
              <option value='all'>전체</option>
              {options ? options.map(
                (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
              ) : <></>}
            </select>&nbsp;
            <input type='text' className='ipt_txt' name={inputName} value={inputValue} onKeyDown={handleKeyPress} onChange={handleOnChange} />
        </dd>
    </dl>
  );
};

export default React.memo(ComSearchSelectInput);
