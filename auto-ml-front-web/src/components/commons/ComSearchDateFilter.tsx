import React ,{useState} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

  interface Props {
    title : string;
    name : string;
    selectFilterName : string;
    handleFilterClick : (e, name : string) => void;
    onCloseCallback : (item : any) => void;
  }

  const ComSearchDateFilter: React.FC<Props> = ({title, name, selectFilterName, handleFilterClick, onCloseCallback}) => {
    const [startDate, setStartDate] = useState(new Date()); //검색시작일
    const [endDate, setEndDate] = useState(new Date());     //검색종료일

    const handleSearchOnClick = () => {
      let item = {
        key : name,
        value : {
          startDate : startDate,
          endDate : endDate 
        }
      }
      onCloseCallback(item);
    }

    return (
      <th scope='col'>설치일
      <a title='기간설정' href='#none' onClick={(e) => {handleFilterClick(e, name)}}><span className='fas fa-calendar-check'></span></a>
      <div className={selectFilterName == name ? 'tbl_filter active' : 'tbl_filter'}>
          <dl>
              <dt>기간</dt>
              <dd className='date'>
                  {/*<input type='text' id='fromDate' className='ipt_txt' autoComplete='off' />*/}
                    <DatePicker id="startDate" className="ipt_txt" autoComplete='off'
                    selected={startDate}
                    onChange={date => setStartDate(date)}
                    dateFormat="yyyy-MM-dd"
                    />
                    <span className='dash' style={{float:'left',marginTop:'3px'}}> - </span>
                    <DatePicker id="endDate" className="ipt_txt" autoComplete='off'
                    selected={endDate}
                    onChange={date => setEndDate(date)}
                    dateFormat="yyyy-MM-dd"
                    />
                  {/*<input type='text' id='toDate' className='ipt_txt' autoComplete='off' />*/}
              </dd>
          </dl>
          <div className='btnBox'>
              <button type='button' className='btn_filter_close' onClick={handleSearchOnClick}>적용</button>
          </div>
      </div>
      </th>
    );
  };



  export default ComSearchDateFilter;
