import { Table } from 'antd';
import { SizeType } from 'antd/lib/config-provider/SizeContext';
import { ColumnsType } from 'antd/lib/table';
import { TableRowSelection } from 'antd/lib/table/interface';
import { ComApiList, IApiListResponse } from 'components/commons/ComApiListHooks';
import React, { useState } from 'react';
import styled from 'styled-components';
import { API } from 'utils/ApiClient';
import { deepCopyObject } from 'utils/CommonUtils';

interface DataInterface {
  id: number | string;
  [data: string]: any;
}

interface Props {
  type: 'STORE_LIST' | 'STORE_MENU_LIST' | 'STORE_MENU_CATEGORY_LIST';
  id?: number | string;
  columns: ColumnsType<any>;
  options?: {
    yScroll?: boolean;
    xScroll?: 'scroll' | 'fixed';
    bordered?: boolean;
  };
  rowSelection?: TableRowSelection<any>;
  dataSource?: DataInterface[];
  totalCount?: number;
  showTotalCount?: boolean;
  scroll?: {
    x?: number | true | string;
    y?: number | string;
  };
  size?: SizeType;
}

const PAGE_SIZE: number = 20;

const ComList: React.FC<Props> = ({ type, id, columns, rowSelection, scroll, size }) => {
  const [current, setCurrent] = useState<any>(0);
  const listApi = deepCopyObject(API[type]);

  if (id) {
    listApi.url = listApi.url.replace('_ID_', id);
  }

  return (
    <ComApiList api={listApi} page={current} size={PAGE_SIZE}>
      {(list: IApiListResponse) => {
        return (
          <Container>
            <Table
              size={size}
              rowKey='id'
              columns={columns}
              rowSelection={rowSelection}
              dataSource={list ? list.content : []}
              pagination={{
                size: 'default',
                current: current + 1,
                pageSize: PAGE_SIZE,
                total: list ? list.totalElements : 0,
              }}
              onChange={(pagination, filters, sorter: any) => {
                if (sorter.order) {
                  const order = sorter.order === 'ascend' ? 'asc' : 'desc';
                  const sort = `${sorter.field},${order}`;
                }

                setCurrent(pagination.current && pagination.current - 1);
              }}
              scroll={scroll}
              loading={!list}
            />
          </Container>
        );
      }}
    </ComApiList>
  );
};

const Container = styled.div`
  width: inherit;
  flex: 1;
`;

export default ComList;
