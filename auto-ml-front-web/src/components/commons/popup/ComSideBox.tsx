import React from 'react';
import ReactDOM from 'react-dom';


interface Props {
    children:  React.ReactNode[] | React.ReactNode;
    show: string;
}


const ComSideBox: React.FC<Props> = ({children, show}) => {
    const el : any = document.getElementById('aside');

    if ( el != null ) {
      if (show != '') {
          el.classList.add('active');
          return ReactDOM.createPortal(children, el);
      } else {
        el.classList.remove('active');
      }
    }


    return null;


};

export default React.memo(ComSideBox);
