import React, {useState} from 'react';

interface Props {
  title : string;
  name : string;
  selectFilterName : string;
  handleFilterClick : (e, name : string) => void;
  onCloseCallback : (item : any) => void;
}

const ComSearchFromToFilter: React.FC<Props> = ({title, name, selectFilterName, handleFilterClick, onCloseCallback}) => {
  const [fromInput, setFromInput] = useState('');
  const [toInput, setToInput] = useState('');

  const enterSearchFilter = (e) => {
    if (e.keyCode == 13) {
      handleSearchOnClick();
      setFromInput('');
      setToInput('');
    }
  };

  const handleSearchOnClick = () => {
    let item = {
      key : name,
      value : {
        from : fromInput,
        to : toInput
      }
    }
    onCloseCallback(item);
  }

  return (
    <th scope='col'>
        <span className='th_tit'>{title}</span>
        <a title='검색' href='#none' onClick={(e) => {handleFilterClick(e, name)}}><span className='fas fa-search'></span></a>
        <div className={selectFilterName == name ? 'tbl_filter active' : 'tbl_filter'}>
            <dl>
                <dt>검색</dt>
                <dd className='perct'>
                    <input type='number' className='ipt_num' autoComplete='off' maxLength={2} value={fromInput} onKeyDown={enterSearchFilter} onChange={(e) => {setFromInput(e.target.value)}} />%
                    <span className='dash'>-</span>
                    <input type='number' className='ipt_num' autoComplete='off' maxLength={3} value={toInput} onKeyDown={enterSearchFilter} onChange={(e) => {setToInput(e.target.value)}} />%
                </dd>
            </dl>
            <div className='btnBox'>
                <button type='button' className='btn_filter_close' onClick={handleSearchOnClick}>검색</button>
            </div>
        </div>
    </th>
  );
};

export default ComSearchFromToFilter;
