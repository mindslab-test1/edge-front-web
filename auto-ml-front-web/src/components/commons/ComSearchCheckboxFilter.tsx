import React, {useState, useEffect} from 'react';

interface Props {
  title : string;
  codeList : any;
  name : string;
  selectFilterName : string;
  handleFilterClick : (e, name : string) => void;
  onCloseCallback : (item : any) => void;
}

const ComSearchCheckboxFilter: React.FC<Props> = ({title, codeList, name, selectFilterName, handleFilterClick, onCloseCallback}) => {
  const [checkItems, setCheckItems] = useState<number[]>([]);

  const handleSingleCheck = (checked, code) => {
    if (checked) {
      setCheckItems([...checkItems, code]);
    } else {
      setCheckItems(checkItems.filter((el) => el != code));
    }
  };

  const handleAllCheck = (checked) => {
    if (checked) {
      const codeArr : number[] = [];

      codeList.map((item) => {
         codeArr.push(item.code);
      });

      setCheckItems(codeArr);
    } else {
      setCheckItems([]);
    }
  };

  const handleChooseClick = (e) => {
    e.preventDefault();

    let item = {
      key : name,
      value : checkItems
    }

    onCloseCallback(item);
  };

  return (
    <th scope='col'>
        <span className='th_tit'>{title}</span>
        <a title='필터' href='#none' onClick={(e) => {handleFilterClick(e, name)}}><span className='fas fa-filter'></span></a>
        <div className={selectFilterName == name ? 'tbl_filter active' : 'tbl_filter'}>
            <dl>
                <dt>필터</dt>
                <dd className='check'>
                    <ul>
                      <li>
                          <div className='checkBox'>
                              <input type='checkbox' name={name} id={name + 'All'} className='ipt_check all' checked={checkItems.length == codeList.length ? true : false} value='all' onChange={(e) => handleAllCheck(e.target.checked)} />
                              <label htmlFor={name + 'All'}>전체</label>
                          </div>
                      </li>
                        {
                          codeList.length > 0 ? (
                            codeList.map((item, index) => ((
                              <li key={index}>
                                  <div className='checkBox'>
                                      <input type='checkbox' name='edge_postin' id={name + '' + index} className='ipt_check' value={item.code} checked={checkItems.includes(item.code) ? true : false} onChange={(e) => handleSingleCheck(e.target.checked, item.code)} />
                                      <label htmlFor={name + '' + index}>{item.codeNm}</label>
                                  </div>
                              </li>
                            )))
                          ) : (
                            <></>
                          )
                        }
                    </ul>
                </dd>
            </dl>
            <div className='btnBox'>
                <button type='button' className='btn_filter_close' onClick={handleChooseClick}>적용</button>
            </div>
        </div>
    </th>
  );
};

export default ComSearchCheckboxFilter;
