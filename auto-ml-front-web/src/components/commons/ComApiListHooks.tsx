import React, { useEffect, useState } from 'react';
import { callApi, IApiDefinition } from 'utils/ApiClient';

export interface IApiListResponse {
  content: object[];
  pageable: {
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    pageNumber: number;
    pageSize: number;
    offset: number;
    paged: boolean;
    unpaged: boolean;
  };
  totalPages: number;
  totalElements: number;
  last: boolean;
  first: boolean;
  sort: {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
  };
  number: number;
  size: number;
  numberOfElements: number;
  empty: boolean;
}

interface IComApiListProps {
  api: IApiDefinition;
  page: number;
  size: number;
  // sort: any;
  // filter: object;
  // refreshCount?: number;
  children: any;
}

const useGetResultApi = (api: IApiDefinition, page: number, size: number) => {
  const [currPage, setCurrPage] = useState(-1);

  if (currPage !== page) {
    setCurrPage(page);
  }
  const [data, setData] = useState(null);
  useEffect(() => {
    const params = {
      page,
      size,
      sort: 'id,desc',
    };

    async function fetchData() {
      const res = await callApi(api, params);
      setData(res.data);
    }
    void fetchData();
  }, [currPage]);
  return data;
};

export const ComApiList: React.FC<IComApiListProps> = ({ api, page, size, children }) => {
  if (!children) return null;
  const list = useGetResultApi(api, page, size);
  return children(list);
};
