import React, {useContext} from 'react';
import RootStore from '../../store/RootStore';


interface Props{
  title : string;
  content : string;
  confirmCallback : (e) => void;
  close : (e) => void;
  closeCallback? : (e) => void;
}

const ContactItem: React.FC<Props> = ({title, content, confirmCallback, close, closeCallback}) => {

    const handleConfirmOnClick = (e) => {
      close(e);
      confirmCallback(e);
    };

    const handleConfirmCloseOnClick = (e) => {
      close(e);
      if (closeCallback) {
        closeCallback(e);
      }
    }

    return (
      <div className='altBox'>
        <div className='msgBox'>
            <p className='msgTxt'>{title}<span>{content}</span></p>
        </div>
        <div className='btnBox sz_small'>
            <button className='btn_alt_close' onClick={handleConfirmCloseOnClick}>취소</button>
            <button className='btn_altclose btn_clr' onClick={handleConfirmOnClick}>확인</button>
        </div>
      </div>
    )
}

export default React.memo(ContactItem);
