import React, {useState} from 'react';
import {ApiListResponse} from '../Project/common/ProjectList';
import ReactTooltip from 'react-tooltip';
import {ERouteUrl} from '../../router/RouteLinks';
import {Link} from 'react-router-dom';
import ComPopBox from './popup/ComPopBox';
import PopContact from '../popup/PopContact';

interface Props {
    item : ApiListResponse;
}

const ComTableRow: React.FC<Props> = ({item}) => {
    const [popShow, setPopShow] = useState(false);

    const setPopIsOpen = () => setPopShow(!popShow);
    const closePopup = () => setPopShow(false);
  const route = ERouteUrl.PROJECT_DETAIL.replace(':id', item.id.toString());

  // "q" = 대기열에서 학습 대기중
  // "t" = 학습 진행중
  // "c" = 학습 완료
  // "e" = 학습중 오류"
  let trainingState;
  if (item.status === 'q') trainingState = '학습 대기 중'
  else if(item.status === 't') trainingState = '학습 진행 중'
  else if(item.status === 'c') trainingState = '학습 완료'
  else if(item.status === 'e') trainingState = '학습 중지(오류)'
  else trainingState = '학습 설정 중'

  return(
      <>
          <tr>
              <td scope='row' ><ReactTooltip />
                <Link to={route} data-tip={item.projectDescription} data-background-color='#3c4c5d'>{item.projectName}</Link>
              </td>
              {/*<td className='userModelID'>{item.modelName}</td>*/}
              <td><span className='bg_rouBox stt'>{item.projectType}</span></td>
              <td>
                <span className='mdStatus step01'>{trainingState}</span>
                {/*<span className='mdStatus step02'>학습 설정 중</span> */}
                {/*<span className='mdStatus step03_pause'>학습 대기 중<em>11개</em></span> */}
                {/*<span className='mdStatus step03_play'>학습 진행 중</span> */}
                {/*<span className='mdStatus step03_stop'>학습 중지</span> */}
                {/*<span className='mdStatus step04'>학습 완료</span> */}
              </td>
              <td>{item.modDt}</td>
              <td>{item.regDt}</td>
              <td><span className='bg_rouBox'>{item.modelCnt}</span></td>
              <td><a className='btn_line btn_lyr_open' onClick={setPopIsOpen}>문의하기</a></td>
          </tr>
          <ComPopBox show={popShow}>
                {/*<PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true} modelName={item.modelName}/>*/}
          </ComPopBox>
      </>
  )
}

export default React.memo(ComTableRow);
