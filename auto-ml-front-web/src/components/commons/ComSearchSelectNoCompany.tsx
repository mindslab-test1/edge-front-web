import React from 'react';

interface Props {
  onChange: (item  :any) => void;
  options: any;
  title: string;
  value?: any;
  name : string;
}

const ComSearchSelect: React.FC<Props> = ({onChange, name, options, title, value}) => {
  const handleOnChange = ({target : {name, value}}) => {
    onChange({name : [name], value : value});
  };

  return (
    <dl>
        <dt>{title}</dt>
        <dd>
            <select className='select' title={title} name={name} onChange={handleOnChange} value={value ? value : '' } data-require='false' >
              <option value=''>선택</option>
              {options.map(
                (option, index) => <option key={index} value={option.clientCompanyId}>{option.clientCompanyName}</option>
              )}
            </select>
        </dd>
    </dl>
  );
};

export default ComSearchSelect;
