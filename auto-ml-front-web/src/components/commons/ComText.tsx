import styled from 'styled-components';
import { EColor } from 'styles/GlobalStyles';

interface TextProps {
  align?: 'center' | 'left' | 'right';
  weight?: 300 | 400 | 600 | 800;
  size?: '10px' | '12px' | '14px' | '16px' | '18px' | '20px' | '30px';
}

export const Text = styled.p<TextProps>`
  color: ${(props) => props.color || EColor.DEFAULT};
  /* font-size: 1em; */
  font-size: ${(props) => props.size || '14px'};
  font-weight: ${(props) => props.weight || 400};
  text-align: ${(props) => props.align || 'left'};
  margin: 0;
`;

export const TextLink = styled.a`
  font-size: 14px;
  text-decoration: underline;
  color: rgb(34, 153, 153, 1);
  cursor: pointer;

  
`;

export const Text12 = styled(Text)`
  font-size: 12px;
`;

export const Text14 = styled(Text)`
  font-size: 14px;
`;

export const Text16 = styled(Text)`
  font-size: 16px;
`;

export const Text20 = styled(Text)`
  font-size: 20px;
`;

export const Text24 = styled(Text)`
  font-size: 24px;
`;
