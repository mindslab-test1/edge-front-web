import React, {memo} from 'react';
import {ApiListResponse} from '../Project/common/ProjectList';
import ComTableRow from './ComTableRow';

interface Props {
    prjList : ApiListResponse[];
}

const ComTable: React.FC<Props> = ({ prjList}) => {
  return(
      <table className='tbl_lst'>
        <caption className='hide'>내가 만든 학습 프로젝트 목록</caption>
          <colgroup>
              <col /><col width='10%' /><col width='15%' /><col width='12%' /><col width='12%' />
              <col width='12%' /><col width='130' />
          </colgroup>
          <thead>
          <tr>
              <th scope='col'>프로젝트 명 & 설명</th>
              {/*<th scope='col'>모델 아이디</th>*/}
              <th scope='col'>모델 유형</th>
              <th scope='col'>진행상황</th>
              <th scope='col'>학습 시작일 시</th>
              <th scope='col'>학습 종료일 시</th>
              <th scope='col'>학습모델 수</th>
              <th scope='col'>문의하기</th>
          </tr>
          </thead>
        <tbody>
        {prjList.map((item)=>((
            <ComTableRow key={item.id} item={item}/>
        )))}
        </tbody>
      </table>
  )
}

export default React.memo(ComTable);
