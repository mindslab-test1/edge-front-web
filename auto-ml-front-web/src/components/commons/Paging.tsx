import React from 'react';

interface Props {
    totalPages : number;
    paginate :Function;
    currentPage :number;
}

const Paging: React.FC<Props> = ({totalPages,paginate,currentPage}) => {

    const pageNumbers:number[] = [];
    const pageSize : number = 10;
    const idx : number = (Math.floor(((currentPage - 1)/pageSize)) * pageSize) + 1;
    const lastPageNumber : number = (Math.ceil(currentPage/pageSize) * pageSize);
    const pageIdx : number = lastPageNumber > totalPages ? totalPages : lastPageNumber;

    for(let i = idx; i <= pageIdx; i++){pageNumbers.push(i);}

    const goFirst = () => {paginate(1)};

    const goPrev = () => {
      if (idx - pageSize >= 1) {
        paginate(idx - pageSize);
      }else if(currentPage !== 1) {
        paginate(currentPage-1);
      }
    };

    const goNext = () => {
      if (idx + pageSize <= totalPages) {
        paginate(idx + pageSize);
      }else if(currentPage !== totalPages) {
        paginate(currentPage+1);
      }
    };

    const goEnd = () => {paginate(totalPages)};

    return (
        <div className='pageing'>
            <a className='first' href={void(0)} onClick={goFirst}>맨 처음 페이지로</a>
            <a className='prev' href={void(0)} onClick={goPrev}>10개씩 이동</a>
            {pageNumbers.map(
                (pagingNumber,index) =>
                    currentPage === pagingNumber ? <strong key={index}>{pagingNumber}</strong> : <a key={index} href={void(0)} onClick={() => paginate(pagingNumber)}>{pagingNumber}</a>
            )}
            <a className='next' href={void(0)} onClick={goNext}>10개씩 이동</a>
            <a className='end' href={void(0)} onClick={goEnd}>맨 마지막 페이지로</a>
        </div>
    )
}
export default React.memo(Paging);
