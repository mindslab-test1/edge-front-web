import React ,{useState} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

interface Props {
  onChange: (e) => void;
  title: string;
  name : string;
  startDateName : string;
  startDateValue : any;
  endDateName : string;
  endDateValue : any;
}

const ComSearchDate: React.FC<Props> = ({onChange, title, name, startDateName, startDateValue, endDateName, endDateValue}) => {

  const [minDate, setMinDate] = useState<Date | null>(null);
  const [maxDate, setMaxDate] = useState<Date | null>(null);
  const [maxTime, setMaxTime] = useState<Date | null>(null);
  const [minTime, setMinTime] = useState<Date | null>(null);

  const dateOnChange = (type : string, data : any) => {
    let selectDate : Date | null = data.value != null ? new Date(data.value) : null;

    if (selectDate == null) {

      switch (type) {
        case 'start' :
          setMinDate(selectDate);
          setMinTime(selectDate);
          break;
        case 'end' :
          setMaxDate(selectDate);
          setMaxTime(selectDate);
          break;
      }

    } else if (type == 'start') {

      if (endDateValue instanceof Date && selectDate.getTime() > endDateValue.getTime()) {
        selectDate = endDateValue;
        data.value = endDateValue;
      }

      setMinDate(selectDate);

      const minMaxDate : Date | null = calculateMinTime( (maxDate == null) ? new Date() : maxDate );
      if (minMaxDate != null && selectDate != null) {
        if (selectDate.getTime() < minMaxDate.getTime()) {
          setMaxTime(calculateMaxTime(maxDate));
          setMinTime(calculateMinTime(selectDate));
        } else {
          setMaxTime(maxDate);
          setMinTime(selectDate);
        }
      }

    } else if (type == 'end') {

      if (startDateValue instanceof Date && startDateValue.getTime() > selectDate.getTime()) {
        selectDate = startDateValue;
        data.value = startDateValue;
      }

      setMaxDate(selectDate);

      const maxMinDate : Date | null = calculateMaxTime((minDate == null) ? new Date() : minDate);
      if (maxMinDate != null && selectDate != null) {
        if (selectDate.getTime() > maxMinDate.getTime()) {
          setMinTime(calculateMinTime(minDate));
          setMaxTime(calculateMaxTime(selectDate));
        } else {
          setMinTime(minDate);
          setMaxTime(selectDate);
        }
      }

    }

    onChange(data);
  }

  const calculateMinTime = date => {

    if (date == null) return null;

    const time : Date = moment(date).toDate();
    time.setHours(0);
    time.setMinutes(0);
    time.setSeconds(0);
    time.setMilliseconds(0);

    return time;
  }

  const calculateMaxTime = date => {
    if (date == null) return null;

    const time : Date = moment(date).toDate();
    time.setHours(23);
    time.setMinutes(59);
    time.setSeconds(59);
    time.setMilliseconds(0);

    return time;
  }

  return (
    <dl>
        <dt>{title}</dt>
        <dd>
            <div className='date'>
                  <DatePicker id="startDate" className="ipt_dateTime" autoComplete='off'
                  onChange={(date) => { dateOnChange('start', {name : startDateName, value : date}) }}
                  selected={startDateValue}
                  name='startDate'
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={5}
                  timeCaption="time"
                  maxDate={maxDate}
                  minTime={calculateMinTime(maxDate)}
                  maxTime={maxTime}
                  dateFormat="yyyy-MM-dd HH:mm"
                  />
                  &nbsp;<span>-</span>&nbsp;
                  <DatePicker id="endDate" className="ipt_dateTime" autoComplete='off'
                  onChange={(date) => { dateOnChange('end', {name : endDateName, value : date}) }}
                  selected={endDateValue}
                  name='endDate'
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={5}
                  timeCaption="time"
                  minDate={minDate}
                  maxTime={calculateMaxTime(minDate)}
                  minTime={minTime}
                  dateFormat="yyyy-MM-dd HH:mm"
                  />
            </div>
        </dd>
    </dl>
  );
};



export default ComSearchDate;
