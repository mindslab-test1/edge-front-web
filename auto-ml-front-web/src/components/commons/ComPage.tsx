import React, {memo} from 'react';


interface Props {
  title: string;
  subTitle: string;
  engineName : string;
  isVisibleSpan : boolean;
  children: React.ReactNode[] | React.ReactNode;
}

const ComPage: React.FC<Props> = ({ title,subTitle, isVisibleSpan, engineName, children}) => {

    return (
        <>
          <div className='contents'>
            <div className='titArea'>
                <h3>{title}</h3>
            </div>
            {children}
          </div>
        </>
    );

};




export default ComPage;
