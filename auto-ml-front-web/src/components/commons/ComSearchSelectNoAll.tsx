import React from 'react';

interface Props {
  onChange: (item  :any) => void;
  options: any;
  title: string;
  value?: any;
  name : string;
}

const ComSearchSelect: React.FC<Props> = ({onChange, name, options, title, value}) => {
  const handleOnChange = ({target : {name, value}}) => {
    onChange({name : [name], value : value});
  };

  return (
    <dl>
        <dt>{title}</dt>
        <dd>
            <select className='select' name={name} onChange={handleOnChange} value={value ? value : '' }>
              {options.map(
                (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
              )}
            </select>
        </dd>
    </dl>
  );
};

export default ComSearchSelect;
