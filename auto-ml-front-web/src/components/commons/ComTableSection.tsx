import    React, {useCallback, useEffect, useState} from 'react';
import {ApiListResponse} from '../Project/common/ProjectList';
import Paging from './Paging';
import {API, callApi} from '../../utils/ApiClient';
import ComTable from './ComTable';
import ComPopBox from './popup/ComPopBox';
import PopContact from '../popup/PopContact';

interface Props {

}

const ComTableSection: React.FC<Props> = () => {
    const [projectList, setProjectList] = useState<ApiListResponse[]>([]);
    const [totalPages, setTotalPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    // const [popShow, setPopShow] = useState(false);

    // const setPopIsOpen = () => setPopShow(!popShow);
    // const closePopup = () => setPopShow(false);
    const callApiProjectList = useCallback(async (pageNum) => {
        try {
            const res = await callApi(API.PROJECT_LIST, {page: pageNum - 1, size: 10});
            // console.log(res);
            setProjectList(res.data.content);
            setTotalPages(res.data.totalPages);
            setCurrentPage(pageNum)

        } catch {
            console.log('error');
        }
    },[totalPages]);

    useEffect(() => {
        callApiProjectList(1).then(r => {
          console.log('call projectList');
        });
    }, [])

  return(
      <>
          <div className='tbl_lstBox'>
              <ComTable prjList={projectList}/>
          </div>
          <Paging totalPages={totalPages} paginate={callApiProjectList} currentPage={currentPage}/>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(ComTableSection);
