import React, {useState} from 'react';

interface Props {
  title : string;
  name : string;
  selectFilterName : string;
  handleFilterClick : (e, name : string) => void;
  onCloseCallback : (item : any) => void;
}

const ComSearchFilter: React.FC<Props> = ({title, name, selectFilterName, handleFilterClick, onCloseCallback}) => {
  const [input, setInput] = useState('');

  const enterSearchFilter = (e) => {
    if (e.keyCode == 13) {
      handleSearchOnClick();
      setInput('');
    }
  };

  const handleSearchOnClick = () => {
    let item = {
      key : name,
      value : input
    }
    onCloseCallback(item);
  }

  return (
    <th scope='col'>{title}
        <a title='검색' href='#none'><span className='fas fa-search' onClick={(e) => {handleFilterClick(e, name)}}></span></a>
        <div className={selectFilterName == name ? 'tbl_filter active' : 'tbl_filter'}>
            <dl>
                <dt>검색</dt>
                <dd className='srch'>
                    <input type='text' className='ipt_txt' autoComplete='off' value={input} onKeyDown={enterSearchFilter} onChange={(e) => {setInput(e.target.value)}} />
                </dd>
            </dl>
            <div className='btnBox'>
                <button type='button' className='btn_filter_close' onClick={handleSearchOnClick}>검색</button>
            </div>
        </div>
    </th>
  );
};

export default ComSearchFilter;
