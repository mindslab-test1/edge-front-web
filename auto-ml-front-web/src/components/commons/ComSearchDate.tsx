import React ,{useState} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface Props {
  onChange: (e) => void;
  title: string;
  name : string;
  startDateName : string;
  startDateValue : any;
  endDateName : string;
  endDateValue : any;
}

const ComSearchDate: React.FC<Props> = ({onChange, title, name, startDateName, startDateValue, endDateName, endDateValue}) => {

  const [minDate, setMinDate] = useState<Date | null>(null);
  const [maxDate, setMaxDate] = useState<Date | null>(null);

  const dateOnChange = (type : string, data : any) => {
    const selectDate : Date | null = data.value != null ? new Date(data.value) : null;

    switch(type) {
      case 'start' :
        setMinDate(selectDate);
        break;
      case 'end' :
        setMaxDate(selectDate);
        break;
    }

    onChange(data);
  }

  return (
    <dl>
        <dt>{title}</dt>
        <dd>
            <div className='date'>
                  <DatePicker id="startDate" className="ipt_date" autoComplete='off'
                  onChange={(date) => { dateOnChange('start',{name : startDateName, value : date}) }}
                  selected={startDateValue}
                  maxDate={maxDate}
                  name='startDate'
                  dateFormat="yyyy-MM-dd"
                  />
                  &nbsp;<span>-</span>&nbsp;
                  <DatePicker id="endDate" className="ipt_date" autoComplete='off'
                  onChange={(date) => { dateOnChange('end',{name : endDateName, value : date}) }}
                  selected={endDateValue}
                  minDate={minDate}
                  name='endDate'
                  dateFormat="yyyy-MM-dd"
                  />
            </div>
        </dd>
    </dl>
  );
};



export default React.memo(ComSearchDate);
