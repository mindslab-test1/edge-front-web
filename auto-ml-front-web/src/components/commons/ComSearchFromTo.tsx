import React from 'react';

interface Props {
  onChange: (item : any) => void;
  title: string;
  fromName : string;
  fromValue : string;
  toName : string;
  toValue : string;
}

const ComSearchFromTo: React.FC<Props> = ({onChange, title, fromName, fromValue, toName, toValue}) => {

  const handleOnChange = ({target : {name, value}}) => {
    onChange({name : [name], value : value});
  }

  return (
    <dl>
        <dt>{title}</dt>
        <dd>
          <input type='number' className='ipt_txt' style={{width:'20px'}} maxLength={4} autoComplete='off' name={fromName} value={fromValue} onChange={handleOnChange} />%
          &nbsp;<span>-</span>&nbsp;
          <input type='number' className='ipt_txt' style={{width:'20px'}} maxLength={4} autoComplete='off' name={toName} value={toValue} onChange={handleOnChange} />%
        </dd>
    </dl>
  );
};

export default React.memo(ComSearchFromTo);
