import React from 'react';

interface Props {
  onChange: (item  :any) => void;
  options: any;
  title: string;
  value?: any;
  name : string;
}

const ComSearchSelect: React.FC<Props> = ({onChange, name, options, title, value}) => {
  const handleOnChange = ({target : {name, value}}) => {
    onChange({name : [name], value : value});
  };

  return (
    <select className='select' name={name} title={title} onChange={handleOnChange} value={value ? value : '' }>
      <option value=''>선택</option>
      {options ? options.map(
        (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
      ) : <></>
    }
    </select>
  );
};

export default React.memo(ComSearchSelect);
