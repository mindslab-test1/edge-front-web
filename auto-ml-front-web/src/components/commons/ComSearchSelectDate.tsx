import React ,{useState} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface Props {
  onChange: (e) => void;
  title: string;
  name : string;
  startDateName : string;
  startDateValue : any;
  endDateName : string;
  endDateValue : any;
  options : any;
  selectName : string;
  selectValue : any;
}

const ComSearchDate: React.FC<Props> = ({onChange, title, name, startDateName, startDateValue, endDateName, endDateValue, options, selectName, selectValue}) => {

  const handleOnChange = ({target : {name, value}}) => {
    onChange({name : [name], value : value});
  }

  return (
    <dl>
        <dt>{title}</dt>
        <dd>
            <select className='select' name={selectName} onChange={handleOnChange} value={selectValue ? selectValue : '' }>
              <option value='all'>전체</option>
              {options.map(
                (option, index) => <option key={index} value={option.code}>{option.codeNm}</option>
              )}
            </select>&nbsp;
            <div className='date'>
                  <DatePicker id="startDate" className="ipt_dateTime" autoComplete='off'
                  onChange={(date) => { onChange({name : startDateName, value : date}) }}
                  selected={startDateValue}
                  name='startDate'
                  dateFormat="yyyy-MM-dd"
                  />
                  &nbsp;<span>-</span>&nbsp;
                  <DatePicker id="endDate" className="ipt_dateTime" autoComplete='off'
                  onChange={(date) => { onChange({name : endDateName, value : date}) }}
                  selected={endDateValue}
                  name='endDate'
                  dateFormat="yyyy-MM-dd"
                  />
            </div>
        </dd>
    </dl>
  );
};



export default React.memo(ComSearchDate);
