import React, {useContext, useState, useEffect, useCallback} from 'react';
import { Link, useLocation, withRouter, useHistory } from 'react-router-dom';
import ComPopBox from './popup/ComPopBox';
import PopContact from '../popup/PopContact';
import imgVisual01 from 'assets/images/img_visual01.png';
import RootStore from '../../store/RootStore';
import { ERouteUrl } from 'router/RouteLinks';
import {observer} from 'mobx-react-lite';
import {API, callApi} from 'utils/ApiClient';
import { initPopupFunc } from 'utils/CommonUtils';

interface Props {}

interface HeaderMenu {
  id: number;
  menuCode: string;
  menuName: string;
  url: string;
}

const ModelContact: React.FC<Props> = () => {

  const { userStore, layoutStore, notiStore } = useContext(RootStore);
  const [popShow, setPopShow] = useState(false);
  let history = useHistory();
  const pathName = useLocation().pathname;
  const [payList, setPayList] = useState<HeaderMenu[]>();

  const callApiHeaderMenuList = useCallback(async () => {
    let isError : boolean = false;

    try {
      const res = await callApi(API.HEADER_MENU);
      if (res.success) {
        setPayList(res.data);
      } else {
        isError = true;
      }

    } catch {
      isError = true;
    } finally {
      if (isError) notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
    }

  },[]);

  useEffect(() => {
    callApiHeaderMenuList();
  },[]);

  const setPopIsOpen = () => {
     console.log('Model == setPopIsOpen');
     setPopShow(!popShow);
  }

  const closePopup = () => {
      setPopShow(false);
  }

  const handleTopMenuOnClick = (e, item : HeaderMenu) => {
    e.preventDefault();
    initPopupFunc();

    history.push(item.url);
  }

  return(

      <div className='svc_sta'>

          <div className='svc_visual'>
              <h3>Edge Analysis</h3>
              <p className='bg_img'><img src={imgVisual01} alt='autoML 이미지' /></p>
          </div>

          <div className='lnb'>
              <h2><a href="/home">Edge Analysis</a></h2>
              <ul className="nav">
                {/* <li><Link className={ERouteUrl.HOME == pathName ? 'active' : ''} to={ERouteUrl.HOME}>Home</Link></li> */}
                {
                  payList && payList.length > 0 ? (
                    payList.map((item, index) => ((
                      <li key={index}>
                        <a className={layoutStore.topMenu == item.id ? 'active' : ''} onClick={(e) => {handleTopMenuOnClick(e, item)}} >{item.menuName}</a>
                      </li>
                    )))

                  ) : ''
                }
              </ul>
              <ul className='nav_etc'>
                <li><a href='/'>Edge AI Platform</a></li>
                <li><a id='btn_contact' className='btn_rou_pstive btn_lyr_open' onClick={setPopIsOpen}>문의하기</a></li>
              </ul>
          </div>
          <ComPopBox show={popShow}>
              <PopContact close={closePopup} popTitle={'Edge Analysis 문의하기'} />
          </ComPopBox>
      </div>

 );

};

export default observer(ModelContact);
