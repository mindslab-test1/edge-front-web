import React, {memo, useState} from 'react';

interface Props {
  callback : (e) => void;
  initBtnCallback : (e) => void;
  children?:  React.ReactNode[] | React.ReactNode;
}

const ComSearchBar: React.FC<Props> = ({callback, children, initBtnCallback}) => {
  const [searchValue, setSearchValue] = useState({});

  const handleOnChange = ({ target: { name, value } }) => {
    setSearchValue({
      ...searchValue,
      [name] : value
    });
  }

  const handleOnClick = () => {
    callback(searchValue);
  };

  return(
      <>
        <div className='tblSrchBox'>
          <div className='srch_field'>
            {children}
          </div>
          <div className='srch_btn'>
              <button type='submit' className='btn_clr' onClick={handleOnClick}>검색</button>&nbsp;
              <button type='button' onClick={initBtnCallback}>검색 초기화</button>
          </div>
        </div>
      </>
  )
}

export default React.memo(ComSearchBar);
