import React from 'react';
import { Layout, Row, Col } from 'antd';
import styled from 'styled-components';
import { Text, Text20 } from 'components/commons/ComText';

const _Footer = Layout.Footer;

const Footer: React.FC = () => {
  return (
    <_Footer>
      <Content>
        <Row gutter={32}>
          <Col>
            <Text20 weight={600}>(주) </Text20>
          </Col>
          <Col span={7}>
            <Row>
              <Col span={24} style={{ marginBottom: 6 }}>
                <Text>(주) </Text>
              </Col>
              <Col span={24} style={{ marginBottom: 6 }}>
                <Text>--</Text>
              </Col>
              <Col span={24} style={{ marginBottom: 6 }}>
                <Text>사업자 등록번호 : </Text>
              </Col>
              <Col span={24} style={{ marginBottom: 6 }}>
                <Text>--</Text>
              </Col>
            </Row>
          </Col>

          <Col>
            <Row>
              <Col span={24} style={{ marginBottom: 6 }}>
                <Text>
                  <span style={{ fontWeight: 600 }}>연락처 :</span>
                </Text>
              </Col>
              <Col span={24}>
                <Text>
                  <span style={{ fontWeight: 600 }}>이메일 :</span>
                </Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </Content>
    </_Footer>
  );
};

const Content = styled.div`
  border-top: 1px solid #a8a8a8;
  height: 200px;
  padding: 20px;
`;

export default Footer;
