import React, { useContext, useEffect, useState, useCallback, useLayoutEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { Link, useLocation, useHistory } from 'react-router-dom';
// import { AppstoreOutlined } from '@ant-design/icons';
// import { Layout, Menu } from 'antd';
import { ERouteUrl } from 'router/RouteLinks';
import RootStore from '../../../store/RootStore';
import {API, callApi} from 'utils/ApiClient';
import { initPopupFunc } from 'utils/CommonUtils';

// const { Sider } = Layout;
// const { SubMenu } = Menu;

interface Props {}

interface SideMenuDefinition {
  id : number;
  depth : number;
  menuCode : string;
  menuName : string;
  topMenuId : number;
  url : string;
  icoName : string;
}

// let key = 1;

const SideMenuList: React.FC<Props> = () => {
  const {layoutStore, userStore, notiStore} = useContext(RootStore);
  const { showTopMenu } = layoutStore;
  const pathName : string = useLocation().pathname;
  const [sideMenuList, setSideMenuList] = useState<SideMenuDefinition[]>();
  const [node, setNode] = useState<React.ReactNode>();
  let history = useHistory();

  const callApiSideMenuList = useCallback(async () => {
    let isError : boolean = false;

    try {
      const res = await callApi(API.SIDE_MENU);
      if (res.success) {
        setSideMenuList(res.data);
      } else {
        isError = true;
      }

    } catch {
      isError = true;
    } finally {
      if (isError) notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
    }

  },[]);

  const handleSideMenuOnClick = (e, item : SideMenuDefinition) => {
    e.preventDefault();
    initPopupFunc();
    history.push(item.url);
  }

  useEffect(() => {
    callApiSideMenuList();
  },[]);

  useEffect(() => {
    setNode(handleSideBar());
  },[pathName, sideMenuList]);


  const handleSideBar = () => {

    let data : Array<any> = [];
    let itemArr : Array<any> = [];
    let topMenuNm : string = '';
    let selectTopMenu : number = 0;

    if (sideMenuList && sideMenuList.length > 0) {
      let selMenu = sideMenuList.find((item) => {
        return pathName === item.url;
      });

      if (!selMenu) return <></>;
      selectTopMenu = selMenu.topMenuId;
      showTopMenu(selectTopMenu);

      for (let i = 0; i < sideMenuList.length; i++) {
        let menu = sideMenuList[i];

        if (selectTopMenu != menu.topMenuId) continue;

        if (menu.depth == 1) {
          if (itemArr.length > 0) {
            data.push(sideMenu(itemArr, topMenuNm + 'Ul'));
            itemArr = [];
          }
          topMenuNm = menu.menuName;
          data.push(<span key={topMenuNm} className='nav_tit'>{menu.menuName}</span>);
        } else {
          itemArr.push(menu);
        }

      }

      if (itemArr.length > 0) {
        data.push(sideMenu(itemArr, topMenuNm + 'Ul'));
        itemArr = [];
      }

    } else {
      data.push(null);
    }

    return data;

  };

  const sideMenu = (itemArr, key) => {

    return (
      <ul className='nav' key={key}>
        {
          itemArr.map((item, index) => {
            return (
              <li key={item.menuName}><a onClick={(e) => {handleSideMenuOnClick(e,item)}} className={pathName == item.url ? 'active' : ''} ><span className={'fas ' + item.icoName}></span>{item.menuName}</a></li>
            )
          })
        }
      </ul>
    )
  }

  return (
    <div className='snb ach'>
      <div className='navBox'>
        {node}
      </div>
    </div>
  );
};

export default observer(SideMenuList);
