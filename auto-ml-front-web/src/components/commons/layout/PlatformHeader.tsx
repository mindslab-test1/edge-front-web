import React, {useEffect, useState} from 'react';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';

import ComLoginBox from './header/ComLoginBox';
import ComLanguageBox from './header/ComLanguageBox';
import ComAppBox from './header/ComAppBox';

import img_visual01 from 'assets/images/img_visual01.png'


interface Props {}

const PlatformHeader: React.FC<Props> = () => {

    const tabList = [
        {
            id : 'stn_overview',
            title : 'Overview'
        },
        {
            id : 'stn_edgeDevice',
            title : 'Edge AI Device'
        },
        {
            id : 'stn_edgeCloud',
            title : 'AI Computing Cloud'
        },
        {
            id : 'stn_svcCase',
            title : '고객 사례'
        },
        {
            id : 'stn_inquiry',
            title : '문의하기'
        }
    ]
    const [select, setSelect] = useState(0);
    const tabSelect = (e, index, itemId) => {

        setSelect(index);
        let headerHeight : number = 0;

        const headerChilds : NodeListOf<HTMLElement> = document.querySelectorAll('#header > div');
        Array.from(headerChilds, child => {
          headerHeight += child!.offsetHeight;
        });

        let moveScrollTop : number = document.getElementById(itemId)!.offsetTop - headerHeight;

        window.scroll({ top: moveScrollTop, behavior: "smooth" });
    }

    const handleScroll = () => {
        let scrollTop : number =  window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        let imgPstion : number = scrollTop;

        let wrap : HTMLElement | null = document.getElementById('wrap');
        if (scrollTop > 0) {
          wrap!.classList.add('transform');
        } else {
          wrap!.classList.remove('transform');
        }

        if (scrollTop > 10) {
          document.querySelector<HTMLElement>('.svc_visual .bg_img')!.style.transform = `matrix(1,0,0,1,0, -${imgPstion > 100 ? 100 : imgPstion})`;
        } else {
          document.querySelector<HTMLElement>('.svc_visual .bg_img')!.style.transform = `matrix(1,0,0,1, 0, 0)`;
        }

        if (scrollTop > 260) {
          wrap!.classList.add('trans');
        } else {
          wrap!.classList.remove('trans');
        }

    }

    useEffect(()=>{
      window.addEventListener('scroll', handleScroll);
      return () => {
        window.removeEventListener('scroll', handleScroll);
      }
    }, [])

    return (

        <div id='header'>
          <div className='maum_sta'>
            <h1><a href='https://maum.ai'>maum.ai</a></h1>

            <div className='maum_gnb'>
              <ul className='nav'>
                <li><a href='https://maum.ai/?lang=ko#service_position'>서비스</a></li>
                <li><a href='http://maumacademy.maum.ai/'>마음 아카데미</a></li>
              </ul>
            </div>

            <div className='maum_etc'>
              <ul className='nav'>
                <li><a href='https://maum.ai/home/pricingPage'>가격정책</a></li>
                <ComLoginBox/>
                <ComLanguageBox/>
                <ComAppBox/>
              </ul>
            </div>

          </div>

          {/* .svc_sta */}
          <div className='svc_sta'>
            {/* svc_visual */}
            <div className='svc_visual'>
              <h3>Edge AI Platform</h3>
              <p className='bg_img'><img src={img_visual01} alt='Edge AI 이미지'/></p>
            </div>
            {/* //svc_visual */}
            {/* .lnb */}
            <div className='lnb'>
              <h2><a href='#none'>Edge AI Platform</a></h2>
              {/* [D] 로컬메뉴  */}
              <ul className='nav'>
                  {tabList.map((item, index) => (
                      <li key={index}><a className={index === select ? 'active' : ''} onClick={(e) => {tabSelect(e, index, item.id)}}>{item.title}</a></li>
                  ))}
              </ul>
              <ul className='nav_etc'>
                <li><Link to='/home'>Edge Analysis</Link></li>
              </ul>
            </div>
            {/* //.lnb */}
          </div>
          {/* //.svc_sta */}

        </div>

    );
};

export default observer(PlatformHeader);
