import React, {useState} from 'react';
import ComToggleBox from './ComToggleBox';

interface Props {
}

const ComLanguageBox: React.FC<Props> = () => {


  return (
    <ComToggleBox title='언어' clsName='lang'>
      <div className='lstBox'>
        <ul className='lst'>
          <li><a href="#none">한국어</a></li>
          <li><a href="#none">English</a></li>
          {/*<li><a href=''>English</a></li>*/}
        </ul>
      </div>
    </ComToggleBox>
  );
};

export default ComLanguageBox;
