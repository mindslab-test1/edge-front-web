import React, {useContext, useState} from 'react';
import ComToggleBox from './ComToggleBox';
import RootStore from '../../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import {logoutProcess, removeUserToken} from '../../../../service/auth/AuthService';
import {setHistory} from '../../../../utils/Navigation';
import {ERouteUrl} from '../../../../router/RouteLinks';
import {Link} from 'react-router-dom';
import icoUserG from 'assets/images/ico_user_g.svg';

interface Props {
}

const loginBtn = () => {
    // window.location.href='https://dev-sso.maum.ai/maum/loginMain?response_type=code&client_id=autoML.local&redirect_uri=http://localhost:10000/login/complete'
    window.location.href= process.env.REACT_APP_SSO_LOGIN_URL + '?response_type=code&client_id='
      + process.env.REACT_APP_SSO_CLIENT_ID + '&redirect_uri=' + process.env.REACT_APP_CALLBACK_URL;
}

const ComLoginBox: React.FC<Props> = () => {
    const { userStore } = useContext(RootStore);

    const logout = (e) => {
        e.preventDefault();
        logoutProcess().then(res => {
          removeUserToken();
          userStore.handleSetProfile(null);
        });

    }

    if (userStore.email === '' || userStore.email === undefined) {
        return (<li><a onClick={loginBtn}>로그인</a></li>)
    }else {
        return (
            <ComToggleBox title='사용자' clsName='user'>
            <div className='lstBox'>
                <ul className='lst'>
                    <li className='userInfo'>
                        <span className='thumb'><img src={icoUserG} alt='사용자' /></span>
                        <span className='txt'>
                                <em className='userName'>{userStore.name}</em>
                                <em className='loginID'>{userStore.email}</em>
                            </span>
                    </li>
                    <li><a href='https://maum.ai/user/profileMain'>프로필</a></li>
                    <li><a href='https://maum.ai/user/apiAccountMain'>API 정보</a></li>
                    <li><a href='https://maum.ai/user/paymentInfoMain'>결제정보</a></li>
                    <li><a onClick={(e) => {logout(e)}}>로그아웃</a></li>
                </ul>
            </div>
        </ComToggleBox>)
    }

};

export default observer(ComLoginBox);
