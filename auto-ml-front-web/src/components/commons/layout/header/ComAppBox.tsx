import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import ComToggleBox from './ComToggleBox';
interface Props {
}

const ComAppBox: React.FC<Props> = () => {


  return (
      <ComToggleBox title='앱' clsName='app'>
          <div className='appBox'>
              <div className='tit'>웹 앱 및 서비스</div>
              <ul className='lst'>
              <li>
                  <a href='https://maum.ai/serviceLanding/krCloudApiMain' target='_blank'>
                      <span className='ico'></span>
                      <em>Cloud API</em>
                  </a>
              </li>
              <li>
                  <a href='https://builder.maum.ai/landing?lang=ko' target='_blank'>
                      <span className='ico'></span>
                      <em>AI Builder</em>
                  </a>
              </li>
              <li>
                  <a href='https://minutes.maum.ai/' target='_blank'>
                      <span className='ico'></span>
                      <em>maum 회의록</em>
                  </a>
              </li>
              <li>
                  <a href='https://fast-aicc.maum.ai/login?lang=ko' target='_blank'>
                      <span className='ico'></span>
                      <em>FAST 대화형 AI</em>
                  </a>
              </li>
              <li>
                  <a href='https://ttsbuilder.maum.ai/login' target='_blank'>
                      <span className='ico'></span>
                      <em>TTS Builder</em>
                  </a>
              </li>
              <li>
                  <a href='/'>
                      <span className='ico'></span>
                      <em>Edge AI Platform</em>
                  </a>
              </li>
              <li>
                  <a href='https://data.maum.ai/?lang=ko' target='_blank'>
                      <span className='ico'></span>
                      <em>maum DATA</em>
                  </a>
              </li>
              <li>
                  <a href='https://maum.ai/home/krEcomindsMain' target='_blank'>
                      <span className='ico'></span>
                      <em>eco MINDs</em>
                  </a>
              </li>
              </ul>
          </div>
      </ComToggleBox>
  );
};

export default ComAppBox;
