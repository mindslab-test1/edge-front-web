import React, { useContext, useEffect, useState, useCallback } from 'react';
import { observer } from 'mobx-react-lite';
import { Link, useLocation } from 'react-router-dom';
// import { AppstoreOutlined } from '@ant-design/icons';
// import { Layout, Menu } from 'antd';
import { ERouteUrl } from 'router/RouteLinks';
import RootStore from '../../../store/RootStore';
import {API, callApi} from 'utils/ApiClient';
import SideMenuList from './SideMenuList';

// const { Sider } = Layout;
// const { SubMenu } = Menu;

interface Props {}

interface SideMenuDefinition {
  id : number;
  depth : number;
  menuCode : string;
  menuName : string;
  topMenuId : number;
  url : string;
  icoName : string;
}

// let key = 1;

const Sidebar: React.FC<Props> = () => {
  const {layoutStore} = useContext(RootStore);
  const { showSidebar } = layoutStore;



  return (
    <>
      {
        showSidebar ? <SideMenuList /> : null
      }
    </>
  );
};

export default observer(Sidebar);
