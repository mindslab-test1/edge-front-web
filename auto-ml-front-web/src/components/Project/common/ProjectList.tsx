import React from 'react';
import {ComPage} from '../../commons';
import ComTableSection from '../../commons/ComTableSection';

interface Props{}

export interface ApiListResponse{
    id: number;
    modDt: string;
    projectDescription: string;
    projectName: string;
    projectType: string;
    modelName: string;
    regDt: string;
    modelCnt: number;
    status: string;
}

const ProjectList: React.FC<Props> = () => {

    return (
        <ComPage title={'내가 만든 학습 프로젝트'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <ComTableSection />
        </ComPage>
    );
};

export default React.memo(ProjectList);
