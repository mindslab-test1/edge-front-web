import React from 'react';
import {ComPage} from '../commons';
import ResultTableSection from './road/result/ResultTableSection';

interface Props{}

export interface ApiListResponse{
  no : number;
  recogSeq : number;
  edgeNodeSeq : number;
  edgeId : string;
  companyNm : string;
  clientCompanyId : string;
  clientCompanyName : string;
  addr : string;
  addrDetail : string;
  cameraDir : string;
  cameraNo : number;
  passTime : string;
  direction : number;
  directionNm : string;
  lane : number;
  carNumber : string;
  carType : number;
  carTypeNm : string;
  confLevel : number;
  editTime : string;
  modName : string;
  userNm : string;
}

const RoadResultList: React.FC<Props> = () => {

    return (
        <ComPage title={'차량 인식 결과'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <ResultTableSection />
        </ComPage>
    );
};

export default React.memo(RoadResultList);
