import React from 'react';
import {ComPage} from '../commons';
import SetHistoryTableSection from './road/settingHistory/SetHistoryTableSection';

interface Props{}

export interface ApiListResponse{
  no : number;
  deviceId : number;
  edgeId : string;
  edgeSetSeq : number;
  clientCompanyId : string;
  clientCompanyName : string;
  userName : string;
  userId : number;
  userTel : string;
  addr : string;
  addrDetail : string;
  cameraDir : string;
  cameraNo : number;
  edgeStatus : number;
  edgeAiStatus : number;
  setUserId : number;
  setUserName : string;
  setDt : string;
  setYn : string;
}

const SetHistoryList: React.FC<Props> = () => {

    return (
        <ComPage title={'설치 이력 조회'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <SetHistoryTableSection />
        </ComPage>
    );
};

export default React.memo(SetHistoryList);
