import React, {useCallback, useEffect, useState, useRef, useContext} from 'react';
import {ApiRoadResponse, ApiCompListResponse, ApiAddrListResponse} from 'components/edge/RoadRealTime';
import RoadRealInfoBox1 from './RoadRealInfoBox1';
import RoadRealInfoBox2 from './RoadRealInfoBox2';
import RoadRealVideo from './RoadRealVideo';
import {API, callApi} from 'utils/ApiClient';
import RootStore from 'store/RootStore';

interface Props {
  edgeRoad? : ApiRoadResponse;
  show : string;
  roiCallback : (item : any) => void;
  recentlyLine1 : any;
  recentlyLine2 : any;
}

const initSearch = {
  clientCompanyId : '',
  addr : '',
  addrDetail : '',
  cameraDir : ''
};

const RoadRealCellView: React.FC<Props> = ({recentlyLine1, recentlyLine2, edgeRoad, show, roiCallback}) => {
  const [search, setSearch] = useState(initSearch);
  const {userStore, notiStore, alertStore} = useContext(RootStore);
  /*const [addrList, setAddrList] = useState([{
    deviceId : '',
    addr : ''
  }]);
  const [addrDetailList, setAddrDetailList] = useState();
  const [cameraDirList, setCameraDirList] = useState();*/

  useEffect(() => {

  }, []);

  return(
      <div>
        {/*<div className='cellTit'>
            <h4>Edge 조회</h4>
        </div>*/}
        <div className='cellCont'>
          <RoadRealVideo edgeRoad={edgeRoad} show={show} roiCallback={roiCallback} />
        </div>
      </div>
  )
}

export default RoadRealCellView;
