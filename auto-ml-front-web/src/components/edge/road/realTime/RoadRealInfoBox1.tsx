import React, {useCallback, useEffect, useState} from 'react';
import {ApiRecentlyResponse} from 'components/edge/RoadRealTime';

interface Props {
    recentlyLine1 : any;
}

const carTypeListNm = (carStr) => {
    if(carStr == "bus"){
      return "버스";
    }else if(carStr == "unknown"){
      return "알수없음";
    }else if(carStr == "normal"){
      return "일반";
    }else if(carStr == "taxi"){
      return "택시";
    }else if(carStr == "old"){
      return "구번호판";
    }else if(carStr == "new"){
      return "신번호판";
    }else{
      return carStr;
    }
  }
  
  const dirStr = (dirStr) => {
    if(dirStr == 'front'){
      return "전면";
    }else if(dirStr == 'back'){
      return "후면";
    }else if(dirStr == 'both'){
      return "양방향";
    }else{
      return dirStr;
    }
  }

const RoadRealInfoBox: React.FC<Props> = ({recentlyLine1}) => {

  return(
    <div className='infoCell'>
          <div className='tit'>
              <h5><span className='fas fa-arrow-down'></span> 차로 정보</h5>
          </div>
          <div className='dlGroup'>
              <dl>
                  <dt>차량 방향</dt>
                  <dd>{dirStr(recentlyLine1.direction)}</dd>
              </dl>
              <dl>
                  <dt>인식 차로</dt>
                  <dd>{recentlyLine1.lane == 0  || recentlyLine1.lane == undefined ? '' : +recentlyLine1.lane+'차로'}</dd>
              </dl>
              <dl>
                  <dt>차량 번호</dt>
                  <dd><span className='ft_point'>{recentlyLine1.car_number}</span></dd>
              </dl>
              <dl>
                  <dt>차량 종류</dt>
                  <dd><span className='ft_point'>{carTypeListNm(recentlyLine1.car_type)}</span></dd>
              </dl>
              <dl>
                  <dt>인식 신뢰도</dt>
                  <dd><span className='ft_point'>{recentlyLine1.conf_level == 0  || recentlyLine1.conf_level == undefined ? null : recentlyLine1.conf_level+'%'}</span></dd>
              </dl>
          </div>
      </div>
  )
}

export default React.memo(RoadRealInfoBox);
