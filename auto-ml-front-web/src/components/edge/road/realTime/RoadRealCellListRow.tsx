import React, {useCallback, useEffect, useState} from 'react';0
import {ApiRoadResponse} from 'components/edge/RoadRealTime';

interface Props {
  item : ApiRoadResponse;
  selectEdgeRoad : (e, item : ApiRoadResponse) => void;
}

const RoadRealCellListRow: React.FC<Props> = ({item, selectEdgeRoad}) => {
  return(
        <li>
            <a href='#none' onClick={(e) => {selectEdgeRoad(e, item)}}>
                <span className='company'>{item.clientCompanyName}</span>
                <span className='place'>{item.addr}</span>
            </a>
        </li>
  )
}

export default React.memo(RoadRealCellListRow);
