import React, {useCallback, useEffect, useState} from 'react';0
import {ApiRoadResponse} from 'components/edge/RoadRealTime';
import RoadRealCellListRow from './RoadRealCellListRow';

interface Props {
  edgeRoadList : ApiRoadResponse[];
  selectEdgeRoad : (e, item : ApiRoadResponse) => void;
}

const RoadRealCellList: React.FC<Props> = ({edgeRoadList, selectEdgeRoad}) => {

  return(
        <div className='cellLst' style={{display:'none'}}>
          <div className='cellTit'>
              <h4>Edge 목록</h4>
          </div>
          <div className='cellCont'>
              <ul className='lst'>
                  {
                    edgeRoadList.length > 0 ? (
                    edgeRoadList.map((item) => ((
                        <RoadRealCellListRow key={item.edgeId} item={item} selectEdgeRoad={selectEdgeRoad} />
                      )))
                    ) : (
                        <li>데이터가 없습니다.</li>
                    )
                  }
              </ul>
          </div>
        </div>
  )
}

export default React.memo(RoadRealCellList);
