import React, {useCallback, useEffect, useState, useContext, useLayoutEffect} from 'react';
import {API, callApi} from 'utils/ApiClient';
import ComSideBox from 'components/commons/popup/ComSideBox';
import {ApiRoadResponse, ApiRoadPolicy, ApiCompListResponse, ApiRecentlyResponse} from 'components/edge/RoadRealTime';
//import RoadRealCellList from './RoadRealCellList';
import RoadRealCellView from './RoadRealCellView';
import EdgeRealTimeSetting from 'components/popup/EdgeRealTimeSetting';
import RootStore from 'store/RootStore';
import {showLoading, hideLoading, deepCopyObject, validationCheck} from 'utils/CommonUtils';
import _ from 'lodash';
import ComSearchSelectNoAll from 'components/commons/ComSearchSelectNoAll';
import ComSearchSelectNoCompany from 'components/commons/ComSearchSelectNoCompany';
import ComSearchSelectNoTitle from 'components/commons/ComSearchSelectNoTitle';
import { pullUserToken } from 'service/auth/AuthService';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

interface Props {

}

const RoadRealTimeSection: React.FC<Props> = () => {

  const initRoiInfo = '0.193,0.195,0.815,0.195,0.082,0.865,0.926,0.865';

  const initSearch = {
    clientCompanyId : '',
    addr : '',
    addrDetail : '',
    cameraDir : '',
    deviceId : 0
  };

  const initAddrs = {
    addr : [{
      code : '',
      codeNm : ''
    }],
    addrDetail : [{
      code : '',
      codeNm : ''
    }],
    cameraDir : [{
      code : '',
      codeNm : ''
    }]
  }

  const codeItems = [{
    code : '',
    codeNm : ''
  }]

  const initRecently = {
    camera_dir : '',
    direction : '',
    lane : 0,
    car_number : '',
    car_type : '',
    conf_level : 0.0
  }

  const {userStore, notiStore} = useContext(RootStore);
  const [edgeRoad, setEdgeRoad] = useState<ApiRoadResponse>();
  //const [compList, setCompList] = useState<ApiCompListResponse[]>(searchCompanyOptions);
  const [compList, setCompList] = useState([]);
  const [popShow, setPopShow] = useState('');
  const [policy, setPolicy] = useState<ApiRoadPolicy>();
  const [search, setSearch] = useState<any>(initSearch);
  const [addrs, setAddrs] = useState<any>(initAddrs);
  const [allAddr, setAllAddr] = useState<boolean>(true);
  const [recentlyLine1, setRecentlyLine1] = useState<any>({});
  const [recentlyLine2, setRecentlyLine2] = useState<any>({});
  const [recently, setRecently] = useState<boolean>(false);
  const [refresh, setRefresh] = useState<Number>(5000);
  //const [searchList, setSearchList] = useState([]);
  const searchList : Array<any> = [];
  const searchObj : Object = {};
  let ws : Stomp.Client | null = null;
  let sendMsgInterval : number | null = null;

  const selectEdgeRoad = () => {
    selectEdgeRoadInfo();
  };

  /*const setSearchBoxInfo = () => {
    const docSearchBox = document.getElementById("searchBox");
    if (docSearchBox != null) {
      Array.from(docSearchBox.children).forEach((item : any) => {
        console.log(item);
        if(item.nodeName == "SELECT"){
          searchList.push(item.name);
        }
      });
    }
    console.log(searchList);
  };*/

  const setSearchBoxInfo = (element, target) => {
    let isHas = false;
    const eList : Array<any> = [];
    const eObj = {};

    if (element == target) {
      return true;
    } else {

      if (element.children.length > 0) {
        Array.from(element.children).forEach((item : any) => {
          if(item.nodeName == "SELECT"){
            if(item.name != "clientCompanyId"){
              searchList.push(item.name);
              searchObj[item.name] = codeItems;
            }
          }
          if(setSearchBoxInfo(item, target)) {
            isHas = true;
            return true;
          }
        });
      }

    }

    return isHas;
  };

  const handleOnChange = ({name, value}) => {
    //console.log("initSearchObj ==> ", searchObj);
    //console.log("length ==> ", Object.keys(searchObj).length);
    if(Object.keys(searchObj).length == 0){
      const docSearchBox = document.getElementById("searchBox");
      setSearchBoxInfo(docSearchBox, "");
    }
    //setSearchBoxInfo(docSearchBox, "");
    //console.log(searchList);
    //if (typeof(activeFilter) != 'undefined') {
      //if(!has(activeFilter, event.target)) {
        //setSelectFilterName('');
      //}
    //}
    //console.log("searchList ==> ",searchList);
    //console.log("searchObj ==> ", searchObj);
    //console.log("initAddrs ==> ", initAddrs);

    let obj = _.cloneDeep(search);

    //console.log(eList);
    if (name == 'clientCompanyId') {
      obj.addr = '';
      obj.addrDetail = '';
      obj.cameraDir = '';
    } else if (name == 'addr') {
      obj.addrDetail = '';
      obj.cameraDir = '';
    } else if (name == 'addrDetail') {
      obj.cameraDir = '';
    }

    setSearch({
      ...obj,
      [name] : value
    });
  };

  const viewEdgeReadRoadInfo = async (e) => {
    e.preventDefault();
    let isError : boolean = false;

    try {

      if (edgeRoad && edgeRoad.policy) {

        const _api = deepCopyObject(API.ROAD_POLICY);
        _api.url = _api.url.replace('_ID_', edgeRoad.policy.edgeNodeSeq);

        const res = await callApi(_api);

        if (res.success) {
          setPolicy(res.data);
          setEdgeRoad({
            ...edgeRoad,
            policy : {
              ...edgeRoad.policy,
              camDir : res.data.camDir,
              vehSide : res.data.vehSide,
              startLane : res.data.startLane,
              rangeLane : res.data.rangeLane,
              roi : res.data.roi ? res.data.roi : initRoiInfo
            }
          });
          setPopShow('aside_setup');
        } else {
          isError = true;
        }

      } else {
        //setRecently(false);
        notiStore.error('Edge를 선택해주세요.');
      }

    } catch {
      //setRecently(false);
      //setRefresh(5000);
      isError = true;
    } finally {
      if (isError) {
        //setRecently(false);
        //setRefresh(5000);
        notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }
    }
  }

  const closePopup = () => {
    setPopShow('');
    if (edgeRoad && edgeRoad.policy) {
      setEdgeRoad({
        ...edgeRoad,
        policy : {
          ...edgeRoad.policy,
          roi : policy && policy.roi ? policy.roi : initRoiInfo
        }
      });
    }
  }

  const callbackRoiLocation = (data) => {
    if (edgeRoad && edgeRoad.policy) {
      setEdgeRoad({
        ...edgeRoad,
        policy : {
          ...edgeRoad.policy,
          roi : data
        }
      });
    }
  }

  const initRoiFunc = () => {

    if (edgeRoad && edgeRoad.policy) {
      setEdgeRoad({
        ...edgeRoad,
        policy : {
          ...edgeRoad.policy,
          roi : initRoiInfo
        }
      });
    }
  }

  const modifyPolicyCallback = useCallback(async (e, item) => {
    e.preventDefault();
    let isError : boolean = false;

    try {

      if (edgeRoad && edgeRoad.policy) {
        item.roi = edgeRoad.policy.roi;

        const _api = deepCopyObject(API.ROAD_POLICY_EDIT);
        _api.url = _api.url.replace('_ID_', edgeRoad.policy.edgeNodeSeq);

        const res = await callApi(_api, item);

        if (res.success) {
          notiStore.info("수정이 완료되었습니다.");

            setEdgeRoad({
              ...edgeRoad,
              policy : {
                ...edgeRoad.policy,
                camDir : item.camDir,
                vehSide : item.vehSide,
                startLane : item.startLane,
                rangeLane : item.rangeLane
              }
            });

          setPopShow('');
        } else {
          isError = true;
        }
      } else {
        notiStore.error('인식 방향 설정 수정 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    } catch {
      //setRecently(false);
      //setRefresh(5000);
      isError = true;
    } finally {
      if (isError) {
        //setRecently(false);
        //setRefresh(5000);
        notiStore.error('인식 방향 설정 수정 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }
    }

  },[edgeRoad]);

  const companyList = async() => {
    try {
      const res = await callApi(API.COMPANY_LIST, {page : 1, size : 100});
      setCompList(res.data.data.content);

    } catch {
      //setRecently(false);
      //setRefresh(5000);
      notiStore.error('회사 정보 조회에 실패했습니다.');
      console.log("error");
    }
  };

  const recentlyEdgeInfo =  useCallback(async (edgeId, vehSide) => {
    if(edgeId == ''){
      notiStore.error('엣지 ID정보가 없습니다.');
      return false;
    }else if(vehSide == ''){
      notiStore.error('인식 방향 정보가 없습니다.');
      return false;
    }
    const _api = deepCopyObject(API.ROAD_RECOG_RECENTLY);
    _api.url = _api.url.replace('_ID_', edgeId);

    const res = await callApi(_api);
    try{
      if(res.success){
        drawRecentlyEdge(edgeId, vehSide, res.data);
        //setRecently(true);
        //setRefresh(5000);
      }else{
        //setRecently(false);
        //setRefresh(5000);
        notiStore.error('정보를 가져오는 중 오류가 발생하였습니다.');
        console.log("error");
      }

      setRefresh(5000);
    }catch{
      //setRecently(false);
      //setRefresh(5000);
      notiStore.error('정보를 가져오는 중 오류가 발생하였습니다.');
      console.log("error");
    }

  },[]);

  const selectEdgeRoadInfo = useCallback(async () => {
    let isError : boolean = false;

    try {

      if (!allAddr && !validationCheck('searchBox')) {
        return false;
      }

      setPopShow('');
      if(history.state.state != undefined){
        let deviceId : number = history.state.state.deviceId;
        search.deviceId = deviceId;
        history.replaceState({state : undefined},"","");
      }else{
        search.deviceId = 0;
      }

      const res = await callApi(API.ROAD_REAL, search);

      if (res.success) {
        const result = res.data;

        if (!result.data) {

          if (!result.success) {

            if (result.message && result.message.toLowerCase() == 'alreadyuse') {
              notiStore.error('다른 기기에서 해당 장비를 보고 있습니다.');
              return false;
            } else {
              isError = true;
              return false;
            }

          }

          notiStore.error('설치 완료된 장비 또는 등록된 장비가 없습니다.');
          return false;
        }



        setSearch({
          clientCompanyId : result.data.clientCompanyId,
          addr : result.data.addr,
          addrDetail : result.data.addrDetail,
          cameraDir : result.data.policy.cameraDir
        });

        setEdgeRoad(result.data);

        if (result.data.policy.roi) {
            //setRecently(true);
            //recentlyEdgeInfo(res.data.edgeId, res.data.policy.vehSide);

            socketOpen(result.data.edgeId, result.data.vehSide);
        }

      } else {
        //setRecently(false);
        isError = true;
      }

    } catch(error) {
      //setRecently(false);
      //setRefresh(5000);
      console.log(error);
      isError = true;
    } finally {
      if (isError) {
        //setRecently(false);
        //setRefresh(5000);
        notiStore.error('정보를 가져오는 중 오류가 발생하였습니다.');
      }
    }

  },[search]);

  const socketOpen = (edgeId : string, vehSide : string) => {
    socketDisconnect();

    ws = Stomp.client(process.env.REACT_WEBSOCKET_URL);
    const token = pullUserToken();

    if (token != null) {
      ws.connect({Authorization:`Bearer ${token}`}, () =>{
        //timeout 끊키지 않게 interval
        if (sendMsgInterval != null) clearInterval(sendMsgInterval);
        sendMsgInterval = setInterval(() => {
          ws.send(`/${edgeId}`,{},'');
        },30000);

        //ws.subscribe(`/edge/${edgeId}`, (message) => {
            //const recv = JSON.parse(message.body);
            //drawRecentlyEdge(edgeId, vehSide, recv);
        //});
      })
    }
  };

  const socketDisconnect = () => {
    if (sendMsgInterval != null) {
      clearInterval(sendMsgInterval);
    }

    if (ws != null) {
      ws.disconnect();
    }
  }

  const drawRecentlyEdge = (edgeId : string, vehSide : string, recv : any) => {
    if(recv){
      //if(edgeRoad && edgeRoad.policy && edgeRoad.policy.vehSide == 'front'){
      if(vehSide == 'front'){
        setRecentlyLine2(recv[0] == undefined ? initRecently : recv[0]);
        setRecentlyLine1(recv[1] == undefined ? initRecently : recv[1]);
      }else{
        setRecentlyLine1(recv[0] == undefined ? initRecently : recv[0]);
        setRecentlyLine2(recv[1] == undefined ? initRecently : recv[1]);
      }
    }else{
      //notiStore.error('해당 정보를 찾을 수 없습니다.');
      setRecentlyLine1(initRecently);
      setRecentlyLine2(initRecently);
    }
  }

  useEffect(() => {
    if (search != initSearch) {
        getSelectAddrData();
    }
  },[search]);

  const getSelectAddrData =  async () => {
    let isError : boolean = false;

    try {
          const params = _.cloneDeep(search);

          if (allAddr) {
            params.allAddr = true;
          } else if (params.cameraDir != '') {
            return false;
          }

          const res = await callApi(API.ROAD_ADDRLIST, params);
          if (res.success) {
            let obj = _.cloneDeep(addrs);
            let keyArr = Object.keys(res.data);
            for (let idx in keyArr) {
              let key : string = keyArr[idx];
              let data : any = res.data[key];

              let codes = data.map(str => {
                return {
                  code : str,
                  codeNm : ((key == 'cameraDir') ? `${str} 방향` : str)
                }
              });

              obj[key] = codes;

              if (!allAddr && codes.length == 0) {
                if (key == 'addr') {
                  obj['addrDetail'] = [];
                  obj['cameraDir'] = [];
                } else if (key == 'addrDetail') {
                  obj['cameraDir'] = [];
                }
              }

            }
            setAddrs(obj);

            if (allAddr) setAllAddr(false);

          } else {
            isError = true;
          }

        } catch {
          //setRecently(false);
          //setRefresh(5000);
          isError = true;
        } finally {
          if (isError) {
            //setRecently(false);
            //setRefresh(5000);
            notiStore.error('정보를 가져오는 중 오류가 발생하였습니다.');
          }
        }
  };

  useEffect(() => {
    //const el : any = document.querySelectorAll('.cellLst ul.lst li a');
    //const el : any = document.querySelectorAll('.cellView .dlBox dl');
    //el[0].
    //el[0].click();
    companyList();
    selectEdgeRoadInfo().then(() => {
      hideLoading();
    });

    return () => {
      socketDisconnect();
    };
  }, []);

  return(
      <>
        <div className='cellBox'>
          <div className='cellView'>
            <div className='cellCont'>
              <div id='searchBox' className='dlBox'>
                  {/*<ComSearchSelectNoAll title='회사명' onChange={handleOnChange} options={searchCompanyOptions} name='clientCompanyId' value={search.clientCompanyId} />*/}
                  <ComSearchSelectNoCompany title='회사명' onChange={handleOnChange} options={compList} name='clientCompanyId' value={search.clientCompanyId} />
                  <dl>
                      <dt>설치장소</dt>
                      <dd>
                      <ComSearchSelectNoTitle title='주소' onChange={handleOnChange} options={addrs.addr} name='addr' value={search.addr} />&nbsp;
                      <ComSearchSelectNoTitle title='상세 주소' onChange={handleOnChange} options={addrs.addrDetail} name='addrDetail' value={search.addrDetail} />&nbsp;
                      <ComSearchSelectNoTitle title='카메라 방향' onChange={handleOnChange} options={addrs.cameraDir} name='cameraDir' value={search.cameraDir} /><button type='button' id='btn_edge_read_search' onClick={() => { selectEdgeRoad() }}>조회</button>
                      </dd>
                      {/*<dd>{edgeRoad.addrNm}</dd>*/}
                  </dl>
                  <dl>
                      <dt>Edge ID</dt>
                      <dd>{edgeRoad ? edgeRoad.edgeId : ''}</dd>
                  </dl>
                  <button type='button' id='btn_edge_setup' className='btn_rou_pstive' onClick={(e) => {viewEdgeReadRoadInfo(e)}} style={{display : edgeRoad ? 'block': 'none'}}>차량 인식 설정</button>
              </div>
              <RoadRealCellView edgeRoad={edgeRoad}
                                show={popShow}
                                recentlyLine1={recentlyLine1}
                                recentlyLine2={recentlyLine2}
                                roiCallback={callbackRoiLocation} />
            </div>
          </div>
        </div>
        <ComSideBox show={popShow}>
          <EdgeRealTimeSetting show={popShow} close={closePopup} policy={policy} initRoiFunc={initRoiFunc} modifyPolicyCallback={modifyPolicyCallback} />
        </ComSideBox>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(RoadRealTimeSection);
