import React, {useCallback, useEffect, useState, useRef} from 'react';
import {ApiRoadResponse} from 'components/edge/RoadRealTime';

interface Props {
  edgeRoad? : ApiRoadResponse;
  show : string;
  roiCallback : (item : any) => void;
}
const RoadRealVideo: React.FC<Props> = ({edgeRoad, show, roiCallback}) => {
  if (!edgeRoad) {
    return(
        <div className='videoBox'>
            <div className='view'>
              <div id='videoArea' style={{width:'100%', height:'100%', position:'absolute', left:'0px', top:'0px'}}>
                  <div id='vxg_media_player1' className='vxgplayer'
                     style={{objectFit: 'fill', position:'absolute', left:'0px', top:'0px', zIndex:3, width : '100%', height : '100%', boxShadow : 'none'}} ></div>
                </div>
            </div>
        </div>
    )
  }

  const nmfSrc : string = process.env.REACT_APP_ENV == 'LOCAL' ? '/src/assets/js/pnacl/Release/media_player.nmf' : '/pnacl/Release/media_player.nmf';
  const point1 = useRef(document.createElement("div"));
  const point2 = useRef(document.createElement("div"));
  const point3 = useRef(document.createElement("div"));
  const point4 = useRef(document.createElement("div"));
  const videoArea = useRef(document.createElement("div"));
  const player = useRef(document.createElement("div"));
  let reconnect_timer : any = null;
  let roi : string = '';

  let targetObj : any;
  let img_L : any;
  let img_T : any;

  const startDrag = (e) => {

    if (show == '') {
      e.preventDefault();
      return false;
    }

    targetObj = e.target;
    const e_obj : any = window.event;
    img_L = getLeft(targetObj) - e_obj.clientX;
    img_T = getTop(targetObj) - e_obj.clientY;

    document.onmousemove = moveDrag;
    document.onmouseup = stopDrag;

    e.preventDefault();
  };

  const moveDrag = () => {
    const e_obj : any = window.event;
    let dmvx : number = parseInt(e_obj.clientX + img_L);
    let dmvy : number = parseInt(e_obj.clientY + img_T);

    let video_width = getWidth(player.current);
    let video_height = getHeight(player.current);

    let point1_top = parseInt(point1.current.style.top);
    let point1_left = parseInt(point1.current.style.left);
    let point2_top = parseInt(point2.current.style.top);
    let point2_left = parseInt(point2.current.style.left);
    let point3_top = parseInt(point3.current.style.top);
    let point3_left = parseInt(point3.current.style.left);
    let point4_top = parseInt(point4.current.style.top);
    let point4_left = parseInt(point4.current.style.left);

    const boundary_margin = 30; // point간 최소 유지 간격

    if (dmvx >= 0 && dmvx <= video_width) {
      // 기준점들 간의 경계 침해 여부 검사
      switch (targetObj.id) {
      case 'point1':
        if ((dmvx <= (point2_left - boundary_margin))
            && (dmvx <= (point3_left - boundary_margin)))
          targetObj.style.left = dmvx + "px";
        break;
      case 'point2':
        if ((dmvx >= (point1_left + boundary_margin))
            && (dmvx >= (point4_left + boundary_margin)))
          targetObj.style.left = dmvx + "px";
        break;
      case 'point3':
        if ((dmvx >= (point1_left + boundary_margin))
            && (dmvx >= (point4_left + boundary_margin)))
          targetObj.style.left = dmvx + "px";
        break;
      case 'point4':
        if ((dmvx <= (point2_left - boundary_margin))
            && (dmvx <= (point3_left - boundary_margin)))
          targetObj.style.left = dmvx + "px";
        break;
      }
    }



    if (dmvy >= 0 && dmvy <= video_height) {

      switch (targetObj.id) {
      case 'point1':
        if ((dmvy <= (point4_top - boundary_margin))
            && (dmvy <= (point3_top - boundary_margin)))
          targetObj.style.top = dmvy + "px";
        break;
      case 'point2':
        if ((dmvy <= (point4_top - boundary_margin))
            && (dmvy <= (point3_top - boundary_margin)))
          targetObj.style.top = dmvy + "px";
        break;
      case 'point3':
        if ((dmvy >= (point1_top + boundary_margin))
            && (dmvy >= (point2_top + boundary_margin)))
          targetObj.style.top = dmvy + "px";
        break;
      case 'point4':
        if ((dmvy >= (point1_top + boundary_margin))
            && (dmvy >= (point2_top + boundary_margin)))
          targetObj.style.top = dmvy + "px";
        break;
      }
    }

    draw_roi();

    return false;
  };

  const draw_roi = () => {

    let wrapper_x = videoArea.current.offsetLeft;
    let wrapper_y = videoArea.current.offsetTop;

    let point1_x = point1.current.offsetLeft - wrapper_x;
    let point1_y = point1.current.offsetTop - wrapper_y;
    let point2_x = point2.current.offsetLeft - wrapper_x;
    let point2_y = point2.current.offsetTop - wrapper_y;
    let point3_x = point3.current.offsetLeft - wrapper_x;
    let point3_y = point3.current.offsetTop - wrapper_y;
    let point4_x = point4.current.offsetLeft - wrapper_x;
    let point4_y = point4.current.offsetTop - wrapper_y;

    let video_width = getWidth(document.getElementById('videoArea'));
    let video_height = getHeight(document.getElementById('videoArea'));

    const edge1 : any = document.getElementById('edge1');
    const edge2 : any = document.getElementById('edge2');
    const edge3 : any = document.getElementById('edge3');
    const edge4 : any = document.getElementById('edge4');

    edge1.setAttribute('x1', point1_x);
    edge1.setAttribute('y1', point1_y);
    edge1.setAttribute('x2', point2_x);
    edge1.setAttribute('y2', point2_y);

    edge2.setAttribute('x1', point2_x);
    edge2.setAttribute('y1', point2_y);
    edge2.setAttribute('x2', point3_x);
    edge2.setAttribute('y2', point3_y);

    edge3.setAttribute('x1', point3_x);
    edge3.setAttribute('y1', point3_y);
    edge3.setAttribute('x2', point4_x);
    edge3.setAttribute('y2', point4_y);

    edge4.setAttribute('x1', point4_x);
    edge4.setAttribute('y1', point4_y);
    edge4.setAttribute('x2', point1_x);
    edge4.setAttribute('y2', point1_y);

    roi = '';
    for (let i = 1; i <= 4; i++) {
      let idx : number = (i == 4) ? 3 : (i == 3) ? 4 : i;
      if (i != 1) {
        roi += ',';
      }
      roi += `${eval('((point' + idx +'_x) / video_width).toFixed(3)')}`;
      roi += `,${eval('((point' + idx +'_y) / video_height).toFixed(3)')}`;
    }


    //roi = `${(point1_x) / video_width},${(point1_y) / video_height},${(point2_x) / video_width},${(point2_y) / video_height},${(point4_x) / video_width},${(point4_y) / video_height},${(point3_x) / video_width},${(point3_y) / video_height}`;

  };

  const stopDrag = () => {
    document.onmousemove = null;
    document.onmouseup = null;

    /*const dataArr = roi.map(location => {
      return location.toFixed(3);
    });*/

    roiCallback(roi);
  };

  const process_camera_view = () => {
    if (!edgeRoad.policy) {
      return false;
    }

    if (reconnect_timer) clearTimeout(reconnect_timer);

    if(!edgeRoad.policy.roi || edgeRoad.policy.roi == ''){
      edgeRoad.policy.roi = '0.193,0.195,0.815,0.195,0.082,0.865,0.926,0.865';
    }
    const roi = edgeRoad.policy.roi;


    const coordinates = roi.split(',').map((location) => {return parseFloat(location) });

    let video_width = getWidth(document.getElementById('videoArea'));
    let video_height = getHeight(document.getElementById('videoArea'));

    const point1_x : number = coordinates[0] * video_width;
    const point1_y : number = coordinates[1] * video_height;
    const point2_x : number = coordinates[2] * video_width;
    const point2_y : number = coordinates[3] * video_height;
    const point3_x : number = coordinates[6] * video_width;
    const point3_y : number = coordinates[7] * video_height;
    const point4_x : number = coordinates[4] * video_width;
    const point4_y : number = coordinates[5] * video_height;

    const edge1 : any = document.getElementById('edge1');
    const edge2 : any = document.getElementById('edge2');
    const edge3 : any = document.getElementById('edge3');
    const edge4 : any = document.getElementById('edge4');

    // 선을 그린다
    edge1.setAttribute('x1', point1_x);
    edge1.setAttribute('y1', point1_y);
    edge1.setAttribute('x2', point2_x);
    edge1.setAttribute('y2', point2_y);

    edge2.setAttribute('x1', point2_x);
    edge2.setAttribute('y1', point2_y);
    edge2.setAttribute('x2', point3_x);
    edge2.setAttribute('y2', point3_y);

    edge3.setAttribute('x1', point3_x);
    edge3.setAttribute('y1', point3_y);
    edge3.setAttribute('x2', point4_x);
    edge3.setAttribute('y2', point4_y);

    edge4.setAttribute('x1', point4_x);
    edge4.setAttribute('y1', point4_y);
    edge4.setAttribute('x2', point1_x);
    edge4.setAttribute('y2', point1_y);


    let wrapper_x : number = videoArea.current.offsetLeft; // 비디오 영역 위치 보정 값
    let wrapper_y  : number = videoArea.current.offsetTop;


    point1.current.style.left = wrapper_x + point1_x + "px";
    point1.current.style.top = wrapper_y + point1_y + "px";
    point2.current.style.left = wrapper_x + point2_x + "px";
    point2.current.style.top = wrapper_y + point2_y + "px";
    point3.current.style.left = wrapper_x + point3_x + "px";
    point3.current.style.top = wrapper_y + point3_y + "px";
    point4.current.style.left = wrapper_x + point4_x + "px";
    point4.current.style.top = wrapper_y + point4_y + "px";

  }

  const getLeft = (o) => {
    return parseInt(o.style.left.replace('px', ''));
  }
  const getTop = (o) => {
    return parseInt(o.style.top.replace('px', ''));
  }

  const getWidth = (o : any) => {
    return parseInt(o.offsetWidth);
  }

  const getHeight = (o  : any) => {
    return parseInt(o.offsetHeight);
  }

  const playVxgPlayer = () => {
    if (edgeRoad.policy) {
        window.vxgplayer('vxg_media_player1').src(edgeRoad.policy.url);
    }
    process_camera_view();
  }


  useEffect(() => {
    return () => {
      document.onmousemove = null;
      document.onmouseup = null;

      if (reconnect_timer != null) {
        clearTimeout(reconnect_timer);
      }
    };
  },[]);

  useEffect(() => {
    if (edgeRoad.policy) {

      if (window.vxgplayer('vxg_media_player1') && window.vxgplayer('vxg_media_player1').module.url != edgeRoad.policy.url) {
        window.vxgplayer.players = {};
        window.vxgplayer('vxg_media_player1', {
            autostart: true,
            width : getWidth(document.getElementById('videoArea')),
            height : getHeight(document.getElementById('videoArea')),
            nmf_path: 'media_player.nmf',
            nmf_src: nmfSrc,
            avsync : true,
            connection_timeout: 5000,
            volume : 0,
            mute : true
        });

        if (window.vxgplayer('vxg_media_player1')) {
          window.vxgplayer('vxg_media_player1').onError(() => {
            // NO_ERROR == -1
            // MEDIA_ERR_URL == 0
            // MEDIA_ERR_NETWORK == 1
            // MEDIA_ERR_SOURCE == 2
            // MEDIA_ERR_CARRIER == 3
            // MEDIA_ERR_AUDIO == 4
            // MEDIA_ERR_VIDEO == 5
            // MEDIA_ERR_AUTHENTICATION == 6
            // MEDIA_ERR_BANDWIDTH == 7
            // MEDIA_ERR_EOF == 8
            const error_code : any = window.vxgplayer('vxg_media_player1').error();

            console.log("player ERROR: " + error_code);

            const retry_interval : number = 2000;

            if(((error_code == 1) || (error_code == 7)) && edgeRoad.policy) {  // 1: network error, 7: MEDIA_ERR_BANDWIDTH
      				console.log('error occurred, retry connect to rtsp server...');
      				reconnect_timer = setTimeout(() => {
      					console.log('reconnecting...')
      					window.vxgplayer('vxg_media_player1').src(edgeRoad.policy.url); // retry connect to rtsp server
      				}, retry_interval);
      			}
          });

          window.vxgplayer('vxg_media_player1').onReadyStateChange( () => {
            console.log('onReadyStateChange');
            window.vxgplayer('vxg_media_player1').play();
          });

          window.vxgplayer('vxg_media_player1').onStateChange( (onState) => {

            // 0 - PLAYER_STOPPED
            // 1 - PLAYER_CONNECTING
            // 2 - PLAYER_PLAYING
            // 3 - PLAYER_STOPPING
            console.log('onStateChange', onState);

            if (onState == 0) {
              window.vxgplayer('vxg_media_player1').play();
            }

          });
          playVxgPlayer();
        }
      } else {
        player.current.style.width = '100%';
        player.current.style.height = '100%';
        if (edgeRoad.policy.roi) process_camera_view();
      }

    }

  },[edgeRoad]);

  return(
      <div className='videoBox'>
          <div className='view'>

            <div id='videoArea' style={{width:'100%', height:'100%', position:'absolute', left:'0px', top:'0px'}} ref={videoArea}>
              <div style={{position:'absolute', left:'0px', top:'0px', zIndex:3, width : '100%', height : '100%'}}>
                <div id='cur_camera' style={{position:'absolute', left:'0px', top:'50px', width:'500px', textAlign:'center', backgroundColor:'#eaeaea'}}></div>
                <div id='vxg_media_player1' className='vxgplayer' nmf-src={nmfSrc}
                   style={{objectFit: 'fill', position:'absolute', left:'0px', top:'0px', zIndex:3, width : '100%', height : '100%', boxShadow : 'none'}} ref={player} ></div>
              </div>


              <div id='point1' style={{cursor:'pointer', height:'12px', width:'12px', border:'5px solid', borderColor:'#0000ff', backgroundColor:'rgba(0,0,255,0.01)', position:'absolute', left:'0px', top:'100px', zIndex:6, boxSizing : 'border-box'}} onMouseDown={(e) => { startDrag(e) }} ref={point1} >1</div>
							<div id='point2' style={{cursor:'pointer', height:'12px', width:'12px', border:'5px solid', borderColor:'#0000ff', backgroundColor:'rgba(0,0,255,0.01)', position:'absolute', left:'100px', top:'100px', zIndex:6, boxSizing : 'border-box'}} onMouseDown={(e) => { startDrag(e) }} ref={point2} >2</div>
							<div id='point4' style={{cursor:'pointer', height:'12px', width:'12px', border:'5px solid', borderColor:'#0000ff', backgroundColor:'rgba(0,0,255,0.01)', position:'absolute', left:'0px', top:'200px', zIndex:6, boxSizing : 'border-box'}} onMouseDown={(e) => { startDrag(e) }} ref={point4} >4</div>
							<div id='point3' style={{cursor:'pointer', height:'12px', width:'12px', border:'5px solid', borderColor:'#0000ff', backgroundColor:'rgba(0,0,255,0.01)', position:'absolute', left:'100px', top:'200px', zIndex:6, boxSizing : 'border-box'}} onMouseDown={(e) => { startDrag(e) }} ref={point3} >3</div>


              <div id='draw_area' style={{position:'relative', zIndex:5, width : '100%', height : '100%'}}>
								<svg style={{width : '100%', height : '100%'}}>
									<line id='edge1' x1='0' y1='0' x2='300' y2='0' style={{stroke:'rgb(255,0,0)', strokeWidth:2, position:'absolute', left:'0px', top:'0px', zIndex:5}}  />
									<line id='edge2' x1='0' y1='0' x2='300' y2='0' style={{stroke:'rgb(255,0,0)', strokeWidth:2, position:'absolute', left:'0px', top:'0px', zIndex:5}}  />
									<line id='edge3' x1='0' y1='0' x2='300' y2='0' style={{stroke:'rgb(255,0,0)', strokeWidth:2, position:'absolute', left:'0px', top:'0px', zIndex:5}}  />
									<line id='edge4' x1='0' y1='0' x2='300' y2='0' style={{stroke:'rgb(255,0,0)', strokeWidth:2, position:'absolute', left:'0px', top:'0px', zIndex:5}}  />
								</svg>
							</div>
            </div>


          </div>
      </div>
  )
}

export default React.memo(RoadRealVideo);
