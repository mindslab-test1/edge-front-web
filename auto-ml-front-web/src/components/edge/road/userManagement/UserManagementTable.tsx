import React, {memo} from 'react';
import {ApiListResponse} from 'components/edge/UserManagementList';
import UserManagementTableRow from './UserManagementTableRow';

interface Props {
    userList : ApiListResponse[];
    checkItems : number[];
    iptCheckAllClick : (e) => void;
    iptCheckOnClick : (e, edgeId : number) => void;
    modifyUserInfo : (e, item : any) => void;
    selectUserId : string;
    currentPage : number;
    listSize : number;
}

const UserManagementTable: React.FC<Props> = ({ currentPage, listSize, userList, iptCheckAllClick, iptCheckOnClick, modifyUserInfo, checkItems, selectUserId}) => {

  return(
      <div className='tbl_mid'>
        <table className='tbl_lst'>
            <caption className='hide'>Edge Road 목록</caption>
            <colgroup>
                <col width='25' />
                <col width='65' />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col width='70' />
            </colgroup>
            <thead>
                <tr>
                    <th scope='col'>
                        <div className='checkOnlyBox'>
                            <input type='checkbox' name='ipt_tbl_check' id='ipt_check_all' className='ipt_check all' checked={(checkItems.length >= userList.length && checkItems.length > 0) ? true : false} onChange={(e) =>{ iptCheckAllClick(e.target.checked) }} />
                            <label htmlFor='ipt_check_all'>체크박스</label>
                        </div>
                    </th>
                    <th scope='col'>No.</th>
                    <th scope='col'>사용자명</th>
                    <th scope='col'>아이디(이메일)</th>
                    <th scope='col'>사용자 연락처</th>
                    <th scope='col'>회사명</th>
                    <th scope='col'>회사ID</th>
                    <th scope='col'>권한유형</th>
                    <th scope='col'>등록일</th>
                    <th scope='col'>사용여부</th>
                    <th scope='col'>수정</th>
                </tr>
            </thead>
            <tbody>
              {
                userList.length > 0 ? (
                  userList.map((item, index)=>((
                    <UserManagementTableRow listSize={listSize} currentPage={currentPage} key={item.id} index={index + 1} item={item} selectUserId={selectUserId} checkItems={checkItems} iptCheckOnClick={iptCheckOnClick} modifyUserInfo={modifyUserInfo} />
                  )))
                ) : (
                  <tr>
                    <td scope='row' colSpan={12} className='dataNone'>데이터가 없습니다.</td>
                  </tr>
                )
              }
            </tbody>
          </table>
      </div>
  )
}

export default React.memo(UserManagementTable);
