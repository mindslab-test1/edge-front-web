import    React, {useCallback, useEffect, useState, useContext} from 'react';
import {ApiListResponse, ApiCompanyListResponse, ApiUserListResponse} from 'components/edge/UserManagementList';
import Paging from 'components/commons/Paging';
import {API, callApi} from 'utils/ApiClient';
import {showLoading, hideLoading, dateDiff, getCodeList} from 'utils/CommonUtils';
import UserManagementTable from './UserManagementTable';
import ComSideBox from 'components/commons/popup/ComSideBox';
import UserManagementModify from 'components/popup/UserManagementModify';
import UserManagementInsert from 'components/popup/UserManagementInsert';
import ComSearchBar from 'components/commons/ComSearchBar';
import ComSearchSelect from 'components/commons/ComSearchSelect';
import ComSearchDate from 'components/commons/ComSearchDate';
import ComSearchSelectInput from 'components/commons/ComSearchSelectInput';
import ComPopBox from 'components/commons/popup/ComPopBox';
import PopUser from 'components/popup/PopUser';
import { Form, Input, Button, message, Row, Col } from 'antd';
import _ from 'lodash';
import RootStore from 'store/RootStore';


interface Props {
  
}

const UserManagementTableSection: React.FC<Props> = () => {
    const initSearch = {
      auth : '',
      startDate : '',
      endDate : '',
      searchType : 'all',
      searchTxt : '',
      page : 1,
      size : 10,
      useYn : '',
      delYn : 'N'
    };

    const initUserSearch = {
      searchUserType : '',
      searchUserTxt : '',
    };

    const searchTypeOptions = [
      {
        code : 'userEmail',
        codeNm : '아이디'
      },
      {
        code : 'userTel',
        codeNm : '연락처'
      },
      {
        code : 'userNm',
        codeNm : '이름'
      },
      {
        code : 'companyName',
        codeNm : '회사명'
      }
    ];

    const options = [
      {
        code : 1,
        codeNm : '일반사용자'
      },
      {
        code : 2,
        codeNm : '설치기사'
      },
      {
        code : 3,
        codeNm : '관리자'
      },
      {
        code : 4,
        codeNm : '회사관리자'
      }
    ];

    const [userList, setUserList] = useState<ApiListResponse[]>([]);
    const [totalPages, setTotalPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [selectItem, setSelectItem] = useState({});
    const [selectItems, setSelectItems] = useState({});
    const [checkItems, setCheckItems] = useState<number[]>([]);

    const [search, setSearch] = useState(initSearch);
    const [userSearch, setUserSearch] = useState(initUserSearch);
    const [popShow, setPopShow] = useState('');
    const [selectUserId, setSelectUserId] = useState('');
    const [userSearchPop, setUserSearchPop] = useState(false);
    const {userStore, notiStore, alertStore} = useContext(RootStore);
    const [input, setInput] = useState('');
    const [codes, setCodes] = useState<any>({});

    const onSearchBtnCallback = (e) => {
      /*if(search.startDate == null || search.endDate == null){
        notiStore.error("시작일과 등록일은 필수로 입력해주시기 바랍니다.");
        return false;
      }*/

      callApiUserList(1);
    };
    
    const initSearchCallback = (e) => {
      setSearch(initSearch);
    }

    const handleOnChange = ({name, value}) => {
      setSearch({
        ...search,
        [name] : value
      });
    };

    const handleOnUserChange = ({name, value}) => {
      setUserSearch({
        ...userSearch,
        [name] : value
      });
    };

    const modifyUserInfo = (e, item) => {
      e.preventDefault();
      setPopShow('aside_modify');

      setSelectItem(item);
      //setSelectUserId(item.userId);
    }

    const userModifyCallback = async (item) => {
      try {
        const res = await callApi(API.USER_EDIT, item);
        //const data : any = res.data;
        notiStore.info("수정에 성공했습니다.");
        closePopup();
        callApiUserList(1);
          
      } catch {
        notiStore.error('수정에 실패했습니다.');
        console.log("error");
      }

    };

    const validationCheck = (item) => {
      //if(item.){
      if(!item){
        notiStore.error('사용자 정보가 없습니다.');
        return false;
      }
      if(item.name == ''){
        notiStore.error('사용자명 정보가 없습니다.');
        return false;
      }else if(item.email == ''){
        notiStore.error('이메일 정보가 없습니다.');
        return false;
      }else if(item.tel == ''){
        notiStore.error('연락처 정보가 없습니다.');
        return false;
      }
      //}
      return true;
    };

    const userEmailCheck = async (item) => {
      try{
        const res = await callApi(API.USER_CHECK, {email : item.email});
        
        if(res.status == 200){
          if(res.data){
            notiStore.error('이미 동일한 이메일로 등록한 사용자가 있습니다.');
            return false;
          }else if(!res.data){
            userInsertCallback(item);
            return true;
          }
        }else{
          //notiStore.error('.');
          notiStore.error('사용자 중복 조회에 실패했습니다.');
          return false;  
        }
      }catch{
        notiStore.error('사용자 중복 조회에 실패했습니다.');
        console.log("error");
      }
    };

    const userInsertCallback = useCallback(async (item) => {

      if(!validationCheck(item)) return false;
      //if(!userEmailCheck(item.email)) return false;
      try{
        const res = await callApi(API.USER_CREATE, item);
        notiStore.info("저장에 성공했습니다.");
        setPopShow('');
        setSelectItem([]);
        setSelectUserId('');

        callApiUserList(1);
      }catch {
        //setUserList(dummyData);
        hideLoading();
        console.log('error');
        notiStore.error("저장에 실패했습니다.");
      }
    },[search]);
    
    const insertEdgeInfo = (e) => {
      e.preventDefault();
      //setSelectItem(items);
      /*const items : Array<any> = [];
      items.push("name","insert");
      const itemss : object = items[0];
      setSelectItems(itemss);*/
      setSelectItems([]);
      setPopShow('aside_add');
    }

    const closePopup = () => {
      setPopShow('');
      setSelectUserId('');
    };

    const callApiUserList = useCallback(async (pageNum) => {
        setCheckItems([]);
        try {

            const searchs = _.cloneDeep(search);
            searchs.page = pageNum;
            searchs.size = 10;

            searchs.startDate = (searchs.startDate) ? searchs.startDate.format('yyyy-MM-dd') : '';
            searchs.endDate = (searchs.endDate) ? searchs.endDate.format('yyyy-MM-dd') : '';
            
            if(!dateDiff(searchs.startDate, searchs.endDate)){
              notiStore.error('종료일이 시작일보다 전입니다. 확인해주세요.');
              return false;
            }

            const res = await callApi(API.USER_LIST, searchs);
            //const res = await callApi(API.USER_LIST, {page: pageNum - 1, size: 20});

            if(res.status != 200){
              setUserList([]);
              setTotalPages(0);
            }else{
              setUserList(res.data.content);
              setTotalPages(res.data.totalPages);
            }
            setCurrentPage(pageNum);
            hideLoading();

        } catch(error) {
            notiStore.error('사용자 조회에 실패했습니다.');
            setUserList([]);
            //setUserList(dummyData);
            hideLoading();
            console.log('error');
        }
    },[totalPages, search]);

    const showAlertConfirm = (e) => {
      e.preventDefault();
      alertStore.confirm('삭제된 항목은 복구가 불가능합니다.','정말 삭제하시겠습니까?', deleteUser);
    }

    const deleteUser = async (e) => {
      e.preventDefault();

      try {
          const res = await callApi(API.USER_DELETE, checkItems);
          const data : any = res.data;

          if (data.success) {

            if (data.data) {
              notiStore.infoBlue('삭제가 완료되었습니다.');
            } else {
              notiStore.error('삭제 할 사용자를 다시 체크 후 삭제 바랍니다.');
            }
            callApiUserList(1);
            setCheckItems([]);
          } else {
            notiStore.error('삭제 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
          }

          //callApiUserList(1);
          //setCheckItems([]);
      } catch {
        notiStore.error('삭제 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }
    }

    /*const iptCheckOneCheck =(checked, edgeId) => {
      if(checkItems.length > 1){
        console.log(checkItems.length);
      }
      if (checked) {
        setCheckItems([...checkItems, edgeId]);
      } else {
        console.log(edgeId);
        setCheckItems(checkItems.filter((el) => el != edgeId));
      }
    };*/

    const iptCheckOnClick = (checked, edgeId) => {
      /*if(checkItems.length = 1){
        console.log(checkItems.length);
      }*/
      if (checked) {
        setCheckItems([...checkItems, edgeId]);
      } else {
        setCheckItems(checkItems.filter((el) => el != edgeId));
      }
    };

    const iptCheckAllClick = (checked) => {
      if (checked) {
        const checkItemsArr : number[] = [];
        userList.map((item) => {
          checkItemsArr.push(item.id);
        });
        
        setCheckItems(checkItemsArr);
      } else {
        setCheckItems([]);
      }
    };

    const userSearchPopOnclick = (e) => {
      e.preventDefault();
      setUserSearchPop(!userSearchPop);
    };

    const userClosePopup = () => {
      setUserSearchPop(false);
    }
    
    /*const modifyUserInfo = (e, item) => {
      e.preventDefault();
      console.log(item);
      setPopShow('aside_modify');

      //setSelectUserId(item.userId);
    }*/

    const sendUser = (e, item) => {
      

      e.preventDefault();
      
      const itemLen = item.length;
      if(itemLen == 0){
        notiStore.error('선택된 사용자 정보가 없습니다.');
        return false;
      }
      if(itemLen > 1){
        notiStore.error('1건만 선택해주시기 바랍니다.');
        return false;
      }

      const items : object = item[0];
      setSelectItem(items);
      setSelectItems(items);
      userClosePopup();
      //setPopShow('aside_add');
    };

    const enterSearchFilter = (e) => {
      if (e.keyCode == 13) {
        //console.log(1234);
        //onSearchBtnCallback(e);
        //setInput('');
      }
    }; 

    const handleSearchOnClick = () => {
      let item = {
        key : name,
        value : input
      }
      //onCloseCallback(item);
    }

    const companyList = async() => {
      try {
        const res = await callApi(API.COMPANY_LIST, {useYn : 'Y'});
        //notiStore.info("수정에 성공했습니다.");
        //console.log("success");
        //closePopup();
        //callApiUserList(1);
        
      } catch {
        notiStore.error('실패했습니다.');
        console.log("error");
      }

    };

    useEffect(() => {
      //console.log("@@@@@@@@@@@@@@@@@@@@");
    });

    useEffect(() => {
      companyList();
      callApiUserList(1).then(r => {
        //console.log('call userList');
      });
      getCodeList(['EDGE_USER_AUTH']).then((result : any) => {
        setCodes(result);
      });
    },[]);
  
  return(
      <>
        <div className='tbl_filterBox'>
            <ComSearchBar callback={onSearchBtnCallback} initBtnCallback={initSearchCallback}>
              <ComSearchSelectInput title='검색어' callback={onSearchBtnCallback} onChange={handleOnChange} options={searchTypeOptions} selectName='searchType' selectValue={search.searchType} inputName='searchTxt' inputValue={search.searchTxt} />
              <ComSearchSelect title='권한 유형' onChange={handleOnChange} options={codes.EDGE_USER_AUTH} name='auth' value={search.auth} />
              <ComSearchDate title='등록일' onChange={handleOnChange} name='edgeStatus' startDateName='startDate' startDateValue={search.startDate} endDateName='endDate' endDateValue={search.endDate} />
            </ComSearchBar>
            <div className='tbl_top'>
              <h4>사용자 목록</h4>
            </div>
            <UserManagementTable
              userList={userList}
              checkItems={checkItems}
              listSize={search.size}
              currentPage={currentPage}
              iptCheckAllClick={iptCheckAllClick}
              iptCheckOnClick={iptCheckOnClick}
              modifyUserInfo={modifyUserInfo}
              selectUserId={selectUserId}
               />
            <div className='tbl_btm'>
              <div className='btnBox fl'>
                <a href='#alt_edgeLstRemove' id='btn_lst_remove' className={checkItems.length > 0 ? 'btn_alt_open' : 'btn_alt_open disabled'} onClick={showAlertConfirm}><span className='fas fa-times'></span>삭제</a>
              </div>
              <Paging totalPages={totalPages} paginate={callApiUserList} currentPage={currentPage}/>
              <div className='btnBox fr'>
                <a id='btn_edge_add' href='#none' onClick={insertEdgeInfo}><span className='fas fa-plus'></span>등록</a>
              </div>
            </div>
          </div>
          <ComSideBox show={popShow}>
            <UserManagementModify show={popShow} close={closePopup} item={selectItem} userModifyCallback={userModifyCallback} userSearchPopOnclick={userSearchPopOnclick} />
            <UserManagementInsert show={popShow} close={closePopup} item={selectItems} userInsertCallback={userEmailCheck} userSearchPopOnclick={userSearchPopOnclick} />
          </ComSideBox>
          <ComPopBox show={userSearchPop} >
            <PopUser onChange={handleOnUserChange} sendUser={sendUser} close={userClosePopup} popTitle={'사용자 검색'} />
          </ComPopBox>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(UserManagementTableSection);
