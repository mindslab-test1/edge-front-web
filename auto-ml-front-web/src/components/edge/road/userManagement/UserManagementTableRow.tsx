import React, {useState} from 'react';
import {ApiListResponse} from 'components/edge/UserManagementList';
import ReactTooltip from 'react-tooltip';
import {ERouteUrl} from 'router/RouteLinks';
import {Link} from 'react-router-dom';
import {phoneFormat, changeDate} from 'utils/CommonUtils';

interface Props {
    item : ApiListResponse;
    checkItems : number[];
    iptCheckOnClick : (e, edgeId : number) => void;
    modifyUserInfo : (e, item : any) => void;
    selectUserId : string;
    index : number;
    currentPage : number;
    listSize : number;
}

const authNameChange = (roleId) => {
    if(roleId == 1){
        return "일반사용자";
    }else if(roleId == 2){
        return "설치기사";
    }else if(roleId == 3){
        return "회사관리자";
    }else if(roleId == 4){
        return "관리자";
    }else{
        return "일반";
    }
};

const UserManagementTableRow: React.FC<Props> = ({currentPage, listSize, item, checkItems, iptCheckOnClick, modifyUserInfo, selectUserId, index}) => {

  return(
      <>
        {/*<tr className={selectUserId == item.userId ? 'active':''}>
            <td scope='row'>
                <div className='checkOnlyBox'>
                    <input type='checkbox' name='ipt_tbl_check' id={'ipt_check' + item.no} className='ipt_check' checked={checkItems.includes(item.userId) ? true : false} onChange={(e) => {iptCheckOnClick(e.target.checked, item.userId)}} />
                    <label htmlFor={'ipt_check' + item.no}>체크박스</label>
                </div>
            </td>
            <td>{item.no}</td>
            <td>{item.userNm}</td>
            <td>{item.userId}</td>
            <td>{phoneFormat(item.hpNo)}</td>
            <td>{item.companyNm}</td>
            <td>{item.clientCompanyId}</td>
            <td>{item.authNm}</td>
            <td>{item.regDt}</td>
            <td>
                <a href='#none' className='btn_line btn_edge_modify' onClick={(e) => {modifyUserInfo(e, item)}}>수정</a>
            </td>
  </tr>*/}
        <tr className={selectUserId == item.email ? 'active':''}>
            <td scope='row'>
                <div className='checkOnlyBox'>
                    <input type='checkbox' name='ipt_tbl_check' id={'ipt_check' + index} className='ipt_check' checked={checkItems.includes(item.id) ? true : false} onChange={(e) => {iptCheckOnClick(e.target.checked, item.id)}} />
                    <label htmlFor={'ipt_check' + index}>체크박스</label>
                </div>
            </td>
            <td>{((currentPage-1)*listSize)+(index)}</td>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td>{phoneFormat(item.tel)}</td>
            <td>{item.clientCompanyName}</td>
            <td>{item.clientCompanyId}</td>
            <td>{authNameChange(item.authId)}</td>
            <td>{changeDate(item.regDt)}</td>
            <td>{item.useYn == 'Y' ? '사용' : '미사용'}</td>
            <td>
                <a href='#none' className='btn_line btn_edge_modify' onClick={(e) => {modifyUserInfo(e, item)}}>수정</a>
            </td>
        </tr>
      </>
  )
}

export default React.memo(UserManagementTableRow);
