import    React, {useCallback, useEffect, useState, useContext, useRef} from 'react';
import {ApiListResponse} from 'components/edge/ManagementList';
import Paging from 'components/commons/Paging';
import RootStore from 'store/RootStore';
import {API, callApi} from 'utils/ApiClient';
import {showLoading, hideLoading, deepCopyObject, validationCheck, getCodeList, dateDiff} from 'utils/CommonUtils';
import _ from 'lodash';
import ManagementTable from './ManagementTable';
import ComSideBox from 'components/commons/popup/ComSideBox';
import EdgeManagementView from 'components/popup/EdgeManagementView';
import EdgeManagementModify from 'components/popup/EdgeManagementModify';
import EdgeManagementInsert from 'components/popup/EdgeManagementInsert';
import ComSearchBar from 'components/commons/ComSearchBar';
import ComSearchSelect from 'components/commons/ComSearchSelect';
import ComSearchDate from 'components/commons/ComSearchDate';
import ComSearchSelectInput from 'components/commons/ComSearchSelectInput';
import ComPopBox from 'components/commons/popup/ComPopBox';
import PopUser from 'components/popup/PopUser';
import { Link, useHistory } from 'react-router-dom';

interface Props {

}

const ManagementTableSection: React.FC<Props> = () => {
    const initSearch = {
      edgeAiStatus : '',
      startDate : '',
      endDate : '',
      searchType : '',
      searchTxt : '',
      page : 1,
      size : 10
    };

    const searchTypeOptions = [
      {
        code : 'companyNm',
        codeNm : '회사명'
      },
      {
        code : 'companyId',
        codeNm : '회사ID'
      },
      {
        code : 'userNm',
        codeNm : '사용자명'
      },
      {
        code : 'edgeId',
        codeNm : 'Edge ID'
      },
      {
        code : 'addrNm',
        codeNm : '설치장소'
      }
    ];

    const initRefreshTime : number = 10;
    const [managementList, setManagementList] = useState<ApiListResponse[]>([]);
    const [totalPages, setTotalPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [selectItem, setSelectItem] = useState({});
    const [edgeInfo, setEdgeInfo] = useState<any>({});
    const [checkItems, setCheckItems] = useState<number[]>([]);
    const [search, setSearch] = useState(initSearch);
    const [popShow, setPopShow] = useState('');
    const [selectEdgeId, setSelectEdgeId] = useState('');
    const [userSearchPop, setUserSearchPop] = useState(false);
    const [selectUser, setSelectUser] = useState<any>(null);
    const [userSearch, setUserSearch] = useState<any>({});
    const {userStore, notiStore, alertStore} = useContext(RootStore);
    const [refreshTime, setRefreshTime] = useState<number>(initRefreshTime);
    const [isInterval, setIsInterval] = useState<boolean>(false);
    const [codes, setCodes] = useState<any>({});
    const [selectClientCompanyId, setSelectClientCompanyId] = useState<string>();
    const [reDupCheck, setReDupCheck] = useState<boolean>(false);
    let history = useHistory();

    let  interval : any = null;

    const onSearchBtnCallback = (e) => {
      callApiEdgeRoadList(1);
    };

    const initSearchCallback = (e) => {
      setSearch(initSearch);
    }

    const handleOnChange = ({name, value}) => {
      setSearch({
        ...search,
        [name] : value
      });
    };

    const duplicateEdgeId = useCallback(async (item) => {
      return new Promise(async (resolve, reject) => {
        const res = await callApi(API.DEVICE_DUPLICATE, item);

        if (res.success) {
            resolve(res.data);
        } else {
            reject('error');
        }

      });
    },[]);

    const viewEdgeInfo = useCallback( async (e, item) => {
      e.preventDefault();
      try {
            const _api = deepCopyObject(API.DEVICE_INFO);
            _api.url = _api.url.replace('_ID_', item.deviceId);

            const res = await callApi(_api);
            if (res.success) {
              setEdgeInfo(res.data);
              setPopShow('aside_view');
              setSelectEdgeId(item.edgeId);
            } else {
              notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
            }

      } catch {
          console.log('error');
          notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    },[]);

    const modifyEdgeInfo = useCallback( async (e, item) => {
      e.preventDefault();
      setIsInterval(false);

      let isError : boolean = false;

      try {
        const _api = deepCopyObject(API.ROAD_DETAIL);
        _api.url = _api.url.replace('_ID_', item.deviceId);

        const res = await callApi(_api);

        if (res.success) {

          /*res.data = {
            ...res.data,
            dupCheck : false
          };*/

          setSelectUser(null);
          setSelectItem(res.data);
          setPopShow('aside_modify');
          setSelectEdgeId(item.edgeId);
        } else {
          isError = true;
        }

      } catch {
        isError = true;
      } finally {
        if (isError) {
          notiStore.error('데이터 호출 중 오류가 발생하였습니다. 잠시후 다시 이용바랍니다.');
          setIsInterval(true);
        }
      }

    },[]);

    const modifySaveCallback = useCallback ( async (item) => {
      let isError : boolean = false;

      try {
            /*if (!item.dupCheck) {
              notiStore.error('아이디 중복 체크 후 등록 가능합니다.');
              return false;
            }*/

            if (!validationCheck('aside_modify')) return false;

            const _api = deepCopyObject(API.ROAD_EDIT);
            _api.url = _api.url.replace('_ID_', item.deviceId);

            const res = await callApi(_api, item);
            if (res.success) {
              notiStore.info('디바이스가 수정되었습니다.');
              closePopup();
              callApiEdgeRoadList(1);
            } else {
              isError = true;
            }

      } catch {
          isError = true;
      } finally {
        if (isError) notiStore.error('수정 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    }, [search]);

    const insertEdgeInfo = (e) => {
      e.preventDefault();
      setSelectUser(null);
      setIsInterval(false);
      setPopShow('aside_add');
    }

    const insertCallback = useCallback( async (item) => {

      let isError : boolean = false;

      try {
            if (!item.dupCheck) {
              notiStore.error('아이디 중복 체크 후 등록 가능합니다.');
              return false;
            }

            if (!validationCheck('aside_add')) return false;

            const res = await callApi(API.ROAD_ADD, item);
            if (res.success) {

              if (res.data) {
                notiStore.info('디바이스가 등록되었습니다.');
                closePopup();
                callApiEdgeRoadList(1);
              } else {
                notiStore.error('사용중인 Edge 아이디 입니다. Edge 아이디를 다시 입력 바랍니다.');
                setReDupCheck(!reDupCheck);
              }

            } else {
              isError = true;
            }

      } catch {
          isError = true;
      } finally {
          if (isError) notiStore.error('등록 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }
    },[search, reDupCheck]);

    const closePopup = () => {
      setPopShow('');
      setIsInterval(true);
      setSelectEdgeId('');
    };

    const handleOnUserChange = ({name, value}) => {
      setUserSearch({
        ...userSearch,
        [name] : value
      });
    };

    const sendUser = (e, items) => {
      e.preventDefault();

      const itemLen = items.length;
      if(itemLen == 0){
        notiStore.error('선택된 사용자 정보가 없습니다.');
        return false;
      }
      if(itemLen > 1){
        notiStore.error('1건만 선택해주시기 바랍니다.');
        return false;
      }

      const item : any = items[0];

      setSelectUser({
        userId : item.id,
        userNm : item.name
      })
      setUserSearchPop(false);
      //setPopShow('aside_add');
    };

    const userSearchPopOnclick = (e, clientCompanyId) => {
      e.preventDefault();
      setSelectClientCompanyId(clientCompanyId);
      setUserSearchPop(!userSearchPop);
    };

    const userClosePopup = () => {
      setUserSearchPop(false);
    };

    const roadEdgeRealTime = async (deviceId) => {
      let url = '/edge/roadRealTime';
      let state = {deviceId : deviceId};
      history.push(url, state);
    };

    const showAlertConfirm = (e) => {
      e.preventDefault();
      setIsInterval(false);
      alertStore.confirm('삭제된 항목은 복구가 불가능합니다.','정말 삭제하시겠습니까?', delClickEdgeRoad, () => { setIsInterval(true); });
    }

    const delClickEdgeRoad = useCallback(async () => {
      let isError : boolean = false;

      try {
        const res = await callApi(API.ROAD_DEL, checkItems);

        if (res.success) {
          notiStore.info('디바이스 삭제가 완료되었습니다.');
          setCheckItems([]);
          callApiEdgeRoadList(1);
          closePopup();
        } else {
          isError = true;
        }

      } catch {
        isError = true;
      } finally {
        if (isError) notiStore.error(`디바이스 삭제 중 오류가 발생하였습니다.`);
      }
    },[checkItems, search]);

    const callApiEdgeRoadList = useCallback(async (pageNum) => {
        let isError : boolean = false;
        setCheckItems([]);
        setPopShow('');

        try {
            setIsInterval(false);
            const params = _.cloneDeep(search);
            params.page = pageNum;
            params.startDate = (params.startDate) ? params.startDate.format('yyyy-MM-dd') : '';
            params.endDate = (params.endDate) ? params.endDate.format('yyyy-MM-dd') : '';

            if(!dateDiff(params.startDate, params.endDate)){
              notiStore.error('종료일이 시작일보다 전입니다. 확인해주세요.');
              return false;
            }

            const res = await callApi(API.ROAD_LIST, params);

            if(res.status != 200){
              isError = true;
              setTotalPages(0);
            }else{
              setManagementList(res.data.content);
              setTotalPages(res.data.totalPages);
              setCurrentPage(pageNum);
              if (res.data.content.length > 0) setIsInterval(true);
            }

        } catch {
            isError = true;
            console.log('error');
        } finally {
            if (isError) notiStore.error(`리스트 호출 중 에러가 발생하였습니다. 잠시 후 다시 이용 바랍니다.`);
        }
    },[totalPages, search]);

    const refreshApiData = useCallback(async () => {
      try {

        setIsInterval(false);
        const params : any = managementList.map(data => {
          return {deviceId : data.deviceId, edgeId : data.edgeId};
        });

        const res = await callApi(API.ROAD_REFRESH, params);

        if (res.success) {
          const list : any = managementList.map(data => {
            const json : any = res.data.find(item => {return data.deviceId == item.deviceId});
            return {
              ...data,
              cpu : json.cpu,
              edgeAiStatus : json.edgeAiStatus,
              gpu : json.gpu,
              gpuMem : json.gpuMem,
              mem : json.mem
            }
          });

          const selEdge : any = res.data.find(item => {return edgeInfo.deviceId == item.deviceId});
          setEdgeInfo(selEdge ? selEdge : {});

          setManagementList(list);
          setIsInterval(true);
        }

      } catch {

      }
    },[managementList, edgeInfo]);


    const iptCheckOnClick = (checked, deviceId : number) => {
      if (checked) {
        setCheckItems([...checkItems, deviceId]);
      } else {
        setCheckItems(checkItems.filter((el) => el != deviceId));
      }
    };

    const iptCheckAllClick = (checked) => {
      if (checked) {
        const checkItemsArr : any = [];
        managementList.map((item) => {
          checkItemsArr.push(item.deviceId);
        });

        setCheckItems(checkItemsArr);
      } else {
        setCheckItems([]);
      }
    };

    useEffect(() => {
        getCodeList(['EDGE_AI_STATUS']).then((result : any) => {
          setCodes(result);
        });
        callApiEdgeRoadList(1).then(() => {
          hideLoading();
        });
    }, []);

    let timer : any = null;
    useEffect(() => {

      if (isInterval) {
        if (refreshTime <= 0) {
          refreshApiData();
        } else {
          timer = setTimeout(() => {
            setRefreshTime(refreshTime - 1);
          },1000);
        }
      } else {
        setRefreshTime(initRefreshTime);
      }

      return () => {
        clearTimeout(timer);
      };

    },[refreshTime, isInterval]);

  return(
      <>
        <div className='tbl_filterBox'>
            <ComSearchBar callback={onSearchBtnCallback} initBtnCallback={initSearchCallback}>
              <ComSearchSelectInput title='검색어' callback={onSearchBtnCallback} onChange={handleOnChange} options={searchTypeOptions} selectName='searchType' selectValue={search.searchType} inputName='searchTxt' inputValue={search.searchTxt} />
              <ComSearchSelect title='Edge 상태' onChange={handleOnChange} options={codes.EDGE_AI_STATUS} name='edgeAiStatus' value={search.edgeAiStatus} />
              <ComSearchDate title='설치일' onChange={handleOnChange} name='edgeStatus' startDateName='startDate' startDateValue={search.startDate} endDateName='endDate' endDateValue={search.endDate} />
            </ComSearchBar>
            <div className='tbl_top'>
              <h4>Edge Road 목록</h4>
              <div className='fr'>
                  <p className='infoTxt'><span className='timer'>{refreshTime}</span>초 후에 자동 새로고침 됩니다.</p>
              </div>
            </div>
            <ManagementTable
              mntList={managementList}
              checkItems={checkItems}
              listSize={search.size}
              currentPage={currentPage}
              iptCheckAllClick={iptCheckAllClick}
              iptCheckOnClick={iptCheckOnClick}
              viewEdgeInfo={viewEdgeInfo}
              closePopup={closePopup}
              modifyEdgeInfo={modifyEdgeInfo}
              selectEdgeId={selectEdgeId}
               />
            <div className='tbl_btm'>
              <div className='btnBox fl'>
                <a href='#alt_edgeLstRemove' id='btn_lst_remove' className={checkItems.length > 0 ? 'btn_alt_open' : 'btn_alt_open disabled'} onClick={showAlertConfirm}><span className='fas fa-times'></span>삭제</a>
              </div>
              <Paging totalPages={totalPages} paginate={callApiEdgeRoadList} currentPage={currentPage}/>
              <div className='btnBox fr'>
                <a id='btn_edge_add' href='#none' onClick={insertEdgeInfo}><span className='fas fa-plus'></span>등록</a>
              </div>
            </div>
          </div>
          <ComSideBox show={popShow}>
            <EdgeManagementView show={popShow} close={closePopup} data={edgeInfo} roadEdgeRealTime={roadEdgeRealTime}/>
            <EdgeManagementModify show={popShow}
                                  close={closePopup}
                                  item={selectItem}
                                  isAdmin={true}
                                  saveBtnCallback={modifySaveCallback}
                                  duplicateFunc={duplicateEdgeId}
                                  selectUser={selectUser}
                                  userSearchPopOnclick={userSearchPopOnclick} />
            <EdgeManagementInsert show={popShow}
                                  close={closePopup}
                                  selectUser={selectUser}
                                  insertCallback={insertCallback}
                                  duplicateFunc={duplicateEdgeId}
                                  userSearchPopOnclick={userSearchPopOnclick}
                                  dupCheck={reDupCheck} />
          </ComSideBox>
          <ComPopBox show={userSearchPop} >
            <PopUser onChange={handleOnUserChange} sendUser={sendUser} close={userClosePopup} clientCompanyId={selectClientCompanyId} popTitle={'사용자 검색'} isEdgeUser={true} />
          </ComPopBox>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(ManagementTableSection);
