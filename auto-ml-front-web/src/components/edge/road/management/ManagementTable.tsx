import React, {memo} from 'react';
import {ApiListResponse} from 'components/edge/ManagementList';
import ManagementTableRow from './ManagementTableRow';

interface Props {
    mntList : ApiListResponse[];
    checkItems : any;
    iptCheckAllClick : (e) => void;
    iptCheckOnClick : (e, deviceId : number) => void;
    closePopup : (e) => void;
    viewEdgeInfo : (e, item : any) => void;
    modifyEdgeInfo : (e, item : any) => void;
    selectEdgeId : string;
    currentPage : number;
    listSize : number;
}

const ManagementTable: React.FC<Props> = ({ mntList, iptCheckAllClick, iptCheckOnClick, viewEdgeInfo, closePopup, modifyEdgeInfo, checkItems, selectEdgeId, currentPage, listSize}) => {

  return(
      <div className='tbl_mid'>
        <table className='tbl_lst'>
            <caption className='hide'>Edge Road 목록</caption>
            <colgroup>
                <col width='25' /><col width='65' /><col /><col /><col />
                <col /><col /><col /><col /><col />
                <col width='80' /><col width='70' />
            </colgroup>
            <thead>
                <tr>
                    <th scope='col'>
                        <div className='checkOnlyBox'>
                            <input type='checkbox' name='ipt_tbl_check' id='ipt_check_all' className='ipt_check all' checked={(checkItems.length >= mntList.length && checkItems.length > 0) ? true : false} onChange={(e) =>{ iptCheckAllClick(e.target.checked) }} />
                            <label htmlFor='ipt_check_all'>체크박스</label>
                        </div>
                    </th>
                    <th scope='col'>No.</th>
                    <th scope='col'>회사명</th>
                    <th scope='col'>회사 ID</th>
                    <th scope='col'>사용자명</th>
                    <th scope='col'>설치장소</th>
                    <th scope='col'>Edge ID</th>
                    <th scope='col'>Edge 상태</th>
                    <th scope='col'>CPU/GPU 사용량</th>
                    <th scope='col'>설치일</th>
                    <th scope='col'>재시작</th>
                    <th scope='col'>수정</th>
                </tr>
            </thead>
            <tbody>
              {
                mntList.length > 0 ? (
                  mntList.map((item, index)=>((
                    <ManagementTableRow key={item.deviceId} item={item} index={((currentPage-1)*listSize)+(index + 1)} currentPage={currentPage} listSize={listSize} selectEdgeId={selectEdgeId} checkItems={checkItems} iptCheckOnClick={iptCheckOnClick} viewEdgeInfo={viewEdgeInfo} closePopup={closePopup} modifyEdgeInfo={modifyEdgeInfo} />
                  )))
                ) : (
                  <tr>
                    <td scope='row' colSpan={12} className='dataNone'>데이터가 없습니다.</td>
                  </tr>
                )
              }
            </tbody>
          </table>
      </div>
  )
}

export default React.memo(ManagementTable);
