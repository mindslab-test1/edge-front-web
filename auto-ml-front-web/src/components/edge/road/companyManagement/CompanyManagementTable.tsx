import React, {memo} from 'react';
import {ApiListResponse} from 'components/edge/CompanyManagementList';
import CompanyManagementTableRow from './CompanyManagementTableRow';

interface Props {
    companyList : ApiListResponse[];
    checkItems : string[];
    iptCheckAllClick : (e) => void;
    iptCheckOnClick : (e, edgeId : string) => void;
    modifyCompanyInfo : (e, clientCompanyId : string) => void;
    selectClientCompanyId : string;
}

const CompanyManagementTable: React.FC<Props> = ({ companyList, iptCheckAllClick, iptCheckOnClick, modifyCompanyInfo, checkItems, selectClientCompanyId}) => {

  return(
      <div className='tbl_mid'>
        <table className='tbl_lst'>
            <caption className='hide'>회사 목록</caption>
            <colgroup>
              <col width="25" />
              <col width="120" />
              <col width="200" />
              <col />
              <col width="120" />
              <col width="120" />
              <col width="120" />
            </colgroup>
            <thead>
                <tr>
                    <th scope='col'>
                        <div className='checkOnlyBox'>
                            <input type='checkbox' name='ipt_tbl_check' id='ipt_check_all' className='ipt_check all' checked={(checkItems.length >= companyList.length && checkItems.length > 0) ? true : false} onChange={(e) =>{ iptCheckAllClick(e.target.checked) }} />
                            <label htmlFor='ipt_check_all'>체크박스</label>
                        </div>
                    </th>
                    <th scope='col'>No.</th>
                    <th scope='col'>회사명</th>
                    <th scope='col'>회사 ID</th>
                    <th scope='col'>사용여부</th>
                    <th scope='col'>등록일</th>
                    <th scope='col'>수정</th>
                </tr>
            </thead>
            <tbody>
              {
                companyList.length > 0 ? (
                  companyList.map((item, index)=>((
                    <CompanyManagementTableRow key={item.clientCompanyId} index={index + 1} item={item} selectClientCompanyId={selectClientCompanyId} checkItems={checkItems} iptCheckOnClick={iptCheckOnClick} modifyCompanyInfo={modifyCompanyInfo} />
                  )))
                ) : (
                  <tr>
                    <td scope='row' colSpan={7} className='dataNone'>데이터가 없습니다.</td>
                  </tr>
                )
              }
            </tbody>
          </table>
      </div>
  )
}

export default React.memo(CompanyManagementTable);
