import    React, {useCallback, useEffect, useState, useContext} from 'react';
import {ApiListResponse, CompanyDetail} from 'components/edge/CompanyManagementList';
import RootStore from 'store/RootStore';
import Paging from 'components/commons/Paging';
import _ from 'lodash';
import {API, callApi} from 'utils/ApiClient';
import {showLoading, hideLoading, deepCopyObject, getCodeList, dateDiff} from 'utils/CommonUtils';
import CompanyManagementTable from './CompanyManagementTable';
import ComSideBox from 'components/commons/popup/ComSideBox';
import CompanyManagementModify from 'components/popup/CompanyManagementModify';
import CompanyManagementInsert from 'components/popup/CompanyManagementInsert';
import ComSearchBar from 'components/commons/ComSearchBar';
import ComSearchSelect from 'components/commons/ComSearchSelect';
import ComSearchDate from 'components/commons/ComSearchDate';
import ComSearchSelectInput from 'components/commons/ComSearchSelectInput';

interface Props {

}

const CompanyManagementTableSection: React.FC<Props> = () => {
    const initSearch = {
      useYn : '',
      startDate : '',
      endDate : '',
      searchType : '',
      searchTxt : '',
      page : 1,
      size : 10
    };

    const searchTypeOptions = [
      {
        code : 'companyNm',
        codeNm : '회사명'
      },
      {
        code : 'clientCompanyId',
        codeNm : '회사ID'
      }
    ];

    const options = [
      {
        code : 'Y',
        codeNm : '사용'
      },
      {
        code : 'N',
        codeNm : '미사용'
      }
    ];

    const clientCompanyDetail = {
      clientCompanyId : '',
      clientCompanyName : '',
      useYn : 'Y',
      dupCheck : false
    }

    const [companyList, setCompanyList] = useState<ApiListResponse[]>([]);
    const [totalPages, setTotalPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [selectItem, setSelectItem] = useState<CompanyDetail>(clientCompanyDetail);
    const [checkItems, setCheckItems] = useState<string[]>([]);
    const [search, setSearch] = useState(initSearch);
    const [popShow, setPopShow] = useState('');
    const [selectClientCompanyId, setSelectClientCompanyId] = useState('');
    const {userStore, notiStore, alertStore} = useContext(RootStore);
    const [codes, setCodes] = useState<any>({});
    const [reDupCheck, setReDupCheck] = useState<boolean>(false);

    const onSearchBtnCallback = (e) => {
      callApiProjectList(1);
    };

    const initSearchCallback = (e) => {
      setSearch(initSearch);
    };

    const handleOnChange = ({name, value}) => {
      setSearch({
        ...search,
        [name] : value
      });
    };

    const modifyCompanyInfo = useCallback(async (e, clientCompanyId) => {
      e.preventDefault();
      let isError : boolean = false;

      try {
        const _api = deepCopyObject(API.COMPANY_DETAIL);
        _api.url = _api.url.replace('_ID_', clientCompanyId);

        const res = await callApi(_api);

        const data : any = res.data;

        if (data.success) {

          data.data = {
            ...data.data,
            dupCheck : false
          };

          setPopShow('aside_modify');
          setSelectItem(data.data);
          setSelectClientCompanyId(clientCompanyId);
        } else {
          isError = true;
        }
      } catch {
        isError = true;
      } finally {
        if (isError) notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    },[]);

    const companyNameCheckFunc = useCallback(async (name, id) => {
      return new Promise(async (resolve, reject) => {
        const res = await callApi(API.COMPANY_DUPLICATE, {clientCompanyName : name, clientCompanyId : id});

        if (res.success) {
            resolve(res.data);
        } else {
            reject(false);
        }

      });
    },[]);

    const validationCheck = (item) => {
      if (item.clientCompanyName == '') {
        notiStore.error('회사 명을 입력해주세요.');
        return false;
      }

      if (!item.dupCheck) {
        notiStore.error('회사명 중복 체크 후 등록 가능합니다.');
        return false;
      }

      return true;
    }

    const companyModifyCallback = useCallback(async (item : CompanyDetail) => {
      if (selectItem.clientCompanyName == item.clientCompanyName) {
        item.dupCheck = true;
      }

      if (!validationCheck(item)) return false;
      let isError : boolean = false;

      try {

        const _api = deepCopyObject(API.COMPANY_EDIT);
        _api.url = _api.url.replace('_ID_', item.clientCompanyId);
        const res = await callApi(_api, item);
        const data : any = res.data;

        if (data.success) {

          if (data.data) {
            notiStore.infoBlue('회사가 정상적으로 수정 되었습니다.');
            closePopup();
            callApiProjectList(1);
          } else {
            notiStore.error('사용 중인 회사명 입니다. 회사명을 다시 입력 바랍니다.');
            setReDupCheck(!reDupCheck);
          }

        } else {
          isError = true;
        }
      } catch {
        isError = true;
      } finally {
        if (isError) notiStore.error('수정 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    },[search, selectItem, reDupCheck]);

    const companyInsertCallback = useCallback(async (item) => {

      if (!validationCheck(item)) return false;
      let isError : boolean = false;

      try {
        const res = await callApi(API.COMPANY_ADD, item);
        const data : any = res.data;

        if (data.success) {

          if (data.data) {
            notiStore.infoBlue('회사가 등록되었습니다.');
            closePopup();
            callApiProjectList(1);
          } else {
            notiStore.error('사용 중인 회사명 입니다. 회사명을 다시 입력 바랍니다.');
            setReDupCheck(!reDupCheck);
          }

        } else {
          isError = true;
        }
      } catch {
        isError = true;
      } finally {
        if (isError) notiStore.error('등록 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    }, [search, reDupCheck]);

    const insertEdgeInfo = (e) => {
      e.preventDefault();
      setPopShow('aside_add');
    };

    const closePopup = () => {
      setPopShow('');
      setSelectClientCompanyId('');
    };

    const callApiProjectList = useCallback(async (pageNum) => {
        setCheckItems([]);
        setPopShow('');

        try {
            const params = _.cloneDeep(search);
            params.page = pageNum;
            params.startDate = (params.startDate) ? params.startDate.format('yyyy-MM-dd') : '';
            params.endDate = (params.endDate) ? params.endDate.format('yyyy-MM-dd') : '';

            if(!dateDiff(params.startDate, params.endDate)){
              notiStore.error('종료일이 시작일보다 전입니다. 확인해주세요.');
              return false;
            }

            const res = await callApi(API.COMPANY_LIST, params);
            const data : any = res.data;

            if (data.success) {

              setCompanyList(data.data.content);
              setTotalPages(data.data.totalPages);
              setCurrentPage(pageNum);
            }

        } catch {
            setCompanyList([]);
            console.log('error');
        }
    },[totalPages, search]);


    const iptCheckOnClick = (checked, clientCompanyId) => {
      if (checked) {
        setCheckItems([...checkItems, clientCompanyId]);
      } else {
        setCheckItems(checkItems.filter((el) => el != clientCompanyId));
      }
    };

    const iptCheckAllClick = (checked) => {
      if (checked) {
        const checkItemsArr : string[] = [];
        companyList.map((item) => {
          checkItemsArr.push(item.clientCompanyId);
        });

        setCheckItems(checkItemsArr);
      } else {
        setCheckItems([]);
      }
    };

    const showAlertConfirm = (e) => {
      e.preventDefault();
      alertStore.confirm('삭제된 항목은 복구가 불가능합니다.','정말 삭제하시겠습니까?', delClickCompany);
    }

    const delClickCompany = useCallback(async (e) => {
      e.preventDefault();
      let isError : boolean = false;

      try {
          const res = await callApi(API.COMPANY_DEL, checkItems);
          const data : any = res.data;

          if (data.success) {

            if (data.data) {
              notiStore.infoBlue('삭제가 완료되었습니다.');
            } else {
              notiStore.error('삭제 할 회사를 다시 체크 후 삭제 바랍니다.');
            }

            callApiProjectList(1);
            setCheckItems([]);
            setPopShow('');

          } else {
            isError = true;
          }

      } catch {
        isError = true;
      } finally {
        if (isError) notiStore.error('삭제 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    },[checkItems, search]);

    useEffect(() => {
        getCodeList(['USE_YN']).then((result : any) => {
          setCodes(result);
        });

        callApiProjectList(1).then(r => {
          hideLoading();
        });
    }, [])

  return(
      <>
        <div className='tbl_filterBox'>
            <ComSearchBar callback={onSearchBtnCallback} initBtnCallback={initSearchCallback}>
              <ComSearchSelectInput title='검색어' callback={onSearchBtnCallback} onChange={handleOnChange} options={searchTypeOptions} selectName='searchType' selectValue={search.searchType} inputName='searchTxt' inputValue={search.searchTxt} />
              <ComSearchSelect title='시스템 사용여부' onChange={handleOnChange} options={codes.USE_YN} name='useYn' value={search.useYn} />
              <ComSearchDate title='등록일' onChange={handleOnChange} name='edgeStatus' startDateName='startDate' startDateValue={search.startDate} endDateName='endDate' endDateValue={search.endDate} />
            </ComSearchBar>
            <div className='tbl_top'>
              <h4>회사 목록</h4>
            </div>
            <CompanyManagementTable
              companyList={companyList}
              checkItems={checkItems}
              iptCheckAllClick={iptCheckAllClick}
              iptCheckOnClick={iptCheckOnClick}
              modifyCompanyInfo={modifyCompanyInfo}
              selectClientCompanyId={selectClientCompanyId}
               />
            <div className='tbl_btm'>
              <div className='btnBox fl'>
                <a id='btn_lst_remove' className={checkItems.length > 0 ? 'btn_alt_open' : 'btn_alt_open disabled'} onClick={showAlertConfirm}><span className='fas fa-times'></span>삭제</a>
              </div>
              <Paging totalPages={totalPages} paginate={callApiProjectList} currentPage={currentPage}/>
              <div className='btnBox fr'>
                <a id='btn_edge_add' href='#none' onClick={insertEdgeInfo}><span className='fas fa-plus'></span>등록</a>
              </div>
            </div>
          </div>
          <ComSideBox show={popShow}>
            <CompanyManagementModify show={popShow} close={closePopup} item={selectItem} companyModifyCallback={companyModifyCallback} duplicateFunc={companyNameCheckFunc} dupCheck={reDupCheck} />
            <CompanyManagementInsert show={popShow} close={closePopup} companyInsertCallback={companyInsertCallback} duplicateFunc={companyNameCheckFunc} dupCheck={reDupCheck} />
          </ComSideBox>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(CompanyManagementTableSection);
