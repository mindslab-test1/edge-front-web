import React, {useState} from 'react';
import {ApiListResponse} from 'components/edge/CompanyManagementList';
import ReactTooltip from 'react-tooltip';
import {ERouteUrl} from 'router/RouteLinks';
import {Link} from 'react-router-dom';
import {phoneFormat} from 'utils/CommonUtils';

interface Props {
    item : ApiListResponse;
    checkItems : string[];
    iptCheckOnClick : (e, edgeId : string) => void;
    modifyCompanyInfo : (e, clientCompanyId : string) => void;
    selectClientCompanyId : string;
    index : number;
}

const CompanyManagementTableRow: React.FC<Props> = ({item, checkItems, iptCheckOnClick, modifyCompanyInfo, selectClientCompanyId, index}) => {

  return(
      <>
        <tr className={selectClientCompanyId == item.clientCompanyId ? 'active':''}>
            <td scope='row'>
                <div className='checkOnlyBox'>
                    <input type='checkbox' name='ipt_tbl_check' id={'ipt_check' + index} className='ipt_check' checked={checkItems.includes(item.clientCompanyId) ? true : false} onChange={(e) => {iptCheckOnClick(e.target.checked, item.clientCompanyId)}} />
                    <label htmlFor={'ipt_check' + index}>체크박스</label>
                </div>
            </td>
            <td>{index}</td>
            <td className='tbl_company'>{item.clientCompanyName}</td>
            <td className='tbl_companyID'>{item.clientCompanyId}</td>
            <td>{item.useYn == 'Y' ? '사용':'미사용'}</td>
            <td>{item.regDt}</td>
            <td>
                <a href='#none' className='btn_line btn_edge_modify' onClick={(e) => {modifyCompanyInfo(e, item.clientCompanyId)}}>수정</a>
            </td>
        </tr>
      </>
  )
}

export default React.memo(CompanyManagementTableRow);
