import React, {memo} from 'react';
import {ApiListResponse} from 'components/edge/InquiryList';
import InquiryTableRow from './InquiryTableRow';

interface Props {
    mntList : ApiListResponse[];
    closePopup : (e) => void;
    viewEdgeInfo : (e, item : any) => void;
    modifyEdgeInfo : (e, item : any) => void;
    selectEdgeId : string;
    currentPage : number;
    listSize : number;
}

const InquiryTable: React.FC<Props> = ({ mntList, viewEdgeInfo, closePopup, modifyEdgeInfo, selectEdgeId, currentPage, listSize}) => {

  return(
      <div className='tbl_mid'>
        <table className='tbl_lst'>
            <caption className='hide'>Edge Road 목록</caption>
            <colgroup>
              <col width='65' />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
            </colgroup>
            <thead>
                <tr>
                  <th scope='col'>No.</th>
                  <th scope='col'>회사명</th>
                  <th scope='col'>사용자명</th>
                  <th scope='col'>Edge ID</th>
                  <th scope='col'>설치장소</th>
                  <th scope='col'>Edge 상태</th>
                  <th scope='col'>CPU/GPU 사용량</th>
                  <th scope='col'>설치일</th>
                  <th scope='col'>재시작</th>
                  <th scope='col'>정보 수정</th>
                </tr>
            </thead>
            <tbody>
              {
                mntList.length > 0 ? (
                  mntList.map((item, index)=>((
                    <InquiryTableRow key={item.deviceId} item={item} index={((currentPage-1)*listSize)+(index + 1)} viewEdgeInfo={viewEdgeInfo} closePopup={closePopup} modifyEdgeInfo={modifyEdgeInfo} selectEdgeId={selectEdgeId} />
                  )))
                ) : (
                  <tr>
                    <td scope='row' colSpan={12} className='dataNone'>데이터가 없습니다.</td>
                  </tr>
                )
              }
            </tbody>
          </table>
      </div>
  )
}

export default React.memo(InquiryTable);
