import React, {useState, useContext, useCallback} from 'react';
import {API, callApi} from 'utils/ApiClient';
import {ApiListResponse} from 'components/edge/InquiryList';
import ReactTooltip from 'react-tooltip';
import {ERouteUrl} from 'router/RouteLinks';
import {Link} from 'react-router-dom';
import RootStore from 'store/RootStore';
import {deepCopyObject} from 'utils/CommonUtils';

interface Props {
    item : ApiListResponse;
    closePopup : (e) => void;
    viewEdgeInfo : (e, item : any) => void;
    modifyEdgeInfo : (e, item : any) => void;
    selectEdgeId : string;
    index : number;
}

const InquiryTableRow: React.FC<Props> = ({item, closePopup, viewEdgeInfo, modifyEdgeInfo, selectEdgeId, index}) => {
  const [isRestart, setIsRestart] = useState(false);
  const {notiStore} = useContext(RootStore);
  let codes = '';

  const edgeStatusView = (status) => {
    if (status.toLowerCase() == 'Working'.toLowerCase()) {
      return <span className='normal' title='모든 모듈 작동 중'>정상 작동</span>;
    } else if (status.toLowerCase() == 'Abnormal'.toLowerCase()) {
      return <span className='abnormal' title='일부 모듈만 작동 중'>비정상 작동</span>;
    } else if (status.toLowerCase() == 'Notworking'.toLowerCase()) {
      return <span className='error' title='모든 모듈 작동 정지'>작동 안 함</span>;
    } else {
      return <span className='not' title='edge 장치와 통신이 안됨'>연결 안 됨</span>;
    }
  };

  const edgeGaugeView = (gaugeName, per) => {
    //CPU
    if ( gaugeName === 'CPU') {
        if ( 70 <= per ) {
            return <em className='warning' style={{width:per + '%'}}></em>;
        } else if ( 60 <= per && per < 70 ) {
            return <em className='warn' style={{width:per + '%'}}></em>;
        } else if ( 0 <= per && per < 60 ) {
            return <em className='safety' style={{width:per + '%'}}></em>;
        }
    }

    //GPU
    if ( gaugeName === 'GPU') {
        if ( 0 < per ) {
            return <em style={{width:per + '%'}}></em>;
        }
    }
  };

  const restartBtnOnClick = useCallback( async (e, deviceId) => {
    e.preventDefault();
    setIsRestart(true);
    let isError : boolean = false;

    try {

            const _api = deepCopyObject(API.DEVICE_RESTART);
            _api.url = _api.url.replace('_ID_', item.deviceId);
            const res = await callApi(_api);
            if (res.success) {
              if (res.data == 'notconnect') {
                  notiStore.error('등록된 Edge 아이디가 아닙니다. 설치 완료 후 재시도 하시길 바랍니다.');
              } else if (res.data == 'fail') {
                  isError = true;
              }

            } else {
              isError = true;
            }

          } catch {
            isError = true;
          } finally {
            if (isError) notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
            setIsRestart(false);
          }

  },[]);


  return(
      <>
        <tr className={selectEdgeId == item.edgeId ? 'active':''}>
            <td>{index}</td>
            <td className='tbl_company'>{item.clientCompanyName}</td>
            <td className='tbl_userName'>{item.name}</td>
            <td className='tbl_edgeID'><a href='#none' className='link w150' onClick={(e) => {viewEdgeInfo(e, item)}}>{item.edgeId}</a></td>
            <td className='tbl_edgePlace'>
              <p>
                  <span>{item.addr}</span>
              </p>
              <p>
                  <em>{item.addrDetail}</em>
                  <strong>{item.cameraDir}&nbsp;방향</strong>
              </p>
            </td>
            <td>
                <div className='status_edge'>
                    {edgeStatusView(item.edgeAiStatus)}
                </div>
            </td>
            <td>
                <div className='status_usage'>
                    <dl>
                        <dt>CPU</dt>
                        <dd className='gauge'>
                            {edgeGaugeView('CPU', item.cpu)}
                            <span>{item.cpu}%</span>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Main_Mem</dt>
                        <dd className='gauge'>
                            {edgeGaugeView('CPU', item.mem)}
                            <span>{item.mem}%</span>
                        </dd>
                    </dl>
                    <dl>
                        <dt>GPU</dt>
                        <dd className='gauge'>
                            {edgeGaugeView('GPU', item.gpu)}
                            <span>{item.gpu}%</span>
                        </dd>
                    </dl>
                    <dl>
                        <dt>GPU_Mem</dt>
                        <dd className='gauge'>
                            {edgeGaugeView('CPU', item.gpuMem)}
                            <span>{item.gpuMem}%</span>
                        </dd>
                    </dl>
                </div>
            </td>
            <td>{item.setDt}</td>
            <td>
                {/*{
                item.edgeAiStatus.toLowerCase() != 'Working'.toLowerCase() ? ((
                  <>
                  <a href='#none' className='btn_line btn_edge_restart' onClick={(e) => {restartBtnOnClick(e, item.deviceId)}}>재시작</a>
                  {isRestart ? <span className='msg_restart'>다시 시작하는 중</span> : ''}
                  </>
                )) : <></>
              }*/}
              <a href='#none' className='btn_line btn_edge_restart' onClick={(e) => {restartBtnOnClick(e, item.deviceId)}}>재시작</a>
              {isRestart ? <span className='msg_restart'>다시 시작하는 중</span> : ''}
            </td>
            <td>
                <a href='#none' className='btn_line btn_edge_modify' onClick={(e) => {modifyEdgeInfo(e, item)}}>수정</a>
            </td>
        </tr>
      </>
  )
}

export default React.memo(InquiryTableRow);
