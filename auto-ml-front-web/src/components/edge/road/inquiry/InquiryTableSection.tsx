import    React, {useCallback, useEffect, useState, useContext} from 'react';
import {ApiListResponse} from 'components/edge/InquiryList';
import Paging from 'components/commons/Paging';
import RootStore from 'store/RootStore';
import {API, callApi} from 'utils/ApiClient';
import {showLoading, hideLoading, deepCopyObject, validationCheck, getCodeList, dateDiff} from 'utils/CommonUtils';
import _ from 'lodash';
import InquiryTable from './InquiryTable';
import ComSideBox from 'components/commons/popup/ComSideBox';
import EdgeManagementView from 'components/popup/EdgeManagementView';
import EdgeManagementModify from 'components/popup/EdgeManagementModify';
import ComSearchBar from 'components/commons/ComSearchBar';
import ComSearchSelect from 'components/commons/ComSearchSelect';
import ComSearchDate from 'components/commons/ComSearchDate';
import ComSearchSelectInput from 'components/commons/ComSearchSelectInput';
import { Link, useHistory } from 'react-router-dom';

interface Props {

}

const InquiryTableSection: React.FC<Props> = (navigation) => {
    const initSearch = {
      edgeAiStatus : '',
      startDate : '',
      endDate : '',
      searchType : '',
      searchTxt : '',
      page : 1,
      size : 10
    };

    const searchTypeOptions = [
      {
        code : 'companyNm',
        codeNm : '회사명'
      },
      {
        code : 'userNm',
        codeNm : '사용자명'
      },
      {
        code : 'edgeId',
        codeNm : 'Edge Id'
      },
      {
        code : 'addrNm',
        codeNm : '설치장소'
      }
    ];

    const initRefreshTime : number = 10;
    const [managementList, setManagementList] = useState<ApiListResponse[]>([]);
    const [totalPages, setTotalPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [selectItem, setSelectItem] = useState({});
    const [edgeInfo, setEdgeInfo] = useState<any>({});
    const [search, setSearch] = useState(initSearch);
    const [popShow, setPopShow] = useState('');
    const [selectEdgeId, setSelectEdgeId] = useState('');
    const [userSearchPop, setUserSearchPop] = useState(false);
    const [selectUser, setSelectUser] = useState<any>(null);
    const [userSearch, setUserSearch] = useState<any>({});
    const {userStore, notiStore, alertStore} = useContext(RootStore);
    const [refreshTime, setRefreshTime] = useState<number>(initRefreshTime);
    const [isInterval, setIsInterval] = useState<boolean>(false);
    const [codes, setCodes] = useState<any>({});
    let history = useHistory();

    const onSearchBtnCallback = (e) => {
      callApiEdgeRoadList(1);
    };

    const initSearchCallback = (e) => {
      setSearch(initSearch);
    }

    const handleOnChange = ({name, value}) => {
      setSearch({
        ...search,
        [name] : value
      });
    };

    const roadEdgeRealTime = async (deviceId) => {
      //roadEdgeSetCheck(deviceId);
      let url = '/edge/roadRealTime';
      let state = {deviceId : deviceId};
      history.push(url, state);
    };

    const roadEdgeSetCheck = async(deviceId) =>{
      try{
        const res = await callApi(API.DEVICE_SET_CHECK, {deviceId : deviceId});

        console.log(res);
        

      }catch{
        console.log('error');
        notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }
    };

    const viewEdgeInfo = useCallback( async (e, item) => {
      e.preventDefault();
      let isError : boolean = false;

      try {
            const _api = deepCopyObject(API.DEVICE_INFO);
            _api.url = _api.url.replace('_ID_', item.deviceId);

            const res = await callApi(_api);
            if (res.success) {
              setPopShow('aside_view');
              setEdgeInfo(res.data);
              setSelectEdgeId(item.edgeId);
            } else {
              isError = true;
            }

      } catch {
          console.log('error');
          isError = true;
      } finally {
        if (isError) notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }

    },[]);

    const modifyEdgeInfo = useCallback( async (e, item) => {
      e.preventDefault();
      setIsInterval(false);
      let isError : boolean = false;

      try {
        const _api = deepCopyObject(API.ROAD_DETAIL);
        _api.url = _api.url.replace('_ID_', item.deviceId);

        const res = await callApi(_api);

        if (res.success) {
          setSelectUser(null);
          setSelectItem(res.data);
          setPopShow('aside_modify');
          setSelectEdgeId(item.edgeId);
        } else {
          isError = true;
        }

      } catch {
        isError = true;
      } finally {
        if (isError) {
          notiStore.error('데이터 호출 중 오류가 발생하였습니다. 잠시후 다시 이용바랍니다.');
          setIsInterval(true);
        }
      }

    },[]);

    const modifySaveCallback = useCallback ( async (item) => {
      let isError : boolean = false;

      try {
            if (!validationCheck('aside_modify')) return false;

            const _api = deepCopyObject(API.ROAD_EDIT);
            _api.url = _api.url.replace('_ID_', item.deviceId);

            const res = await callApi(_api, item);
            if (res.success) {
              notiStore.info('디바이스가 수정되었습니다.');
              closePopup();
              callApiEdgeRoadList(1);
            } else {
              isError = true;
            }

      } catch {
          console.log('error');
          isError = true;
      } finally {
        if (isError) notiStore.error('수정 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      }
    }, [search]);



    const userSearchPopOnclick = (e) => {
      e.preventDefault();
    };

    const closePopup = () => {
      setPopShow('');
      setIsInterval(true);
      setSelectEdgeId('');
    };

    const callApiEdgeRoadList = useCallback(async (pageNum) => {
        let isError : boolean = false;
        setPopShow('');

        try {

            setIsInterval(false);
            const params = _.cloneDeep(search);
            params.page = pageNum;
            params.startDate = (params.startDate) ? params.startDate.format('yyyy-MM-dd') : '';
            params.endDate = (params.endDate) ? params.endDate.format('yyyy-MM-dd') : '';

            if(!dateDiff(params.startDate, params.endDate)){
              notiStore.error('종료일이 시작일보다 전입니다. 확인해주세요.');
              return false;
            }

            const res = await callApi(API.ROAD_LIST, params);

            if(res.status != 200){
              isError = true;
              setTotalPages(0);
            }else{
              setManagementList(res.data.content);
              setTotalPages(res.data.totalPages);
              setCurrentPage(pageNum);
              if (res.data.content.length > 0) setIsInterval(true);
            }

        } catch {
            isError = true;
            console.log('error');
        } finally {
            if (isError) notiStore.error(`리스트 호출 중 에러가 발생하였습니다. 잠시 후 다시 이용 바랍니다.`);
        }
    },[totalPages, search]);

    const refreshApiData = useCallback(async () => {
      try {
        setIsInterval(false);
        const params : any = managementList.map(data => {
          return {deviceId : data.deviceId, edgeId : data.edgeId};
        });

        const res = await callApi(API.ROAD_REFRESH, params);

        if (res.success) {
          const list : any = managementList.map(data => {
            const json : any = res.data.filter(item => data.deviceId == item.deviceId && data.edgeId == item.edgeId)[0];
            return {
              ...data,
              cpu : json.cpu,
              edgeAiStatus : json.edgeAiStatus,
              gpu : json.gpu,
              gpuMem : json.gpuMem,
              mem : json.mem
            }
          });

          const selEdge : any = res.data.find(item => {return edgeInfo.deviceId == item.deviceId});
          setEdgeInfo(selEdge ? selEdge : {});

          setManagementList(list);
          setIsInterval(true);
        }

      } catch {

      }
    },[managementList, edgeInfo]);

    useEffect(() => {
        getCodeList(['EDGE_AI_STATUS']).then((result : any) => {
          setCodes(result);
        });
        callApiEdgeRoadList(1).then(() => {
          hideLoading();
        });
    }, []);

    let timer : any = null;
    useEffect(() => {

      if (isInterval) {
        if (refreshTime <= 0) {
          refreshApiData();
        } else {
          timer = setTimeout(() => {
            setRefreshTime(refreshTime - 1);
          },1000);
        }
      } else {
        setRefreshTime(initRefreshTime);
      }

      return () => {
        clearTimeout(timer);
      };

    },[refreshTime, isInterval]);

  return(
      <>
        <div className='tbl_filterBox'>
          <ComSearchBar callback={onSearchBtnCallback} initBtnCallback={initSearchCallback}>
          <ComSearchSelectInput title='검색어' callback={onSearchBtnCallback} onChange={handleOnChange} options={searchTypeOptions} selectName='searchType' selectValue={search.searchType} inputName='searchTxt' inputValue={search.searchTxt} />
            <ComSearchSelect title='Edge 상태' onChange={handleOnChange} options={codes.EDGE_AI_STATUS} name='edgeAiStatus' value={search.edgeAiStatus} />
            <ComSearchDate title='설치일' onChange={handleOnChange} name='edgeStatus' startDateName='startDate' startDateValue={search.startDate} endDateName='endDate' endDateValue={search.endDate} />
          </ComSearchBar>
          <div className='tbl_top'>
              <h4>Edge Road 목록</h4>
              <div className='fr'>
                  <p className='infoTxt'><span className='timer'>{refreshTime}</span>초 후에 자동 새로고침 됩니다.</p>
              </div>
          </div>
          <InquiryTable
                mntList={managementList}
                viewEdgeInfo={viewEdgeInfo}
                closePopup={closePopup}
                modifyEdgeInfo={modifyEdgeInfo}
                selectEdgeId={selectEdgeId}
                listSize={search.size}
                currentPage={currentPage}
                 />
          <div className='tbl_btm'>
            <Paging totalPages={totalPages} paginate={callApiEdgeRoadList} currentPage={currentPage}/>
          </div>
        </div>
          <ComSideBox show={popShow}>
            <EdgeManagementView show={popShow} close={closePopup} data={edgeInfo} roadEdgeRealTime={roadEdgeRealTime}/>
            <EdgeManagementModify show={popShow}
                                  close={closePopup}
                                  item={selectItem}
                                  isAdmin={false}
                                  saveBtnCallback={modifySaveCallback}
                                  userSearchPopOnclick={userSearchPopOnclick} />
          </ComSideBox>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(InquiryTableSection);
