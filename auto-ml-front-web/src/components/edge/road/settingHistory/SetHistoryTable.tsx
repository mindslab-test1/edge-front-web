import React, {memo} from 'react';
import {ApiListResponse} from 'components/edge/SettingHistoryList';
import SetHistoryTableRow from './SetHistoryTableRow';

interface Props {
    mntList : ApiListResponse[];
    closePopup : (e) => void;
    viewEdgeInfo : (e, edgeId : string) => void;
    modifyEdgeInfo : (e, item, viewYn : any) => void;
    selectEdgeId : string;
}

const SetHistoryTable: React.FC<Props> = ({ mntList, viewEdgeInfo, closePopup, modifyEdgeInfo, selectEdgeId}) => {

  return(
      <div className='tbl_mid'>
        <table className='tbl_lst'>
            <caption className='hide'>Edge Road 목록</caption>
            <colgroup>
              <col width='65' />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
              <col />
            </colgroup>
            <thead>
                <tr>
                  <th scope='col'>No.</th>
                  <th scope='col'>회사명</th>
                  <th scope='col'>고객명</th>
                  <th scope='col'>연락처</th>
                  <th scope='col'>Edge ID</th>
                  <th scope='col'>설치장소</th>
                  <th scope='col'>Edge 상태</th>
                  <th scope='col'>재시작</th>
                  <th scope='col'>설치자</th>
                  <th scope='col'>설치완료일시</th>
                  <th scope='col'>설치상태</th>
                  <th scope='col'>설치완료처리</th>
                </tr>
            </thead>
            <tbody>
              {
                mntList.length > 0 ? (
                  mntList.map((item, index)=>((
                    <SetHistoryTableRow index={index} key={item.edgeId} item={item} viewEdgeInfo={viewEdgeInfo} closePopup={closePopup} modifyEdgeInfo={modifyEdgeInfo} selectEdgeId={selectEdgeId} />
                  )))
                ) : (
                  <tr>
                    <td scope='row' colSpan={12} className='dataNone'>데이터가 없습니다.</td>
                  </tr>
                )
              }
            </tbody>
          </table>
      </div>
  )
}

export default React.memo(SetHistoryTable);
