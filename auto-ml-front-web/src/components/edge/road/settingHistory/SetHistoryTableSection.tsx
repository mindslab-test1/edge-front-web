import    React, {useCallback, useEffect, useState, useContext} from 'react';
import {ApiListResponse} from 'components/edge/SettingHistoryList';
import Paging from 'components/commons/Paging';
import {API, callApi} from 'utils/ApiClient';
import {showLoading, hideLoading, getCodeList, apiShowLoading, apiHideLoading, dateDiff} from 'utils/CommonUtils';
import SetHistoryTable from './SetHistoryTable';
import ComSideBox from 'components/commons/popup/ComSideBox';
import EdgeManagementView from 'components/popup/EdgeManagementView';
import EdgeManagementModify from 'components/popup/EdgeManagementModify';
import SettingHistoryModify from 'components/popup/SettingHistoryModify';
import ComSearchBar from 'components/commons/ComSearchBar';
import ComSearchSelect from 'components/commons/ComSearchSelect';
import ComSearchDateTime from 'components/commons/ComSearchDateTime';
import ComSearchSelectInput from 'components/commons/ComSearchSelectInput';
import RootStore from 'store/RootStore';
import _ from 'lodash';

interface Props {

}

const SetHistoryTableSection: React.FC<Props> = () => {
    const initSearch = {
      edgeAiStatus : '',
      state : '',
      setState : '',
      startDate : '',
      endDate : '',
      searchType : 'all',
      searchTxt : ''
    };

    const [deviceSetList, setDeviceSetList] = useState<ApiListResponse[]>([]);
    const [totalPages, setTotalPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [selectItem, setSelectItem] = useState({});
    const [checkItems, setCheckItems] = useState<string[]>([]);
    const [popShow, setPopShow] = useState('');
    const [search, setSearch] = useState(initSearch);
    const [selectEdgeId, setSelectEdgeId] = useState('');
    const {userStore, notiStore, alertStore} = useContext(RootStore);
    const [codes, setCodes] = useState<any>({});
    const [deviceStatus, setDeviceStatus] = useState<boolean>(false);

    const searchTypeOptions = [
      {
        code : 'clientCompanyName',
        codeNm : '회사명'
      },
      {
        code : 'userName',
        codeNm : '사용자명'
      },
      {
        code : 'edgeId',
        codeNm : 'Edge ID'
      },
      {
        code : 'addr',
        codeNm : '설치장소'
      }
    ];

    const options = [
      {
        code : 1,
        codeNm : '정상 작동'
      },
      {
        code : 2,
        codeNm : '비정상 작동'
      },
      {
        code : 3,
        codeNm : '작동 안함'
      },
      {
        code : 4,
        codeNm : '연결 안 됨'
      }
    ];

    const setOptions = [
      {
        code : 'N',
        codeNm : '설치중'
      },
      {
        code : 'Y',
        codeNm : '설치완료'
      }
    ];

    const onSearchBtnCallback = (e) => {
      callApiDeviceSetList(1);
    };

    const initSearchCallback = (e) => {
      setSearch(initSearch);
    }

    const handleOnChange = ({name, value}) => {
      setSearch({
        ...search,
        [name] : value
      });
    };


    const viewEdgeInfo = (e, edgeId) => {
      e.preventDefault();

      setPopShow('aside_view');
      setSelectEdgeId(edgeId);

    }

    const modifyEdgeInfo = (e, item, viewYn) => {
      e.preventDefault();

      setPopShow('aside_modify');
      if(viewYn == 'Y'){
        item.viewYn = 'Y'
      }else{
        item.viewYn = 'N'
      }

      setSelectItem(item);
      setSelectEdgeId(item.edgeId);

    }

    const testChangeValue = ({ target: { name, value } }) => {

      setSelectItem({
        ...selectItem,
        [name] : value
      });

    };

    const closePopup = () => setPopShow('');

    const deviceAiStatusCheck = useCallback(async (item) => {
      apiShowLoading();
      try{
        const res = await callApi(API.DEVICE_SET_CHECK, item);
        if(res.data){
          setModifyCallback(item);
        }else{
          apiHideLoading();
          notiStore.error('Edge 상태가 연결 안됨 상태입니다. 확인 후 다시 진행해주시기 바랍니다.');
          return false;
        }
      }catch{
        notiStore.error('실패했습니다.');
      }
    },[deviceStatus]);

    const setModifyCallback = async (item) => {
      /*if(item.edgeAiStatus == 'Notconnect'){
        notiStore.error('Edge 상태가 연결 안됨 상태입니다. 확인 후 다시 진행해주시기 바랍니다.');
        return false;
      }*/
      //apiShowLoading();
      try {
        const res = await callApi(API.DEVICE_SET_EDIT, item);
        //const data : any = res.data;
        console.log(res);
        if(res.data.result == 'success'){
          notiStore.info("수정에 성공했습니다.");
          closePopup();
          callApiDeviceSetList(1);
        }else if(res.data.result == 'fail'){
          notiStore.error(res.data.msg);
          closePopup();
          callApiDeviceSetList(1);
        }else{
          notiStore.error('수정에 실패했습니다.');
        }
      } catch {
        apiHideLoading();
        notiStore.error('수정에 실패했습니다.');
        console.log("error");
      } finally {
        apiHideLoading();
      }
      
    };

    const callApiDeviceSetList = useCallback(async (pageNum) => {
        try {
            const searchs = _.cloneDeep(search);

            searchs.page = pageNum;
            searchs.size = 10;
            searchs.startDate = (searchs.startDate) ? searchs.startDate.format('yyyy-MM-dd HH:mm:ss') : '';
            searchs.endDate = (searchs.endDate) ? searchs.endDate.format('yyyy-MM-dd HH:mm:ss') : '';

            if(!dateDiff(searchs.startDate, searchs.endDate)){
              notiStore.error('종료일시가 시작일시보다 전입니다. 확인해주세요.');
              return false;
            }

            const res = await callApi(API.DEVICE_SET_LIST, searchs);

            if (res.success) {
              setDeviceSetList(res.data.content);
              setTotalPages(res.data.totalPages);
              setCurrentPage(pageNum);
            }

            hideLoading();

        } catch {
            setDeviceSetList([]);
            hideLoading();
            console.log('error');
        }
    },[totalPages, search]);

    useEffect(() => {
        callApiDeviceSetList(1);
        getCodeList(['EDGE_AI_STATUS']).then((result : any) => {
          setCodes(result);
        });
    }, [])

  return(
      <>
        <div className='tbl_filterBox'>
          <ComSearchBar callback={onSearchBtnCallback} initBtnCallback={initSearchCallback}>
            <ComSearchSelectInput title='검색어' callback={onSearchBtnCallback} onChange={handleOnChange} options={searchTypeOptions} selectName='searchType' selectValue={search.searchType} inputName='searchTxt' inputValue={search.searchTxt} />
            <ComSearchSelect title='Edge 상태' onChange={handleOnChange} options={codes.EDGE_AI_STATUS} name='edgeAiStatus' value={search.edgeAiStatus} />
            <ComSearchSelect title='설치 상태' onChange={handleOnChange} options={setOptions} name='setState' value={search.setState} />
            <ComSearchDateTime title='설치완료일시' onChange={handleOnChange} name='edgeStatus' startDateName='startDate' startDateValue={search.startDate} endDateName='endDate' endDateValue={search.endDate} />
          </ComSearchBar>
          <div className='tbl_top'>
              <h4>설치 이력 목록</h4>
          </div>
          <SetHistoryTable
                mntList={deviceSetList}
                viewEdgeInfo={viewEdgeInfo}
                closePopup={closePopup}
                modifyEdgeInfo={modifyEdgeInfo}
                selectEdgeId={selectEdgeId}
                 />
          <div className='tbl_btm'>
            <Paging totalPages={totalPages} paginate={callApiDeviceSetList} currentPage={currentPage}/>
          </div>
        </div>
          <ComSideBox show={popShow}>
            <SettingHistoryModify show={popShow} close={closePopup} item={selectItem} setModifyCallback={deviceAiStatusCheck} testChangeValue={testChangeValue} isAdmin={false} />
          </ComSideBox>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(SetHistoryTableSection);
