import React, {useState, useCallback, useContext} from 'react';
import {ApiListResponse} from 'components/edge/SettingHistoryList';
import {API, callApi} from 'utils/ApiClient';
import RootStore from 'store/RootStore';
import ReactTooltip from 'react-tooltip';
import {ERouteUrl} from 'router/RouteLinks';
import {Link} from 'react-router-dom';
import {phoneFormat, changeDate, changeDateTime, deepCopyObject} from 'utils/CommonUtils';

interface Props {
    item : ApiListResponse;
    closePopup : (e) => void;
    viewEdgeInfo : (e, edgeId : string) => void;
    modifyEdgeInfo : (e, item, viewYn : any) => void;
    selectEdgeId : string;
    index : number;
}

const SetHistoryTableRow: React.FC<Props> = ({item, closePopup, viewEdgeInfo, modifyEdgeInfo, selectEdgeId, index}) => {
  const [popShow, setPopShow] = useState('qweeq');
  const [isRestart, setIsRestart] = useState(false);
  const {notiStore} = useContext(RootStore);
  let codes = '';
  //const route = ERouteUrl.PROJECT_DETAIL.replace(':id', item.no.toString());

  const edgeStatusView = (status) => {
    if (status.toLowerCase() == 'Working'.toLowerCase()) {
      return <span className='normal' title='모든 모듈 작동 중'>정상 작동</span>;
    } else if (status.toLowerCase() == 'Abnormal'.toLowerCase()) {
      return <span className='abnormal' title='일부 모듈만 작동 중'>비정상 작동</span>;
    } else if (status.toLowerCase() == 'Notworking'.toLowerCase()) {
      return <span className='error' title='모든 모듈 작동 정지'>작동 안 함</span>;
    } else {
      return <span className='not' title='edge 장치와 통신이 안됨'>연결 안 됨</span>;
    }
  };

  const edgeSetStatusView = (setStatus) => {
    if (setStatus == 'Y') {
      return '설치완료';
    } else {
      return '설치중';
    }
  };

  const edgeSetState = (status) => {
    if(status != 'Working'){
      return <a href='#none' className='btn_line btn_edge_restart' onClick={(e) => {restartBtnOnClick(e, item)}}>재시작</a>;
    }
  };

  const edgeSetStatusModify = (setStatus =>{
    if (setStatus == 'Y') {
      return <a href='#none' className='btn_line btn_edge_modify' onClick={(e) => {modifyEdgeInfo(e, item, 'Y')}}>수정하기</a>;
    } else {
      return <a href='#none' className='btn_line btn_edge_install' onClick={(e) => {modifyEdgeInfo(e, item, 'N')}}>처리하기</a>;
    }
  });

  const restartBtnOnClick = useCallback( async (e, item) => {
    e.preventDefault();
    setIsRestart(true);

    try {

      const _api = deepCopyObject(API.DEVICE_RESTART);
      _api.url = _api.url.replace('_ID_', item.deviceId);
      const res = await callApi(_api);

      if (res.success) {
        if (res.data == 'notconnect') {
          notiStore.error('등록된 Edge 아이디가 아닙니다. 설치 완료 후 재시도 하시길 바랍니다.');
        }
        setIsRestart(false);
      } else {
        notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
        setIsRestart(false);
      }

    } catch {
      notiStore.error('통신 중 오류가 발생하였습니다. 잠시 후 다시 이용 바랍니다.');
      setIsRestart(false);
    }

  },[]);

  const testFunc = (e) => {
    e.preventDefault();
    setIsRestart(true);

    setTimeout(() => {
      setIsRestart(false);
    }, 3000);
  };


  return(
      <>
        <tr className={selectEdgeId == item.edgeId ? 'active':''}>
            <td>{index+1}</td>
            <td className='tbl_company'>{item.clientCompanyName}</td>
            <td className='tbl_userName'>{item.userName}</td>
            <td className='tbl_tel'>{phoneFormat(item.userTel)}</td>
            <td className='tbl_edgeID'>{item.edgeId}</td>
            <td className='tbl_edgePlace'>
              <p>
                  <span>{item.addr}</span>
              </p>
              <p>
                  <em>{item.addrDetail}</em>
                  <strong>{item.cameraDir}&nbsp;방향</strong>
                  {/*<i>{item.cameraNo}번&nbsp;카메라</i>*/}
              </p>
            </td>
            <td>
                <div className='status_edge'>
                    {edgeStatusView(item.edgeAiStatus)}
                </div>
            </td>
            <td>
                {/*{edgeSetState(item.edgeAiStatus)}
                {isRestart ? <span className='msg_restart'>다시 시작하는 중</span> : ''}*/}
                <a href='#none' className='btn_line btn_edge_restart' onClick={(e) => {restartBtnOnClick(e, item)}}>재시작</a>
                {isRestart ? <span className='msg_restart'>다시 시작하는 중</span> : ''}
            </td>
            <td>{item.setUserName}</td>
            <td>{changeDateTime(item.setDt)}</td>
            <td>
                {edgeSetStatusView(item.setYn)}
            </td>
            <td>
                {edgeSetStatusModify(item.setYn)}
            </td>
        </tr>
      </>
  )
}

export default React.memo(SetHistoryTableRow);
