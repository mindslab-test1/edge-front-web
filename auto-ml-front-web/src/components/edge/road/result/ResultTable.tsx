import React, {memo} from 'react';
import {ApiListResponse} from 'components/edge/RoadResultList';
import ResultTableRow from './ResultTableRow';

interface Props {
    resultList : ApiListResponse[];
    tableOnClick : (e, item : any) => void;
    selectTable : number;
    selectFilterName : string;
    onCloseCallback : (item : any) => void;
    handleFilterClick : (e, name : string) => void;
    currentPage : number;
    listSize : number;
}

const ResultTable: React.FC<Props> = ({currentPage, listSize, resultList, tableOnClick, selectTable, selectFilterName, onCloseCallback, handleFilterClick}) => {

  return(
      <div className='tbl_mid'>
      <table className='tbl_lst'>
          <caption className='hide'>인식결과 목록</caption>
          <colgroup>
              <col /><col /><col /><col /><col />
              <col /><col /><col /><col /><col />
              <col /><col />
          </colgroup>
          <thead>
            <tr>
              <th scope='col'>No.</th>
              <th scope='col'>회사명</th>
              {/*<th scope='col'>사용자명</th>*/}
              <th scope='col'>설치장소</th>
              <th scope='col'>Edge ID</th>
              <th scope='col'>차량 통과 시간</th>
              <th scope='col'>인식 방향</th>
              <th scope='col'>인식 차로</th>
              <th scope='col'>차량 번호</th>
              <th scope='col'>차량 종류</th>
              <th scope='col'>인식 신뢰도</th>
              <th scope='col'>처리 완료 시간</th>
              <th scope='col'>처리자</th>
            </tr>
          </thead>
          <tbody>
            {
              resultList.length > 0 ? (
              resultList.map((item, index)=>((
                <ResultTableRow index={index + 1} listSize={listSize} currentPage={currentPage} key={index} item={item} tableOnClick={tableOnClick} selectTable={selectTable} />
            )))) : (
              <tr>
                <td scope='row' colSpan={12} className='dataNone'>데이터가 없습니다.</td>
              </tr>
            )
          }
          </tbody>
        </table>
      </div>
  )
}

export default React.memo(ResultTable);
