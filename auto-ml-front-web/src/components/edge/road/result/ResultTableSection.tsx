import    React, {useCallback, useEffect, useState, useContext} from 'react';
import {ApiListResponse} from 'components/edge/RoadResultList';
import Paging from 'components/commons/Paging';
import {API, callApi} from 'utils/ApiClient';
import ComSideBox from 'components/commons/popup/ComSideBox';
import ResultTable from './ResultTable';
import EdgeRoadResultInfo from 'components/popup/EdgeRoadResultInfo';
import {showLoading, hideLoading, getCodeList, dateDiff} from 'utils/CommonUtils';
import ComSearchBar from 'components/commons/ComSearchBar';
import ComSearchSelect from 'components/commons/ComSearchSelect';
//import ComSearchSelectDate from 'components/commons/ComSearchSelectDate';
import ComSearchSelectDateTime from 'components/commons/ComSearchSelectDateTime';
import ComSearchSelectInput from 'components/commons/ComSearchSelectInput';
import ComSearchFromTo from 'components/commons/ComSearchFromTo';
import RootStore from 'store/RootStore';
import _ from 'lodash';

interface Props {

}

const ResultTableSection: React.FC<Props> = () => {

  const initSearch = {
    direction : '',
    lane : 0,
    carType : '',
    searchDateType : '',
    startDate : '',
    endDate : '',
    confLevelFrom : '',
    confLevelTo : '',
    searchType : 'all',
    searchTxt : '',
    page : 1,
    size : 10
  };

  const directionOptions = [
    {
      code : 'front',
      codeNm : '전면'
    },
    {
      code : 'back',
      codeNm : '후면'
    }
  ];

  const laneOptions = [
    {
      code : 1,
      codeNm : '1차로'
    },
    {
      code : 2,
      codeNm : '2차로'
    },
    {
      code : 3,
      codeNm : '3차로'
    },
    {
      code : 4,
      codeNm : '4차로'
    },
    {
      code : 5,
      codeNm : '5차로'
    },
    {
      code : 6,
      codeNm : '6차로'
    },
    {
      code : 7,
      codeNm : '7차로'
    },
    {
      code : 8,
      codeNm : '8차로'
    },
    {
      code : 9,
      codeNm : '9차로'
    },
    {
      code : 10,
      codeNm : '10차로'
    }
  ];

  const carTypeOptions = [
    {
      code : 'unknown',
      codeNm : '알수없음'
    },
    {
      code : 'normal',
      codeNm : '일반'
    },
    {
      code : 'taxi',
      codeNm : '택시'
    },
    {
      code : 'bus',
      codeNm : '버스'
    },
    {
      code : 'old',
      codeNm : '노후'
    },
    {
      code : 'new',
      codeNm : '신규'
    }
  ];

  const searchTypeOptions = [
    {
      code : 'companyName',
      codeNm : '회사명'
    },
    {
      code : 'name',
      codeNm : '사용자명'
    },
    {
      code : 'edgeId',
      codeNm : 'Edge ID'
    },
    {
      code : 'addr',
      codeNm : '설치 장소'
    },
    {
      code : 'carNumber',
      codeNm : '차량번호'
    }
  ];

  const options = [
    {
      code : 1,
      codeNm : '정상 작동'
    },
    {
      code : 2,
      codeNm : '비정상 작동'
    },
    {
      code : 3,
      codeNm : '작동 안 함'
    },
    {
      code : 4,
      codeNm : '연결 안 됨'
    }
  ];

  const dateSearchOptions = [
    {
      code : 'passTime',
      codeNm : '차량 통과 시간'
    },
    {
      code : 'editTime',
      codeNm : '처리 완료 시간'
    }
  ];

  const [resultList, setResultList] = useState<ApiListResponse[]>([]);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectItem, setSelectItem] = useState({});
  const [selectTable, setSelectTable] = useState(0);
  const [selectFilterName, setSelectFilterName] = useState('');
  const [search, setSearch] = useState(initSearch);
  const [popShow, setPopShow] = useState('');
  const {userStore, notiStore, alertStore} = useContext(RootStore);
  const [codes, setCodes] = useState<any>({});

  const onSearchBtnCallback = (e) => {
    callApiRoadRecogList(1);
  };

  const initSearchCallback = (e) => {
    setSearch(initSearch);
  }

  const handleOnChange = ({name, value}) => {
    setSearch({
      ...search,
      [name] : value
    });
  };

  const handleFilterClick = (e, name) => {
    e.preventDefault();
    setSelectFilterName(name);
  };

  const onCloseCallback = (item) => {
    setSelectFilterName('');
  };

  const handleClickOutside = (event: Event) => {
      const activeFilter = document.getElementsByClassName('tbl_filter active')[0];

      if (typeof(activeFilter) != 'undefined') {
        if(!has(activeFilter, event.target)) {
          setSelectFilterName('');
        }
      }
  };

  const has = (element, target) => {
    let isHas = false;

    if (element == target) {
      return true;
    } else {

      if (element.children.length > 0) {
        Array.from(element.children).forEach((item : any) => {
             if(has(item, target)) {
               isHas = true;
               return true;
             }
        });
      }

    }
    return isHas;
  };

  const resultModifyCallback = async (item) => {
    
    try {
      const res = await callApi(API.ROAD_RECOG_EDIT, item);
      //const data : any = res.data;

      notiStore.info("수정에 성공했습니다.");
      closePopup();
      callApiRoadRecogList(1);
        
    } catch {
      notiStore.error('수정에 실패했습니다.');
      console.log("error");
    }
  };

  const callApiRoadRecogList = useCallback(async (pageNum) => {
    try {

        //search.auth = "1";
        
        const searchs = _.cloneDeep(search);
        searchs.page = pageNum;
        searchs.size = 10;

        searchs.startDate = (searchs.startDate) ? searchs.startDate.format('yyyy-MM-dd HH:mm:ss') : '';
        searchs.endDate = (searchs.endDate) ? searchs.endDate.format('yyyy-MM-dd HH:mm:ss') : '';

        if(!dateDiff(searchs.startDate, searchs.endDate)){
          notiStore.error('종료일시가 시작일시보다 전입니다. 확인해주세요.');
          return false;
        }

        const res = await callApi(API.ROAD_RECOG_LIST, searchs);

        if(res.status != 200){
          setResultList([]);
          setTotalPages(0);
        }else{
          setResultList(res.data.content);
          setTotalPages(res.data.totalPages);
        }
        setCurrentPage(pageNum);
        hideLoading();

    } catch(error) {
        notiStore.error('차량 인식 결과 조회에 실패했습니다.');
        setResultList([]);
        //setUserList(dummyData);
        hideLoading();
        console.log('error');
    }
},[totalPages, search]);


  const closePopup = () => {
    setPopShow('');
    setSelectTable(0);
  }

  const tableOnClick = (e, item) => {
    e.preventDefault();
    
    setPopShow('aside_modify');
    //setSelectTable(item.no);
    setSelectTable(item.recogSeq);
    setSelectItem(item);

  };

  const onChangeValue = ({ target: { name, value } }) => {
    setSelectItem({
      ...selectItem,
      [name] : value
    });

  };
  

  useEffect(() => {
      callApiRoadRecogList(1).then(r => {
        //console.log('call roadRecogList');
      });
      getCodeList(['CAR_TYPE','DIRECTION']).then((result : any) => {
        setCodes(result);
      });
      document.addEventListener('click', handleClickOutside, true);
  }, [])

  return(
      <>
        <div className='tbl_filterBox'>
            <ComSearchBar callback={onSearchBtnCallback} initBtnCallback={initSearchCallback}>
              <ComSearchSelectInput title='검색어' callback={onSearchBtnCallback} onChange={handleOnChange} options={searchTypeOptions} selectName='searchType' selectValue={search.searchType} inputName='searchTxt' inputValue={search.searchTxt} />
              <ComSearchSelect title='인식 방향' onChange={handleOnChange} options={directionOptions} name='direction' value={search.direction} />
              <ComSearchSelect title='인식 차로' onChange={handleOnChange} options={laneOptions} name='lane' value={search.lane} />
              <ComSearchSelect title='차량 종류' onChange={handleOnChange} options={codes.CAR_TYPE} name='carType' value={search.carType} />
              <ComSearchSelectDateTime title='시간'
                             onChange={handleOnChange}
                             name='edgeStatus'
                             startDateName='startDate'
                             startDateValue={search.startDate}
                             endDateName='endDate'
                             endDateValue={search.endDate}
                             options={dateSearchOptions}
                             selectName='searchDateType'
                             selectValue={search.searchDateType}
                             />
              <ComSearchFromTo title='인식 신뢰도' onChange={handleOnChange} fromName='confLevelFrom' fromValue={search.confLevelFrom} toName='confLevelTo' toValue={search.confLevelTo} />
            </ComSearchBar>
            <div className='tbl_top'>
              <h4>차량 인식 결과 목록</h4>
            </div>
              <ResultTable resultList={resultList}
                           tableOnClick={tableOnClick}
                           listSize={search.size}
                           currentPage={currentPage}
                           selectTable={selectTable}
                           selectFilterName={selectFilterName}
                           handleFilterClick={handleFilterClick}
                           onCloseCallback={onCloseCallback}
                           />
              <div className='tbl_btm'>
                <Paging totalPages={totalPages} paginate={callApiRoadRecogList} currentPage={currentPage}/>
              </div>
          </div>
          <ComSideBox show={popShow}>
            <EdgeRoadResultInfo show={popShow} close={closePopup} item={selectItem} resultModifyCallback={resultModifyCallback} directionOptions={directionOptions} />
          </ComSideBox>
          {/*<ComPopBox show={popShow}>*/}
          {/*    <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true}/>*/}
          {/*</ComPopBox>*/}
      </>
  )
}

export default React.memo(ResultTableSection);
