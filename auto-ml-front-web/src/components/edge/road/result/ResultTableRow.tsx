import React, {useState} from 'react';
import {ApiListResponse} from 'components/edge/RoadResultList';
import {Link} from 'react-router-dom';
import {phoneFormat, changeDate, changeDateTime} from 'utils/CommonUtils';

interface Props {
    item : ApiListResponse;
    tableOnClick : (e, item : any) => void;
    selectTable : number;
    index : number;
    currentPage : number;
    listSize : number;
}


const modifyName = (modNm) => {
    if(modNm == "" || modNm == null){
      return "EdgeRoad";
    }else{
      return modNm;
    }
}

const carTypeListNm = (carStr) => {
  if(carStr == "bus"){
    return "버스";
  }else if(carStr == "unknown"){
    return "알수없음";
  }else if(carStr == "normal"){
    return "일반";
  }else if(carStr == "taxi"){
    return "택시";
  }else if(carStr == "old"){
    return "구번호판";
  }else if(carStr == "new"){
    return "신번호판";
  }else{
    return carStr;
  }
}

const dirStr = (dirStr) => {
  if(dirStr == 'front'){
    return "전면";
  }else if(dirStr == 'back'){
    return "후면";
  }else if(dirStr == 'both'){
    return "양방향";
  }else{
    return dirStr;
  }
}

const ResultTableRow: React.FC<Props> = ({currentPage, listSize, index, item, tableOnClick, selectTable}) => {
  
  return(
      <>
        <tr className={selectTable == item.recogSeq ? 'active':''} onClick={(e) => {tableOnClick(e, item)}}>
            {/*<td scope='row'>{item.no}</td>*/}
            {/*<td scope='row'>{index}</td>*/}
            <td scope='row'>{((currentPage-1)*listSize)+(index)}</td>
            <td className='tbl_company'>{item.clientCompanyName}</td>
            {/*<td className='tbl_user'>{item.userNm}</td>*/}
            <td className='tbl_edgePlace'>
            <p>
                  <span>{item.addr}</span>
              </p>
              <p>
                  <em>{item.addrDetail}</em>
                  <strong>{item.cameraDir}&nbsp;방향</strong>
                  {/*<i>{item.cameraNo}번&nbsp;카메라</i>*/}
              </p>
            </td>
            <td className='tbl_edgeID'>{item.edgeId}</td>
            <td>{changeDateTime(item.passTime)}</td>
            <td>{dirStr(item.direction)}</td>
            <td>{item.lane}차로</td>
            <td className='tbl_carNumber'>{item.carNumber}</td>
            {/*<td>{item.carType}</td>*/}
            <td>{carTypeListNm(item.carType)}</td>
            <td>{item.confLevel}%</td>
            <td>{changeDateTime(item.editTime)}</td>
            <td>{modifyName(item.modName)}</td>
        </tr>
      </>
  )
}

export default React.memo(ResultTableRow);
