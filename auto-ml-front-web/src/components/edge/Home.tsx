import React from 'react';
import HomeSection from './home/HomeSection';

interface Props{}

export interface ApiListResponse{
  code : string;
  title : string;
  description : string;
  icoName : string;
  imgName : string;
}

const Home: React.FC<Props> = () => {
    return (
      <>
        <div className='contents_home'>
        <HomeSection />
        </div>
      </>
    );
};

export default React.memo(Home);
