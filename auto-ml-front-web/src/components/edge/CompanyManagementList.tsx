import React from 'react';
import {ComPage} from '../commons';
import CompanyManagementTableSection from './road/companyManagement/CompanyManagementTableSection';

interface Props{}

export interface ApiListResponse{
  no : number;
  clientCompanyId : string;
  clientCompanyName : string;
  useYn : string;
  useYnNm : string;
  regDt : string;
}

export interface CompanyDetail {
  clientCompanyId : string;
  clientCompanyName : string;
  useYn : string;
  dupCheck : boolean;
}

const CompanyManagementList: React.FC<Props> = () => {

    return (
        <ComPage title={'회사 관리'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <CompanyManagementTableSection />
        </ComPage>
    );
};

export default React.memo(CompanyManagementList);
