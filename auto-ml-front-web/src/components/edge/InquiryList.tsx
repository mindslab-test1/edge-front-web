import React from 'react';
import {ComPage} from '../commons';
import InquiryTableSection from './road/inquiry/InquiryTableSection';

interface Props{}

export interface ApiListResponse{
  no : number;
  deviceId : number;
  edgeId : string;
  clientCompanyName : string;
  clientCompanyId : string;
  name : string;
  addr : string;
  addrDetail : string;
  cameraDir : string;
  cameraNo : number;
  edgeAiStatus : string;
  cpu : number;
  mem : number;
  gpu : number;
  gpuMem : number;
  setDt : string;
}

const InquiryList: React.FC<Props> = () => {

    return (
        <ComPage title={'Edge Road 조회'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <InquiryTableSection />
        </ComPage>
    );
};

export default React.memo(InquiryList);
