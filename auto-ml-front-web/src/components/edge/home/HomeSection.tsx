import React, {useCallback, useEffect, useState, useContext} from 'react';
import RootStore from 'store/RootStore';
import {API, callApi} from 'utils/ApiClient';
import {ApiListResponse} from 'components/edge/Home';
import HomeNonPurchaseTab from './HomeNonPurchaseTab';
import ComPopBox from 'components/commons/popup/ComPopBox';
import InquiryEstimate from 'components/popup/InquiryEstimate';
import HomePurchaseTab from './HomePurchaseTab';
import { emailExp, numberExp, hideLoading, deepCopyObject } from 'utils/CommonUtils';

import 'assets/images/ico_edgeCloud01.svg';
import 'assets/images/ico_edgeCloud02.svg';
import 'assets/images/ico_edgeCloud03.svg';
import 'assets/images/ico_edgeCloud04.svg';
import 'assets/images/ico_edgeCloud05.svg';
import 'assets/images/ico_edgeCloud06.svg';

import 'assets/images/img_product_edgeRoad.png';
import 'assets/images/img_product_edgeStreet.png';
import 'assets/images/img_product_edgeFactory.png';
import 'assets/images/img_product_edgeFarm.png';
import 'assets/images/img_product_edgeHome.png';
import 'assets/images/img_product_edgeDrone.png';

interface Props {

}

const HomeSection: React.FC<Props> = () => {
  const {userStore, notiStore} = useContext(RootStore);
  const [popShow, setPopShow] = useState(false);
  const [select, setSelect] = useState(0);
  const [tabNode, setTabNode] = useState<React.ReactNode>(null);
  const [edgeList, setEdgeList] = useState<ApiListResponse[]>([]);

  const edgeInfoList = [
    {
      title : 'Edge Road',
      description : `엣지 컴퓨팅(Edge Computing)을 이용한 차량 통행 도로 AI surveillance입니다. <br /> 차량 종류 및 번호를 인식하고 차선 위반, 속도위반 등의 정보를 검출할 수 있습니다.`,
      icoName : 'ico_edgeCloud01.svg',
      imgName : 'img_product_edgeRoad.png'
    },
    {
      title : 'Edge Street',
      description : `엣지 컴퓨팅(Edge Computing)을 이용한 거리 통행 행인 AI surveillance입니다. <br>이상행동(폭행, 납치, 실신 등) 탐지 및 통보가 가능하며 특정인 수색 및 추적할 수 있습니다.`,
      icoName : 'ico_edgeCloud02.svg',
      imgName : 'img_product_edgeStreet.png'
    },
    {
      title : 'Edge Factory',
      description : `엣지 컴퓨팅(Edge Computing)을 이용한 고가의 GPU를 대체하는 실용적인 솔루션 제공합니다. <br>AI 기반의 실시간 분석 및 제어를 할 수 있습니다.`,
      icoName : 'ico_edgeCloud03.svg',
      imgName : 'img_product_edgeFactory.png'
    },
    {
      title : 'Edge Farm',
      description : `엣지 컴퓨팅(Edge Computing)을 이용한 스마트 농장 AI surveillance입니다. <br>스마트 농장 실시간 제어, 농장 침입자 및 도둑 탐지가 가능하며 작황 및 생육 상황 분석을 제공합니다.`,
      icoName : 'ico_edgeCloud04.svg',
      imgName : 'img_product_edgeFarm.png'
    },
    {
      title : 'Edge Home',
      description : `개인정보 비식별화가 필요한 사적/공적 공간 AI surveillance입니다.<br>이상행동 검출 및 통보, 행동 패턴 분석을 제공합니다.`,
      icoName : 'ico_edgeCloud05.svg',
      imgName : 'img_product_edgeHome.png'
    },
    {
      title : 'Edge Drone',
      description : `강력한 AI 계산 기능을 갖춘 소형의 저전력 드론 솔루션 제공 합니다.<br>물체 및 인물 인식 및 추적 기능, 드론 자율 비행을 위한 메타 데이터 추출을 할 수 있습니다.`,
      icoName : 'ico_edgeCloud06.svg',
      imgName : 'img_product_edgeDrone.png'
    }
  ];

  const openPopup = (e) => {
    e.preventDefault();
    setPopShow(true);
  }

  const closePopup =(e) => {
    e.preventDefault();
    setPopShow(false);
  }

  const tabSelect = (e, index) => {
    e.preventDefault();
    setSelect(index);
    callApiUserEdgeList(index);
  };

  const estimateCallback = async (item) => {
    try {

      if (!validationCheck(item)) return false;

      const res = await callApi(API.REQUEST_ESTIMATE, item);
      if(res.success){
        notiStore.infoBlue('문의가 접수되었습니다. 담당자 확인 후 연락드리겠습니다.');  
      }else{
        notiStore.error('[실패] 잠시 후 다시 시도해 주세요.');
        return false;
      }
    } catch {
      console.log('error');
      notiStore.error('[실패] 잠시 후 다시 시도해 주세요.');
      return false;
    }

    setPopShow(false);

  };

  const validationCheck = (item) => {
    let isBlank : boolean = false;
    let keyArr = Object.keys(item);
    let key = '';

    for (let idx in keyArr) {
      key = keyArr[idx];

      if (item[key] == '') {
        isBlank = true;
        break;
      }
    }

    if (isBlank) {
      const blankElement : any = document.getElementById('inquiry_' + key);
      notiStore.error(`${blankElement.title}을 입력해주세요.`);
      blankElement.focus();
      return false;
    }

    if (!emailExp(item.email)) {
      const blankElement : any = document.getElementById('inquiry_email');
      notiStore.error(`이메일 형식이 올바르지 않습니다.`);
      blankElement.focus();
      return false;
    }

    if (!numberExp(item.tel)) {
      const blankElement : any = document.getElementById('inquiry_tel');
      notiStore.error(`전화번호는 숫자만 입력 가능합니다.`);
      blankElement.focus();
      return false;
    }

    return true;
  }


  const callApiUserEdgeList = async (index) => {

    try {

      const _api = deepCopyObject(API.EDGE_STATUS);
      _api.url = _api.url.replace('_ID_', edgeList[index].code);

      const res = await callApi(_api);

      let isPurchase : boolean = false;

      if (res.success) {

        if (res.data.data.deviceCnt) {
          isPurchase = true;
        }

      } else {
        notiStore.error(`오류가 발생하였습니다.\n 오류가 계속 발생 할 시 관리자에게 문의바랍니다.`);
      }

      if (isPurchase) {
        setTabNode(<HomePurchaseTab edgeInfo={res.data.data} />);
      } else {
        setTabNode(<HomeNonPurchaseTab edgeInfo={edgeList[index]} openPopup={openPopup}/>);
      }

    } catch {
      console.log("error");
      setTabNode(<HomeNonPurchaseTab edgeInfo={edgeList[index]} openPopup={openPopup}/>);
    } finally {
      hideLoading();
    }
  };


  const initHomeSection = useCallback( async () => {
    let isError : boolean = false;
    try {

      const res = await callApi(API.EDGE_LIST);

      if (res.success) {
        setEdgeList(res.data.data);
      } else {
        isError = true;
      }


    } catch {
        isError = true;
    } finally {
        if (isError) notiStore.error(`오류가 발생하였습니다.\n 오류가 계속 발생 할 시 관리자에게 문의바랍니다.`);
    }
  },[]);

  useEffect(() => {
    initHomeSection();
  },[]);

  useEffect(() => {

    if (edgeList.length > 0) {
        callApiUserEdgeList(0);
    }

  },[edgeList]);


  return(
      <>
        <p className='msg_greeting'>
          <span>{userStore.getUserName()}님,</span>Edge Analysis에 오신 것을 환영합니다.
        </p>
        <div className='serviceBox'>
          <ul className='tab_nav al_c'>
            {
              edgeList.map((item, index)=>((
                <li key={index}><a className={ index == select ? 'active' : ''} href="#none" onClick={(e) => {tabSelect(e, index)}}>{item.title}</a></li>
              )))
            }
          </ul>
          <div className='tab_container'>
            {tabNode}
          </div>
        </div>
        <ComPopBox show={popShow}>
          <InquiryEstimate close={closePopup} edge={edgeList[select]} estimateCallback={estimateCallback} />
        </ComPopBox>
      </>
  )
}

export default React.memo(HomeSection);
