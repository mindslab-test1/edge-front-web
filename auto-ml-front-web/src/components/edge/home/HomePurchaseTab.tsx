import React, {useCallback, useEffect, useState, useContext} from 'react';

interface Props {
  edgeInfo : any;
}

const HomePurchaseTab: React.FC<Props> = ({edgeInfo}) => {
  return(
    <ul className='dashboard'>
        <li>
            <span className='ft_point_black'>{edgeInfo.deviceCnt}</span>
            <em>My Device</em>
        </li>
        <li>
            <span className='ft_point_blue'>{edgeInfo.normalCnt}</span>
            <em>작동 중</em>
        </li>
        <li>
            <span className='ft_point_orange'>{edgeInfo.abnormalCnt}</span>
            <em>비정상 작동</em>
        </li>
        <li>
            <span className='ft_point_red'>{edgeInfo.errorCnt}</span>
            <em>작동 안 함</em>
        </li>
        <li>
            <span className='ft_point_gray'>{edgeInfo.notCnt}</span>
            <em>연결 안 됨</em>
        </li>
    </ul>
  )
}

export default HomePurchaseTab;
