import React, {useCallback, useEffect, useState, useContext} from 'react';

interface Props {
  edgeInfo : any;
  openPopup : (e) => void;
}

const HomeNonPurchaseTab: React.FC<Props> = ({edgeInfo, openPopup}) => {
  return(
    <div className='tab_content' style={{display : 'block'}}>
      <div className='productBox'>
        <div className='txtBox'>
            <dl>
                <dt>
                    <span className='ico'><img src={process.env.REACT_APP_IMG_SRC + edgeInfo.icoName} alt={edgeInfo.title + ' icon'} /></span>
                    <strong>{edgeInfo.title}</strong>
                </dt>
                <dd className='txt' dangerouslySetInnerHTML={{__html : edgeInfo.description}}></dd>
                <dd className='btn'><a className='btn_lyr_open' href='#lyr_inquiry_estimate' onClick={openPopup}>견적요청하기</a></dd>
            </dl>
        </div>
        <div className='imgBox'><img src={process.env.REACT_APP_IMG_SRC + edgeInfo.imgName} alt={edgeInfo.title} /></div>
      </div>
    </div>
  )
}

export default HomeNonPurchaseTab;
