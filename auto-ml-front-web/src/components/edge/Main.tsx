import React from 'react';
import MainSection from './main/MainSection';

interface Props{}

export interface ApiListResponse{
  code : string;
  title : string;
  description : string;
  icoName : string;
  imgName : string;
}

const Main: React.FC<Props> = () => {
    return (
      <>
        <div className='content'>
        <MainSection />
        </div>
          <iframe style={{width:'100%', border: 'none', height: '127px'}} src={'https://maum.ai/serviceLanding/krFooter'}/>
      </>
    );
};

export default React.memo(Main);
