import React from 'react';
import {ComPage} from '../commons';
import UserManagementTableSection from './road/userManagement/UserManagementTableSection';

interface Props{}

export interface ApiListResponse{
  /*no : number;
  userNm : string;
  userId : string;
  hpNo : string;
  clientCompanyId : string;
  companyNm : string;
  auth : number;
  authNm : string;
  regDt : string;
  useYn : number;*/
  id:number;
  name : string;
  email : string;
  clientCompanyId : string;
  clientCompanyName : string;
  tel : string;
  password : string;
  role : any;
  authId : number;
  useYn : string;
  regId : number;
  regDt : string;
  modId : number;
  modDt : string;
}

export interface ApiUserListResponse{
    name:string;
    email:string;
}

export interface ApiCompanyListResponse{
    clientCompanyId : string;
    clientCompanyName : string;
}

const UserManagementList: React.FC<Props> = () => {

    return (
        <ComPage title={'사용자 관리'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <UserManagementTableSection />
        </ComPage>
    );
};

export default React.memo(UserManagementList);
