import React from 'react';
import {ComPage} from '../commons';
import RoadRealTimeSection from './road/realTime/RoadRealTimeSection';

interface Props{}

export interface ApiRoadPolicy {
  camDir : string;
  cameraDir : string;
  cameraDomain : string;
  cameraNo : number;
  connect : string;
  edgeNodeSeq : number;
  oneWay : string;
  rangeLane : number;
  roi : string;
  startLane : number;
  url : string;
  vehSide : string;
}

export interface ApiRoadResponse{
  deviceId : number;
  edgeId : string;
  clientCompanyName : string;
  clientCompanyId : string;
  addr : string;
  addrDetail : string;
  policy : ApiRoadPolicy;
}

export interface ApiCompListResponse{
    clientCompanyId : string;
    clientCompanyName : string;
}

export interface ApiAddrListResponse{
    edgeId : string;
    addr : string;
    addrDetail : string;
    cameraDir : string;
}

export interface ApiRecentlyResponse{
    cameraDir : string;
    direction : number;
    lane : number;
    carNumber : string;
    carType : string;
    confLevel : number;
}

const RoadRealTime: React.FC<Props> = () => {

    return (
        <ComPage title={'실시간 차량 인식'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <RoadRealTimeSection />
        </ComPage>
    );
};

export default React.memo(RoadRealTime);
