import React, {useState, useContext} from 'react';

import img_edgeCloud_main from 'assets/images/img_edgeCloud_main.png';
import img_inquiry from 'assets/images/img_inquiry.png';
import MainEdgeAiDevice from './MainEdgeAiDevice';
import PopContact from '../../popup/PopContact';
import ComPopBox from '../../commons/popup/ComPopBox';
import RootStore from 'store/RootStore';


interface Props {

}

const MainSection: React.FC<Props> = () => {


    const [popShow, setPopShow] = useState(false);
    const { userStore } = useContext(RootStore);

    const setPopIsOpen = () => {
        setPopShow(!popShow);
    }

    const closePopup = () => {
        console.log(popShow);
        setPopShow(false);
    }

    return(
      <>
          {/* svc_disb */}
          <div id='stn_overview' className='svc_disb'>
              <div className='disbBox'>
                  <p><span>Edge AI Platform</span>은<br/><span>Edge AI Device</span>와 <span>maum AI Cloud</span>를 연동한 신개념 <span>AI Platform</span>입니다.</p>

                  <ul className='svc_card_list'>
                      <li>
                          <dt>Edge AI Device</dt>
                          <dd>특징 추출 및 실시간 제어</dd>
                      </li>
                      <li>
                          <dt>AI Computing Cloud</dt>
                          <dd>데이터 분석</dd>
                      </li>
                  </ul>

                  <small>*엣지 컴퓨팅(Edge Computing)은 중앙 서버가 모든 데이터를 처리하는 클라우드 컴퓨팅과는 다른 방식으로 분산된 소형 서버를 통해 실시간으로 데이터를 처리하는 기술입니다.</small>
              </div>
          </div>
          {/* //svc_disb */}

          {/* bg_whiteBox */}
          <MainEdgeAiDevice />
          {/* //bg_whiteBox */}

          {/* svc_disb */}
          <div id='stn_edgeCloud' className='svc_disb'>
              <div className='disbBox'>
                  <dl>
                      <dt>AI Computing Cloud</dt>
                      <dd>
                          <ul className='cloud_list fl'>
                              <li>
                                  <dl>
                                      <dt>Edge Road</dt>
                                      <dd>
                                          <ul>
                                              <li>차량 통행 도로 AI surveillance</li>
                                              <li>차량 종류 및 번호 인식</li>
                                              <li>차선위반, 속도 위반 등 검출</li>
                                          </ul>
                                      </dd>
                                  </dl>
                                  <span className='ico'/>
                              </li>
                              <li>
                                  <dl>
                                      <dt>Edge Street</dt>
                                      <dd>
                                          <ul>
                                              <li>거리 통행 행인 AI surveillance</li>
                                              <li>
                                                  이상행동 탐지 및 통보<br/>
                                                  (ex: 폭행, 납치, 실신 등)
                                              </li>
                                              <li>특정인 수색 및 추적</li>
                                          </ul>
                                      </dd>
                                  </dl>
                                  <span className='ico'/>
                              </li>
                              <li>
                                  <dl>
                                      <dt>Edge Factory</dt>
                                      <dd>
                                          <ul>
                                              <li>
                                                  고가의 GPU를 대체하는<br/>
                                                  실용적인 솔루션 제공
                                              </li>
                                              <li>AI 기반의 실시간 분석 및 제어</li>
                                          </ul>
                                      </dd>
                                  </dl>
                                  <span className='ico'/>
                              </li>
                          </ul>

                          <span className='img'>
                                <img src={img_edgeCloud_main} alt='Edge Cloud 이미지'/>
                            </span>

                          <ul className='cloud_list fr'>
                              <li>
                                  <span className='ico'/>
                                  <dl>
                                      <dt>Edge Farm</dt>
                                      <dd>
                                          <ul>
                                              <li>스마트 농장 AI surveillance</li>
                                              <li>스마트 농장 실시간 제어</li>
                                              <li>농장 침입자 및 도둑 탐지</li>
                                              <li>작황 및 생육 상황 분석</li>
                                          </ul>
                                      </dd>
                                  </dl>
                              </li>
                              <li>
                                  <span className='ico'/>
                                  <dl>
                                      <dt>Edge Home</dt>
                                      <dd>
                                          <ul>
                                              <li>
                                                  개인정보 비식별화가 필요한<br/>
                                                  사적/공적 공간 AI surveillance
                                              </li>
                                              <li>이상행동 검출 및 통보</li>
                                              <li>행동 패턴 분석</li>
                                          </ul>
                                      </dd>
                                  </dl>
                              </li>
                              <li>
                                  <span className='ico'/>
                                  <dl>
                                      <dt>Edge Drone</dt>
                                      <dd>
                                          <ul>
                                              <li>
                                                  강력한 AI 계산 기능을 갖춘<br/>
                                                  소형의 저전력 드론 솔루션 제공
                                              </li>
                                              <li>물체 및 인물 인식 및 추적 기능</li>
                                              <li>드론 자율 비행을 위한 메타 데이터 추출</li>
                                          </ul>
                                      </dd>
                                  </dl>
                              </li>
                          </ul>
                      </dd>
                  </dl>
                  <div className='btnBox'>
                      <a href='/home'>시작하기</a>
                  </div>
              </div>
          </div>
          {/* //svc_disb */}

          {/* bg_navyBox */}
          <div id='stn_svcCase' className='bg_navyBox'>
              <dl>
                  <dt>고객사례</dt>
                  <dd>
                      <ul className='svc_case_list'>
                          <li>
                              <span className='tit'>노후경유 차량 검출 프로젝트</span>
                              <a className='img' href='https://m.post.naver.com/viewer/postView.nhn?volumeNo=29675697&memberNo=457' target='_blank'>
                                  <span>자세히보기<em className='fas fa-chevron-right'/></span>
                              </a>
                          </li>
                          <li>
                              <span className='tit'>이상행동 CCTV</span>
                              <a className='img' href='https://m.post.naver.com/viewer/postView.nhn?volumeNo=29675697&memberNo=4' target='_blank'>
                                  <span>자세히보기<em className='fas fa-chevron-right'/></span>
                              </a>
                          </li>
                          <li>
                              <span className='tit'>독거노인 모니터링 기능</span>
                              <a className='img' href='https://m.post.naver.com/viewer/postView.nhn?volumeNo=29737645&memberNo=45' target='_blank'>
                                  <span>자세히보기<em className='fas fa-chevron-right'/></span>
                              </a>
                          </li>
                      </ul>
                  </dd>
              </dl>
          </div>
          {/* //bg_navyBox */}

          {/* svc_disb */}
          <div id='stn_inquiry' className='svc_disb'>
              <div className='disbBox'>
                  <div className='img'>
                      <img src={img_inquiry} alt='문의하기 이미지'/>
                  </div>
                  <div className='txt'>
                      <span>문의하기</span>
                      <p>도움이 필요하시면 여기를 클릭해 주세요.</p>
                      <div className='btnBox'>
                          <a className='btn_lyr_open btn_contact' onClick={setPopIsOpen}>문의하기</a>
                      </div>
                  </div>
              </div>
          </div>
          {/* //svc_disb */}

          <ComPopBox show={popShow}>
              <PopContact close={closePopup} popTitle={'문의하기'} />
          </ComPopBox>

      </>
  )
}

export default React.memo(MainSection);
