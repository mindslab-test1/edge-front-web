import React, {useContext, useState} from 'react';
import MainEdgeAiDeviceList from './MainEdgeAiDeviceList';
import ComPopBox from '../../commons/popup/ComPopBox';
import InquiryEstimateSimple from '../../popup/InquiryEstimateSimple';


import img_device01 from 'assets/images/img_device01.png';
import img_device02 from 'assets/images/img_device02.png';
import img_device03 from 'assets/images/img_device03.png';
import {API, callApi} from 'utils/ApiClient';
import {emailExp, numberExp} from 'utils/CommonUtils';
import RootStore from 'store/RootStore';

interface Props {
}

const MainEdgeAiDevice: React.FC<Props> = ({}) => {

    const deviceList = [
        {
            id: 0,
            tit: 'Device 1',
            specify_list:
                <>
                    <li>GSP : <span>16 core (16 TOPS)</span></li>
                    <li>CPU : <span>ARM A53 (1GHz)</span></li>
                    <li>RAM : <span>DDR4 2/4/8 GB</span></li>
                    <li>Power : <span>7W</span></li>
                    <li>Camera : <span>MIPI CSI2</span></li>
                    <li>Display : <span>MIPI DSI</span></li>
                    <li>Ethernet, USB</li>
                </>,
            deviceId: 'device1',
            price: '99,000원',
            srcImg: img_device01
        },
        {
            id: 1,
            tit: 'Device 2',
            specify_list: <>
                <li>GSP : <span>17 core (16 TOPS)</span></li>
                <li>CPU : <span>ARM A53 (1GHz)</span></li>
                <li>RAM : <span>DDR4 2/4/8 GB</span></li>
                <li>Power : <span>7W</span></li>
                <li>Camera : <span>MIPI CSI2</span></li>
                <li>Display : <span>MIPI DSI</span></li>
                <li>Ethernet, USB</li>
                </>,
            deviceId: 'device2',
            price: '109,000원',
            srcImg: img_device02
        },
        {
            id: 2,
            tit: 'Device 3',
            specify_list: <>
                <li>GSP : <span>18 core (16 TOPS)</span></li>
                <li>CPU : <span>ARM A53 (1GHz)</span></li>
                <li>RAM : <span>DDR4 2/4/8 GB</span></li>
                <li>Power : <span>7W</span></li>
                <li>Camera : <span>MIPI CSI2</span></li>
                <li>Display : <span>MIPI DSI</span></li>
                <li>Ethernet, USB</li>
            </>,
            deviceId: 'device3',
            price: '130,000원',
            srcImg: img_device03
        }

    ]

    const {notiStore} = useContext(RootStore);
    const [popShow, setPopShow] = useState(false);
    const [deviceInfo, setDeviceInfo] = useState(deviceList[0]);

    const setSelectedDevice = (deviceId) => {
        setDeviceInfo(deviceList[deviceId]);
    }

    const openPopup = (e) => {
        e.preventDefault();
        setPopShow(true);
    }

    const closePopup =(e) => {
        e.preventDefault();
        setPopShow(false);
    }

    const estimateCallback = async (item) => {
        try {

            if (!validationCheck(item)) {
                console.log('fail');
                return false;
            }

            const res = await callApi(API.REQUEST_ESTIMATE, item);
            notiStore.infoBlue('문의가 접수되었습니다. 담당자 확인 후 연락드리겠습니다.');
        } catch {
            console.log('error');
            notiStore.error('[실패] 잠시 후 다시 시도해 주세요.');
        }

        setPopShow(false);

    };

    const validationCheck = (item) => {
        let isBlank : boolean = false;
        let keyArr = Object.keys(item);
        let key = '';

        console.log(item);

        for (let idx in keyArr) {
            key = keyArr[idx];

            if (item[key] === '') {
                isBlank = true;
                break;
            }
        }

        if (isBlank) {
            const blankElement : any = document.getElementById('inquiry_' + key);
            notiStore.error(`${blankElement.title}을 입력해주세요.`);
            blankElement.focus();
            return false;
        }

        if (!emailExp(item.email)) {
            const blankElement : any = document.getElementById('inquiry_email');
            notiStore.error(`이메일 형식이 올바르지 않습니다.`);
            blankElement.focus();
            return false;
        }

        if (!numberExp(item.tel)) {
            const blankElement : any = document.getElementById('inquiry_tel');
            notiStore.error(`전화번호는 숫자만 입력 가능합니다.`);
            blankElement.focus();
            return false;
        }

        return true;
    }



    return(
        <>
            <div id='stn_edgeDevice' className='bg_whiteBox'>
                <dl>
                    <dt>Edge AI Device</dt>
                    <dd>
                        <ul className='dvc_card_list'>
                            {deviceList.map((device) => (
                                <MainEdgeAiDeviceList
                                    key={device.id}
                                    id={device.id}
                                    tit={device.tit}
                                    deviceId = {device.deviceId}
                                    setSelectedDevice = {setSelectedDevice}
                                />
                            ))}
                        </ul>
                    </dd>
                </dl>

                <div className='btnBox'>
                    <a className='btn_lyr_open' onClick={openPopup}>견적요청하기</a>
                </div>
            </div>


            <ComPopBox show={popShow}>
                <InquiryEstimateSimple deviceInfo={deviceInfo} close={closePopup} estimateCallback={estimateCallback}/>
            </ComPopBox>
        </>
    )
}

export default MainEdgeAiDevice;