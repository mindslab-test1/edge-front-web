import React from 'react';

interface Props {
    id : number;
    tit : string;
    deviceId : string;
    setSelectedDevice: (deviceId : number) => void;
}

const MainEdgeAiDeviceList: React.FC<Props> = ({id, tit, deviceId, setSelectedDevice}) => {

    const setDevice = (e) => {
        setSelectedDevice(e.target.value);
    }

    return(
        <li>
            <span className='tit'>{tit}</span>

            <div className='dvc_detail' >
                <div className='radioBox'>
                    <input type='radio' name='device' id={deviceId} value={id} onClick={setDevice}/>
                    <label htmlFor={deviceId}/>
                </div>
            </div>
        </li>
    )
}

export default MainEdgeAiDeviceList;