import React from 'react';
import {ComPage} from '../commons';
import ManagementTableSection from './road/management/ManagementTableSection';

interface Props{}

export interface ApiListResponse{
  no : number;
  deviceId : number;
  edgeId : string;
  clientCompanyName : string;
  clientCompanyId : string;
  name : string;
  addr : string;
  addrDetail : string;
  cameraDir : string;
  cameraNo : number;
  edgeAiStatus : string;
  cpu : number;
  mem : number;
  gpu : number;
  gpuMem : number;
  setDt : string;
  explanList : Array<string>;
  edgeInfoList : Array<string>;
}

const ManagementList: React.FC<Props> = () => {

    return (
        <ComPage title={'Edge Road 관리'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <ManagementTableSection />
        </ComPage>
    );
};

export default React.memo(ManagementList);
