import React from 'react';
import { Row, Col, Divider } from 'antd';
import { Text12, Text16 } from 'components/commons/ComText';
import styled from 'styled-components';
import { EColor } from 'styles/GlobalStyles';

interface Props {
  subject: string;
  subtitle?: string;
  children?: React.ReactNode;
}

const ComSubjectLine: React.FC<Props> = ({ ...props }) => {
  return (
    <TitleRow align='middle' justify='space-between'>
      <Col>
        <Row gutter={16} align='middle'>
          <Col>
            <Text16 weight={600}>{props.subject}</Text16>
          </Col>
          {props.subtitle && (
            <Col>
              <Text12>{props.subtitle}</Text12>
            </Col>
          )}
        </Row>
      </Col>
      {props.children && <Col>{props.children}</Col>}
    </TitleRow>
  );
};

const TitleRow = styled(Row)`
  /* border-bottom: solid 2px ${EColor.MAIN_COLOR}; */
  border-bottom: solid 1px #e8e8e8;
  padding-bottom: 5px;
  margin: 20px 0px;
`;

export default ComSubjectLine;
