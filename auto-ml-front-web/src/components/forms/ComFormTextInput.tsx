import React from 'react';
import { Row, Col } from 'antd';
import ComTextInput, { IComTextInputProps } from 'components/forms/input/ComTextInput';
import { ComFormErrorMessage } from 'components/forms';
import { Text14 } from 'components/commons/ComText';

interface Props extends IComTextInputProps {
  title: string;
  error?: string;
}

const ComFormTextInput: React.FC<Props> = ({ ...props }) => {
  return (
    <Row>
      <Col span={24}>
        <Text14>{props.title}</Text14>
      </Col>
      <Col span={24}>
        <ComTextInput {...props} />
      </Col>

      {props.error && <ComFormErrorMessage message={props.error} />}
    </Row>
  );
};

export default ComFormTextInput;
