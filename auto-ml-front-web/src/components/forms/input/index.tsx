import _ComCheckbox from './ComCheckbox';
import _ComSelect from './ComSelect';
import _ComTextArea from './ComTextArea';
import _ComTextInput from './ComTextInput';

export const ComCheckbox = _ComCheckbox;
export const ComSelect = _ComSelect;
export const ComTextArea = _ComTextArea;
export const ComTextInput = _ComTextInput;
