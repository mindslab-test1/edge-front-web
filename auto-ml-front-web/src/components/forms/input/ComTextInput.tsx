import React from 'react';
import styled from 'styled-components';
import { Input } from 'antd';

export interface IComTextInputProps {
  value?: string | number | readonly string[] | undefined;
  placeholder?: string;
  disabled?: boolean | undefined;
  defaultValue?: string | number | readonly string[] | undefined;
  onBlur?: (event: React.FocusEvent<HTMLElement>) => void;
  onInput?: (event: React.FormEvent<HTMLInputElement>) => void;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const ComTextInput: React.FC<IComTextInputProps> = ({ ...props }) => {
  return <TextInput {...props} />;
};

const TextInput = styled(Input)`
  width: 100%;
  border-radius: 4px;
`;

export default ComTextInput;
