import React from 'react';
import styled from 'styled-components';
import { Select } from 'antd';
import { LabeledValue } from 'antd/lib/select';

const { Option } = Select;

interface OptionInterface {
  name: string;
  value: string | number;
  disabled?: boolean;
}

export interface IComSelectProps {
  id?: string;
  defaultValue?: string | number;
  options: OptionInterface[];
  onChange?: (value: any, option: any) => void;
  onBlur?: (event: React.FocusEvent<HTMLElement>) => void;
  style?: React.CSSProperties;
  placeholder?: string;
  value?: string | number | React.ReactText[] | LabeledValue | LabeledValue[];
}

const ComSelect: React.FC<IComSelectProps> = ({ ...props }) => {
  return (
    <_Select defaultValue={props.defaultValue} onChange={props.onChange} onBlur={props.onBlur} value={props.value} placeholder={props.placeholder}>
      {props.options.map(({ name, value, disabled }, index) => (
        <Option value={value} disabled={disabled} key={index}>
          {name}
        </Option>
      ))}
    </_Select>
  );
};

const _Select = styled(Select)`
  width: 100%;
`;

export default ComSelect;
