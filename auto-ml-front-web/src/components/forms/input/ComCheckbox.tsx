import React from 'react';
import { Checkbox } from 'antd';
import styled from 'styled-components';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';

export interface IComCheckboxProps {
  onChange?: (e: CheckboxChangeEvent) => void;
  indeterminate?: boolean;
  checked?: boolean;
  children?: any;
  value?: string | number;
}

const ComCheckbox: React.FC<IComCheckboxProps> = ({ ...props }) => {
  return <_Checkbox {...props}>{props.children}</_Checkbox>;
};

const _Checkbox = styled(Checkbox)`
  display: flex;
  align-items: center;
`;

export default ComCheckbox;
