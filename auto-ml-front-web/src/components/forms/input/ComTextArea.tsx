import React from 'react';
import styled from 'styled-components';
import { Input } from 'antd';

const { TextArea } = Input;

interface Props {
  placeholder?: string;
  onInput: (event: React.FormEvent<HTMLTextAreaElement>) => void;
  onChange?: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
  rows?: number;
  defaultValue?: string | number | readonly string[] | undefined;
}

const ComTextArea: React.FC<Props> = ({ rows = 4, ...props }) => {
  return <_TextArea {...props} />;
};

const _TextArea = styled(TextArea)`
  width: 100%;
`;

export default ComTextArea;
