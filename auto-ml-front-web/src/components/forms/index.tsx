import _ComSubjectLine from './ComSubjectLine';
import _ComFormImageButton from './ComFormImageButton';
import _ComFormTextInput from './ComFormTextInput';
import _ComFormErrorMessage from './ComFormErrorMessage';

export const ComSubjectLine = _ComSubjectLine;
export const ComFormImageButton = _ComFormImageButton;
export const ComFormTextInput = _ComFormTextInput;
export const ComFormErrorMessage = _ComFormErrorMessage;
