import React from 'react';
import { Row, Col } from 'antd';
import { PlusCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { Text14 } from 'components/commons/ComText';
import { EColor } from 'styles/GlobalStyles';

interface Props {
  index: number;
  imageSrc?: any;
  onImageLoad: (base64: string | ArrayBuffer | null) => void;
  onDelete: () => void;
}

const ComFormImageButton: React.FC<Props> = ({ index, imageSrc, onImageLoad, onDelete }) => {
  const onButtonPress = () => {
    if (document.getElementById(`imageUpload_${index}`) !== null) {
      document.getElementById(`imageUpload_${index}`)?.click();
    }
  };

  const handleChange = (e) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (!file || file === null) return;

    const types = ['image/png', 'image/jpeg'];
    if (types.every((type) => file.type !== type)) {
      return;
    }

    reader.onloadend = () => {
      onImageLoad(reader.result);
    };

    reader.readAsDataURL(file);
  };

  const renderDeleteButton = () => {
    if (imageSrc) {
      return (
        <DeleteButton onClick={onDelete}>
          <Row justify='end'>
            <Col>
              <CloseCircleOutlined style={{ fontSize: 20, color: EColor.RED }} />
            </Col>
          </Row>
        </DeleteButton>
      );
    } else {
      return null;
    }
  };

  return (
    <Container>
      <Button onClick={onButtonPress}>
        {imageSrc && <Image src={imageSrc} />}

        <ButtonInside>
          <PlusCircleOutlined style={{ fontSize: 20 }} />
          <Text14 align='center'>업로드</Text14>
        </ButtonInside>
        <input type='file' accept='.jpg,.jpeg,.png' id={`imageUpload_${index}`} onChange={handleChange} />
      </Button>
      {renderDeleteButton()}
    </Container>
  );
};

const Container = styled.div`
  width: 200px;
`;

const Button = styled.div`
  width: 200px;
  height: 150px;
  border: dashed 1px #acacac;
  cursor: pointer;
  flex: 1;
  position: relative;
  background-color: #f6f6f6;
  /* padding-top: 35%; */
`;

const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const ButtonInside = styled.div`
  text-align: center;
  position: absolute;
  top: 35%;
  left: 40%;
`;

const DeleteButton = styled.div`
  position: absolute;
  top: -9px;
  right: -0px;
  cursor: pointer;
  z-index: 1000;
`;

export default ComFormImageButton;
