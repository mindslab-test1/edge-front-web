import React from 'react';
import { Row, Col } from 'antd';
import styled from 'styled-components';
import { Text12 } from 'components/commons/ComText';
import { EColor } from 'styles/GlobalStyles';

interface Props {
  message: string;
}

const ComFormErrorMessage: React.FC<Props> = ({ message }) => {
  return (
    <Warning>
      <Col>
        <Text12 color={EColor.RED} weight={600}>
          {message}
        </Text12>
      </Col>
    </Warning>
  );
};

const Warning = styled(Row)`
  margin-top: -7px;
  padding-left: 3px;
`;

export default ComFormErrorMessage;
