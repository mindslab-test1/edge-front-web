import React, {useContext} from 'react';
import {IContainerBaseProps} from '../../constants/BaseInterface';
import RootStore from '../../store/RootStore';
import errImg from 'assets/images/error2.svg';
import '../../assets/css/error.css';
import {goBack} from 'utils/Navigation';

const PageNoMatch: React.FC<IContainerBaseProps> = ({...props}) => {
  const {layoutStore, userStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;

  toggleHeader(false);
  toggleSidebar(false);

  const back = (e) => {
    e.preventDefault();
    goBack();
  };

  // <>
  //   <img className='errorImg' src={errImg} alt='error Image'/>
  // </>
  return (
    <div className='errorWrap'>
      <div className='service_tit'>
          <h1><a href='https://maum.ai/' title='마음에이아이 바로가기'><img src='https://maum.ai/aiaas/common/images/logo_maumAi_clr.svg' alt='maum.ai' /></a></h1>
      </div>
      <div className='errorBox'>
          <div className='error_tit'>
              <strong>죄송합니다.<br />요청하신 페이지를 찾을 수 없습니다.</strong>
          </div>
          <div className='error_cnt'>
              <p>방문하시려는 페이지의 주소가 잘못 입력되었거나,<br />페이지의 주소가 변경 혹은 삭제되어 요청하신 페이지를 찾을 수 없습니다.</p>
              <p>입력하신 주소가 정확한지 다시 한번 확인해 주시기 바랍니다.</p>
              <p>관련 문의사항은 <a href='mailto:hello@mindslab.ai' >고객센터</a>에 알려주시면 친절하게 안내해 드리겠습니다.</p>
              <p>감사합니다.</p>
          </div>
          <div className='error_btn'>
              <a href='#none' onClick={(e) => {back(e)}}>이전</a>
              <a className='btn_clr' href='/'>홈으로 이동</a>
          </div>
      </div>
      <div className='copyRight'><span>MINDsLab © 2020</span></div>
    </div>
  );
};

export default PageNoMatch;
