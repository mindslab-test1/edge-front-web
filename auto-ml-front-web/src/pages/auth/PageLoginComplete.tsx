import React, {useContext} from 'react';
import { ConLoginComplete } from 'containers/auth';
import { IContainerBaseProps } from 'constants/BaseInterface';
import RootStore from '../../store/RootStore';
import {showLoading, hideLoading} from 'utils/CommonUtils';

const PageLoginComplete: React.FC<IContainerBaseProps> = ({ ...props }) => {

  const {layoutStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;
  showLoading();

  toggleHeader(false);
  toggleSidebar(false);

  return <ConLoginComplete {...props} />;
};

export default PageLoginComplete;
