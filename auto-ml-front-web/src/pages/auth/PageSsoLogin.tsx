import React, {useContext} from 'react';
import { ConSsoLogin } from 'containers/auth';
import { IContainerBaseProps } from 'constants/BaseInterface';
import RootStore from '../../store/RootStore';
import {showLoading, hideLoading} from 'utils/CommonUtils';

const PageSsoLogin: React.FC<IContainerBaseProps> = ({ ...props }) => {

  const {layoutStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;
  showLoading();

  toggleHeader(false);
  toggleSidebar(false);

  return <ConSsoLogin {...props} />;
};

export default PageSsoLogin;
