import React from 'react';
import { ConSignup } from 'containers/auth';
import { IContainerBaseProps } from 'constants/BaseInterface';

const PageSignup: React.FC<IContainerBaseProps> = ({ ...props }) => {
  return <ConSignup {...props} />;
};

export default PageSignup;
