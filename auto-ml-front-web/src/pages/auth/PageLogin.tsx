import React from 'react';
import { ConLogin } from 'containers/auth';
import { IContainerBaseProps } from 'constants/BaseInterface';

const PageLogin: React.FC<IContainerBaseProps> = ({ ...props }) => {
  return <ConLogin {...props} />;
};

export default PageLogin;
