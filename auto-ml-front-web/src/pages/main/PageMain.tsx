import React, {useContext, useEffect, useState} from 'react';
import Main from 'components/edge/Main';
import { IContainerBaseProps} from '../../constants/BaseInterface';
import RootStore from '../../store/RootStore';

const PageMain: React.FC<IContainerBaseProps> = ({...props}) => {

  const {layoutStore} = useContext(RootStore);
  const { toggleMainHeader,  toggleSidebar } = layoutStore;

  useEffect(() => {
    toggleMainHeader(true, true);
    toggleSidebar(false);
  },[]);

  return <Main {...props} />
}

export default PageMain;
