import _PageManagementList from './PageManagementList';
import _PageRoadRealTime from './PageRoadRealTime';
import _PageRoadResult from './PageRoadResult';
import _PageInquiryList from './PageInquiryList';
import _PageUserManagementList from './PageUserManagementList';
import _PageCompanyManagementList from './PageCompanyManagementList';
import _PageSettingList from './PageSettingList';

export const PageManagementList = _PageManagementList;
export const PageRoadRealTime = _PageRoadRealTime;
export const PageRoadResult = _PageRoadResult;
export const PageInquiryList = _PageInquiryList;
export const PageUserManagementList = _PageUserManagementList;
export const PageSettingList = _PageSettingList;
export const PageCompanyManagementList = _PageCompanyManagementList;
