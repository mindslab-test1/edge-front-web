import React, {useContext, useEffect, useState} from 'react';
import SettingList from 'components/edge/SettingHistoryList';
import { IContainerBaseProps} from '../../constants/BaseInterface';
import RootStore from '../../store/RootStore';
import {isLoggedIn} from '../../service/auth/AuthService';
import {ERouteUrl} from '../../router/RouteLinks';
import {showLoading, hideLoading} from 'utils/CommonUtils';
const PageSettingList: React.FC<IContainerBaseProps> = ({...props}) => {

  const {layoutStore} = useContext(RootStore);
  const [isLogin, setIsLogin] = useState<boolean>(false);
  const { toggleHeader, toggleSidebar, showTopMenu } = layoutStore;
  useEffect(() => {
    showLoading();
    isLoggedIn()
      .then((res) => {
        if (res) {
          setIsLogin(true);
          toggleHeader(true);
          toggleSidebar(true);
        } else {
          props.history.replace(ERouteUrl.SSO_LOGIN);
        }
      });
  },[]);

  return (
      <>
      {
          isLogin ? <SettingList {...props} /> : null
      }
      </>
  )

}

export default PageSettingList;
