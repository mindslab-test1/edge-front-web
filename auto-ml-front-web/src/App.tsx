import { hot } from 'react-hot-loader/root';
import React, { Component, useContext, useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch, withRouter, useLocation } from 'react-router-dom';

import { Header, Sidebar, Footer } from 'components/commons/layout';
import { ROUTES } from 'router/RouteLinks';
import {PageNoMatch} from 'pages/error';

/* 다음 주소 API */
declare global {
  interface Window {
    daum: any;
    vxgplayer : any;
  }
}

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Header  />
        <div id='container' className='view'>
          <Sidebar />
            <Switch>
              {ROUTES.map((route, index) => (
                <Route key={index} path={route.path.toString()} exact={route.exact} component={withRouter(route.component)}/>
              ))}
              <Route component={PageNoMatch}/>
            </Switch>
          <div id='aside' className='aside'>
          </div>
          {/*<Footer />*/}
        </div>
      </BrowserRouter>
    );
  }
}

export default hot(App);
