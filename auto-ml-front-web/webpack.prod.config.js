const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require( 'webpack' );
const HtmlWebPackPlugin = require( 'html-webpack-plugin' );
const dotenv = require( 'dotenv' );
const path = require( 'path' );
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = () => {
  const env = dotenv.config( {
    path: path.resolve( './.env.prod' )
  } ).parsed;

  // reduce it to a nice object, the same as before
  const envKeys = Object.keys( env ).reduce( ( prev, next ) => {
    prev[ `process.env.${ next }` ] = JSON.stringify( env[ next ] );
    return prev;
  }, {} );

  return {
    mode: 'production',
    entry: {
      bundle: [
        '@babel/polyfill',
        './src/index.tsx',
      ],
    },
    output: {
      path: path.resolve( 'dist/prod/' ),
      publicPath: '/',
      filename: '[name].[hash].js',
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
      runtimeChunk: true,
    },
    module: {
      rules: [ {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.html$/,
          use: [ {
            loader: "html-loader",
            options: {
              minimize: true
            }
          } ]
        },
        {
          test: /\.css$/,
          loader: "style-loader!css-loader",
        },
        {
          test: /\.(woff|woff2|eot|ttf|png|jpg|jpeg|gif|ico|svg)$/,
          loader: 'url-loader',
          options: {
            name: 'assets/[name].[ext]?[hash]',
            limit: 5000, //10kb
            publicPath: '/',
          }
        },
        {
          test: /\.(sa|sc|)ss$/,
          use: [ "style-loader", "css-loader", "sass-loader" ]
        },
        {
          test: /\.tsx?$/,
          loader: "ts-loader"
        },
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.DefinePlugin( envKeys ),
      new CopyWebpackPlugin({
        patterns: [
          {from: './public/robots.txt', to: './robots.txt'},
          {from: './src/assets/js/pnacl/Release', to:'pnacl/Release'}
        ]
      }),
      new HtmlWebPackPlugin( {
        template: "./public/index.html",
        filename: "./index.html",
      } ),
    ],
    resolve: {
      modules: [
        path.join( 'src' ),
        'node_modules'
      ],
      extensions: [ '*', '.js', '.jsx', '.css', '.less', ".ts", ".tsx" ],
    },
    node: {
      fs: 'empty',
    },
  }
}
