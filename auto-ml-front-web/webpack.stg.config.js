const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require( 'webpack' );
const HtmlWebPackPlugin = require( 'html-webpack-plugin' );
const dotenv = require( 'dotenv' );
const path = require( 'path' );
const DashboardPlugin = require( "webpack-dashboard/plugin" );
// const {
//   getThemeVariables
// } = require( 'antd/dist/theme' );

module.exports = () => {
  // call dotenv and it will return an Object with a parsed key 
  const env = dotenv.config( {
    path: path.resolve( './.env.stg' )
  } ).parsed

  // reduce it to a nice object, the same as before
  const envKeys = Object.keys( env ).reduce( ( prev, next ) => {
    prev[ `process.env.${ next }` ] = JSON.stringify( env[ next ] );
    return prev;
  }, {} );

  return {
    mode: 'production',
    entry: {
      bundle: [
        '@babel/polyfill',
        './src/index.tsx',
      ],
    },
    output: {
      path: path.resolve( 'dist/stg/' ),
      publicPath: '/',
      filename: '[name].[hash].js',
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
      runtimeChunk: true,
    },
    module: {
      rules: [ {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.html$/,
          use: [ {
            loader: "html-loader",
            options: {
              minimize: true
            }
          } ]
        },
        {
          test: /\.css$/,
          loader: "style-loader!css-loader",
        },
        {
          test: /\.(woff|woff2|eot|ttf|png|jpg|jpeg|gif|ico|svg)$/,
          loader: 'url-loader',
          options: {
            name: 'assets/[name].[ext]?[hash]',
            limit: 10000, //10kb
            publicPath: '/',
          }
        },
        {
          test: /\.(sa|sc|)ss$/,
          use: [ "style-loader", "css-loader", "sass-loader" ]
        },
        {
          test: /\.tsx?$/,
          loader: "ts-loader"
        },
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.DefinePlugin( envKeys ),
      new HtmlWebPackPlugin( {
        template: "./public/index.html",
        filename: "./index.html",
      } ),
    ],
    resolve: {
      alias: {
        'react-dom': '@hot-loader/react-dom'
      },
      modules: [
        path.join( 'src' ),
        'node_modules',
      ],
      extensions: [ '*', '.js', '.jsx', '.css', '.less', ".ts", ".tsx" ],
    },
  }
};
